/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include "quakedef.h"

qboolean      OnChange_gl_anisotropicfilter (cvar_t * var, char *string);
cvar_t        gl_anisotropy = { "gl_anisotropy", "4", CVAR_NONE, OnChange_gl_anisotropicfilter };

qboolean      OnChange_gl_max_size (cvar_t * var, char *string);
cvar_t        gl_max_size = { "gl_max_size", "1024", CVAR_NONE, OnChange_gl_max_size };

qboolean      OnChange_gl_mipmap (cvar_t * var, char *string);
cvar_t        gl_mipmap = { "gl_mipmap", "1", CVAR_NONE, OnChange_gl_mipmap };  // lxndr: defaulting to 1 so pbo can be set

extern qboolean OnChange_gl_picmip (cvar_t * var, char *string);
cvar_t        gl_picmip = { "gl_picmip", "0", CVAR_NONE, OnChange_gl_picmip };

qboolean      OnChange_gl_pbo (cvar_t * var, char *string);
cvar_t        gl_pbo = { "gl_pbo", "0", CVAR_NONE, OnChange_gl_pbo };

cvar_t        gl_texturechecksum = { "gl_texturechecksum", "0" };
cvar_t        gl_texturecompression = { "gl_texturecompression", "0" };

qboolean      OnChange_gl_texturefilter (cvar_t * var, char *string);
cvar_t        gl_texturefilter = { "gl_texturefilter", "GL_LINEAR_MIPMAP_NEAREST", CVAR_NONE, OnChange_gl_texturefilter };

cvar_t        gl_loadtextures = { "gl_loadtextures", "1", CVAR_AFTER_CHANGE, OnChange_reload };
cvar_t        gl_loadtexturesthreads = { "gl_loadtexturesthreads", "1" };

static qboolean no24bit;

int           gl_max_size_default;
int           gl_lightmap_format = 3;
int           gl_solid_format = 3;
int           gl_alpha_format = 4;
int           gl_compressed_format = GL_COMPRESSED_RGB_ARB;

static int    gl_filter_min = GL_NEAREST;
static int    gl_filter_max = GL_NEAREST;

extern byte   vid_gamma_table[256];
extern float  vid_gamma;

typedef struct {
    char         *glname, *hname;
    int           minimize, maximize;
} glfilter_t;

glfilter_t    filterlist[] = {
    {"GL_NEAREST", "Nearest", GL_NEAREST, GL_NEAREST},
    {"GL_LINEAR", "Linear", GL_LINEAR, GL_LINEAR},
    {"GL_NEAREST_MIPMAP_NEAREST", "Trinearest", GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST},
    {"GL_NEAREST_MIPMAP_LINEAR", "Binearest", GL_NEAREST_MIPMAP_LINEAR, GL_NEAREST},
    {"GL_LINEAR_MIPMAP_NEAREST", "Bilinear", GL_LINEAR_MIPMAP_NEAREST, GL_LINEAR},
    {"GL_LINEAR_MIPMAP_LINEAR", "Trilinear", GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR}
};

#define NUM_FILTERS	(sizeof(filterlist) / sizeof(glfilter_t))

gltexture_t   gltextures[MAX_GLTEXTURES];
int           numgltextures;
int           texture_extension_number = 1;
int           currenttexture = -1;     // to avoid unnecessary texture sets
static qboolean mtexenabled = false;

#define Q_ROUND_POWER2(in, out) for (out = 1; out < in; out <<= 1);

void          GL_UpdateFilters (float anisotropicfilter);

/*
 *
 * OnChange
 *
 */

qboolean OnChange_gl_anisotropicfilter (cvar_t * var, char *string) {
    int           i;
    qboolean      newval = Q_atoi (string);

    if (!gl_ext.tex_filteranisotropic) {
        if (key_dest == key_console)
            Con_Printf ("Your hardware doesn't support anisotropic filtering\n");
        return true;
    }

    Q_ROUND_POWER2 (newval, i);
    if (i != newval && newval > 0) {
        Con_Printf ("Valid values for %s %f are powers of 2 only\n", var->name, newval);
        return true;
    }

    GL_UpdateFilters (newval);
    return false;
}

qboolean OnChange_gl_max_size (cvar_t * var, char *string) {
    int           i;
    float         newval = Q_atof (string);

    if (newval > gl_max_size_default) {
        Con_Printf ("Your hardware doesn't support texture sizes bigger than %dx%d\n", gl_max_size_default, gl_max_size_default);
        return true;
    }

    Q_ROUND_POWER2 (newval, i);

    if (i != newval) {
        Con_Printf ("Valid values for %s are powers of 2 only\n", var->name);
        return true;
    }

    return false;
}

qboolean OnChange_gl_mipmap (cvar_t * var, char *string) {
    int           i;
    gltexture_t  *glt;
    int           newval = Q_atoi (string);

    if ((!gl_ext.generate_mipmap && newval == 1) || (!gl_ext.frame_bufferobject && newval == 2)) {
        Con_Printf ("Your hardware doesn't support this feature\n");
        return true;
    }

    if (gl_mipmap.value == 1 || newval == 1) {
        if (newval == 2)
            newval = 0;
        for (i = 0, glt = gltextures; i < numgltextures; i++, glt++) {
            if (glt->texmode & TEX_MIPMAP) {
                GL_Bind (glt->texnum);
                glTexParameteri (GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, newval);
            }
        }
    }

    return false;
}

qboolean OnChange_gl_pbo (cvar_t * var, char *string) {
    if (!gl_ext.pixel_bufferobject) {
        Con_Printf ("Your hardware doesn't support this feature\n");
        return true;
    }

    return false;
}

qboolean OnChange_gl_texturefilter (cvar_t * var, char *string) {
    int           i;

    for (i = 0; i < NUM_FILTERS; i++) {
        if (!Q_strcasecmp (filterlist[i].glname, string))
            break;
    }

    if (i == NUM_FILTERS) {
        Con_Printf ("bad filter name: %s\n", string);
        return true;
    }

    gl_filter_min = filterlist[i].minimize;
    gl_filter_max = filterlist[i].maximize;

    GL_UpdateFilters (gl_anisotropy.value);
    return false;
}

/*
 *
 * Filters
 *
 */
void GL_ApplyFilters (int mode) {
    if (scr_consmooth.value && (mode & TEX_CONSOLE)) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if (scr_crosshairsmooth.value && (mode & TEX_CROSSHAIR)) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if (scr_menusmooth.value && (mode & TEX_MENU)) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if (scr_sbarsmooth.value && (mode & TEX_SBAR)) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if (scr_textsmooth.value && (mode & TEX_TEXT)) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if (scr_weaponsmooth.value && (mode & TEX_WEAPON)) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if (mode & TEX_LINEAR) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if (mode & TEX_BILINEAR) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if (mode & TEX_TRILINEAR) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if ((mode & TEX_MIPMAP) && (mode & (TEX_ALIAS | TEX_BRUSH | TEX_ITEM))) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, gl_filter_min);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, gl_filter_max);
        if (gl_ext.tex_filteranisotropic)
            glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, gl_anisotropy.value);
    } else {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }

    if ((mode & TEX_MIPMAP) && gl_ext.generate_mipmap && gl_mipmap.value == 1)
        glTexParameteri (GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, gl_mipmap.value);

    if (mode & TEX_CONSOLE) {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    } else {
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
}

const char   *GL_GetFilterString (void) {
    int           i;

    for (i = 0; i < NUM_FILTERS; i++) {
        if (!Q_strcasecmp (filterlist[i].glname, gl_texturefilter.string))
            break;
    }

    return filterlist[i].hname;
}

void GL_NextFilter (void) {
    int           i;

    for (i = 0; i < NUM_FILTERS; i++)
        if (!Q_strcasecmp (filterlist[i].glname, gl_texturefilter.string))
            break;
    if (i == NUM_FILTERS - 1)
        i = 0;
    else
        i++;
    Cvar_Set (&gl_texturefilter, filterlist[i].glname);
}

void GL_PrevFilter (void) {
    int           i;

    for (i = 0; i < NUM_FILTERS; i++)
        if (!Q_strcasecmp (filterlist[i].glname, gl_texturefilter.string))
            break;
    if (i == 0)
        i = NUM_FILTERS - 1;
    else
        i--;
    Cvar_Set (&gl_texturefilter, filterlist[i].glname);
}

void GL_ToggleFilter (qboolean linear, int texmode) {
    int           i;
    gltexture_t  *glt;

    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++) {
        if (glt->texmode & texmode) {
            GL_Bind (glt->texnum);
            if (linear) {
                glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            } else {
                glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            }
        }
    }
}

void GL_UpdateFilters (float anisotropicfilter) {
    int           i;
    gltexture_t  *glt;

    // change all the existing mipmap texture objects
    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++) {
        if ((glt->texmode & TEX_MIPMAP) && (glt->texmode & (TEX_ALIAS | TEX_BRUSH | TEX_ITEM))) {
            GL_Bind (glt->texnum);
            glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, gl_filter_min);
            glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, gl_filter_max);
            if (gl_ext.tex_filteranisotropic)
                glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropicfilter);
        }
    }
}

void R_TexPrint (gltexture_t *glt) {
    Con_Printf ("%4i %10s ", glt->texnum, va ("%ix%i", glt->scaled_width, glt->scaled_height));
    Con_Printf ("%7i ", glt->allocated);
    switch (glt->transition) {
        case TEX_TRANS_NONE:
            Con_Printf ("      none");
            break;
        case TEX_TRANS_RETRIEVING:
            Con_Printf ("retrieving");
            break;
        case TEX_TRANS_PROCESSING:
            Con_Printf ("processing");
            break;
        case TEX_TRANS_UPLOADING:
            Con_Printf (" uploading");
            break;
    }
    Con_Printf (" %6s ", glt->used ? "used" : glt->loaded ? "loaded" : "none");
    Con_Printf ("%s\n", glt->identifier);
}

void R_TexFind_f (void) {
    int           i;
    gltexture_t  *glt;

    if (Cmd_Argc () != 2) {
        Con_Printf ("Usage: %s <pattern>\n", Cmd_Argv (0));
        return;
    }

    Con_Printf (" num      w x h    size     action  state identifier\n");
    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++)
        if (strstr (glt->identifier, Cmd_Argv (1)))
            R_TexPrint (glt);
}

void R_TexList (int select) {
    int           i;
    gltexture_t  *glt;

    Con_Printf (" num      w x h    size     action  state identifier\n");
    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++)
        if (select == 1 || (select == 2 && (glt->texmode & TEX_BRUSH)) || (select == 3 && !(glt->texmode & TEX_BRUSH)) || (select == 4 && glt->used))
            R_TexPrint (glt);
}

void R_TexList_f (void) {
    if (Cmd_Argc () == 2) {
        if (strcmp (Cmd_Argv (1), "brushes") && strcmp (Cmd_Argv (1), "nonbrushes") && strcmp (Cmd_Argv (1), "used")) {
            Con_Printf ("usage: %s {brushes|nonbrushes|used}\n", Cmd_Argv (0));
            return;
        }
    }

    if (!strcmp (Cmd_Argv (1), "brushes"))
        R_TexList (2);
    else if (!strcmp (Cmd_Argv (1), "nonbrushes"))
        R_TexList (3);
    else if (!strcmp (Cmd_Argv (1), "used"))
        R_TexList (4);
    else
        R_TexList (1);
}

byte nulltexture_data[16] = {25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25};
int nulltexture;
/*
 * R_InitTexture
 */
void R_InitTexture (void) {
    Cvar_Register (&gl_loadtextures);
    Cvar_Register (&gl_loadtexturesthreads);
    Cvar_Register (&gl_max_size);
    Cvar_Register (&gl_mipmap);
    Cvar_Register (&gl_picmip);
    Cvar_Register (&gl_pbo);
    Cvar_Register (&gl_texturechecksum);
    Cvar_Register (&gl_texturecompression);
    Cvar_Register (&gl_texturefilter);

    Cmd_AddLegacyCommand ("gl_texturemode", "gl_texturefilter");

    Cmd_AddCommand ("texturefind", R_TexFind_f);
    Cmd_AddCommand ("texturelist", R_TexList_f);

    glGetIntegerv (GL_MAX_TEXTURE_SIZE, &gl_max_size_default);
    Cvar_SetDefaultValue (&gl_max_size, gl_max_size_default);

    no24bit = Com_CheckParm ("-no24bit") ? true : false;

    memset (gltextures, 0, MAX_GLTEXTURES * sizeof (gltexture_t));

//    static byte nulltexture_data[16] = {127,191,255,255,0,0,0,255,0,0,0,255,127,191,255,255};
    nulltexture = GL_LoadTexture ("texture_null", 2, 2, nulltexture_data, 0, 1);
}

/*
 * ResampleTextureLerpLine
 */
static inline void ResampleTextureLerpLine (byte * in, byte * out, int inwidth, int outwidth) {
    int           j, xi, oldx = 0, f, fstep, endx = (inwidth - 1), lerp;

    fstep = (inwidth << 16) / outwidth;
    for (j = 0, f = 0; j < outwidth; j++, f += fstep) {
        xi = f >> 16;
        if (xi != oldx) {
            in += (xi - oldx) * 4;
            oldx = xi;
        }
        if (xi < endx) {
            lerp = f & 0xFFFF;
            *out++ = (byte) ((((in[4] - in[0]) * lerp) >> 16) + in[0]);
            *out++ = (byte) ((((in[5] - in[1]) * lerp) >> 16) + in[1]);
            *out++ = (byte) ((((in[6] - in[2]) * lerp) >> 16) + in[2]);
            *out++ = (byte) ((((in[7] - in[3]) * lerp) >> 16) + in[3]);
        } else {                       // last pixel of the line has no pixel to lerp to
            *out++ = in[0];
            *out++ = in[1];
            *out++ = in[2];
            *out++ = in[3];
        }
    }
}

/*
 * ResampleTexture
 */
static void ResampleTextureBest (unsigned *indata, int inwidth, int inheight, unsigned *outdata, int outwidth, int outheight) {
    int           i, j, yi, oldy, f, fstep, endy = (inheight - 1), lerp;
    int           inwidth4 = inwidth * 4, outwidth4 = outwidth * 4;
    byte         *inrow, *out, *row1, *row2, *memalloc;

    out = (byte *) outdata;
    fstep = (inheight << 16) / outheight;

    memalloc = Q_malloc (2 * outwidth4);
    row1 = memalloc;
    row2 = memalloc + outwidth4;
    inrow = (byte *) indata;
    oldy = 0;
    ResampleTextureLerpLine (inrow, row1, inwidth, outwidth);
    ResampleTextureLerpLine (inrow + inwidth4, row2, inwidth, outwidth);
    for (i = 0, f = 0; i < outheight; i++, f += fstep) {
        yi = f >> 16;
        if (yi < endy) {
            lerp = f & 0xFFFF;
            if (yi != oldy) {
                inrow = (byte *) indata + inwidth4 * yi;
                if (yi == oldy + 1)
                    memcpy (row1, row2, outwidth4);
                else
                    ResampleTextureLerpLine (inrow, row1, inwidth, outwidth);
                ResampleTextureLerpLine (inrow + inwidth4, row2, inwidth, outwidth);
                oldy = yi;
            }
            for (j = outwidth; j; j--) {
                out[0] = (byte) ((((row2[0] - row1[0]) * lerp) >> 16) + row1[0]);
                out[1] = (byte) ((((row2[1] - row1[1]) * lerp) >> 16) + row1[1]);
                out[2] = (byte) ((((row2[2] - row1[2]) * lerp) >> 16) + row1[2]);
                out[3] = (byte) ((((row2[3] - row1[3]) * lerp) >> 16) + row1[3]);
                out += 4;
                row1 += 4;
                row2 += 4;
            }
            row1 -= outwidth4;
            row2 -= outwidth4;
        } else {
            if (yi != oldy) {
                inrow = (byte *) indata + inwidth4 * yi;
                if (yi == oldy + 1)
                    memcpy (row1, row2, outwidth4);
                else
                    ResampleTextureLerpLine (inrow, row1, inwidth, outwidth);
                oldy = yi;
            }
            memcpy (out, row1, outwidth4);
        }
    }
    Q_free (memalloc);
}

static void ResampleTexture (unsigned *indata, int inwidth, int inheight, unsigned *outdata, int outwidth, int outheight) {
    int           i, j;
    unsigned int  frac, fracstep, *inrow, *out;

    out = outdata;

    fracstep = inwidth * 0x10000 / outwidth;
    for (i = 0; i < outheight; i++) {
        inrow = (unsigned int *) indata + inwidth * (i * inheight / outheight); // lxndr: int* > unsigned int*
        frac = fracstep >> 1;
        j = outwidth - 4;
        while (j >= 0) {
            out[0] = inrow[frac >> 16];
            frac += fracstep;
            out[1] = inrow[frac >> 16];
            frac += fracstep;
            out[2] = inrow[frac >> 16];
            frac += fracstep;
            out[3] = inrow[frac >> 16];
            frac += fracstep;
            out += 4;
            j -= 4;
        }
        if (j & 2) {
            out[0] = inrow[frac >> 16];
            frac += fracstep;
            out[1] = inrow[frac >> 16];
            frac += fracstep;
            out += 2;
        }
        if (j & 1) {
            out[0] = inrow[frac >> 16];
            frac += fracstep;
            out += 1;
        }
    }
}

/*
 * MipMap
 *
 * Operates in place, quartering the size of the texture
 */
static void MipMap (byte * in, int *width, int *height) {
    int           i, j, nextrow;
    byte         *out;

    nextrow = *width << 2;

    out = in;
    if (*width > 1) {
        *width >>= 1;
        if (*height > 1) {
            *height >>= 1;
            for (i = 0; i < *height; i++, in += nextrow) {
                for (j = 0; j < *width; j++, out += 4, in += 8) {
                    out[0] = (in[0] + in[4] + in[nextrow + 0] + in[nextrow + 4]) >> 2;
                    out[1] = (in[1] + in[5] + in[nextrow + 1] + in[nextrow + 5]) >> 2;
                    out[2] = (in[2] + in[6] + in[nextrow + 2] + in[nextrow + 6]) >> 2;
                    out[3] = (in[3] + in[7] + in[nextrow + 3] + in[nextrow + 7]) >> 2;
                }
            }
        } else {
            for (i = 0; i < *height; i++) {
                for (j = 0; j < *width; j++, out += 4, in += 8) {
                    out[0] = (in[0] + in[4]) >> 1;
                    out[1] = (in[1] + in[5]) >> 1;
                    out[2] = (in[2] + in[6]) >> 1;
                    out[3] = (in[3] + in[7]) >> 1;
                }
            }
        }
    } else if (*height > 1) {
        *height >>= 1;
        for (i = 0; i < *height; i++, in += nextrow) {
            for (j = 0; j < *width; j++, out += 4, in += 4) {
                out[0] = (in[0] + in[nextrow + 0]) >> 1;
                out[1] = (in[1] + in[nextrow + 1]) >> 1;
                out[2] = (in[2] + in[nextrow + 2]) >> 1;
                out[3] = (in[3] + in[nextrow + 3]) >> 1;
            }
        }
    }
}

static void ScaleDimensions (int width, int height, int *scaled_width, int *scaled_height, int mode) {
    int           maxsize;

    Q_ROUND_POWER2 (width, *scaled_width);
    Q_ROUND_POWER2 (height, *scaled_height);

    if (mode & TEX_MIPMAP) {
        *scaled_width >>= (int) gl_picmip.value;
        *scaled_height >>= (int) gl_picmip.value;
    }

    maxsize = (mode & TEX_MIPMAP) ? gl_max_size.value : gl_max_size_default;

    *scaled_width = bound (1, *scaled_width, maxsize);
    *scaled_height = bound (1, *scaled_height, maxsize);
}

static int GetInternalFormat (int mode) {
    if (mode & TEX_ALPHA)
        return gl_alpha_format;
    else if (gl_ext.tex_compression && gl_texturecompression.value && ((mode & TEX_MIPMAP) && (mode & (TEX_ALIAS | TEX_BRUSH | TEX_ITEM))))
        return gl_compressed_format;
    return gl_solid_format;
}

inline unsigned *GL_BindPBO (int size, qboolean block) {
    int           value = 1;
    unsigned     *buffer = NULL;
    static int    index = 2;

    glGetError (); // lxndr: clear previous errors
    do {
        qglBindBuffer (GL_PIXEL_UNPACK_BUFFER_ARB, pboIds[index++]);
        qglGetBufferParameteriv (GL_PIXEL_UNPACK_BUFFER_ARB, GL_BUFFER_MAPPED, &value);
        if (index == MAX_PBO)
            index = 2;
        if (value) {
            Con_Debugf (0, "PBO buffer is already mapped (%d, %d)\n", size, value);
            if (!block)
                return NULL;
        }
    } while (value);

    do {
        qglGetBufferParameteriv (GL_PIXEL_UNPACK_BUFFER_ARB, GL_BUFFER_SIZE, &value);
        if (glGetError () != GL_NO_ERROR) {
            Con_Debugf (0, "Couldn't retrieve PBO buffer size (%d, %d)\n", size, value);
            if (block)
                continue;
            else
                return NULL;
        }

        if (size > value)
            qglBufferData (GL_PIXEL_UNPACK_BUFFER_ARB, size, 0, GL_DYNAMIC_DRAW_ARB);
        else
            qglBufferSubData (GL_PIXEL_UNPACK_BUFFER_ARB, 0, size, 0);
        if (glGetError () != GL_NO_ERROR) {
            Con_Debugf (0, "Couldn't allocate PBO buffer (%d, %d)\n", size, value);
            if (block)
                continue;
            else
                return NULL;
        }

        buffer = qglMapBuffer (GL_PIXEL_UNPACK_BUFFER_ARB, GL_WRITE_ONLY_ARB);
        if (!buffer) {
            Con_Debugf (0, "Couldn't map PBO buffer (%d, %d)\n", size, value);
            if (!block)
                return NULL;
        }
    } while (!buffer);

    return buffer;
}

/*
 * assume buffer is already bound and mapped
 */
inline void GL_UploadPBO (unsigned int *scaled, int width, int height, int mode) {
    int           internal_format, miplevel;

    internal_format = GetInternalFormat (mode);
    GL_ApplyFilters (mode);

    qglUnmapBuffer (GL_PIXEL_UNPACK_BUFFER_ARB);
    glTexImage2D (GL_TEXTURE_2D, 0, internal_format, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    if (mode & TEX_MIPMAP) {
        if (gl_mipmap.value) {
            if (gl_ext.frame_bufferobject && gl_mipmap.value == 2)
                qglGenerateMipmap (GL_TEXTURE_2D);
        } else {
            miplevel = 0;
            while (width > 1 || height > 1) {
                scaled = qglMapBuffer (GL_PIXEL_UNPACK_BUFFER_ARB, GL_WRITE_ONLY_ARB); // lxndr: can return NULL sometimes
                if (scaled) {
                    MipMap ((byte *) scaled, &width, &height);
                    qglUnmapBuffer (GL_PIXEL_UNPACK_BUFFER_ARB);
                    glTexImage2D (GL_TEXTURE_2D, ++miplevel, internal_format, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
                }
            }
        }
    }
    qglBindBuffer (GL_PIXEL_UNPACK_BUFFER_ARB, 0);
}

inline void GL_UploadSlow (unsigned int *scaled, int width, int height, int mode) {
    int           internal_format, miplevel;

    internal_format = GetInternalFormat (mode);
    GL_ApplyFilters (mode);

    glTexImage2D (GL_TEXTURE_2D, 0, internal_format, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, scaled);
    if (mode & TEX_MIPMAP) {
        if (gl_mipmap.value) {         // lxndr: gl_mipmap == 1 is handled differently
            if (gl_ext.frame_bufferobject && gl_mipmap.value == 2)
                qglGenerateMipmap (GL_TEXTURE_2D);
        } else {
            miplevel = 0;
            while (width > 1 || height > 1) {
                MipMap ((byte *) scaled, &width, &height);
                glTexImage2D (GL_TEXTURE_2D, ++miplevel, internal_format, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, scaled);
            }
        }
    }
}

/*
 * GL_Upload32
 */
int GL_Upload32 (unsigned *data, int width, int height, int mode) {
    int           scaled_width, scaled_height;
    unsigned int *scaled = NULL;

    ScaleDimensions (width, height, &scaled_width, &scaled_height, mode);

    if (data) {
        if (gl_ext.pixel_bufferobject && gl_pbo.value)
            scaled = GL_BindPBO (scaled_width * scaled_height * 4, true);
        else
            scaled = Q_malloc (scaled_width * scaled_height * 4);
    }

    if (width < scaled_width || height < scaled_height) {
        if (data) {
            if (gl_lerptextures.value)
                ResampleTextureBest (data, width, height, scaled, scaled_width, scaled_height);
            else
               ResampleTexture (data, width, height, scaled, scaled_width, scaled_height);
        }
        width = scaled_width;
        height = scaled_height;
    } else {
        if (width > scaled_width || height > scaled_height)
            Con_Debugf (0, "texture size trouble %dx%d vs %dx%d\n", width, height, scaled_width, scaled_height);
        if (data)
            memcpy (scaled, data, width * height * 4);
    }

    if (gl_ext.pixel_bufferobject && gl_pbo.value) {
        GL_UploadPBO (scaled, width, height, mode);
    } else {
        GL_UploadSlow (scaled, width, height, mode);
        if (scaled)
            Q_free (scaled);
    }

    return mode;                       // lxndr: for compatibility with GL_Upload8
}

/*
 * GL_Upload8
 */
int GL_Upload8 (byte * data, int width, int height, int mode) {
    int           i, size, p;
    unsigned     *table;
    static unsigned trans[640 * 480];

    table = d_8to24table;
    size = width * height;

    if (size * 4 > sizeof (trans)) // lxndr:
        Sys_Warnf ("GL_Upload8: image too big");

    if (mode & TEX_FULLBRIGHT) {
        // this is a fullbright mask, so make all non-fullbright colors transparent
        mode |= TEX_ALPHA;
        for (i = 0; i < size; i++) {
            p = data[i];
            if (p < 224)
                trans[i] = table[p] & 0x00FFFFFF;       // transparent
            else
                trans[i] = table[p];   // fullbright
        }
    } else if (mode & TEX_ALPHA) {
        // if there are no transparent pixels, make it a 3 component texture even if it was specified as otherwise
        mode &= ~TEX_ALPHA;
        for (i = 0; i < size; i++) {
            p = data[i];
            if (p == 255)
                mode |= TEX_ALPHA;
            trans[i] = table[p];
        }
    } else {
        if (!game.free && (size & 3))
            Sys_Error ("GL_Upload8: bad size (%d)", size);

        for (i = 0; i < size; i++)
            trans[i] = table[data[i]];
    }

    return GL_Upload32 (trans, width, height, mode);
}

inline void GL_TryUploadFast (unsigned int *scaled, gltexture_t * glt) {
    if (gl_mipmap.value) {
        glTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, glt->scaled_width, glt->scaled_height, GL_RGBA, GL_UNSIGNED_BYTE, scaled);
        if (gl_ext.frame_bufferobject && gl_mipmap.value == 2)
            qglGenerateMipmap (GL_TEXTURE_2D);
    } else {
//        GL_Upload32 ((unsigned *) scaled, glt->scaled_width, glt->scaled_height, glt->texmode);
        GL_UploadSlow (scaled, glt->scaled_width, glt->scaled_height, glt->texmode);
    }
}

qboolean CheckTextureLoaded (gltexture_t * glt, int width, int height, int scaled_width, int scaled_height, int mode, int bpp, int crc) {
    if (glt->loaded && width == glt->width && height == glt->height && scaled_width == glt->scaled_width && scaled_height == glt->scaled_height
        && bpp == glt->bpp && crc == glt->crc && mode == glt->texmode)
        return true;
    return false;
}

qboolean      refresh_textures = false;
int           thread_textures = 0;

/*
 * GL_LoadTexture
 */
int GL_LoadTextureWithTexnum (char *identifier, int width, int height, byte * data, int mode, int bpp, int texnum) {
    int           i, scaled_width, scaled_height, crc = 0;
    gltexture_t  *glt;

    if (!identifier[0])
        Host_Error ("texture with no identifier");

    ScaleDimensions (width, height, &scaled_width, &scaled_height, mode);

    if (data && (gl_texturechecksum.value || mode & (TEX_CONSOLE | TEX_CROSSHAIR | TEX_SKY | TEX_TEXT)))
        crc = CRC_Block (data, width * height * bpp);

    // see if the texture is already present
    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++) {
        if (!strncmp (identifier, glt->identifier, sizeof (glt->identifier) - 1)) {
            if (CheckTextureLoaded (glt, width, height, scaled_width, scaled_height, mode, bpp, crc)) {
                Con_Debugf (5, "%s (%s) was in cache: %i %i\n", glt->identifier, glt->filename, glt->texnum, i);
                glBindTexture (GL_TEXTURE_2D, glt->texnum);     // lxndr: direct binding
                VID_ThreadLock ();
                glt->used = true;
                if (r_levels.value) {
                    glt->action = TEX_ACTION_RETRIEVE;
                    refresh_levels = true;
                }
                VID_ThreadUnlock ();
                return glt->texnum;    // texture cached
            }
            Con_Debugf (5, "%s (%s) set to be reloaded: %i %i\n", glt->identifier, glt->filename, glt->texnum, i);
            goto GL_LoadTexture_setup; // reload the texture into the same slot
        }
    }

    if (numgltextures == MAX_GLTEXTURES)
        Sys_Error ("GL_LoadTexture: numgltextures == MAX_GLTEXTURES");

    glt = &gltextures[numgltextures++];
    if (texnum)
        glt->texnum = texnum;
    else
        glt->texnum = texture_extension_number++;
    refresh_textures = true;

    Q_strncpyz (glt->identifier, identifier, sizeof (glt->identifier));
    if (!strncmp (identifier, "pic:num_", 8) || !strncmp (identifier, "pic:anum_", 9)
        || !strncmp (identifier, "pic:face", 8) || !strncmp (identifier, "pic:sb_", 7)
        || !strncmp (identifier, "pic:inv", 7))
        mode |= TEX_TEXT;
    else if (!strncmp (identifier, "pic:ibar", 8) || !strncmp (identifier, "pic:sbar", 8) || !strncmp (identifier, "pic:scorebar", 12)
             || !strncmp (identifier, "pic:backtile", 12) || !strncmp (identifier, "pic:conback", 11) || !strncmp (identifier, "pic:disc", 8)
             || !strncmp (identifier, "pic:net", 7) || !strncmp (identifier, "pic:ram", 7) || !strncmp (identifier, "pic:turtle", 10))
        mode |= TEX_SBAR;
    else if (!strncmp (identifier, "pic:gfx", 7))
        mode |= TEX_MENU;

    Con_Debugf (5, "%s (%s) set to be loaded: %i %i\n", glt->identifier, glt->filename, glt->texnum, i);

  GL_LoadTexture_setup:
    glBindTexture (GL_TEXTURE_2D, glt->texnum); // lxndr: direct binding
    glt->texmode = mode;
    switch (bpp) {
        case 1:
            if (data) {
                glt->texmode = GL_Upload8 (data, width, height, glt->texmode);
                VID_ThreadLock2 ();
                glt->loaded = true;
                VID_ThreadUnlock2 ();
            }
            break;

        case 4:
            if (data) {
                glt->texmode = GL_Upload32 ((void *) data, width, height, glt->texmode);
                VID_ThreadLock2 ();
                glt->loaded = true;
                VID_ThreadUnlock2 ();
            }
            break;

        default:
            Sys_Error ("GL_LoadTexture: unknown bpp");
            break;
    }

    glt->width = width;
    glt->height = height;
    glt->scaled_width = scaled_width;
    glt->scaled_height = scaled_height;
    glt->crc = crc;
    glt->bpp = bpp;
    glt->allocated = 0;
    VID_ThreadLock2 ();
    glt->backup = NULL;
    glt->process = NULL;
    VID_ThreadUnlock2 ();
    VID_ThreadLock ();
    glt->used = true;
    if (r_levels.value) {
        glt->action = TEX_ACTION_RETRIEVE;
        refresh_levels = true;
    } else {
        glt->action = TEX_ACTION_NONE;
    }
    VID_ThreadUnlock ();
    Con_Debugf (5, "width=%i height=%i mode=%x %s: %i\n", glt->width, glt->height, glt->texmode, glt->identifier, glt->texnum);
    return glt->texnum;
}

int GL_LoadTexture (char *identifier, int width, int height, byte * data, int mode, int bpp) {
    return GL_LoadTextureWithTexnum (identifier, width, height, data, mode, bpp, 0);
}

void R_TexturesThread (void) {
    int           i;
    gltexture_t  *glt;

    thread_textures++;

    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++) {
        VID_ThreadLock2 ();
        if (!glt->loaded && glt->filename) {
            VID_ThreadUnlock2 ();
            VID_ThreadLock ();
            if (!glt->locked) {
                glt->locked = true;
                VID_ThreadUnlock ();
                Con_Debugf (4, "loading %s from working thread\n", glt->filename);
                glBindTexture (GL_TEXTURE_2D, glt->texnum);
                if (glt->texmode & (TEX_CONSOLE | TEX_MENU | TEX_SBAR | TEX_TEXT))
                    GL_LoadPicImage (glt->filename, glt->identifier, glt->width, glt->height, glt->texmode);
                else
                    GL_LoadTextureImage (glt->filename, glt->identifier, glt->width, glt->height, glt->texmode);
                glt->locked = false;
            } else {
                if (glt->locked)
                    Con_Debugf (2, "texture %s is locked\n", glt->identifier);
                VID_ThreadUnlock ();
            }
        } else {
            VID_ThreadUnlock2 ();
        }
    }

    thread_textures--;
    refresh_textures = false;
}

void R_Textures (void) {
    if (gl_loadtexturesthreads.value && refresh_textures && !thread_textures)
        VID_ThreadStart (R_TexturesThread, (int) gl_loadtexturesthreads.value);
}

static void R_TexturesBind (int texnum) {
    gltexture_t *glt;

    glt = GL_FindTextureByTexnum (texnum);
    VID_ThreadLock2 ();
    if (glt && !glt->loaded && glt->filename) {
        VID_ThreadUnlock2 ();
        VID_ThreadLock ();
        if (!glt->locked) {
            glt->locked = true;
            glt->scaled_width = glt->scaled_height = 0; // lxndr: force loading
            VID_ThreadUnlock ();
            Con_Debugf (4, "loading %s from main thread\n", glt->filename);
            if (glt->texmode & (TEX_CONSOLE | TEX_MENU | TEX_SBAR | TEX_TEXT))
                GL_LoadPicImage (glt->filename, glt->identifier, glt->width, glt->height, glt->texmode);
            else
                GL_LoadTextureImage (glt->filename, glt->identifier, glt->width, glt->height, glt->texmode);
            glt->locked = false;
        } else {
            if (glt->locked)
                Con_Debugf (2, "texture %s is locked\n", glt->identifier);
            VID_ThreadUnlock ();
        }
    } else {
        VID_ThreadUnlock2 ();
    }
}

/*
 * GL_FindTexture
 */
static gltexture_t *GL_FindTexture (char *identifier) {
    int           i;

    if (!identifier)
        return NULL;

    for (i = 0; i < numgltextures; i++)
        if (!strcmp (identifier, gltextures[i].identifier))
            return &gltextures[i];

    return NULL;
}

inline gltexture_t *GL_FindTextureByTexnum (int texnum) {
    int           i;

    for (i = 0; i < numgltextures; i++)
        if (gltextures[i].texnum == texnum)
            return &gltextures[i];

    return NULL;
}

/*
 * GL_LoadPicTexture
 * upload image to gc from the data provided
 */
int GL_LoadPicTexture (char *name, mpic_t * pic, byte * data, int mode) {
    int           i, width, height;
    char          identifier[MAX_QPATH] = "pic:";
    int           texnum;

    Q_ROUND_POWER2 (pic->width, width);
    Q_ROUND_POWER2 (pic->height, height);

    Q_strncpyz (identifier + 4, name, sizeof (identifier) - 4);
    if (width == pic->width && height == pic->height) {
        texnum = GL_LoadTexture (identifier, width, height, data, TEX_ALPHA | mode, 1);
        pic->sl = 0;
        pic->sh = 1;
        pic->tl = 0;
        pic->th = 1;
    } else {
        byte         *src, *dest, *buf;

        buf = Q_calloc (width * height, 1);

        src = data;
        dest = buf;
        for (i = 0; i < pic->height; i++) {
            memcpy (dest, src, pic->width);
            src += pic->width;
            dest += width;
        }

        texnum = GL_LoadTexture (identifier, width, height, buf, TEX_ALPHA | mode, 1);
        Q_free (buf);

        pic->sl = 0;
        pic->sh = (float) pic->width / width;
        pic->tl = 0;
        pic->th = (float) pic->height / height;
    }

    pic->glt = GL_FindTextureByTexnum (texnum);
    return pic->glt->texnum;
}

static qboolean IsImageToBeLoaded (FILE ** f, char *name, char *identifier, int matchwidth, int matchheight, int mode) {
    int           scaled_width, scaled_height;
    gltexture_t  *glt = NULL;

    if (Com_OpenFile (name, f) != -1) {
        if (identifier)                // lxndr: FIXME: quick fix
            glt = GL_FindTexture (identifier);

        VID_ThreadLock2 ();
        if (glt && glt->loaded) {
            VID_ThreadUnlock2 ();
            ScaleDimensions (matchwidth, matchheight, &scaled_width, &scaled_height, mode);
            if (glt->scaled_width == scaled_width && glt->scaled_height == scaled_height) {
                fclose (*f);
                VID_ThreadLock ();
                glt->used = true;
                if (r_levels.value) {
                    glt->action = TEX_ACTION_RETRIEVE;
                    refresh_levels = true;
                }
                VID_ThreadUnlock ();
                Con_Debugf (5, "file %s (%s) already loaded\n", name, identifier);
                return false;
            }
            Con_Debugf (5, "file %s (%s) exists and loaded but wrong size, try loading...\n", name, identifier);
            return true;
        } else {
            VID_ThreadUnlock2 ();
        }
        Con_Debugf (5, "file %s (%s) exists but not loaded, try loading...\n", name, identifier);
        return true;
    }
    Con_Debugf (10, "file %s (%s) not found (or just loaded ?)\n", name, identifier);
    return false;
}

/*
 * GL_LoadImage
 * load image data from file trying different file formats
 */
byte *GL_LoadImage (char *filename, char *identifier, int *matchwidth, int *matchheight, int mode, qboolean header) {
    char         *c, basename[MAX_QPATH], name[MAX_QPATH];      // lxndr: *c was byte
    byte         *data;
    FILE         *f;

    Com_StripExtension (filename, basename);
    for (c = basename; *c; c++)
        if (*c == '*')
            *c = '#';
    *matchwidth = *matchheight = 0;

    Q_snprintfz (name, sizeof (name), "%s.tga", basename);
    if (IsImageToBeLoaded (&f, name, identifier, *matchwidth, *matchheight, mode)) {
        if ((data = Image_LoadTGA (f, name, matchwidth, matchheight, header)))
            return data;
    } else {
        Q_snprintfz (name, sizeof (name), "%s.png", basename);
        if (IsImageToBeLoaded (&f, name, identifier, *matchwidth, *matchheight, mode)) {
            if ((data = Image_LoadPNG (f, name, matchwidth, matchheight, header)))
                return data;
        } else {
            Q_snprintfz (name, sizeof (name), "%s.jpg", basename);
            if (IsImageToBeLoaded (&f, name, identifier, *matchwidth, *matchheight, mode)) {
                if ((data = Image_LoadJPEG (f, name, matchwidth, matchheight, header)))
                    return data;
            }
        }
    }

    return NULL;
}

byte *GL_LoadImageHeader (char *filename, char *identifier, int *matchwidth, int *matchheight, int mode) {
    return GL_LoadImage (filename, identifier, matchwidth, matchheight, mode, true);
}

byte *GL_LoadImagePixels (char *filename, char *identifier, int *matchwidth, int *matchheight, int mode) {
    return GL_LoadImage (filename, identifier, matchwidth, matchheight, mode, false);
}

byte          player_texels32[1024 * 1024 * 4]; // HEAVY TESTING!!!
int           player32_width, player32_height;

/*
 * GL_LoadTextureImage
 * upload image to gc from file trying different file formats
 */
int GL_LoadTextureImage (char *filename, char *identifier, int matchwidth, int matchheight, int mode) {
    int           texnum;
    byte         *data;
    int           i, j, image_size;
    qboolean      gamma;
    gltexture_t  *glt;

    if (no24bit)
        return 0;

    if (!identifier)
        identifier = filename;
    glt = GL_FindTexture (identifier);
    mode |= TEX_EXTERNAL;

    if (gl_loadtexturesthreads.value && !glt && !cls.demoplayback) {
        GL_LoadImageHeader (filename, identifier, &matchwidth, &matchheight, mode);
        if (matchwidth && matchheight) {
            Con_Debugf (4, "loaded %s headers\n", filename);
            texnum = GL_LoadTexture (identifier, matchwidth, matchheight, NULL, mode, 4);
            glt = GL_FindTextureByTexnum (texnum);
            glt->filename = Z_Strdup (filename);
            return texnum;
        }
        return 0;
    }

    if (!(data = GL_LoadImagePixels (filename, identifier, &matchwidth, &matchheight, mode)))
        return glt ? glt->texnum : 0;
    Con_Debugf (4, "loaded %s pixels\n", filename);

    gamma = (vid_gamma != 1);
    image_size = matchwidth * matchheight;
    if (mode & TEX_LUMA) {
        gamma = false;
    } else if (mode & TEX_ALPHA) {
        mode &= ~TEX_ALPHA;
        for (j = 0; j < image_size; j++) {
            if (((((unsigned *) data)[j] >> 24) & 0xFF) < 255) {
                mode |= TEX_ALPHA;
                break;
            }
        }
    }
    if (gamma) {
        for (i = 0; i < image_size; i++) {
            data[4 * i + 0] = vid_gamma_table[data[4 * i + 0]];
            data[4 * i + 1] = vid_gamma_table[data[4 * i + 1]];
            data[4 * i + 2] = vid_gamma_table[data[4 * i + 2]];
        }
    }

    // save 32 bit texels if hi-res player skin found
    if (!strncmp (identifier, "player_", 7)) {
        memcpy (player_texels32, data, image_size * 4);
        player32_width = matchwidth;
        player32_height = matchheight;
    }

    texnum = GL_LoadTexture (identifier, matchwidth, matchheight, data, mode, 4);

    Q_free (data);

    glt = GL_FindTextureByTexnum (texnum);
    glt->filename = Z_Strdup (filename);
    return texnum;
}

/*
 * GL_LoadPicImage
 * upload image to gc from file trying different file formats
 */
mpic_t       *GL_LoadPicImage (char *filename, char *id, int matchwidth, int matchheight, int mode) {
    int           width, height, i;
    char          identifier[MAX_QPATH] = "pic:";
    byte         *data = NULL, *src, *dest, *buf;
    int           texnum = 0;
    gltexture_t  *glt;
    static mpic_t pic;

    if (no24bit)
        return NULL;

    glt = GL_FindTexture (id);
    mode |= TEX_EXTERNAL;

    if (gl_loadtexturesthreads.value && !glt && !cls.demoplayback) {
        Q_strncpyz (identifier + 4, id ? id : filename, sizeof (identifier) - 4);
        GL_LoadImageHeader (filename, identifier, &matchwidth, &matchheight, mode);
        if (matchwidth && matchheight) {
            Con_Debugf (4, "loaded %s headers\n", filename);
            Q_ROUND_POWER2 (matchwidth, width);
            Q_ROUND_POWER2 (matchheight, height);
            texnum = GL_LoadTexture (identifier, width, height, NULL, mode, 4);
            goto GL_LoadPicImage_setup;
        }
        return NULL;
    }
    if (glt)
        Q_strncpyz (identifier, glt->identifier, sizeof (identifier));
    else
        Q_strncpyz (identifier + 4, id ? id : filename, sizeof (identifier) - 4);

    if (!(data = GL_LoadImagePixels (filename, identifier, &matchwidth, &matchheight, mode)))
        return NULL;
    Con_Debugf (4, "loaded %s pixels\n", filename);
    Q_ROUND_POWER2 (matchwidth, width);
    Q_ROUND_POWER2 (matchheight, height);

    if (mode & TEX_ALPHA) {
        mode &= ~TEX_ALPHA;
        for (i = 0; i < matchwidth * matchheight; i++) {
            if (((((unsigned *) data)[i] >> 24) & 0xFF) < 255) {
                mode |= TEX_ALPHA;
                break;
            }
        }
    }

    if (width == matchwidth && height == matchheight) {
        if (texnum)
            GL_LoadTextureWithTexnum (identifier, matchwidth, matchheight, data, mode, 4, texnum);
        else
            texnum = GL_LoadTexture (identifier, matchwidth, matchheight, data, mode, 4);
    } else {
        buf = Q_calloc (width * height, 4);

        src = data;
        dest = buf;
        for (i = 0; i < matchheight; i++) {
            memcpy (dest, src, matchwidth * 4);
            src += matchwidth * 4;
            dest += width * 4;
        }

        if (texnum)
            GL_LoadTextureWithTexnum (identifier, width, height, buf, mode, 4, texnum);
        else
            texnum = GL_LoadTexture (identifier, width, height, buf, mode, 4);
        Q_free (buf);
    }

    Q_free (data);

  GL_LoadPicImage_setup:
    pic.width = matchwidth;
    pic.height = matchheight;

    if (width == pic.width && height == pic.height) {
        pic.sl = 0;
        pic.sh = 1;
        pic.tl = 0;
        pic.th = 1;
    } else {
        pic.sl = 0;
        pic.sh = (float) pic.width / width;
        pic.tl = 0;
        pic.th = (float) pic.height / height;
    }

    pic.glt = GL_FindTextureByTexnum (texnum);
    pic.glt->filename = Z_Strdup (filename);
    return &pic;
}

int GL_LoadCharsetImage (char *filename, char *identifier, int texnum, int mode) {
    int           i, j, image_size;
    byte         *data, *buf, *dest, *src;
    qboolean      transparent = false;
    int           matchwidth, matchheight;
    gltexture_t  *glt;

    if (no24bit)
        return 0;

    matchwidth = matchheight = 0;      // lxndr: for image.c

    if (!(data = GL_LoadImagePixels (filename, identifier, &matchwidth, &matchheight, mode | TEX_ALPHA | TEX_TEXT)))
        return 0;

    if (!identifier)
        identifier = filename;

    image_size = matchwidth * matchheight;

    for (j = 0; j < image_size; j++) {
        if (((((unsigned *) data)[j] >> 24) & 0xFF) < 255) {
            transparent = true;
            break;
        }
    }
    if (!transparent) {
        for (i = 0; i < image_size; i++) {
            if (data[i * 4] == data[0] && data[i * 4 + 1] == data[1] && data[i * 4 + 2] == data[2])
                data[i * 4 + 3] = 0;
        }
    }

    buf = dest = Q_calloc (image_size * 2, 4);
    src = data;
    for (i = 0; i < 16; i++) {
        memcpy (dest, src, image_size >> 2);
        src += image_size >> 2;
        dest += image_size >> 1;
    }

    GL_LoadTextureWithTexnum (identifier, matchwidth, matchheight * 2, buf, mode | TEX_ALPHA | TEX_TEXT, 4, texnum);

    Q_free (buf);
    Q_free (data);

    glt = GL_FindTextureByTexnum (texnum);
    glt->filename = Z_Strdup (filename);
    return texnum;
}

/*
 *
 * GL routines
 *
 */

static GLenum oldtarget = GL_TEXTURE0_ARB;
static int    cnttextures[4] = { -1, -1, -1, -1 };      // cached

static inline const char *GetGLErrorString (GLenum error) {
    switch (error) {
        case GL_INVALID_ENUM:
            return "INVALID ENUM";
        case GL_INVALID_OPERATION:
            return "INVALID OPERATION";
        case GL_INVALID_VALUE:
            return "INVALID VALUE";
        case GL_NO_ERROR:
            return "NO ERROR";
        case GL_OUT_OF_MEMORY:
            return "OUT OF MEMORY";
        case GL_STACK_OVERFLOW:
            return "STACK OVERFLOW";
        case GL_STACK_UNDERFLOW:
            return "STACK UNDERFLOW";
        case GL_TABLE_TOO_LARGE:
            return "GL_TABLE_TOO_LARGE";
    }
    return "UNKNOWN ERROR";
}

inline void GL_CheckForError () {
    GLenum        error;

    error = glGetError ();
    while (error != GL_NO_ERROR) {
        Con_DPrintf ("GL_ERROR: '%s' (0x%x)\n", GetGLErrorString (error), error);
        error = glGetError ();
    }
}

inline void GL_Bind (int texnum) {
    if (currenttexture == texnum)
        return;

    currenttexture = texnum;
    glBindTexture (GL_TEXTURE_2D, texnum);

    if (refresh_textures)
        R_TexturesBind (texnum);
    else if (refresh_levels)
        R_LevelsBind (texnum);
}

inline void GL_SelectTexture (GLenum target) {
    if (target == oldtarget)
        return;

    qglActiveTexture (target);

    cnttextures[oldtarget - GL_TEXTURE0_ARB] = currenttexture;
    currenttexture = cnttextures[target - GL_TEXTURE0_ARB];
    oldtarget = target;
}

inline void GL_DisableMultitexture (void) {
    if (mtexenabled) {
        glDisable (GL_TEXTURE_2D);
        GL_SelectTexture (GL_TEXTURE0_ARB);
        mtexenabled = false;
    }
}

inline void GL_EnableMultitexture (void) {
    if (gl_ext.multitex) {
        GL_SelectTexture (GL_TEXTURE1_ARB);
        glEnable (GL_TEXTURE_2D);
        mtexenabled = true;
    }
}

inline void GL_EnableTMU (GLenum target) {
    GL_SelectTexture (target);
    glEnable (GL_TEXTURE_2D);
}

inline void GL_DisableTMU (GLenum target) {
    GL_SelectTexture (target);
    glDisable (GL_TEXTURE_2D);
}

inline void GL_EnableCombine (GLfloat scale) {
    glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
    glTexEnvi (GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_MODULATE);
    glTexEnvi (GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_TEXTURE);
    glTexEnvi (GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_PREVIOUS_EXT);
//    glTexEnvi (GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_PRIMARY_COLOR_EXT);
    glTexEnvf (GL_TEXTURE_ENV, GL_RGB_SCALE_EXT, scale);
}

inline void GL_DisableCombine () {
    glTexEnvf (GL_TEXTURE_ENV, GL_RGB_SCALE_EXT, 1.0f);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

inline void GL_Set2D (int x, int y, int width, int height, int nearval, int farval) {
    glViewport (x, y, width, height);
    glMatrixMode (GL_PROJECTION);
//    glPushMatrix ();
    glLoadIdentity ();
    glOrtho (x, width, height, y, nearval, farval);
    glMatrixMode (GL_MODELVIEW);
//    glPushMatrix ();
    glLoadIdentity ();
}

inline void GL_Set3D (void) {
    glMatrixMode (GL_PROJECTION);
//    glPopMatrix ();
    glMatrixMode (GL_MODELVIEW);
//    glPopMatrix ();
}

/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
#include "quakedef.h"

/*
 *
 * key up events are sent even if in console mode
 *
 */

char          key_lines[HISTSIZE][MAXCMDLINE];
int           key_linepos;
int           key_lastpress;

int           edit_line = 0;
int           history_line = 0;

qboolean      chat = false;
qboolean      shell = false;

keydest_t     key_dest;

int           key_insert = 1;
int           key_count;               // incremented every key event

char         *keybindings[256];
qboolean      consolekeys[256];        // if true, can't be rebound while in console
qboolean      menubound[256];          // if true, can't be rebound while in menu
int           keyshift[256];           // key to map to if shift held down in console
int           key_repeats[256];        // if > 1, it is autorepeating
qboolean      keydown[256];

typedef struct {
    char         *name;
    int           keynum;
    const char   *defaultcommand;
} keybind_t;

keybind_t     keylist[] = {
    {"TAB", K_TAB, "+showscores"},
    {"ENTER", K_ENTER, "+showstatus"},
    {"ESCAPE", K_ESCAPE, "togglemenu"},
    {"SPACE", K_SPACE, "+jump"},
    {"BACKSPACE", K_BACKSPACE, NULL},

    {"CAPSLOCK", K_CAPSLOCK, NULL},
    {"PRINTSCR", K_PRINTSCR, "screenshot"},
    {"SCRLCK", K_SCRLCK, NULL},
    {"SCROLLOCK", K_SCRLCK, NULL},
    {"PAUSE", K_PAUSE, "pause"},

    {"UPARROW", K_UPARROW, "+forward"},
    {"DOWNARROW", K_DOWNARROW, "+back"},
    {"LEFTARROW", K_LEFTARROW, "+moveleft"},
    {"RIGHTARROW", K_RIGHTARROW, "+moveright"},

    {"ALT", K_ALT, "+strafe"},
    {"LALT", K_LALT, NULL},
    {"RALT", K_RALT, NULL},
    {"CTRL", K_CTRL, "+jump"},
    {"LCTRL", K_LCTRL, NULL},
    {"RCTRL", K_RCTRL, NULL},
    {"SHIFT", K_SHIFT, "+slow"},
    {"LSHIFT", K_LSHIFT, NULL},
    {"RSHIFT", K_RSHIFT, NULL},

    {"WINKEY", K_WIN, NULL},
    {"LWINKEY", K_LWIN, NULL},
    {"RWINKEY", K_RWIN, NULL},
    {"POPUPMENU", K_MENU, NULL},

    {"F1", K_F1, "menu_game"},
    {"F2", K_F2, "menu_save"},
    {"F3", K_F3, "menu_load"},
    {"F4", K_F4, "menu_options"},
    {"F5", K_F5, "menu_videooptions"},
    {"F6", K_F6, "quicksave"},
    {"F7", K_F7, NULL},
    {"F8", K_F8, NULL},
    {"F9", K_F9, "quickload"},
    {"F10", K_F10, "quit"},
    {"F11", K_F11, NULL},
    {"F12", K_F12, "screenshot"},

    {"INS", K_INS, "+lookup"},
    {"DEL", K_DEL, "+lookdown"},
    {"PGDN", K_PGDN, "+movedown"},
    {"PGUP", K_PGUP, "+moveup"},
    {"HOME", K_HOME, "ambienceprev"},
    {"END", K_END, "ambiencenext"},

    {"MUTE", K_MUTE, "volumemute"},
    {"VOLUMEUP", K_VOLUMEUP, "volumeup"},
    {"VOLUMEDOWN", K_VOLUMEDOWN, "volumedown"},

    {"TILDE", '~', "toggleconsole"},
    {"BACKQUOTE", '`', "toggleconsole"},
    {"+", '+', "sizeup"},
    {"=", '=', "sizeup"},
    {"-", '-', "sizedown"},
    {"0", '0', "impulse 0"},
    {"1", '1', "impulse 1"},
    {"2", '2', "impulse 2"},
    {"3", '3', "impulse 3"},
    {"4", '4', "impulse 4"},
    {"5", '5', "impulse 5"},
    {"6", '6', "impulse 6"},
    {"7", '7', "impulse 7"},
    {"8", '8', "impulse 8"},
    {"9", '9', "impulse 9"},
    {"A", 'a', "+moveleft"},
    {"D", 'd', "+moveright"},
    {"S", 's', "+forward"},
    {"Z", 'z', "+back"},
    {"R", 'r', "messagemode3"},
    {"T", 't', "messagemode2"},
    {"Y", 'y', "messagemode"},

    {"SEMICOLON", ';', NULL},          // because a raw semicolon seperates commands

    // keypad keys

    {"NUMLOCK", KP_NUMLOCK, NULL},
    {"KP_NUMLCK", KP_NUMLOCK, NULL},
    {"KP_NUMLOCK", KP_NUMLOCK, NULL},
    {"KP_SLASH", KP_SLASH, NULL},
    {"KP_DIVIDE", KP_SLASH, NULL},
    {"KP_STAR", KP_STAR, NULL},
    {"KP_MULTIPLY", KP_STAR, NULL},

    {"KP_MINUS", KP_MINUS, NULL},

    {"KP_HOME", KP_HOME, NULL},
    {"KP_7", KP_HOME, NULL},
    {"KP_UPARROW", KP_UPARROW, NULL},
    {"KP_8", KP_UPARROW, NULL},
    {"KP_PGUP", KP_PGUP, NULL},
    {"KP_9", KP_PGUP, NULL},
    {"KP_PLUS", KP_PLUS, NULL},

    {"KP_LEFTARROW", KP_LEFTARROW, NULL},
    {"KP_4", KP_LEFTARROW, NULL},
    {"KP_5", KP_5, NULL},
    {"KP_RIGHTARROW", KP_RIGHTARROW, NULL},
    {"KP_6", KP_RIGHTARROW, NULL},

    {"KP_END", KP_END, NULL},
    {"KP_1", KP_END, NULL},
    {"KP_DOWNARROW", KP_DOWNARROW, NULL},
    {"KP_2", KP_DOWNARROW, NULL},
    {"KP_PGDN", KP_PGDN, NULL},
    {"KP_3", KP_PGDN, NULL},

    {"KP_INS", KP_INS, NULL},
    {"KP_0", KP_INS, NULL},
    {"KP_DEL", KP_DEL, NULL},
    {"KP_ENTER", KP_ENTER, NULL},

    // mouse keys

    {"MWHEELUP", K_MWHEELUP, "impulse 10"},
    {"MWHEELDOWN", K_MWHEELDOWN, "impulse 12"},

    {"MOUSE1", K_MOUSE1, "+attack"},
    {"MOUSE2", K_MOUSE2, "+zoom"},
    {"MOUSE3", K_MOUSE3, "+speed"},
    {"MOUSE4", K_MOUSE4, NULL},
    {"MOUSE5", K_MOUSE5, NULL},
    {"MOUSE6", K_MOUSE6, NULL},
    {"MOUSE7", K_MOUSE7, NULL},
    {"MOUSE8", K_MOUSE8, NULL},

    // joystick keys

    {"JOY1", K_JOY1, NULL},
    {"JOY2", K_JOY2, NULL},
    {"JOY3", K_JOY3, NULL},
    {"JOY4", K_JOY4, NULL},

    // auxiliary keys

    {"AUX1", K_AUX1, NULL},
    {"AUX2", K_AUX2, NULL},
    {"AUX3", K_AUX3, NULL},
    {"AUX4", K_AUX4, NULL},
    {"AUX5", K_AUX5, NULL},
    {"AUX6", K_AUX6, NULL},
    {"AUX7", K_AUX7, NULL},
    {"AUX8", K_AUX8, NULL},
    {"AUX9", K_AUX9, NULL},
    {"AUX10", K_AUX10, NULL},
    {"AUX11", K_AUX11, NULL},
    {"AUX12", K_AUX12, NULL},
    {"AUX13", K_AUX13, NULL},
    {"AUX14", K_AUX14, NULL},
    {"AUX15", K_AUX15, NULL},
    {"AUX16", K_AUX16, NULL},
    {"AUX17", K_AUX17, NULL},
    {"AUX18", K_AUX18, NULL},
    {"AUX19", K_AUX19, NULL},
    {"AUX20", K_AUX20, NULL},
    {"AUX21", K_AUX21, NULL},
    {"AUX22", K_AUX22, NULL},
    {"AUX23", K_AUX23, NULL},
    {"AUX24", K_AUX24, NULL},
    {"AUX25", K_AUX25, NULL},
    {"AUX26", K_AUX26, NULL},
    {"AUX27", K_AUX27, NULL},
    {"AUX28", K_AUX28, NULL},
    {"AUX29", K_AUX29, NULL},
    {"AUX30", K_AUX30, NULL},
    {"AUX31", K_AUX31, NULL},
    {"AUX32", K_AUX32, NULL},

    {NULL, 0}
};

/*
 *
 *
 * LINE TYPING INTO THE CONSOLE
 *
 *
 */

qboolean CheckForCommand (void) {
    char         *s, command[256];

    Q_strncpyz (command, key_lines[edit_line] + 1, sizeof (command));
    for (s = command; *s > ' '; s++);
    *s = 0;

    return (Cvar_FindVar (command) || Cmd_FindCommand (command) || Cmd_FindCommandSyn (command) || Cmd_FindAlias (command));
}

static void AdjustConsoleHeight (int delta) {
    int           height;
    extern cvar_t scr_consize;

    if (con_forcedup)
        return;

    height = (scr_consize.value * vid.height + delta + 5) / 10;
    height *= 10;
    if (delta < 0 && height < 10)
        height = 10;
    if (delta > 0 && height > vid.height - 10)
        height = vid.height - 10;
    Cvar_SetValue (&scr_consize, (float) height / vid.height);
}

void Key_Extra (int *key) {
    if (keydown[K_CTRL]) {
        if (*key >= '0' && *key <= '9') {
            *key = *key - '0' + 0x12;  // yellow number
        } else {
            switch (*key) {
                case '[':
                    *key = 0x10;
                    break;
                case ']':
                    *key = 0x11;
                    break;
                case 'g':
                    *key = 0x86;
                    break;
                case 'r':
                    *key = 0x87;
                    break;
                case 'y':
                    *key = 0x88;
                    break;
                case 'b':
                    *key = 0x89;
                    break;
                case '(':
                    *key = 0x80;
                    break;
                case '=':
                    *key = 0x81;
                    break;
                case ')':
                    *key = 0x82;
                    break;
                case 'a':
                    *key = 0x83;
                    break;
                case '<':
                    *key = 0x1d;
                    break;
                case '-':
                    *key = 0x1e;
                    break;
                case '>':
                    *key = 0x1f;
                    break;
                case ',':
                    *key = 0x1c;
                    break;
                case '.':
                    *key = 0x9c;
                    break;
                case 'B':
                    *key = 0x8b;
                    break;
                case 'C':
                    *key = 0x8d;
                    break;
            }
        }
    }

    if (keydown[K_ALT])
        *key |= 0x80;                  // red char
}

/*
 * Key_Console
 *
 * Interactive line editing and console scrollback
 */
void Key_Console (int key) {
    int           i;
    char         *cmd, *p;
    FILE         *f;
    char          buf[256];
    char         *cliptext;
    int           len;

    switch (key) {
        case K_ENTER:
            con_backscroll = 0;
            if (con_message) {
                key_dest = key_game;
                con_message = false;
            }
            if (!key_lines[edit_line][1]) {
                Con_Printf ("%c\n", shell ? SHELL_PROMPT : chat ? CHAT_PROMPT : CONSOLE_PROMPT);
                return;
            }
            if (shell && !strcmp ("exit", key_lines[edit_line] + 1)) {
                Con_Printf ("%s\n", key_lines[edit_line]);
                shell = false;
            } else if (key_lines[edit_line][0] == SHELL_PROMPT) {
                Con_Printf ("%s\n", key_lines[edit_line]);
                if ((f = popen (va ("%s 2>&1", key_lines[edit_line] + 1), "r"))) {
                    while (fgets (buf, 256, f))
                        Con_Printf ("%s", buf);
                    pclose (f);
                }
            } else if (key_lines[edit_line][1] == '!') {
                Con_Printf ("%s\n", key_lines[edit_line]);
                if ((f = popen (va ("%s 2>&1", key_lines[edit_line] + 2), "r"))) {
                    while (!feof (f))
                        Con_Printf ("%c", fgetc (f));
                    pclose (f);
                }
            } else {
                if (chat && cls.state == ca_connected && !CheckForCommand ())
                    Cbuf_AddText ("say ");
                Cbuf_AddText (key_lines[edit_line] + 1);    // skip the prompt
                Cbuf_AddText ("\n");
                Con_Printf ("%s\n", key_lines[edit_line]);
            }
            if (strcmp (key_lines[(edit_line - 1) & (HISTSIZE - 1)], key_lines[edit_line])) // joe: don't save same commands multiple times
                edit_line = (edit_line + 1) & (HISTSIZE - 1);
            history_line = edit_line;
            key_lines[edit_line][0] = shell ? SHELL_PROMPT : chat ? CHAT_PROMPT : CONSOLE_PROMPT;
            key_lines[edit_line][1] = 0;
            key_linepos = 1;
            if (cls.state == ca_disconnected)
                SCR_UpdateScreen ();   // force an update, because the command may take some time
            return;

        case K_TAB:
            con_backscroll = 0;
            // various parameter completions -- by joe
            cmd = key_lines[edit_line] + 1;
            if (strstr (cmd, "playdemo ") == cmd || strstr (cmd, "capturestart ") == cmd || strstr (cmd, "capturedemo ") == cmd)
                Cmd_CompleteParameter (cmd, "*.dem");
            else if (strstr (cmd, "printtxt ") == cmd)
                Cmd_CompleteParameter (cmd, "*.txt");
            else if (strstr (cmd, "map ") == cmd)
                Cmd_CompleteParameter (cmd, "*.bsp");
            else if (strstr (cmd, "exec ") == cmd)
                Cmd_CompleteParameter (cmd, "*.cfg");
            else if (strstr (cmd, "load ") == cmd)
                Cmd_CompleteParameter (cmd, "*.sav");
            else if (strstr (cmd, "play ") == cmd)
                Cmd_CompleteParameter (cmd, "*.wav");
            else if (strstr (cmd, "loadcharset ") == cmd || strstr (cmd, "loadconchars ") == cmd || strstr (cmd, "loadhudchars ") == cmd)
                Cmd_CompleteParameter (cmd, "*.tga");
            else if (strstr (cmd, "loadconback ") == cmd)
                Cmd_CompleteParameter (cmd, "*.tga");
            else if (strstr (cmd, "loadcrosshair ") == cmd)
                Cmd_CompleteParameter (cmd, "*.tga");
            else if (strstr (cmd, "loadsky ") == cmd || strstr (cmd, "sky ") == cmd)
                Cmd_CompleteParameter (cmd, "*.tga");
            else if (strstr (cmd, "game ") == cmd)
                Cmd_CompleteParameter (cmd, "*");
            else {                     // command completion
                if (cl_advancedcompletion.value) {
                    Cmd_CompleteAdvanced ();
                } else {
                    if (!(p = Cmd_CompleteCommand (cmd)))
                        p = Cvar_CompleteVariable (cmd);

                    if (p) {
                        strcpy (cmd, p);
                        key_linepos = strlen (p) + 1;
                        key_lines[edit_line][key_linepos++] = ' ';
                        key_lines[edit_line][key_linepos] = 0;
                    }
                }
            }
            return;

        case K_BACKSPACE:
            con_backscroll = 0;
            if (key_linepos > 1) {
                strcpy (key_lines[edit_line] + key_linepos - 1, key_lines[edit_line] + key_linepos);
                key_linepos--;
            }
            return;

        case K_DEL:
            con_backscroll = 0;
            if (key_linepos < strlen (key_lines[edit_line]))
                strcpy (key_lines[edit_line] + key_linepos, key_lines[edit_line] + key_linepos + 1);
            return;

        case K_LEFTARROW:
            con_backscroll = 0;
            if (keydown[K_CTRL]) { // word left
                while (key_linepos > 1 && key_lines[edit_line][key_linepos - 1] == ' ')
                    key_linepos--;
                while (key_linepos > 1 && key_lines[edit_line][key_linepos - 1] != ' ')
                    key_linepos--;
                return;
            }
            if (key_linepos > 1)
                key_linepos--;
            return;

        case K_RIGHTARROW:
            con_backscroll = 0;
            if (keydown[K_CTRL]) { // word right
                i = strlen (key_lines[edit_line]);
                while (key_linepos < i && key_lines[edit_line][key_linepos] != ' ')
                    key_linepos++;
                while (key_linepos < i && key_lines[edit_line][key_linepos] == ' ')
                    key_linepos++;
                return;
            }
            if (key_linepos < strlen (key_lines[edit_line]))
                key_linepos++;
            return;

        case K_INS:
            con_backscroll = 0;
            key_insert ^= 1;
            return;

        case K_UPARROW:
            con_backscroll = 0;
            if (keydown[K_CTRL]) {
                AdjustConsoleHeight (-10);
                return;
            }
            if (history_line == ((edit_line + 1) & (HISTSIZE - 1)) || !key_lines[(history_line - 1) & (HISTSIZE - 1)][1])
                return;

            history_line = (history_line - 1) & (HISTSIZE - 1);
            if (history_line == edit_line) {
                key_lines[edit_line][0] = shell ? SHELL_PROMPT : chat ? CHAT_PROMPT : CONSOLE_PROMPT;
                key_lines[edit_line][1] = 0;
                key_linepos = 1;
            } else {
                strcpy (key_lines[edit_line], key_lines[history_line]);
                key_linepos = strlen (key_lines[edit_line]);
            }
            return;

        case K_DOWNARROW:
            con_backscroll = 0;
            if (keydown[K_CTRL]) {
                AdjustConsoleHeight (10);
                return;
            }
            if (history_line == edit_line)
                return;

            history_line = (history_line + 1) & (HISTSIZE - 1);
            if (history_line == edit_line) {
                key_lines[edit_line][0] = shell ? SHELL_PROMPT : chat ? CHAT_PROMPT : CONSOLE_PROMPT;
                key_lines[edit_line][1] = 0;
                key_linepos = 1;
            } else {
                strcpy (key_lines[edit_line], key_lines[history_line]);
                key_linepos = strlen (key_lines[edit_line]);
            }
            return;

        case K_PGUP:
        case K_MWHEELUP:
            if (keydown[K_SHIFT])
                con_backscroll += ((int) scr_conlines - 16) >> 4;
            else if (keydown[K_CTRL])
                con_backscroll += ((int) scr_conlines - 16) >> 6;
            else
                con_backscroll += 2;
            if (con_backscroll > con_numlines)
                con_backscroll = con_numlines;
            return;

        case K_PGDN:
        case K_MWHEELDOWN:
            if (keydown[K_SHIFT])
                con_backscroll -= ((int) scr_conlines - 16) >> 4;
            else if (keydown[K_CTRL])
                con_backscroll -= ((int) scr_conlines - 16) >> 6;
            else
                con_backscroll -= 2;
            if (con_backscroll < 0)
                con_backscroll = 0;
            return;

        case K_HOME:
            if (keydown[K_CTRL])
                con_backscroll = con_numlines;
            else
                key_linepos = 1;
            return;

        case K_END:
            if (keydown[K_CTRL])
                con_backscroll = 0;
            else
                key_linepos = strlen (key_lines[edit_line]);
            return;
    }

    if ((key == 'V' || key == 'v') && keydown[K_CTRL]) {
        if ((cliptext = Sys_GetClipboardData ())) {
            len = strlen (cliptext);
            if (len + strlen (key_lines[edit_line]) > MAXCMDLINE - 1)
                len = MAXCMDLINE - 1 - strlen (key_lines[edit_line]);
            if (len > 0) {             // insert the string
                memmove (key_lines[edit_line] + key_linepos + len, key_lines[edit_line] + key_linepos, strlen (key_lines[edit_line]) - key_linepos + 1);
                memcpy (key_lines[edit_line] + key_linepos, cliptext, len);
                key_linepos += len;
            }
        }
        return;
    }

    if (key < 32 || key > 127)
        return;                        // non printable

    con_backscroll = 0;

//    Key_Extra (&key);
    if (keydown[K_ALT])
        key |= 128;  // yellow

    i = strlen (key_lines[edit_line]);
    if (i >= MAXCMDLINE - 1)
        return;

    // This also moves the ending \0
    memmove (key_lines[edit_line] + key_linepos + 1, key_lines[edit_line] + key_linepos, i - key_linepos + 1);
    key_lines[edit_line][key_linepos] = key;
    key_linepos++;
}

// 

#define	MAX_CHAT_SIZE	45
char          chat_buffer[MAX_CHAT_SIZE];
qboolean      team_message = false;

void Key_Message (int key) {
    static int    chat_bufferlen = 0;

    if (key == K_ENTER) {
        if (team_message)
            Cbuf_AddText ("say_team \"");
        else
            Cbuf_AddText ("say \"");
        Cbuf_AddText (chat_buffer);
        Cbuf_AddText ("\"\n");

        key_dest = key_game;
        chat_bufferlen = 0;
        chat_buffer[0] = 0;
        return;
    }

    if (key == K_ESCAPE) {
        key_dest = key_game;
        chat_bufferlen = 0;
        chat_buffer[0] = 0;
        return;
    }

    if (key < 32 || key > 127)
        return;                        // non printable

    if (key == K_BACKSPACE) {
        if (chat_bufferlen) {
            chat_bufferlen--;
            chat_buffer[chat_bufferlen] = 0;
        }
        return;
    }

    if (chat_bufferlen == MAX_CHAT_SIZE - (team_message ? 3 : 1))
        return;                        // all full

    chat_buffer[chat_bufferlen++] = key;
    chat_buffer[chat_bufferlen] = 0;
}

// 

/*
 * Key_StringToKeynum
 *
 * Returns a key number to be used to index keybindings[] by looking at the
 * given string. Single ascii characters return themselves, while the K_* names
 * are matched up.
 */
int Key_StringToKeynum (char *str) {
    keybind_t    *k;

    if (!str || !str[0])
        return -1;

    if (!str[1])
        return str[0];

    for (k = keylist; k->name; k++) {
        if (!Q_strcasecmp (str, k->name))
            return k->keynum;
    }
    return -1;
}

/*
 * Key_KeynumToString
 *
 * Returns a string (either a single ascii char, or a K_* name) for the given
 * keynum. FIXME: handle quote special (general escape sequence?)
 *
 */
char         *Key_KeynumToString (int keynum) {
    keybind_t    *k;
    static char   tinystr[2];

    if (keynum == -1)
        return NULL;

    if (keynum > 32 && keynum < 127) { // printable ascii
        tinystr[0] = keynum;
        tinystr[1] = 0;
        return tinystr;
    }

    for (k = keylist; k->name; k++)
        if (keynum == k->keynum)
            return k->name;

    return NULL;
}

/*
 * Key_SetBinding
 */
void Key_SetBinding (int keynum, char *binding) {
    if (keynum == -1 || !binding) {
        Con_DPrintf ("%d %s\n", keynum, binding);
        return;
    }

    if (keynum == K_CTRL || keynum == K_ALT || keynum == K_SHIFT || keynum == K_WIN) {
        Key_SetBinding (keynum + 1, binding);
        Key_SetBinding (keynum + 2, binding);
        return;
    }

    // free old bindings
    if (keybindings[keynum]) {
        Z_Free (keybindings[keynum]);
        keybindings[keynum] = NULL;
    }

    // allocate memory for new binding
    keybindings[keynum] = Z_Strdup (binding);
}

void Key_Reset (int keynum) {
    keybind_t    *k;

    for (k = keylist; k->name; k++) {
        if (k->keynum == keynum) {
            Key_SetBinding (k->keynum, (char *) k->defaultcommand);
            break;
        }
    }
}

/*
 * Key_Unbind
 */
void Key_Unbind (int keynum) {
    if (keynum == -1)
        return;

    if (keynum == K_CTRL || keynum == K_ALT || keynum == K_SHIFT || keynum == K_WIN) {
        Key_Unbind (keynum + 1);
        Key_Unbind (keynum + 2);
        return;
    }

    if (keybindings[keynum]) {
        Z_Free (keybindings[keynum]);
        keybindings[keynum] = NULL;
    }
}

void Key_Print (int keynum) {
    Con_Printf ("%10s : %s\n", Key_KeynumToString (keynum), keybindings[keynum] ? keybindings[keynum] : "");
}

void Key_BindList_f (void) {
    int i, counter;

    for (i = 0, counter = 0; i < 256; i++) {
        if (keybindings[i]) {
            Key_Print (i);
            counter++;
        }
    }

    Con_Printf ("------------\n%d bindings\n", counter);
}

/*
 * Key_Reset_f
 */
void Key_Reset_f (void) {
    int           b;

    if (Cmd_Argc () != 2) {
        Con_Printf ("Usage: %s <key>\n", Cmd_Argv (0));
        return;
    }

    b = Key_StringToKeynum (Cmd_Argv (1));
    if (b == -1) {
        Con_Printf ("\"%s\" isn't a valid key\n", Cmd_Argv (1));
        return;
    }

    Key_Reset (b);
}

/*
 * Key_Unbind_f
 */
void Key_Unbind_f (void) {
    int           b;

    if (Cmd_Argc () != 2) {
        Con_Printf ("Usage: %s <key>\n", Cmd_Argv (0));
        return;
    }

    b = Key_StringToKeynum (Cmd_Argv (1));
    if (b == -1) {
        Con_Printf ("\"%s\" isn't a valid key\n", Cmd_Argv (1));
        return;
    }

    Key_Unbind (b);
}

void Key_UnbindAll (void) {
    int           i;

    for (i = 0; i < 256; i++)
        if (keybindings[i])
            Key_Unbind (i);
}

void Key_UnbindAll_f (void) {
    Key_UnbindAll ();
}

void Key_ResetAll (void) {
    keybind_t    *k;

    Key_UnbindAll ();
    for (k = keylist; k->name; k++)
        if (k->defaultcommand)
            Key_SetBinding (k->keynum, (char *) k->defaultcommand);
}

void Key_ResetBinds_f (void) {
    Key_ResetAll ();
}

void Key_List_f (void) {
    keybind_t    *k;

    for (k = keylist; k->name; k++)
        Key_Print (k->keynum);
}

static void Key_PrintBindInfo (int keynum, char *keyname) {
    if (!keyname)
        keyname = Key_KeynumToString (keynum);

    if (keynum == -1 && keyname) {
        Con_Printf ("\"%s\" isn't a valid key\n", keyname);
        return;
    }

    if (keybindings[keynum])
        Key_Print (keynum);
    else
        Con_Printf ("\"%s\" is not bound\n", keyname);
}

/*
 * Key_Bind_f
 */
void Key_Bind_f (void) {
    int           i, c, b;
    char          cmd[1024];

    c = Cmd_Argc ();
    if (c != 2 && c != 3) {
        Con_Printf ("bind <key> [command] : attach a command to a key\n");
        return;
    }

    b = Key_StringToKeynum (Cmd_Argv (1));
    if (b == -1) {
        Con_Printf ("\"%s\" isn't a valid key\n", Cmd_Argv (1));
        return;
    }

    if (c == 2) {
        if ((b == K_CTRL || b == K_ALT || b == K_SHIFT || b == K_WIN) && (keybindings[b + 1] || keybindings[b + 2])) {
            if (keybindings[b + 1] && keybindings[b + 2] && !strcmp (keybindings[b + 1], keybindings[b + 2])) {
                Key_Print (b + 1);
            } else {
                Key_PrintBindInfo (b + 1, NULL);
                Key_PrintBindInfo (b + 2, NULL);
            }
        } else {
            // and the following should print "ctrl (etc) is not bound" since K_CTRL cannot be bound
            Key_PrintBindInfo (b, Cmd_Argv (1));
        }
        return;
    }
    // copy the rest of the command line
    cmd[0] = 0;                        // start out with a NULL string
    for (i = 2; i < c; i++) {
        if (i > 2)
            strcat (cmd, " ");
        strcat (cmd, Cmd_Argv (i));
    }

    Key_SetBinding (b, cmd);
}

/*
 * Key_WriteBindings
 *
 * Writes lines containing "bind key value"
 */
void Key_WriteBindings (FILE * f) {
    int           i;

    for (i = 0; i < 256; i++)
        if (keybindings[i])
            if (*keybindings[i])
                fprintf (f, "bind \"%s\" \"%s\"\n", Key_KeynumToString (i), keybindings[i]);
}

void Key_Shell_f (void) {
    Con_Printf ("Welcome to the %s pseudo shell !\n", ENGINE_NAME);
    key_lines[edit_line][0] = SHELL_PROMPT;
    shell = true;
}

void Key_ToggleChat_f (void) {
    if (chat) {
       key_lines[edit_line][0] = CONSOLE_PROMPT;
       chat = false;
    } else {
       key_lines[edit_line][0] = CHAT_PROMPT;
       chat = true;
    }
}

void Key_History_f (void) {
    int           i;

    i = edit_line;
    do {
        i = (i + 1) & (HISTSIZE - 1);
        if (key_lines[i][1])
            Con_Printf ("%s\n", key_lines[i]);
    } while (i != edit_line);
}

void Key_WriteHistory (void) {
    int           i;
    FILE         *f;

    if (con_initialized && (f = fopen (va ("%s/history", com_homedir), "wt"))) {
        fprintf (f, "// Generated by %s\n", ENGINE_NAME);
        i = edit_line;
        do {
            i = (i + 1) & (HISTSIZE - 1);
            if (key_lines[i][1])
                fprintf (f, "%s\n", key_lines[i]);
        } while (i != edit_line);
        fclose (f);
    }
}

/*
 * Key_Init
 */
void Key_Init (void) {
    int           i, j;
    char          buf[MAXCMDLINE];
    FILE         *f;

    for (i = 0; i < HISTSIZE; i++) {
        key_lines[i][0] = CONSOLE_PROMPT;
        key_lines[i][1] = 0;
    }
    key_linepos = 1;
    if ((f = fopen (va ("%s/history", com_homedir), "rt"))) {
        i = 0;
        while (fgets (buf, MAXCMDLINE, f) && i < HISTSIZE) {
            if (buf[0] == '/' && buf[1] == '/')
                continue;
            for (j = 0; j < MAXCMDLINE && j < strlen (buf) - 1; j++)
                key_lines[i][j] = buf[j];
            key_lines[i][j + 1] = 0;
            i++;
        }
        fclose (f);
        if (i == HISTSIZE) {
            history_line = edit_line = 0;
            key_lines[0][0] = CONSOLE_PROMPT;
            key_lines[0][1] = 0;
        } else {
            history_line = edit_line = i;
        }
    }

    // init ascii characters in console mode
    for (i = 32; i < 128; i++)
        consolekeys[i] = true;
    consolekeys[K_ENTER] = true;
    consolekeys[K_TAB] = true;
    consolekeys[K_LEFTARROW] = true;
    consolekeys[K_RIGHTARROW] = true;
    consolekeys[K_UPARROW] = true;
    consolekeys[K_DOWNARROW] = true;
    consolekeys[K_BACKSPACE] = true;
    consolekeys[K_INS] = true;
    consolekeys[K_DEL] = true;
    consolekeys[K_HOME] = true;
    consolekeys[K_END] = true;
    consolekeys[K_PGUP] = true;
    consolekeys[K_PGDN] = true;
    consolekeys[K_ALT] = true;
    consolekeys[K_LALT] = true;
    consolekeys[K_RALT] = true;
    consolekeys[K_CTRL] = true;
    consolekeys[K_LCTRL] = true;
    consolekeys[K_RCTRL] = true;
    consolekeys[K_SHIFT] = true;
    consolekeys[K_LSHIFT] = true;
    consolekeys[K_RSHIFT] = true;
    consolekeys[K_MWHEELUP] = true;
    consolekeys[K_MWHEELDOWN] = true;
    consolekeys['`'] = false;
    consolekeys['~'] = false;

    for (i = 0; i < 256; i++)
        keyshift[i] = i;
    for (i = 'a'; i <= 'z'; i++)
        keyshift[i] = i - 'a' + 'A';
    keyshift['1'] = '!';
    keyshift['2'] = '@';
    keyshift['3'] = '#';
    keyshift['4'] = '$';
    keyshift['5'] = '%';
    keyshift['6'] = '^';
    keyshift['7'] = '&';
    keyshift['8'] = '*';
    keyshift['9'] = '(';
    keyshift['0'] = ')';
    keyshift['-'] = '_';
    keyshift['='] = '+';
    keyshift[','] = '<';
    keyshift['.'] = '>';
    keyshift['/'] = '?';
    keyshift[';'] = ':';
    keyshift['\''] = '"';
    keyshift['['] = '{';
    keyshift[']'] = '}';
    keyshift['`'] = '~';
    keyshift['\\'] = '|';

    menubound[K_ESCAPE] = true;
    for (i = 0; i < 12; i++)
        menubound[K_F1 + i] = true;

    // register our functions
    Cmd_AddCommand ("bind", Key_Bind_f);
    Cmd_AddCommand ("bindlist", Key_BindList_f);
    Cmd_AddCommand ("bindreset", Key_Reset_f);
    Cmd_AddCommand ("resetbinds", Key_ResetBinds_f);
    Cmd_AddCommand ("unbind", Key_Unbind_f);
    Cmd_AddCommand ("unbindall", Key_UnbindAll_f);
    Cmd_AddCommand ("keylist", Key_List_f);
    Cmd_AddCommand ("history", Key_History_f);
    Cmd_AddCommand ("qsh", Key_Shell_f);
    Cmd_AddCommand ("togglechat", Key_ToggleChat_f);
}

qboolean Key_isSpecial (int key) {
    if (key == K_INS || key == K_DEL || key == K_HOME || key == K_END || key == K_ALT || key == K_CTRL)
        return true;

    return false;
}

qboolean Key_DemoKey (int key) {
    switch (key) {
        case K_LEFTARROW:
            Cvar_SetValue (&cl_demorewind, 1);
            return true;

        case K_RIGHTARROW:
            Cvar_SetValue (&cl_demorewind, 0);
            return true;

        case K_UPARROW:
            Cvar_SetValue (&cl_demospeed, cl_demospeed.value + 0.1);
            return true;

        case K_DOWNARROW:
            Cvar_SetValue (&cl_demospeed, cl_demospeed.value - 0.1);
            return true;

        case K_ENTER:
            Cvar_SetValue (&cl_demorewind, 0);
            Cvar_SetValue (&cl_demospeed, 1);
            return true;
    }

    return false;
}

void Key_DestDump (void) {
    switch (key_dest) {
        case key_console:
            Con_Printf ("console\n");
            break;
        case key_game:
            Con_Printf ("game\n");
            break;
        case key_menu:
            Con_Printf ("menu\n");
            break;
        case key_message:
            Con_Printf ("message\n");
            break;

        default:
            Con_Printf ("undefined\n");
    }
}

/*
 * Key_Event
 *
 * Called by the system between frames for both key up and key down events
 * Should NOT be called during an interrupt!
 */
qboolean Key_Event (int key, qboolean down) {
    char         *kb, cmd[1024];

    if (key == K_LALT || key == K_RALT)
        Key_Event (K_ALT, down);
    else if (key == K_LCTRL || key == K_RCTRL)
        Key_Event (K_CTRL, down);
    else if (key == K_LSHIFT || key == K_RSHIFT)
        Key_Event (K_SHIFT, down);
    else if (key == K_LWIN || key == K_RWIN)
        Key_Event (K_WIN, down);

    keydown[key] = down;

    key_lastpress = key;
    key_count++;
    if (key_count <= 0)
        return false;                  // just catching keys for Con_NotifyBox

    // update auto-repeat status
/*    if (down) {
        key_repeats[key]++;
        if (key_repeats[key] > 1) {     // joe: modified to work as ZQuake
//            if ((key != K_BACKSPACE && key != K_DEL && key != K_LEFTARROW && key != K_RIGHTARROW && key != K_UPARROW && key != K_DOWNARROW
//                && key != K_PGUP && key != K_PGDN && (key < 32 || key > 126 || key == '`'))
//                || (key_dest == key_game && cls.state == ca_connected))
                return false;                // ignore most autorepeats
        }
    } else {
        key_repeats[key] = 0;
    }*/

    // handle escape specialy, so the user can never unbind it
    switch (key) {
        case K_ESCAPE:
            if (!down)
                return false;

            switch (key_dest) {
                case key_message:
                    Key_Message (key);
                    break;

                case key_menu:
                    M_Keydown (key);
                    break;

                case key_console:
                    Con_ToggleConsole_f ();
                    break;

                case key_game:
                    M_ToggleMenu_f ();
                    break;

                default:
                    Sys_Error ("Bad key_dest");
            }
            return true;

        case K_ENTER:
            if (down && keydown[K_ALT]) {
                VID_ToggleFullscreen ();
                return true;
            }

/*        case K_ALT:
            if (down && !_windowed_mouse.value)
                Cvar_SetValue (&_windowed_mouse, 1);
            if (!down && _windowed_mouse.value)
                Cvar_SetValue (&_windowed_mouse, 0);
            break; */
    }

    // key up events only generate commands if the game key binding is a button command (leading + sign).
    // These will occur even in console mode, to keep the character from continuing an action started before a console switch.
    // Button commands include the kenum as a parameter, so multiple downs can be matched with ups
    if (!down) {
        kb = keybindings[key];
        if (kb && kb[0] == '+') {
            Q_snprintfz (cmd, sizeof (cmd), "-%s %i\n", kb + 1, key);
            Cbuf_AddText (cmd);
        }
        if (keyshift[key] != key) {
            kb = keybindings[keyshift[key]];
            if (kb && kb[0] == '+') {
                Q_snprintfz (cmd, sizeof (cmd), "-%s %i\n", kb + 1, key);
                Cbuf_AddText (cmd);
            }
        }
        return true;
    }
    // if not a consolekey, send to the interpreter no matter what mode is
    if ((key_dest == key_menu && menubound[key]) || (key_dest == key_console && !consolekeys[key]) || (key_dest == key_game && (!con_forcedup || !consolekeys[key]))) {
        if (cls.demoplayback && Key_DemoKey (key))
            return false;

        if ((kb = keybindings[key])) {
            if (kb[0] == '+') {        // button commands add keynum as a parm
                Q_snprintfz (cmd, sizeof (cmd), "%s %i\n", kb, key);
                Cbuf_AddText (cmd);
            } else {
                Cbuf_AddText (kb);
                Cbuf_AddText ("\n");
            }
        }
        return true;
    }

    if (!down)
        return false;                  // other systems only care about key down events

    if (keydown[K_SHIFT])
        key = keyshift[key];

    switch (key) {
        case K_MUTE:
            S_VolumeMute_f ();
            break;

        case K_VOLUMEUP:
            S_VolumeUp_f ();
            break;

        case K_VOLUMEDOWN:
            S_VolumeDown_f ();
            break;
    }

    switch (key_dest) {
        case key_message:
            Key_Message (key);
            break;

        case key_menu:
            M_Keydown (key);
            break;

        case key_game:
        case key_console:
            Key_Console (key);
            break;

        default:
            Sys_Error ("Bad key_dest");
    }

    return true;                       // lxndr: just testing
}

/*
 * Key_ClearStates
 */
void Key_ClearStates (void) {
    int           i;

    for (i = 0; i < 256; i++) {
        keydown[i] = false;
        key_repeats[i] = 0;
    }
}

/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// show.c -- various hud elements

#include "quakedef.h"

cvar_t        show_clock = { "show_clock", "0" };
cvar_t        show_clock_x = { "show_clock_x", "-1" };
cvar_t        show_clock_y = { "show_clock_y", "1" };
#ifdef DEBUG
cvar_t        show_debug = { "show_debug", "0", CVAR_VOLATILE };
cvar_t        show_debug_x = { "show_debug_x", "-1" };
cvar_t        show_debug_y = { "show_debug_y", "5" };
#endif
cvar_t        show_fps = { "show_fps", "0" };
cvar_t        show_fps_x = { "show_fps_x", "-1" };
cvar_t        show_fps_y = { "show_fps_y", "2" };
cvar_t        show_location = { "show_location", "0" };
cvar_t        show_speed = { "show_speed", "0" };
cvar_t        show_stats = { "show_stats", "0" };
cvar_t        show_stats_x = { "show_stats_x", "-1" };
cvar_t        show_stats_y = { "show_stats_y", "0" };
cvar_t        show_stats_small = { "show_stats_small", "1" };
cvar_t        show_stats_length = { "show_stats_length", "2" };
cvar_t        show_deathmatch = { "show_deathmatch", "1" };
cvar_t        show_net = { "show_net", "1" };
cvar_t        show_pause = { "show_pause", "1" };
cvar_t        show_ram = { "show_ram", "1" };
cvar_t        show_turtle = { "show_turtle", "1" };
cvar_t        show_volume = { "show_volume", "1" };

mpic_t       *scr_nums[2][11];
mpic_t       *scr_colon;
mpic_t       *scr_slash;
mpic_t       *scr_net;
mpic_t       *scr_ram;
mpic_t       *scr_turtle;

int           fragsort[MAX_SCOREBOARD];

char          scoreboardtext[MAX_SCOREBOARD][20];
int           scoreboardtop[MAX_SCOREBOARD];
int           scoreboardbottom[MAX_SCOREBOARD];
int           scoreboardcount[MAX_SCOREBOARD];
int           scoreboardlines;

#define	ELEMENT_X_COORD(var)	((var##_x.value < 0) ? hudwidth - strlen(str) * 8 + 8 * var##_x.value: 8 * var##_x.value)
#define	ELEMENT_Y_COORD(var)	((var##_y.value < 0) ? hudheight - sb_lines + 8 * var##_y.value : 8 * var##_y.value)

/*
 * SCR_itoa
 */
int SCR_itoa (int num, char *buf) {
    char         *str;
    int           pow10, dig;

    str = buf;

    if (num < 0) {
        *str++ = '-';
        num = -num;
    }

    for (pow10 = 10; num >= pow10; pow10 *= 10);

    do {
        pow10 /= 10;
        dig = num / pow10;
        *str++ = '0' + dig;
        num -= dig * pow10;
    }
    while (pow10 != 1);

    *str = 0;

    return str - buf;
}

int SCR_ColorForMap (int m) {
    return m < 128 ? m + 8 : m + 8;
}

/*
 * SCR_IntermissionNumber
 */
void SCR_IntermissionNumber (int x, int y, int num, int digits, int color) {
    char          str[12], *ptr;
    int           l, frame;

    l = SCR_itoa (num, str);
    ptr = str;
    if (l > digits)
        ptr += (l - digits);
    if (l < digits)
        x += (digits - l) * 24;

    while (*ptr) {
        frame = (*ptr == '-') ? STAT_MINUS : *ptr - '0';

        Draw_TransPic (x, y, scr_nums[color][frame]);
        x += 24;
        ptr++;
    }
}

/*
 * SCR_SortFrags
 */
void SCR_SortFrags (void) {
    int           i, j, k;

    // sort by frags
    scoreboardlines = 0;
    for (i = 0; i < cl.maxclients; i++) {
        if (cl.scores[i].name[0]) {
            fragsort[scoreboardlines] = i;
            scoreboardlines++;
        }
    }

    for (i = 0; i < scoreboardlines; i++)
        for (j = 0; j < scoreboardlines - 1 - i; j++)
            if (cl.scores[fragsort[j]].frags < cl.scores[fragsort[j + 1]].frags) {
                k = fragsort[j];
                fragsort[j] = fragsort[j + 1];
                fragsort[j + 1] = k;
            }
}

/*
 * SCR_UpdateScoreboard
 */
void SCR_UpdateScoreboard (void) {
    int           i, k, top, bottom;
    scoreboard_t *s;

    SCR_SortFrags ();

    // draw the text
    memset (scoreboardtext, 0, sizeof (scoreboardtext));

    for (i = 0; i < scoreboardlines; i++) {
        k = fragsort[i];
        s = &cl.scores[k];
        sprintf (&scoreboardtext[i][1], "%3i %s", s->frags, s->name);

        top = s->colors & 0xf0;
        bottom = (s->colors & 15) << 4;
        scoreboardtop[i] = SCR_ColorForMap (top);
        scoreboardbottom[i] = SCR_ColorForMap (bottom);
    }
}

//
/*
 * SCR_DrawClock
 */
void SCR_DrawClock (void) {
    int           x, y;
    char         *str;

    if (!show_clock.value)
        return;

    if ((con_forcedup && key_dest == key_menu) || cls.signon == 0)
        return;

    switch ((int) show_clock.value) {
        case 2:
            str = Com_LocalTime ("%I:%M:%S %P");
            break;

        case 3:
            str = Com_LocalTime ("%a %b %d %I:%M:%S %P");
            break;

        case 4:
            str = Com_LocalTime ("%a %d %b %H:%M:%S");
            break;

        case 5:
            str = Com_LocalTime ("%Y-%m-%d %H:%M:%S");
            break;

        default:
            str = Com_LocalTime ("%H:%M:%S");
            break;
    }

    x = ELEMENT_X_COORD (show_clock);
    y = ELEMENT_Y_COORD (show_clock);
    Draw_String (x, y, str);
}

#ifdef DEBUG
/*
 * SCR_DrawDebug
 */
void SCR_DrawDebug (void) {
    static char   str[80];
    int           x, y;
    static double lastrealtime;

    if (!show_debug.value)
        return;

    if (realtime - lastrealtime > 0.1) {
        lastrealtime = realtime;
        Q_strncpyz (str, debugstr, sizeof (str));
    }

    x = ELEMENT_X_COORD (show_debug);
    y = ELEMENT_Y_COORD (show_debug);
    Draw_String (x, y, str);
}
#endif

/*
 * SCR_DrawFPS
 */
void SCR_DrawFPS (void) {
    static char   str[80];
    int           x, y;
    static double lastrealtime;

    if (!show_fps.value)
        return;

    if (con_forcedup)
        return;

    if (realtime - lastrealtime > 0.1) {
        lastrealtime = realtime;

        if (show_fps.value == 2)
            Q_strncpyz (str, va ("%3.1f (%3.1f) ", v_fpshist[0], v_fpsmean), sizeof (str));
        else
            Q_strncpyz (str, va ("%3.1f ", v_fpshist[0]), sizeof (str));
    }

    x = ELEMENT_X_COORD (show_fps);
    y = ELEMENT_Y_COORD (show_fps);
    Draw_String (x, y, str);
}

void SCR_DrawLocation (void) {
    int           x, y;
    char          st[32], loc[16];

    if (!show_location.value)
        return;

    if (con_forcedup)
        return;

    if (cl.time - cl.last_loc_time > 1) {
        sprintf (st, "%s", Loc_GetLocation (cl_entities[cl.viewentity].origin));
        cl.last_loc_time = cl.time;
    } else {
        sprintf (st, "%s", cl.last_loc_name);
    }

    x = (hudwidth - strlen (st) * 8) / 2;
    y = hudheight - sb_lines - 8;
    if (show_speed.value)
        y -= 24;
    if (scr_sbarinvpos.value == 1 && scr_viewsize.value == 100)
        y += 24;

    Draw_Character (x - 8, y, 16);     //[
    Draw_String (x, y, st);
    Draw_Character (x + (strlen (st) * 8), y, 17);      //]

    if (show_location.value == 2) {
        sprintf (loc, "%5.1f %5.1f %5.1f", cl_entities[cl.viewentity].origin[0], cl_entities[cl.viewentity].origin[1], cl_entities[cl.viewentity].origin[2]);
        x = (hudwidth - strlen (loc) * 8) / 2;
        y -= 10;

        Draw_Character (x - 8, y + 1, 29);      //(
        Draw_String (x, y, loc);
        Draw_Character (x + (strlen (loc) * 8), y + 1, 31);     //)
    }
}

void SCR_DrawPalette (void) {
    int i, j;
    char c[3];

    for (j = 0; j < 26; j++) {
        for (i = 0; i < 10; i++) {
            SCR_itoa (j * 10 + i, c);
            Draw_Fill (0 + i * 24, 0 + j * 10, 24, 10, j * 10 + i);
            Draw_String (0 + i * 24, 0 + j * 10 + 1, c);
        }
    }
}

/*
 * SCR_DrawSpeed
 */
void SCR_DrawSpeed (void) {
    int           x, y;
    char          st[8];
    float         speed, speedunits;
    static float  maxspeed = 0, display_speed = -1;
    static double lastrealtime = 0;
    extern qboolean sb_showscores, sb_showstatus;

    if (!show_speed.value || sb_showscores || sb_showstatus)
        return;

    if (con_forcedup || scr_con_current == conheight)
        return;                        // console is full screen

    if (lastrealtime > realtime) {
        lastrealtime = 0;
        display_speed = -1;
        maxspeed = 0;
    }

    speed = VectorLength (cl.velocity);
    if (speed > maxspeed)
        maxspeed = speed;

    if (display_speed >= 0) {
        sprintf (st, "%d", (int) display_speed);

        x = hudwidth / 2 - 80;
        y = hudheight;
        if (scr_viewsize.value >= 120)
            y -= 16;
        if (scr_viewsize.value < 120)
            y -= 40;
        if (scr_viewsize.value < 100 || (scr_sbarinvpos.value == 1 && scr_viewsize.value == 100))
            y -= 24;
        if (cl.intermission)
            y -= -16;

        Draw_Fill (x, y - 1, 160, 1, 1);
        Draw_Fill (x, y + 9, 160, 1, 1);
        Draw_Fill (x + 32, y - 2, 1, 13, 8);
        Draw_Fill (x + 64, y - 2, 1, 13, 8);
        Draw_Fill (x + 96, y - 2, 1, 13, 8);
        Draw_Fill (x + 128, y - 2, 1, 13, 8);

        Draw_Fill (x, y, 160, 9, 52);

        speedunits = display_speed;
        if (display_speed <= 500) {
            Draw_Fill (x, y, (int) (display_speed / 3.125), 9, 2);
        } else {
            while (speedunits > 500)
                speedunits -= 500;
            Draw_Fill (x, y, (int) (speedunits / 3.125), 9, 68);
        }
        Draw_String (x + 37 - strlen (st) * 8, y, st);
    }

    if (realtime - lastrealtime >= 0.01) {
        lastrealtime = realtime;
        display_speed = maxspeed;
        maxspeed = 0;
    }
}

float         drawstats_limit;

/*
 * SCR_DrawStats
 */
void SCR_DrawStats (void) {
    int           x, y;
    char          str[80];
    int           mins, secs, tens;
//    extern mpic_t *scr_colon, *sb_nums[2][11];

    if (!show_stats.value || ((show_stats.value == 3 || show_stats.value == 4) && drawstats_limit < cl.time))
        return;

    if (con_forcedup || cl.maxclients > 1)
        return;

    mins = cl.ctime / 60;
    secs = cl.ctime - 60 * mins;
    tens = (int) (cl.ctime * 10) % 10;

    if (!show_stats_small.value) {
        if (con_forcedup || scr_con_current == conheight)
            return;                    // console is full screen

        SCR_IntermissionNumber (hudwidth / 8, 0, mins, 2, 0);

        Draw_TransPic (hudwidth / 8 + 46, 0, scr_colon);
        Draw_TransPic (hudwidth / 8 + 64, 0, scr_nums[0][secs / 10]);
        Draw_TransPic (hudwidth / 8 + 88, 0, scr_nums[0][secs % 10]);

        Draw_TransPic (hudwidth / 8 + 110, 0, scr_colon);
        Draw_TransPic (hudwidth / 8 + 128, 0, scr_nums[0][tens]);

        if (show_stats.value == 2 || show_stats.value == 4) {
            SCR_IntermissionNumber (hudwidth / 8 + 160, 0, cl.stats[STAT_MONSTERS], 3, 0);
            SCR_IntermissionNumber (hudwidth / 8 + 240, 0, cl.stats[STAT_SECRETS], 2, 0);
        }
    } else {

        if (show_stats.value == 2 || show_stats.value == 4)
            Q_strcpy (str, va ("%3i kills %2i secrets %02i:%02i:%i ", cl.stats[STAT_MONSTERS], cl.stats[STAT_SECRETS], mins, secs, tens));
        else
            Q_strcpy (str, va ("%02i:%02i:%i ", mins, secs, tens));

        x = ELEMENT_X_COORD (show_stats);
        y = ELEMENT_Y_COORD (show_stats);

        Draw_String (x, y, str);
    }
}

/*
 * SCR_DrawVolume
 */
void SCR_DrawVolume (void) {
    int           i;
    char          bar[14];
    static float  volume_time = 0;
    extern qboolean volume_changed;

    if (!show_volume.value)
        return;

    if (key_dest == key_menu)
        return;

    if (realtime < volume_time - 2.0) {
        volume_time = 0;
        return;
    }

    if (volume_changed) {
        volume_time = realtime + 2.0;
        volume_changed = false;
    } else if (realtime > volume_time) {
        return;
    }

    bar[0] = 128;
    for (i = 1; i < 12; i++)
        bar[i] = ((int) (s_volume.value * 10) == i - 1) ? 131 : 129;
    bar[12] = 130;
    bar[13] = 0;

    Draw_String (hudwidth - 120, 56, bar);
    Draw_String (hudwidth - 120, 64, "volume");
}

qboolean      pbstats_changed = false;

/*
 * SCR_DrawPlaybackStats
 */
void SCR_DrawPlaybackStats (void) {
    static float  pbstats_time = 0;

    if (!cls.demoplayback)
        return;

    if (realtime < pbstats_time - 2.0) {
        pbstats_time = 0;
        return;
    }

    if (pbstats_changed) {
        pbstats_time = realtime + 2.0;
        pbstats_changed = false;
    } else if (realtime > pbstats_time) {
        return;
    }

    Draw_String (hudwidth - 192, 32, va ("playback mode: %s", cl_demorewind.value ? "rewind" : "forward"));
    Draw_String (hudwidth - 192, 40, va ("demo speed: %.1lfx", cl_demospeed.value));
}

//
/*
 * SCR_DrawDeathmatchOverlay 
 */
void SCR_DrawDeathmatchOverlay (void) {
    int           i, j, k, top, bottom, xofs, yofs, y;
    char          num[12];
    mpic_t       *pic;
    scoreboard_t *s;
    extern qboolean sb_showscores;

    if (cl.gametype != GAME_DEATHMATCH)
        return;

    if (!sb_showscores && !cl.intermission)
        return;

    if (key_dest == key_menu)
        return;

    if (cl.last_ping_time < cl.time - 1) {
        MSG_WriteByte (&cls.message, clc_stringcmd);
        SZ_Print (&cls.message, "ping\n");
        cl.last_ping_time = cl.time;
    }

    if (iplog_size && (cl.last_status_time < cl.time - 1)) {
        MSG_WriteByte (&cls.message, clc_stringcmd);
        SZ_Print (&cls.message, "status\n");
        cl.last_status_time = cl.time;
    }

    scr_copyeverything = 1;
    scr_fullupdate = 0;

    xofs = (hudwidth - 320) >> 1;
    yofs = (hudheight - 240) >> 1;

    Draw_FadeScreen ();
    pic = Draw_CachePic ("gfx/ranking.lmp");
    Draw_TransPic (xofs + 160 - pic->width / 2, yofs, pic);

    // scores
    SCR_SortFrags ();

    // draw the stats
    Draw_String (xofs + 80, yofs + 24, "ping frags name");
    for (i = 0, y = 40; i < scoreboardlines; i++, y += 10) {
        k = fragsort[i];
        s = &cl.scores[k];
        if (!s->name[0])
            continue;

        // draw background
        top = s->colors & 0xf0;
        bottom = (s->colors & 15) << 4;
        top = SCR_ColorForMap (top);
        bottom = SCR_ColorForMap (bottom);

        Draw_Fill (xofs + 120, yofs + y, 40, 4, top);
        Draw_Fill (xofs + 120, yofs + y + 4, 40, 4, bottom);

        // draw ping
        Q_snprintfz (num, sizeof (num), "%4i", s->ping);
        for (j = 0; j < 4; j++)
            Draw_Character (xofs + 80 + 8 * j, yofs + y, num[j]);

        // draw frags
        Q_snprintfz (num, sizeof (num), "%3i", s->frags);
        for (j = 0; j < 3; j++)
            Draw_Character (xofs + 128 + 8 * j, yofs + y, num[j]);

        if (k == cl.viewentity - 1) {
            Draw_Character (xofs + 120, yofs + y, 16);
            Draw_Character (xofs + 152, yofs + y, 17);
        }

        // draw name
        Draw_String (xofs + 168, yofs + y, s->name);
    }
}

/*
 * SCR_DrawMiniDeathmatchOverlay
 */
void SCR_DrawMiniDeathmatchOverlay (void) {
    int           i, k, l, top, bottom, x, y, f, numlines;
    char          num[12];
    scoreboard_t *s;

    if (!show_deathmatch.value)
        return;

#ifdef GLQUAKE
    if (scr_hudsize.value > 1 || !sb_lines)
        return;
#endif

    if (cl.maxclients == 1 && !cls.demoplayback)
        return;

    scr_copyeverything = 1;
    scr_fullupdate = 0;

    // scores
    SCR_SortFrags ();

    // draw the text
    l = scoreboardlines;
    y = hudheight - sb_lines;
    numlines = sb_lines / 8;
    if (numlines < 3)
        return;

    // find us
    for (i = 0; i < scoreboardlines; i++)
        if (fragsort[i] == cl.viewentity - 1)
            break;

    i = (i == scoreboardlines) ? 0 : i - numlines / 2;
    i = bound (0, i, scoreboardlines - numlines);

    switch ((int) scr_sbarpos.value) {
        case 0:
            x = 320;
            break;
        case 1:
            x = ((hudwidth - 320) >> 1) + 320;
            break;
        default:
            x = 0;
            break;
    }

    for (; i < scoreboardlines && y < hudheight - 8; i++) {
        k = fragsort[i];
        s = &cl.scores[k];
        if (!s->name[0])
            continue;

        // draw background
        top = s->colors & 0xf0;
        bottom = (s->colors & 15) << 4;
        top = SCR_ColorForMap (top);
        bottom = SCR_ColorForMap (bottom);

        Draw_Fill (x, y + 1, 40, 3, top);
        Draw_Fill (x, y + 4, 40, 4, bottom);

        // draw number
        if (cls.demoplayback && cl.maxclients == 1)
            f = cl.stats[STAT_MONSTERS];
        else
            f = s->frags;
        Q_snprintfz (num, sizeof (num), "%3i", f);

        Draw_Character (x + 8, y, num[0]);
        Draw_Character (x + 16, y, num[1]);
        Draw_Character (x + 24, y, num[2]);

        if (k == cl.viewentity - 1) {
            Draw_Character (x, y, 16);
            Draw_Character (x + 32, y, 17);
        }
        // draw name
        Draw_String (x + 48, y, s->name);

        y += 8;
    }
}

/*
 * SCR_DrawFinaleOverlay
 */
void SCR_DrawFinaleOverlay (void) {
    mpic_t       *pic;

    scr_copyeverything = 1;

    Draw_FadeScreen ();
    pic = Draw_CachePic ("gfx/finale.lmp");
    Draw_TransPic ((hudwidth - pic->width) >> 1, 16, pic);
}

/*
 * SCR_DrawIntermissionOverlay
 */
void SCR_DrawIntermissionOverlay (void) {
    mpic_t       *pic;
    int           dig, num, xofs, yofs;

    if (key_dest == key_menu)
        return;

    if (cl.gametype == GAME_DEATHMATCH) {
        SCR_DrawDeathmatchOverlay ();
        return;
    }

    scr_copyeverything = 1;
    scr_fullupdate = 0;

    xofs = (hudwidth - 320) >> 1;
    yofs = (hudheight - 240) >> 1;

    Draw_FadeScreen ();
    pic = Draw_CachePic ("gfx/complete.lmp");
    Draw_TransPic (xofs + 64, yofs + 24, pic);

    pic = Draw_CachePic ("gfx/inter.lmp");
    Draw_TransPic (xofs, yofs + 56, pic);

    // time
    dig = cl.completed_time / 60;
    SCR_IntermissionNumber (xofs + 160, yofs + 64, dig, 3, 0);
    num = cl.completed_time - dig * 60;
    Draw_TransPic (xofs + 234, yofs + 64, scr_colon);
    Draw_TransPic (xofs + 246, yofs + 64, scr_nums[0][num / 10]);
    Draw_TransPic (xofs + 266, yofs + 64, scr_nums[0][num % 10]);

    // secrets
    SCR_IntermissionNumber (xofs + 160, yofs + 104, cl.stats[STAT_SECRETS], 3, 0);
    Draw_TransPic (xofs + 232, yofs + 104, scr_slash);
    SCR_IntermissionNumber (xofs + 240, yofs + 104, cl.stats[STAT_TOTALSECRETS], 2, 0);

    // monsters
    SCR_IntermissionNumber (xofs + 160, yofs + 144, cl.stats[STAT_MONSTERS], 3, 0);
    Draw_TransPic (xofs + 232, yofs + 144, scr_slash);
    SCR_IntermissionNumber (xofs + 240, yofs + 144, cl.stats[STAT_TOTALMONSTERS], 3, 0);
}

/*
 * SCR_DrawLoading
 */
void SCR_DrawLoading (void) {
    mpic_t       *pic;
    extern qboolean scr_drawloading, scr_drawtransition;

    if (!scr_drawloading)
        return;

//    if (!scr_drawtransition)
//        Draw_FadeScreen ();

    pic = Draw_CachePic ("gfx/loading.lmp");
    Draw_TransPic ((hudwidth - pic->width) / 2, (hudheight - 48 - pic->height) / 2, pic);
}

/*
 * SCR_DrawNet
 */
void SCR_DrawNet (void) {
    if (!show_net.value)
        return;

    if (!cls.signon || cls.demoplayback)
        return;

    if (realtime - cl.last_received_message < 0.3)
        return;

    Draw_Pic (64, 0, scr_net);
}

/*
 * SCR_DrawPause
 */
void SCR_DrawPause (void) {
    mpic_t       *pic;

    if (!show_pause.value)             // turn off for screenshots
        return;

    if (!cl.paused)
        return;

    if (key_dest == key_menu)
        return;

    Draw_FadeScreen ();
    pic = Draw_CachePic ("gfx/pause.lmp");
    Draw_TransPic ((hudwidth - pic->width) / 2, (hudheight - 48 - pic->height) / 2, pic);
}

/*
 * SCR_DrawRam
 */
void SCR_DrawRam (void) {
    if (!show_ram.value || !r_cache_thrash)
        return;

    Draw_Pic (32, 0, scr_ram);
}

/*
 * SCR_DrawTurtle
 */
void SCR_DrawTurtle (void) {
    static int    count;

    if (!show_turtle.value)
        return;

    if (host_frametime < 0.1) {
        count = 0;
        return;
    }

    count++;
    if (count < 3)
        return;

    Draw_Pic (0, 0, scr_turtle);
}

/*
 * SCR_BeginLoadingPlaque
 */
void SCR_BeginLoadingPlaque (void) {
    int old_key_dest = key_dest;
    extern qboolean scr_drawloading, scr_drawtransition;

//    if (scr_disabled_for_loading)
//        return;

    key_dest = key_game;
    if (cls.state == ca_disconnected) { // lxndr: needed to force key_game while disconnected
        scr_drawtransition = true;
//        SCR_UpdateScreen ();
        SCR_UpdateScreen ();
    } else if (scr_drawtransition) {
/*        SCR_UpdateScreen ();
        SCR_UpdateScreen ();
        SCR_UpdateScreen ();*/
    }
    scr_drawloading = true;
    SCR_UpdateScreen ();
    scr_drawloading = false;
    scr_drawtransition = false;
    key_dest = old_key_dest;

    scr_disabled_for_loading = true;

#ifdef GLQUAKE
    R_ClearLevels ();
#endif
}
void SCR_BeginLoadingPlaque2 (void) {
}

/*
 * SCR_EndLoadingPlaque
 */
void SCR_EndLoadingPlaque (void) {
    int old_key_dest = key_dest;
    extern qboolean scr_drawtransition;

    key_dest = key_game;
#ifdef GLQUAKE
    if (cls.state == ca_disconnected) { // clear screen
        scr_drawtransition = true;
        SCR_UpdateScreen ();
        scr_drawtransition = true;
    }
    if (host_loading) {                 // fade screen
        scr_drawtransition = true;
        SCR_UpdateScreen ();            // 3 times for proper clearing, don't know why it's required
        SCR_UpdateScreen (); 
        SCR_UpdateScreen ();
    } else if (scr_drawtransition) {
        scr_drawtransition = false;
    }
#endif
    key_dest = old_key_dest;

    scr_disabled_for_loading = false;
}
void SCR_EndLoadingPlaque2 (void) {
}

/*
 * Show_Init
 */
void Show_Init (void) {
    int           i;

    Cvar_Register (&show_clock);
    Cvar_Register (&show_clock_x);
    Cvar_Register (&show_clock_y);
#ifdef DEBUG
    Cvar_Register (&show_debug);
    Cvar_Register (&show_debug_x);
    Cvar_Register (&show_debug_y);
#endif
    Cvar_Register (&show_fps);
    Cvar_Register (&show_fps_x);
    Cvar_Register (&show_fps_y);
    Cvar_Register (&show_location);
    Cvar_Register (&show_speed);
    Cvar_Register (&show_stats);
    Cvar_Register (&show_stats_x);
    Cvar_Register (&show_stats_y);
    Cvar_Register (&show_stats_small);
    Cvar_Register (&show_stats_length);
    Cvar_Register (&show_deathmatch);
    Cvar_Register (&show_net);
    Cvar_Register (&show_pause);
    Cvar_Register (&show_ram);
    Cvar_Register (&show_turtle);
    Cvar_Register (&show_volume);

    for (i = 0; i < 10; i++) {
        scr_nums[0][i] = Draw_PicFromWad (va ("num_%i", i));
        scr_nums[1][i] = Draw_PicFromWad (va ("anum_%i", i));
    }
    scr_nums[0][10] = Draw_PicFromWad ("num_minus");
    scr_nums[1][10] = Draw_PicFromWad ("anum_minus");
    scr_colon = Draw_PicFromWad ("num_colon");
    scr_slash = Draw_PicFromWad ("num_slash");

    scr_net = Draw_PicFromWad ("net");
    scr_ram = Draw_PicFromWad ("ram");
    scr_turtle = Draw_PicFromWad ("turtle");
}

/*
 * Copyright (C) 2009-2010 lxndr.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#include "quakedef.h"

static int    xss_timeout, xss_interval, xss_blanking, xss_exposures;

Display      *dpy;
Window        win;
XVisualInfo  *visinfo;
int           scrnum;

#ifdef HAVE_MISC
#  include <X11/extensions/xf86misc.h>
#endif

#ifdef HAVE_VMODE
#  include <X11/extensions/xf86vmode.h>
static XF86VidModeModeInfo **vidmodes;
static int    num_vidmodes;

unsigned short *currentgammaramp = NULL;
static unsigned short systemgammaramp[3][256];

qboolean      vid_gammaworks = false;
qboolean      customgamma = false;

static qboolean vidmode_ext = false;
static qboolean vidmode_active = false;
#endif

qboolean      vid_hwgamma_enabled = false;

cvar_t        m_filter = { "m_filter", "0" };
cvar_t        cl_keypad = { "cl_keypad", "1" };

qboolean      OnChange_windowed_mouse (cvar_t * var, char *value);
cvar_t        _windowed_mouse = { "_windowed_mouse", "1", CVAR_VOLATILE, OnChange_windowed_mouse };
cvar_t        _windowed_height = { "_windowed_height", "600", CVAR_INIT };
cvar_t        _windowed_width = { "_windowed_width", "800", CVAR_INIT };

cvar_t        vid_hwgammacontrol = { "vid_hwgammacontrol", "1" };
cvar_t        vid_mode = { "vid_mode", "0" };

qboolean OnChange_windowed_mouse (cvar_t * var, char *value) {
#ifdef HAVE_VMODE
    if (vidmode_active && !Q_atof (value)) {
        Con_Printf ("Cannot turn %s off when using -fullscreen mode\n", var->name);
        return true;
    }
#endif

    return false;
}

#ifdef HAVE_XEXT_DEBUG
#  include <X11/extensions/dpms.h>
static BOOL   dpms_on;
#endif

#ifdef HAVE_DBUS
#  include <dbus/dbus.h>

#  define DBUS_SERVICE   "org.gnome.ScreenSaver"
#  define DBUS_PATH      "/org/gnome/ScreenSaver"
#  define DBUS_INTERFACE "org.gnome.ScreenSaver"

DBusConnection *connection;
uint          cookie;

void DBUS_DisableScreenSaver (void) {
    DBusError     error;
    DBusMessage  *reply_message;
    DBusMessage  *message;
    DBusBusType   type = DBUS_BUS_SESSION;
    const char   *app;
    const char   *reason;

    dbus_error_init (&error);
    connection = dbus_bus_get_private (type, &error);
    if (!connection) {
        Sys_Printf ("Failed to open connection to %s message bus: %s\n", (type == DBUS_BUS_SYSTEM) ? "system" : "session", error.message);
        dbus_error_free (&error);
        return;
    }

    message = dbus_message_new_method_call (DBUS_SERVICE, DBUS_PATH, DBUS_INTERFACE, "Inhibit");
    if (!message) {
        Sys_Printf ("Screensaver inhibit failed");
        dbus_error_free (&error);
        return;
    }

    app = Q_strdup (ENGINE_NAME);
    reason = Q_strdup ("playing fullscreen");
    dbus_message_append_args (message, DBUS_TYPE_STRING, &app, DBUS_TYPE_STRING, &reason, DBUS_TYPE_INVALID);
    reply_message = dbus_connection_send_with_reply_and_block (connection, message, 200, &error);
    if (reply_message) {
        dbus_message_get_args (reply_message, &error, DBUS_TYPE_INT32, &cookie, NULL);
        dbus_message_unref (reply_message);
    }

    dbus_message_unref (message);
    dbus_error_free (&error);
}

void DBUS_RestoreScreenSaver (void) {
    DBusMessage  *message;

    if (connection != NULL) {
        message = dbus_message_new_method_call (DBUS_SERVICE, DBUS_PATH, DBUS_INTERFACE, "UnInhibit");
        dbus_message_append_args (message, DBUS_TYPE_INT32, &cookie, DBUS_TYPE_INVALID);
        dbus_connection_send (connection, message, NULL);
        dbus_message_unref (message);
        dbus_connection_dispatch (connection);
        dbus_connection_close (connection);
    }
}
#endif

void VID_DisableScreenSaver (void) {
#ifdef HAVE_XEXT_DEBUG
    CARD16        dpms_state;
    int           dpms_dummy;
#endif

    XLockDisplay (dpy);
    XGetScreenSaver (dpy, &xss_timeout, &xss_interval, &xss_blanking, &xss_exposures);
    XSetScreenSaver (dpy, 0, 0, xss_blanking, xss_exposures);
    XUnlockDisplay (dpy);
#ifdef HAVE_DBUS
    DBUS_DisableScreenSaver ();
#endif
#ifdef HAVE_XEXT_DEBUG
    if ((DPMSQueryExtension (dpy, &dpms_dummy, &dpms_dummy)) && (DPMSCapable (dpy))) {
        DPMSInfo (dpy, &dpms_state, &dpms_on);
        DPMSDisable (dpy);
    }
#endif
}

void VID_RestoreScreenSaver (void) {
#ifdef HAVE_XEXT_DEBUG
    int           dpms_dummy;

    if ((DPMSQueryExtension (dpy, &dpms_dummy, &dpms_dummy)) && (DPMSCapable (dpy)) && dpms_on) {
        DPMSEnable (dpy);
    }
#endif
#ifdef HAVE_DBUS
    DBUS_RestoreScreenSaver ();
#endif
    XLockDisplay (dpy);
    XSetScreenSaver (dpy, xss_timeout, xss_interval, xss_blanking, xss_exposures);
    XUnlockDisplay (dpy);
    XResetScreenSaver (dpy);           // lxndr: FIXME
}

#ifdef HAVE_MISC
// lxndr: from xpilot-ng project
void VID_Manage3rdButton (qboolean disable) {
    static qboolean first_run = true;
    static qboolean working = true;
    static qboolean already_warned = false;
    static int    orig_timeout;
    int           MajorVersion, MinorVersion;
    XF86MiscMouseSettings m;
    Status        status;

    if (!working)
        return;

    MajorVersion = MinorVersion = 0;
    if (!XF86MiscQueryVersion (dpy, &MajorVersion, &MinorVersion)) {
        Con_Warnf ("MiscExtension not found\n");
        working = false;
        return;
    }

    Con_DPrintf ("Using XFree86-MiscExtension Version %d.%d\n", MajorVersion, MinorVersion);
    if (!(MajorVersion > 0) && !(MajorVersion == 0 && MinorVersion > 0)) {
        Con_Warnf ("Invalid major or minor opcode\n");
        working = false;
        return;
    }

    status = XF86MiscGetMouseSettings (dpy, &m);
    if (status != 1) {
        Con_Warnf ("Failed to retrieve mouse settings from X server\n");
        working = false;
        return;
    }

    if (first_run) {
        if (m.emulate3buttons) {
            Con_Warnf ("Emulate3Buttons is enabled");
            orig_timeout = m.emulate3timeout;
        } else {
            working = false;           // Emulate3Buttons disabled from the start, so function is turned inactive
            return;
        }
    }

    m.emulate3buttons = !disable;
    m.emulate3timeout = disable ? 0 : orig_timeout;

    status = XF86MiscSetMouseSettings (dpy, &m);
    if (status != 1) {
        Con_Warnf ("Failed to set X server mouse settings");
        working = false;
        return;
    }

    XF86MiscGetMouseSettings (dpy, &m);

    if (m.emulate3buttons != (!disable) && !already_warned) {
        Con_Warnf ("Failed to disable Emulate3Buttons. Just setting timeout to 0");
        already_warned = true;
        if (m.emulate3timeout != (disable ? 0 : orig_timeout)) {
            Con_Warnf ("Can't set timeout. Giving up...");
            working = false;
        }
    }

    first_run = false;
}
#endif

Cursor X_CreateNullCursor (void) {
    Pixmap        cursormask;
    XGCValues     xgc;
    GC            gc;
    XColor        dummycolour;
    Cursor        cursor;

    cursormask = XCreatePixmap (dpy, win, 1, 1, 1);
    xgc.function = GXclear;
    gc = XCreateGC (dpy, cursormask, GCFunction, &xgc);
    XFillRectangle (dpy, cursormask, gc, 0, 0, 1, 1);
    dummycolour.pixel = 0;
    dummycolour.red = 0;
    dummycolour.flags = 04;
    cursor = XCreatePixmapCursor (dpy, cursormask, cursormask, &dummycolour, &dummycolour, 0, 0);
    XFreePixmap (dpy, cursormask);
    XFreeGC (dpy, gc);

    return cursor;
}

int X_LateKey (XKeyEvent * ev) {
    int           key, kp;
    //      char          buf[64];
    KeySym        keysym;
    extern cvar_t cl_keypad;

    key = 0;
    kp = (int) cl_keypad.value;

    //      XLookupString (ev, buf, sizeof buf, &keysym, 0);
    keysym = XLookupKeysym (ev, 0);

    switch (keysym) {
        case XK_Scroll_Lock:
            key = K_SCRLCK;
            break;

        case XK_Caps_Lock:
            key = K_CAPSLOCK;
            break;

        case XK_Num_Lock:
            key = kp ? KP_NUMLOCK : K_PAUSE;
            break;

        case XK_KP_Page_Up:
            key = kp ? KP_PGUP : K_PGUP;
            break;
        case XK_Page_Up:
            key = K_PGUP;
            break;

        case XK_KP_Page_Down:
            key = kp ? KP_PGDN : K_PGDN;
            break;
        case XK_Page_Down:
            key = K_PGDN;
            break;

        case XK_KP_Home:
            key = kp ? KP_HOME : K_HOME;
            break;
        case XK_Home:
            key = K_HOME;
            break;

        case XK_KP_End:
            key = kp ? KP_END : K_END;
            break;
        case XK_End:
            key = K_END;
            break;

        case XK_KP_Left:
            key = kp ? KP_LEFTARROW : K_LEFTARROW;
            break;
        case XK_Left:
            key = K_LEFTARROW;
            break;

        case XK_KP_Right:
            key = kp ? KP_RIGHTARROW : K_RIGHTARROW;
            break;
        case XK_Right:
            key = K_RIGHTARROW;
            break;

        case XK_KP_Down:
            key = kp ? KP_DOWNARROW : K_DOWNARROW;
            break;

        case XK_Down:
            key = K_DOWNARROW;
            break;

        case XK_KP_Up:
            key = kp ? KP_UPARROW : K_UPARROW;
            break;

        case XK_Up:
            key = K_UPARROW;
            break;

        case XK_Escape:
            key = K_ESCAPE;
            break;

        case XK_KP_Enter:
            key = kp ? KP_ENTER : K_ENTER;
            break;

        case XK_Return:
            key = K_ENTER;
            break;

        case XK_Tab:
            key = K_TAB;
            break;

        case XK_F1:
            key = K_F1;
            break;

        case XK_F2:
            key = K_F2;
            break;

        case XK_F3:
            key = K_F3;
            break;

        case XK_F4:
            key = K_F4;
            break;

        case XK_F5:
            key = K_F5;
            break;

        case XK_F6:
            key = K_F6;
            break;

        case XK_F7:
            key = K_F7;
            break;

        case XK_F8:
            key = K_F8;
            break;

        case XK_F9:
            key = K_F9;
            break;

        case XK_F10:
            key = K_F10;
            break;

        case XK_F11:
            key = K_F11;
            break;

        case XK_F12:
            key = K_F12;
            break;

        case XK_BackSpace:
            key = K_BACKSPACE;
            break;

        case XK_KP_Delete:
            key = kp ? KP_DEL : K_DEL;
            break;
        case XK_Delete:
            key = K_DEL;
            break;

        case XK_Pause:
            key = K_PAUSE;
            break;

        case XK_Shift_L:
            key = K_LSHIFT;
            break;
        case XK_Shift_R:
            key = K_RSHIFT;
            break;

        case XK_Execute:
        case XK_Control_L:
            key = K_LCTRL;
            break;
        case XK_Control_R:
            key = K_RCTRL;
            break;

        case XK_Alt_L:
        case XK_Meta_L:
            key = K_LALT;
            break;
        case XK_Alt_R:
        case XK_Meta_R:
            key = K_RALT;
            break;

        case XK_Super_L:
            key = K_LWIN;
            break;
        case XK_Super_R:
            key = K_RWIN;
            break;
        case XK_Menu:
            key = K_MENU;
            break;

        case XK_KP_Begin:
            key = kp ? KP_5 : '5';
            break;

        case XK_KP_Insert:
            key = kp ? KP_INS : K_INS;
            break;
        case XK_Insert:
            key = K_INS;
            break;

        case XK_KP_Multiply:
            key = kp ? KP_STAR : '*';
            break;

        case XK_KP_Add:
            key = kp ? KP_PLUS : '+';
            break;

        case XK_KP_Subtract:
            key = kp ? KP_MINUS : '-';
            break;

        case XK_KP_Divide:
            key = kp ? KP_SLASH : '/';
            break;

        case XK_Print:
            key = K_PRINTSCR;
            break;

        case XF86XK_AudioMute:
            key = K_MUTE;
            break;

        case XF86XK_AudioLowerVolume:
            key = K_VOLUMEDOWN;
            break;

        case XF86XK_AudioRaiseVolume:
            key = K_VOLUMEUP;
            break;

        default:
            if (keysym >= 32 && keysym <= 126)
                key = tolower (keysym);
            break;
    }
    return key;
}

#ifdef DEBUG
void X_DumpEvent (XEvent e) {
    switch (e.type) {
        case KeyPress:
        case KeyRelease:
            Con_Printf ("serial=%lu send_event=%d state=%u keycode=%u\n", e.xkey.serial, e.xkey.send_event, e.xkey.state, e.xkey.keycode);
            break;
        case MotionNotify:
            Con_Printf ("serial=%lu send_event=%d state=%d x=%d y=%d\n", e.xmotion.serial, e.xmotion.send_event, e.xmotion.state, e.xmotion.x,
                        e.xmotion.y);
            break;
        case ButtonPress:
        case ButtonRelease:
            Con_Printf ("serial=%lu send_event=%d state=%d keycode=%d\n", e.xbutton.serial, e.xbutton.send_event, e.xbutton.state, e.xbutton.button);
            break;
        case MappingNotify:
            Con_Printf ("serial=%lu send_event=%d request=%d first_keycode=%d count=%d\n", e.xmapping.serial, e.xmapping.send_event,
                        e.xmapping.request, e.xmapping.first_keycode, e.xmapping.count);
    }
}
#endif

const char   *X_GetEventName (int type) {
    switch (type) {
        case KeyPress:
            return "KeyPress";
        case KeyRelease:
            return "KeyRelease";
        case ButtonPress:
            return "ButtonPress";
        case ButtonRelease:
            return "ButtonRelease";
        case MotionNotify:
            return "MotionNotify";
        case EnterNotify:
            return "EnterNotify";
        case LeaveNotify:
            return "LeaveNotify";
        case FocusIn:
            return "FocusIn";
        case FocusOut:
            return "FocusOut";
        case KeymapNotify:
            return "KeymapNotify";
        case Expose:
            return "Expose";
        case GraphicsExpose:
            return "GraphicsExpose";
        case NoExpose:
            return "NoExpose";
        case VisibilityNotify:
            return "VisibilityNotify";
        case CreateNotify:
            return "CreateNotify";
        case DestroyNotify:
            return "DestroyNotify";
        case UnmapNotify:
            return "UnmapNotify";
        case MapNotify:
            return "MapNotify";
        case MapRequest:
            return "MapRequest";
        case ReparentNotify:
            return "ReparentNotify";
        case ConfigureNotify:
            return "ConfigureNotify";
        case ConfigureRequest:
            return "ConfigureRequest";
        case GravityNotify:
            return "GravityNotify";
        case ResizeRequest:
            return "ResizeRequest";
        case CirculateNotify:
            return "CirculateNotify";
        case CirculateRequest:
            return "CirculateRequest";
        case PropertyNotify:
            return "PropertyNotify";
        case SelectionClear:
            return "SelectionClear";
        case SelectionRequest:
            return "SelectionRequest";
        case SelectionNotify:
            return "SelectionNotify";
        case ColormapNotify:
            return "ColormapNotify";
        case ClientMessage:
            return "ClientMessage";
        case MappingNotify:
            return "MappingNotify";
    }
    return "unknown event";
}

int VID_ErrorHandler (Display *dpy, XErrorEvent *event) {
    char str[64];

    XGetErrorText (dpy, event->error_code, str, sizeof (str));
    Con_Printf ("Got a %s error\n", str);
    return 0;
}

void VID_InitCommon (void) {
    Cvar_Register (&cl_keypad);
    Cvar_Register (&vid_hwgammacontrol);
//    Cvar_Register (&vid_mode);

    if (!Com_CheckParm ("-nomouse")) {
        Cvar_Register (&_windowed_mouse);
        Cvar_Register (&m_filter);
    }

#ifdef HAVE_VMODE
    Cmd_AddCommand ("togglefullscreen", VID_ToggleFullscreen);
    Cmd_AddCommand ("changeresolution", VID_ChangeResolution_f);
#endif

    XSetErrorHandler (VID_ErrorHandler);
}

void VID_InitEnd (void) {
    if (Com_CheckParm ("-nograb")) {
        VID_UngrabInputs ();
        Cvar_SetValue (&_windowed_mouse, 0);
    }
    if (developer.value)
        VID_Print ();
    Con_Printf ("Video mode %dx%d initialized.\n", vid.width, vid.height);

    vid.recalc_refdef = 1;             // force a surface cache flush
}

qboolean VID_InitWindowSize (void) {
    int           i, width, height;

    width = (int) _windowed_width.value;
    height = (int) _windowed_height.value;
    vid.maxwidth = DisplayWidth (dpy, scrnum);
    vid.maxheight = DisplayHeight (dpy, scrnum);

    if ((i = Com_CheckParmSyn ("-window", "-winsize"))) {
        if (Com_IsParmData (i + 1) && Com_IsParmData (i + 2)) {
            width = Q_atoi (com_argv[i + 1]);
            height = Q_atoi (com_argv[i + 2]);
            if (!width || !height)
                Sys_Error ("VID: Bad window width/height\n");
        }
        Cvar_SetValue (&_windowed_width, (float) width);
        Cvar_SetValue (&_windowed_height, (float) height);

    } else if ((i = Com_CheckParm ("-fullscreen"))) {
        if (Com_IsParmData (i + 1) && Com_IsParmData (i + 2)) {
            width = Q_atoi (com_argv[i + 1]);
            height = Q_atoi (com_argv[i + 2]);
            if (!width || !height)
                Sys_Error ("VID: Bad window width/height\n");
        } else {
            width = vid.maxwidth;
            height = vid.maxheight;
        }
        vid.fullscreen = true;

    } else if (Com_CheckParm ("-current")) {
        width = vid.maxwidth;
        height = vid.maxheight;

    } else if (Com_CheckParm ("-width") || Com_CheckParm ("-height")) {
        if ((i = Com_CheckParm ("-width")) && Com_IsParmData (i + 1))
            width = Q_atoi (com_argv[i + 1]);
        if ((i = Com_CheckParm ("-height")) && Com_IsParmData (i + 1))
            height = Q_atoi (com_argv[i + 1]);
        if (!width || !height)
            Sys_Error ("VID: Bad window width/height\n");
        Cvar_SetValue (&_windowed_width, (float) width);
        Cvar_SetValue (&_windowed_height, (float) height);

    } else {
        width = vid.maxwidth;
        height = vid.maxheight;
        vid.fullscreen = true;
    }

    vid.width = width;
    vid.height = height;
    return vid.fullscreen;
}

#ifdef HAVE_VMODE
void VID_InitHWGamma (void) {
    int           xf86vm_gammaramp_size;

    if (Com_CheckParm ("-nohwgamma"))
        return;

    XF86VidModeGetGammaRampSize (dpy, scrnum, &xf86vm_gammaramp_size);

    vid_gammaworks = (xf86vm_gammaramp_size == 256);

    if (vid_gammaworks)
        XF86VidModeGetGammaRamp (dpy, scrnum, xf86vm_gammaramp_size, systemgammaramp[0], systemgammaramp[1], systemgammaramp[2]);
}

void VID_RestoreHWGamma (void) {
    if (vid_gammaworks && customgamma) {
        customgamma = false;
        XF86VidModeSetGammaRamp (dpy, scrnum, 256, systemgammaramp[0], systemgammaramp[1], systemgammaramp[2]);
    }
}

void VID_SetGamma (qboolean fullscreen) {
    static qboolean old_hwgamma_enabled;

    vid_hwgamma_enabled = vid_hwgammacontrol.value && vid_gammaworks;
    vid_hwgamma_enabled = vid_hwgamma_enabled && (vid.fullscreen || vid_hwgammacontrol.value == 2);
    if (vid_hwgamma_enabled != old_hwgamma_enabled) {
        old_hwgamma_enabled = vid_hwgamma_enabled;
        if (vid_hwgamma_enabled && currentgammaramp)
            VID_SetDeviceGammaRamp (currentgammaramp);
        else
            VID_RestoreHWGamma ();
    }
}

#ifndef DEVEL0
void VID_SetDeviceGammaRamp (unsigned short *ramps) {
    if (vid_gammaworks) {
        currentgammaramp = ramps;
        if (vid_hwgamma_enabled) {
            XF86VidModeSetGammaRamp (dpy, scrnum, 256, ramps, ramps + 256, ramps + 512);
            customgamma = true;
        }
    }
}
#  else
void VID_SetDeviceGammaRamp (unsigned short *ramps) {
    int           i;
    XColor        colors[256];
    XSetWindowAttributes attr;
    static qboolean alloc = false;
    Colormap      cmap;

    if (visinfo->class != DirectColor)
        Sys_Error ("no directcolor");

    if (!alloc) {
        cmap = XCreateColormap (dpy, win, visinfo->visual, AllocNone);
        attr.colormap = cmap;
        XChangeWindowAttributes (dpy, win, CWColormap, &attr);
        alloc = true;
    }

    int           f = 65536;               //65536;
    for (i = 0; i < 256; i++) {
        colors[i].pixel = i;
        colors[i].flags = DoRed | DoGreen | DoBlue;
        colors[i].red = ramps[i] * f;
        colors[i].green = ramps[i + 256] * f;
        colors[i].blue = ramps[i + 512] * f;
    }

    XLockDisplay (dpy);
    XStoreColors (dpy, cmap, colors, 256);
    XInstallColormap (dpy, cmap);
//    XSync (dpy, false);
//    XFlush (dpy);
    XUnlockDisplay (dpy);
    XSetWindowColormap (dpy, win, cmap);
}
#  endif

void VID_Print (void) {
    int i, number, width, height;

    if (vidmode_ext) {
        number = width = height = 0;
        Con_Printf ("Video modes available:\n");
        for (i = num_vidmodes - 1; i >= 0; i--) {
            if (vidmodes[i]->hdisplay != width || vidmodes[i]->vdisplay != height)
                Con_Printf ("%2d  %dx%d\n", ++number, vidmodes[i]->hdisplay, vidmodes[i]->vdisplay);
            width = vidmodes[i]->hdisplay;
            height = vidmodes[i]->vdisplay;
        }
    }
}

void VID_SetFullscreen (qboolean setFullscreen, int width, int height) {
    int           i, best_fit, best_dist, dist, x, y;

    // Are we going fullscreen? If so, let's change video mode
    if (vidmode_ext && setFullscreen) {
        best_dist = 9999999;
        best_fit = -1;

        for (i = 0; i < num_vidmodes; i++) {
            if (width > vidmodes[i]->hdisplay || height > vidmodes[i]->vdisplay)
                continue;

            x = width - vidmodes[i]->hdisplay;
            y = height - vidmodes[i]->vdisplay;
            dist = (x * x) + (y * y);
            if (dist < best_dist) {
                best_dist = dist;
                best_fit = i;
            }
        }

        if (best_fit != -1) {
            // change to the mode
            XF86VidModeSwitchToMode (dpy, scrnum, vidmodes[best_fit]);
            vidmode_active = true;

            // Move the viewport to top left
//            XF86VidModeSetViewPort (dpy, scrnum, 0, 0); // lxndr: works much better without this
//            vid.width = vidmodes[best_fit]->hdisplay; // lxndr: possibly not synchronized
//            vid.height = vidmodes[best_fit]->vdisplay; // lxndr: but works much better without

            vid.fullscreen = true;
        } else {
            vid.fullscreen = false;
        }
    }
}

void VID_InitVMode (qboolean setFullscreen, XSetWindowAttributes * attr, unsigned long *mask) {
    int           MajorVersion, MinorVersion;

    // Get video mode list
    MajorVersion = MinorVersion = 0;
    if (!XF86VidModeQueryVersion (dpy, &MajorVersion, &MinorVersion)) {
        vidmode_ext = false;
    } else {
        Con_DPrintf ("Using XFree86-VidModeExtension Version %d.%d\n", MajorVersion, MinorVersion);
        vidmode_ext = true;
        XF86VidModeGetAllModeLines (dpy, scrnum, &num_vidmodes, &vidmodes);
        XF86VidModeSetViewPort (dpy, scrnum, 0, 0);

        *mask |= CWSaveUnder | CWBackingStore | CWOverrideRedirect;
        attr->override_redirect = setFullscreen ? True : False;
        attr->backing_store = NotUseful;
        attr->save_under = False;
    }
}

void VID_ShutdownVMode (void) {
    if (vidmode_active)
        XF86VidModeSwitchToMode (dpy, scrnum, vidmodes[0]);
    vidmode_active = false;
}
#else
void VID_SetDeviceGammaRamp (unsigned short *ramps) {
}
#endif

static qboolean grabbed = false;

void VID_GrabInputs (void) {
#ifdef HAVE_DGA
    int           DGAflags = 0;

    if (!Com_CheckParm ("-nomdga"))
        DGAflags |= XF86DGADirectMouse;
    if (!Com_CheckParm ("-nokdga"))
        DGAflags |= XF86DGADirectKeyb;

    if (!Com_CheckParm ("-nodga") && DGAflags) {
        XF86DGADirectVideo (dpy, DefaultScreen (dpy), DGAflags);
        if (DGAflags & XF86DGADirectMouse)
            dgamouse = true;
        if (DGAflags & XF86DGADirectKeyb)
            dgakeyb = true;
    }
#endif

    while (GrabSuccess != XGrabPointer (dpy, win, True, 0, GrabModeAsync, GrabModeAsync, win, None, CurrentTime))
        Con_Printf ("trying to grab pointer...\n");
/*    XSelectInput (dpy, win, COMMON_MASK & ~MOTION_MASK);
    XWarpPointer (dpy, None, win, 0, 0, 0, 0, (vid.width / 2), (vid.height / 2));
    XSelectInput (dpy, win, COMMON_MASK);*/
    while (GrabSuccess != XGrabKeyboard (dpy, win, True, GrabModeAsync, GrabModeAsync, CurrentTime))
        Con_Printf ("trying to grab keyboard...\n");
//    XGrabButton (dpy, AnyButton, AnyModifier, win, True, 0, GrabModeAsync, GrabModeAsync, win, None);
    grabbed = true;
}

void VID_UngrabInputs (void) {
    if (!grabbed)
       return;

#ifdef HAVE_DGA
    if (dgamouse || dgakeyb) {
        XF86DGADirectVideo (dpy, DefaultScreen (dpy), 0);
        dgamouse = dgakeyb = false;
    }
#endif

    XUngrabPointer (dpy, CurrentTime);
    XUngrabKeyboard (dpy, CurrentTime);
//    XUngrabButton (dpy, AnyButton, AnyModifier, win);
    grabbed = false;
}

void VID_SetResolution (qboolean setFullscreen, int width, int height) {
    XWindowChanges change;

    if (width == vid.maxwidth && height == vid.maxheight) {
        change.x = change.y = 0;
    } else {
        change.x = (vid.maxwidth - width) / 2;
        change.y = (vid.maxheight - height) / 2;
    }
    change.width = vid.width = width;
    change.height = vid.height = height;

    XConfigureWindow (dpy, win, CWX | CWY | CWWidth | CWHeight, &change);
#ifdef HAVE_VMODE
    if (setFullscreen) {
        VID_SetFullscreen (true, width, height);
        if (vid.fullscreen)
            vid.windowedmouse = false;
    } else {
        if (vidmode_active)
            XF86VidModeSwitchToMode (dpy, scrnum, vidmodes[0]);
        vidmode_active = false;
        vid.fullscreen = false;
        vid.windowedmouse = true;
    }
#endif

    VID_GrabInputs ();
}

void VID_ChangeResolution_f (void) {
    int width, height;

    if (Cmd_Argc () == 1) {
        VID_Print ();
        Con_Printf ("Usage: %s <width> <height>\n", Cmd_Argv (0));
        return;
    }
    if (Cmd_Argc () != 3) {
        Con_Printf ("Usage: %s <width> <height>\n", Cmd_Argv (0));
        return;
    }

    width = Q_atoi (Cmd_Argv (1));
    height = Q_atoi (Cmd_Argv (2));
    if (width > vid.maxwidth || width < 160 || height > vid.maxheight || height < 120) {
        Con_Printf ("out of range\n");
        return;
    }

    VID_SetResolution (vid.fullscreen, width, height);
}

void VID_WaitEvent (int type) {
    XEvent event;

    do {
        XMaskEvent(dpy, COMMON_MASK, &event);
        Con_DPrintf ("Got a %s event while waiting for %s\n", X_GetEventName (event.type), X_GetEventName (type));
    } while ((event.type != type) || (event.xmap.event != win));
}

void VID_ToggleFullscreen (void) {
    XSetWindowAttributes attr;

#ifdef GLQUAKE
    extern int thread_levels, thread_textures;

    if (thread_levels || thread_textures) {
        Con_Warnf ("couldn't switch video mode, please retry later.\n");
        return;
    }
#endif

    if (vid.fullscreen) {
        attr.override_redirect = False;

        XUnmapWindow (dpy, win);
        XChangeWindowAttributes(dpy, win, CWOverrideRedirect, &attr);
        XMapRaised (dpy, win);
        VID_WaitEvent (MapNotify);
        XSync (dpy, False);

        if (vid.width == vid.maxwidth && vid.height == vid.maxheight)
            VID_SetResolution (false, (int) _windowed_width.value, (int) _windowed_height.value);
        else
            VID_SetResolution (false, vid.width, vid.height);
    } else {
        attr.override_redirect = True;

        XUnmapWindow (dpy, win);
        VID_WaitEvent (UnmapNotify);
        XChangeWindowAttributes (dpy, win, CWOverrideRedirect, &attr);
        do { // lxndr: something is sucking somewhere, still get BadMatch at some point when toggling fullscreen repeatedly
            XMapRaised (dpy, win);
            VID_WaitEvent (MapNotify);
            XSync (dpy, False);
        } while ((GrabSuccess != XGrabPointer (dpy, win, True, 0, GrabModeAsync, GrabModeAsync, win, None, CurrentTime)));

        VID_SetResolution (true, vid.maxwidth, vid.maxheight);
    }
}

/*
 * Copyright (C) 2009-2010 lxndr.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#include "quakedef.h"

#ifdef GLQUAKE
extern cvar_t s_ambientfade;
extern cvar_t s_ambientlevel;

qboolean      OnChange_ambience (cvar_t * var, char *string);
cvar_t        ambience = { "ambience", "quake", CVAR_AGGREGATE, OnChange_ambience };
cvar_t        ambience_auto = { "ambience_auto", "0" };
cvar_t        ambience_hd = { "ambience_hd", "0" };
cvar_t        ambience_loop = { "ambience_loop", "0" };
cvar_t        ambience_switchtime = { "ambience_switchtime", "0" };
cvar_t        ambience_switchsteps = { "ambience_switchsteps", "100" };

ambience_t   *ambiencelist = NULL;

static int step = 0;

void ambience_custom_f (char *name) {
    Cbuf_AddText (va ("exec %s.cfg\n", name));
}

void ambience_quake_f (void) {
    Cvar_ResetVar (&gl_bloomalpha);
    Cvar_ResetVar (&gl_bloomcolor);
    Cvar_ResetVar (&gl_bloomdarken);
    Cvar_ResetVar (&gl_bloomdiamond);
    Cvar_ResetVar (&gl_bloomintensity);
    Cvar_ResetVar (&gl_bluralpha);
    Cvar_ResetVar (&gl_blurbrightness);
    Cvar_ResetVar (&gl_blurscale);
    Cvar_ResetVar (&gl_dcontrastcolor);
    Cvar_ResetVar (&gl_dcontrastscale);
    Cvar_ResetVar (&gl_dcontrastspeed);
    Cvar_ResetVar (&gl_blendintensity);
    Cvar_ResetVar (&gl_fogbrightness);
    Cvar_ResetVar (&gl_fogcolor);
    Cvar_ResetVar (&gl_fogdensity);
    Cvar_ResetVar (&gl_fogdistance);
    Cvar_ResetVar (&gl_smokesalpha);
    Cvar_ResetVar (&gl_smokesradius);
    Cvar_ResetVar (&r_dynamicintensity);
    if (ambience_hd.value) {
        Cvar_SetValue (&r_levelsmodels, 0.7);
        Cvar_SetValue (&r_levelsweapon, 0.9);
    } else {
        Cvar_ResetVar (&r_levelsmodels);
        Cvar_ResetVar (&r_levelsweapon);
    }
    Cvar_ResetVar (&r_levelsblood);
    Cvar_ResetVar (&r_levelsgrey);
    Cvar_ResetVar (&r_levelsmax);
    Cvar_ResetVar (&r_levelsthresholdhigh);
    Cvar_ResetVar (&r_levelsthresholdlow);
    Cvar_ResetVar (&r_shadowsmax);
    Cvar_ResetVar (&r_shadowsmin);
    Cvar_ResetVar (&r_shadowsmodels);
    Cvar_ResetVar (&r_shadowssize);
    Cvar_ResetVar (&r_shadowsworld);
    Cvar_ResetVar (&s_ambientlevel);
}
void ambience_castle_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.4);
    Cvar_SetValue (&gl_bloomcolor, 0.5);
    Cvar_SetValue (&gl_bloomdarken, 2);
    Cvar_SetValue (&gl_bloomdiamond, 1);
    Cvar_SetValue (&gl_bloomintensity, 4);
    Cvar_SetValue (&gl_bluralpha, 0.5);
    Cvar_SetValue (&gl_blurbrightness, 0.5);
    Cvar_SetValue (&gl_blurscale, 0.2);
    Cvar_SetValue (&gl_dcontrastcolor, 0.5);
    Cvar_SetValue (&gl_dcontrastscale, 0.5);
    Cvar_SetValue (&gl_dcontrastspeed, 0.5);
    Cvar_SetValue (&gl_blendintensity, 0.1);
    Cvar_SetValue (&gl_fogbrightness, 0.5);
    Cvar_SetValue (&gl_fogcolor, 0.5);
    Cvar_SetValue (&gl_fogdensity, 0.4);
    Cvar_SetValue (&gl_fogdistance, 0.4);
    Cvar_SetValue (&gl_smokesalpha, 0.8);
    Cvar_SetValue (&gl_smokesradius, 2);
    Cvar_SetValue (&r_dynamicintensity, 1);
    Cvar_SetValue (&r_levelsmodels, 0.9);
    Cvar_SetValue (&r_levelsweapon, 1);
    Cvar_SetValue (&r_levelsblood, 0);
    Cvar_SetValue (&r_levelsgrey, 0);
    Cvar_SetValue (&r_levelsmax, 1);
    Cvar_SetValue (&r_levelsthresholdhigh, 0.2);
    Cvar_SetValue (&r_levelsthresholdlow, 0.2);
    Cvar_SetValue (&r_shadowsmax, 1);
    Cvar_SetValue (&r_shadowsmin, 0);
    Cvar_SetValue (&r_shadowsmodels, 0.2);
    Cvar_SetValue (&r_shadowssize, 1);
    Cvar_SetValue (&r_shadowsworld, 0.2);
    Cvar_SetValue (&s_ambientlevel, 0.25);
}
void ambience_despair_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.2);
    Cvar_SetValue (&gl_bloomcolor, 0.5);
    Cvar_SetValue (&gl_bloomdarken, 1);
    Cvar_SetValue (&gl_bloomdiamond, 1);
    Cvar_SetValue (&gl_bloomintensity, 5);
    Cvar_SetValue (&gl_bluralpha, 1);
    Cvar_SetValue (&gl_blurbrightness, 0.8);
    Cvar_SetValue (&gl_blurscale, 1);
    Cvar_SetValue (&gl_dcontrastcolor, 0.5);
    Cvar_SetValue (&gl_dcontrastscale, 0.5);
    Cvar_SetValue (&gl_dcontrastspeed, 0.5);
    Cvar_SetValue (&gl_blendintensity, 0.25);
    Cvar_SetValue (&gl_fogbrightness, 0);
    Cvar_SetValue (&gl_fogcolor, 0.1);
    Cvar_SetValue (&gl_fogdensity, 0.4);
    Cvar_SetValue (&gl_fogdistance, 0.5);
    Cvar_SetValue (&gl_smokesalpha, 0.8);
    Cvar_SetValue (&gl_smokesradius, 0);
    Cvar_SetValue (&r_dynamicintensity, 2);
    Cvar_SetValue (&r_levelsmodels, 0.8);
    Cvar_SetValue (&r_levelsweapon, 1);
    Cvar_SetValue (&r_levelsblood, 0);
    Cvar_SetValue (&r_levelsgrey, 0);
    Cvar_SetValue (&r_levelsmax, 1);
    Cvar_SetValue (&r_levelsthresholdhigh, 0);
    Cvar_SetValue (&r_levelsthresholdlow, 0.2);
    Cvar_SetValue (&r_shadowsmax, 1);
    Cvar_SetValue (&r_shadowsmin, 0);
    Cvar_SetValue (&r_shadowsmodels, 0.1);
    Cvar_SetValue (&r_shadowssize, 1);
    Cvar_SetValue (&r_shadowsworld, 0.1);
    Cvar_SetValue (&s_ambientlevel, 0.25);
}
void ambience_epic_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.5);
    Cvar_SetValue (&gl_bloomcolor, 0.5);
    Cvar_SetValue (&gl_bloomdarken, 1);
    Cvar_SetValue (&gl_bloomdiamond, 1);
    Cvar_SetValue (&gl_bloomintensity, 2);
    Cvar_SetValue (&gl_bluralpha, 0.5);
    Cvar_SetValue (&gl_blurbrightness, 0.5);
    Cvar_SetValue (&gl_blurscale, 0.2);
    Cvar_SetValue (&gl_dcontrastcolor, 0.8);
    Cvar_SetValue (&gl_dcontrastscale, 0.2);
    Cvar_SetValue (&gl_dcontrastspeed, 0.5);
    Cvar_SetValue (&gl_blendintensity, 0.25);
    Cvar_SetValue (&gl_fogbrightness, 0.1);
    Cvar_SetValue (&gl_fogcolor, 0.8);
    Cvar_SetValue (&gl_fogdensity, 0.2);
    Cvar_SetValue (&gl_fogdistance, 0.1);
    Cvar_SetValue (&gl_smokesalpha, 0.8);
    Cvar_SetValue (&gl_smokesradius, 2);
    Cvar_SetValue (&r_dynamicintensity, 1);
    Cvar_SetValue (&r_levelsmodels, 0.8);
    Cvar_SetValue (&r_levelsweapon, 0.9);
    Cvar_SetValue (&r_levelsblood, 0);
    Cvar_SetValue (&r_levelsgrey, 0);
    Cvar_SetValue (&r_levelsmax, 1);
    Cvar_SetValue (&r_levelsthresholdhigh, 0.4);
    Cvar_SetValue (&r_levelsthresholdlow, 0.2);
    Cvar_SetValue (&r_shadowsmax, 1);
    Cvar_SetValue (&r_shadowsmin, 0);
    Cvar_SetValue (&r_shadowsmodels, 0.2);
    Cvar_SetValue (&r_shadowssize, 2);
    Cvar_SetValue (&r_shadowsworld, 0.2);
    Cvar_SetValue (&s_ambientlevel, 0.25);
}
void ambience_exorcist_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.5);
    Cvar_SetValue (&gl_bloomcolor, 0.5);
    Cvar_SetValue (&gl_bloomdarken, 1);
    Cvar_SetValue (&gl_bloomdiamond, 1);
    Cvar_SetValue (&gl_bloomintensity, 1);
    Cvar_SetValue (&gl_bluralpha, 0.5);
    Cvar_SetValue (&gl_blurbrightness, 0);
    Cvar_SetValue (&gl_blurscale, 0.2);
    Cvar_SetValue (&gl_dcontrastcolor, 0.5);
    Cvar_SetValue (&gl_dcontrastscale, 1);
    Cvar_SetValue (&gl_dcontrastspeed, 0.5);
    Cvar_SetValue (&gl_blendintensity, 0.1);
    Cvar_SetValue (&gl_fogbrightness, 0.2);
    Cvar_SetValue (&gl_fogcolor, 0.8);
    Cvar_SetValue (&gl_fogdensity, 0.2);
    Cvar_SetValue (&gl_fogdistance, 0.5);
    Cvar_SetValue (&gl_smokesalpha, 0.5);
    Cvar_SetValue (&gl_smokesradius, 2);
    Cvar_SetValue (&r_dynamicintensity, 1);
    Cvar_SetValue (&r_levelsmodels, 0.7);
    Cvar_SetValue (&r_levelsweapon, 1);
    Cvar_SetValue (&r_levelsblood, 0.2);
    Cvar_SetValue (&r_levelsgrey, 1);
    Cvar_SetValue (&r_levelsmax, 1);
    Cvar_SetValue (&r_levelsthresholdhigh, 0.4);
    Cvar_SetValue (&r_levelsthresholdlow, 0.2);
    Cvar_SetValue (&r_shadowsmax, 1);
    Cvar_SetValue (&r_shadowsmin, 0);
    Cvar_SetValue (&r_shadowsmodels, 0.4);
    Cvar_SetValue (&r_shadowssize, 2);
    Cvar_SetValue (&r_shadowsworld, 0.4);
    Cvar_SetValue (&s_ambientlevel, 0.25);
}
void ambience_graveyard_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.4);
    Cvar_SetValue (&gl_bloomcolor, 1);
    Cvar_SetValue (&gl_bloomdarken, 1);
    Cvar_SetValue (&gl_bloomdiamond, 0);
    Cvar_SetValue (&gl_bloomintensity, 2);
    Cvar_SetValue (&gl_bluralpha, 0.8);
    Cvar_SetValue (&gl_blurbrightness, 1);
    Cvar_SetValue (&gl_blurscale, 0.8);
    Cvar_SetValue (&gl_dcontrastcolor, 0.9);
    Cvar_SetValue (&gl_dcontrastscale, 1);
    Cvar_SetValue (&gl_dcontrastspeed, 0.5);
    Cvar_SetValue (&gl_blendintensity, 0.25);
    Cvar_SetValue (&gl_fogbrightness, 0.5);
    Cvar_SetValue (&gl_fogcolor, 0.7);
    Cvar_SetValue (&gl_fogdensity, 0.2);
    Cvar_SetValue (&gl_fogdistance, 0);
    Cvar_SetValue (&gl_smokesalpha, 1);
    Cvar_SetValue (&gl_smokesradius, 1);
    Cvar_SetValue (&r_dynamicintensity, 2);
    Cvar_SetValue (&r_levelsmodels, 0.5);
    Cvar_SetValue (&r_levelsweapon, 0.8);
    Cvar_SetValue (&r_levelsblood, 0.2);
    Cvar_SetValue (&r_levelsgrey, 0);
    Cvar_SetValue (&r_levelsmax, 0.5);
    Cvar_SetValue (&r_levelsthresholdhigh, 1);
    Cvar_SetValue (&r_levelsthresholdlow, 0.1);
    Cvar_SetValue (&r_shadowsmax, 1);
    Cvar_SetValue (&r_shadowsmin, 0.02);
    Cvar_SetValue (&r_shadowsmodels, 0.2);
    Cvar_SetValue (&r_shadowssize, 2);
    Cvar_SetValue (&r_shadowsworld, 0.2);
    Cvar_SetValue (&s_ambientlevel, 0.8);
}
void ambience_inferno_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.5);
    Cvar_SetValue (&gl_bloomcolor, 0.1);
    Cvar_SetValue (&gl_bloomdarken, 1);
    Cvar_SetValue (&gl_bloomdiamond, 1);
    Cvar_SetValue (&gl_bloomintensity, 2);
    Cvar_SetValue (&gl_bluralpha, 1);
    Cvar_SetValue (&gl_blurbrightness, 0.8);
    Cvar_SetValue (&gl_blurscale, 1);
    Cvar_SetValue (&gl_dcontrastcolor, 0.9);
    Cvar_SetValue (&gl_dcontrastscale, 0.5);
    Cvar_SetValue (&gl_dcontrastspeed, 0.5);
    Cvar_SetValue (&gl_blendintensity, 0.1);
    Cvar_SetValue (&gl_fogbrightness, 0.7);
    Cvar_SetValue (&gl_fogcolor, 0.1);
    Cvar_SetValue (&gl_fogdensity, 0.2);
    Cvar_SetValue (&gl_fogdistance, 0);
    Cvar_SetValue (&gl_smokesalpha, 0.8);
    Cvar_SetValue (&gl_smokesradius, 2);
    Cvar_SetValue (&r_dynamicintensity, 2);
    Cvar_SetValue (&r_levelsmodels, 0.8);
    Cvar_SetValue (&r_levelsweapon, 0.9);
    Cvar_SetValue (&r_levelsblood, 0.666);
    Cvar_SetValue (&r_levelsgrey, 0);
    Cvar_SetValue (&r_levelsmax, 0.5);
    Cvar_SetValue (&r_levelsthresholdhigh, 0.1);
    Cvar_SetValue (&r_levelsthresholdlow, 0.2);
    Cvar_SetValue (&r_shadowsmax, 0.5);
    Cvar_SetValue (&r_shadowsmin, 0.02);
    Cvar_SetValue (&r_shadowsmodels, 1);
    Cvar_SetValue (&r_shadowssize, 2);
    Cvar_SetValue (&r_shadowsworld, 1);
    Cvar_SetValue (&s_ambientlevel, 0.8);
}
void ambience_macabre_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.5);
    Cvar_SetValue (&gl_bloomcolor, 0.2);
    Cvar_SetValue (&gl_bloomdarken, 2);
    Cvar_SetValue (&gl_bloomdiamond, 0);
    Cvar_SetValue (&gl_bloomintensity, 2);
    Cvar_SetValue (&gl_bluralpha, 0.5);
    Cvar_SetValue (&gl_blurbrightness, 0.5);
    Cvar_SetValue (&gl_blurscale, 0.5);
    Cvar_SetValue (&gl_dcontrastcolor, 0.9);
    Cvar_SetValue (&gl_dcontrastscale, 0.2);
    Cvar_SetValue (&gl_dcontrastspeed, 0.5);
    Cvar_SetValue (&gl_blendintensity, 0.1);
    Cvar_SetValue (&gl_fogbrightness, 0.1);
    Cvar_SetValue (&gl_fogcolor, 0.1);
    Cvar_SetValue (&gl_fogdensity, 0.666);
    Cvar_SetValue (&gl_fogdistance, 0.1);
    Cvar_SetValue (&gl_smokesalpha, 0.8);
    Cvar_SetValue (&gl_smokesradius, 2);
    Cvar_SetValue (&r_dynamicintensity, 1);
    Cvar_SetValue (&r_levelsmodels, 0.5);
    Cvar_SetValue (&r_levelsweapon, 0.9);
    Cvar_SetValue (&r_levelsblood, 0.2);
    Cvar_SetValue (&r_levelsgrey, 0);
    Cvar_SetValue (&r_levelsmax, 1);
    Cvar_SetValue (&r_levelsthresholdhigh, 0.2);
    Cvar_SetValue (&r_levelsthresholdlow, 0.2);
    Cvar_SetValue (&r_shadowsmax, 1);
    Cvar_SetValue (&r_shadowsmin, 0);
    Cvar_SetValue (&r_shadowsmodels, 0.1);
    Cvar_SetValue (&r_shadowssize, 1);
    Cvar_SetValue (&r_shadowsworld, 0.1);
    Cvar_SetValue (&s_ambientlevel, 1);
}
void ambience_night_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.4);
    Cvar_SetValue (&gl_bloomcolor, 0.1);
    Cvar_SetValue (&gl_bloomdarken, 2);
    Cvar_SetValue (&gl_bloomdiamond, 0);
    Cvar_SetValue (&gl_bloomintensity, 2);
    Cvar_SetValue (&gl_bluralpha, 0.8);
    Cvar_SetValue (&gl_blurbrightness, 1);
    Cvar_SetValue (&gl_blurscale, 0.8);
    Cvar_SetValue (&gl_dcontrastcolor, 0.9);
    Cvar_SetValue (&gl_dcontrastscale, 1);
    Cvar_SetValue (&gl_dcontrastspeed, 0);
    Cvar_SetValue (&gl_blendintensity, 0.25);
    Cvar_SetValue (&gl_fogbrightness, 0.8);
    Cvar_SetValue (&gl_fogcolor, 0.8);
    Cvar_SetValue (&gl_fogdensity, 0.1);
    Cvar_SetValue (&gl_fogdistance, 0.1);
    Cvar_SetValue (&gl_smokesalpha, 0.5);
    Cvar_SetValue (&gl_smokesradius, 5);
    Cvar_SetValue (&r_dynamicintensity, 5);
    Cvar_SetValue (&r_levelsmodels, 0.8);
    Cvar_SetValue (&r_levelsweapon, 0.9);
    Cvar_SetValue (&r_levelsblood, 0.666);
    Cvar_SetValue (&r_levelsgrey, 0);
    Cvar_SetValue (&r_levelsmax, 0.5);
    Cvar_SetValue (&r_levelsthresholdhigh, 0);
    Cvar_SetValue (&r_levelsthresholdlow, 1);
    Cvar_SetValue (&r_shadowsmax, 0.5);
    Cvar_SetValue (&r_shadowsmin, 0);
    Cvar_SetValue (&r_shadowsmodels, 0.1);
    Cvar_SetValue (&r_shadowssize, 1);
    Cvar_SetValue (&r_shadowsworld, 0.1);
    Cvar_SetValue (&s_ambientlevel, 0.8);
}
void ambience_nightmare_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.1);
    Cvar_SetValue (&gl_bloomcolor, 0.4);
    Cvar_SetValue (&gl_bloomdarken, 1);
    Cvar_SetValue (&gl_bloomdiamond, 1);
    Cvar_SetValue (&gl_bloomintensity, 10);
    Cvar_SetValue (&gl_bluralpha, 1);
    Cvar_SetValue (&gl_blurbrightness, 0.8);
    Cvar_SetValue (&gl_blurscale, 1);
    Cvar_SetValue (&gl_dcontrastcolor, 0.2);
    Cvar_SetValue (&gl_dcontrastscale, 0.5);
    Cvar_SetValue (&gl_dcontrastspeed, 0.5);
    Cvar_SetValue (&gl_blendintensity, 0.1);
    Cvar_SetValue (&gl_fogbrightness, 0.1);
    Cvar_SetValue (&gl_fogcolor, 0.5);
    Cvar_SetValue (&gl_fogdensity, 0.2);
    Cvar_SetValue (&gl_fogdistance, 0.2);
    Cvar_SetValue (&gl_smokesalpha, 1);
    Cvar_SetValue (&gl_smokesradius, 0);
    Cvar_SetValue (&r_dynamicintensity, 5);
    Cvar_SetValue (&r_levelsmodels, 0.8);
    Cvar_SetValue (&r_levelsweapon, 1);
    Cvar_SetValue (&r_levelsblood, 0.2);
    Cvar_SetValue (&r_levelsgrey, 0);
    Cvar_SetValue (&r_levelsmax, 1);
    Cvar_SetValue (&r_levelsthresholdhigh, 0.1);
    Cvar_SetValue (&r_levelsthresholdlow, 1);
    Cvar_SetValue (&r_shadowsmax, 1);
    Cvar_SetValue (&r_shadowsmin, 0);
    Cvar_SetValue (&r_shadowsmodels, 0.2);
    Cvar_SetValue (&r_shadowssize, 2);
    Cvar_SetValue (&r_shadowsworld, 0.2);
    Cvar_SetValue (&s_ambientlevel, 0.5);
}
void ambience_underworld_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.5);
    Cvar_SetValue (&gl_bloomcolor, 0.5);
    Cvar_SetValue (&gl_bloomdarken, 2);
    Cvar_SetValue (&gl_bloomdiamond, 1);
    Cvar_SetValue (&gl_bloomintensity, 1);
    Cvar_SetValue (&gl_bluralpha, 1);
    Cvar_SetValue (&gl_blurbrightness, 1);
    Cvar_SetValue (&gl_blurscale, 1);
    Cvar_SetValue (&gl_dcontrastcolor, 0.5);
    Cvar_SetValue (&gl_dcontrastscale, 1);
    Cvar_SetValue (&gl_dcontrastspeed, 0.5);
    Cvar_SetValue (&gl_blendintensity, 0.1);
    Cvar_SetValue (&gl_fogbrightness, 0.2);
    Cvar_SetValue (&gl_fogcolor, 0.5);
    Cvar_SetValue (&gl_fogdensity, 0);
    Cvar_SetValue (&gl_fogdistance, 0.1);
    Cvar_SetValue (&gl_smokesalpha, 0.7);
    Cvar_SetValue (&gl_smokesradius, 2);
    Cvar_SetValue (&r_dynamicintensity, 2);
    Cvar_SetValue (&r_levelsmodels, 0.8);
    Cvar_SetValue (&r_levelsweapon, 0.9);
    Cvar_SetValue (&r_levelsblood, 0.666);
    Cvar_SetValue (&r_levelsgrey, 0);
    Cvar_SetValue (&r_levelsmax, 0.8);
    Cvar_SetValue (&r_levelsthresholdhigh, 0);
    Cvar_SetValue (&r_levelsthresholdlow, 0.4);
    Cvar_SetValue (&r_shadowsmax, 1);
    Cvar_SetValue (&r_shadowsmin, 0.02);
    Cvar_SetValue (&r_shadowsmodels, 0.2);
    Cvar_SetValue (&r_shadowssize, 2);
    Cvar_SetValue (&r_shadowsworld, 0.2);
    Cvar_SetValue (&s_ambientlevel, 0.5);
}
void ambience_violence_f (void) {
    Cvar_SetValue (&gl_bloomalpha, 0.5);
    Cvar_SetValue (&gl_bloomcolor, 0.2);
    Cvar_SetValue (&gl_bloomdarken, 1);
    Cvar_SetValue (&gl_bloomdiamond, 0);
    Cvar_SetValue (&gl_bloomintensity, 1);
    Cvar_SetValue (&gl_bluralpha, 1);
    Cvar_SetValue (&gl_blurbrightness, 0.8);
    Cvar_SetValue (&gl_blurscale, 1);
    Cvar_SetValue (&gl_dcontrastcolor, 0.8);
    Cvar_SetValue (&gl_dcontrastscale, 1);
    Cvar_SetValue (&gl_dcontrastspeed, 0.5);
    Cvar_SetValue (&gl_blendintensity, 0.1);
    Cvar_SetValue (&gl_fogbrightness, 0.2);
    Cvar_SetValue (&gl_fogcolor, 0.2);
    Cvar_SetValue (&gl_fogdensity, 0.5);
    Cvar_SetValue (&gl_fogdistance, 0);
    Cvar_SetValue (&gl_smokesalpha, 0.8);
    Cvar_SetValue (&gl_smokesradius, 2);
    Cvar_SetValue (&r_dynamicintensity, 2);
    Cvar_SetValue (&r_levelsmodels, 0.7);
    Cvar_SetValue (&r_levelsweapon, 0.9);
    Cvar_SetValue (&r_levelsblood, 0.2);
    Cvar_SetValue (&r_levelsgrey, 0);
    Cvar_SetValue (&r_levelsmax, 0.5);
    Cvar_SetValue (&r_levelsthresholdhigh, 0);
    Cvar_SetValue (&r_levelsthresholdlow, 1);
    Cvar_SetValue (&r_shadowsmax, 1);
    Cvar_SetValue (&r_shadowsmin, 0);
    Cvar_SetValue (&r_shadowsmodels, 0.2);
    Cvar_SetValue (&r_shadowssize, 2);
    Cvar_SetValue (&r_shadowsworld, 0.2);
    Cvar_SetValue (&s_ambientlevel, 1);
}

ambience_t    enginelist[] = {
    {"Quake", ambience_quake_f, 2},
    {"Castle", ambience_castle_f, 0},
    {"Despair", ambience_despair_f, 0},
    {"Epic", ambience_epic_f, 0},
    {"Exorcist", ambience_exorcist_f, 0},
    {"Graveyard", ambience_graveyard_f, 1},
    {"Inferno", ambience_inferno_f, 1},
    {"Macabre", ambience_macabre_f, 0},
    {"Night", ambience_night_f, 1},
    {"Nightmare", ambience_nightmare_f, 0},
    {"Underworld", ambience_underworld_f, 1},
    {"Violence", ambience_violence_f, 1}
};

#  define NUM_AMBIENCES (sizeof(enginelist) / sizeof(ambience_t))

/*
 * OnChange_ambience
 */
qboolean OnChange_ambience (cvar_t * var, char *string) {
    ambience_t *a;

    a = Ambience_Get (string);
    if (!a) {
        Con_Printf ("Unknown ambience name: %s\n", string);
        return true;
    }

    Ambience_Set (a);
    return false;
}

void Ambience_Delete_f (void) {
    char          name[MAX_OSPATH];
    const char   *ret;

    if (Cmd_Argc () != 2) {
        Con_Printf ("Usage: %s <name>\n", Cmd_Argv (0));
        return;
    }

    Com_StripExtension (Cmd_Argv (1), name);
    Ambience_Delete (name, &ret);
    Con_Printf ((char *) ret);
}

void Ambience_List_f (void) {
    int           i = 0;
    ambience_t   *a;

    for (a = ambiencelist; ; a = a->next) {
        Con_Printf ("%s\n", a->name);
        i++;
        if (a->next == ambiencelist)
            break;
    }

    Con_Printf ("------------\n%d ambiences\n", i);
}

void Ambience_Next_f (void) {
    ambience_t *a = ambiencelist;

    do {
        a = a->next;
    } while (a->select != 2 && a->select != ambience_hd.value);
    Cvar_Set (&ambience, a->name);
}

void Ambience_Prev_f (void) {
    ambience_t *a = ambiencelist;

    do {
        a = a->prev;
    } while (a->select != 2 && a->select != ambience_hd.value);
    Cvar_Set (&ambience, a->name);
}

void Ambience_Save_f (void) {
    char          name[MAX_OSPATH];
    const char   *ret;

    if (Cmd_Argc () != 2) {
        Con_Printf ("Usage: %s <name>\n", Cmd_Argv (0));
        return;
    }

    Com_StripExtension (Cmd_Argv (1), name);
    Ambience_Save (name, &ret);
    Con_Printf ((char *) ret);
}

void Ambience_Reset_f (void) {
    Ambience_Set (Ambience_Get (ambience.string));
}

ambience_t *Ambience_Add (char *name, void (* apply) (void), int select) {
    ambience_t *new;

    new = Z_Malloc (sizeof (ambience_t));
    if (ambiencelist) {
        new->next = ambiencelist->next;
        new->prev = ambiencelist;
        new->next->prev = new;
        new->prev->next = new;
    } else {
        new->next = new;
        new->prev = new;
    }
    memset (new->name, 0, sizeof (new->name));
    Q_strcpy (new->name, name);
    new->apply = apply;
    new->select = select;
    ambiencelist = new;
    return new;
}

qboolean Ambience_Delete (char *name, const char **ret) {
    ambience_t *a = Ambience_Get (name);
    if (!a) {
        *ret = "Unknown ambience name\n";
        return false;
    }
    if (a->apply) {
        *ret = "Internal ambiences cannot be deleted\n";
        return false;
    }

    Com_DeleteFile (va ("%s/%s.cfg", com_ambiencedir, a->name));

    Cvar_Set (&ambience, a->prev->name);
    a->next->prev = a->prev;
    a->prev->next = a->next;
    Z_Free (a);

    *ret = "Ambience deleted\n";
    return true;
}

ambience_t *Ambience_Get (char *name) {
    ambience_t *a;

    for (a = ambiencelist; ; a = a->next) {
        if (!Q_strcasecmp (a->name, name))
            return a;
        if (a->next == ambiencelist)
            break;
    }
    return NULL;
}

qboolean Ambience_IsInternal (void) {
    if (ambiencelist->apply)
        return true;
    return false;
}

qboolean Ambience_Save (char *name, const char **ret) {
    FILE         *f;
    ambience_t *a;

    a = Ambience_Get (name);
    if (!a) {
        a = Ambience_Add (name, NULL, 2);
        Cvar_Set (&ambience, name);
    } else if (a->apply) {
        *ret = "Internal ambiences cannot be overwritten\n";
        return false;
    }

//    Com_ForceExtension (name, ".cfg"); // nasty thing
    if (!(f = fopen (va ("%s/%s.cfg", com_ambiencedir, name), "wt"))) {
        Con_Warnf ("Couldn't write %s in %s\n", name, com_ambiencedir);
        *ret = "Couldn't write to disk\n";
        return false;
    }
    Ambience_Write (f);
    fclose (f);

    *ret = "Ambience saved\n";
    return true;
}

void Ambience_Set (ambience_t *a) {
    if (a->apply == NULL) 
        ambience_custom_f (a->name);
    else
        a->apply ();
    if (a != ambiencelist) {
        ambiencelist = a;

        if (cls.signon == SIGNONS) {
            if (key_dest == key_game)
                SCR_CenterPrint (a->name);
            else if (a->apply != NULL)
                Con_Printf ("Set ambience to %s\n", a->name);
        }
    }
    step = 0;
}

void Ambience_Update (void) {
    float value;
    cvar_t *var;
    static float time = 0;

    if (!ambience_switchtime.value || ambience_switchsteps.value < step)
        return;

    time += host_frametime;
    if (time > ambience_switchtime.value) {
        step++;
        time = 0;

        for (var = cvar_vars; var; var = var->next) {
            if (var->flags & CVAR_AMBIENCE) {
               value = var->prevvalue + (var->nextvalue - var->prevvalue) / ambience_switchsteps.value * step;
//Con_Printf ("4: %s %f\n", var->name, var->value);
               Cvar_SetValue (var, value);
            }
        }
        if (step == ambience_switchsteps.value && ambience_loop.value)
            Ambience_Next_f ();
    }
}

void Ambience_Write (FILE * f) {
    cvar_t *var;

    for (var = cvar_vars; var; var = var->next) {
        if (var->flags & CVAR_AMBIENCE)
            fprintf (f, "%s \"%s\"\n", var->name, var->string);
    }
}

void Ambience_Init (void) {
    int i;

    Cvar_Register (&ambience);
    Cvar_Register (&ambience_auto);
    Cvar_Register (&ambience_hd);
    Cvar_Register (&ambience_loop);
    Cvar_Register (&ambience_switchtime);
    Cvar_Register (&ambience_switchsteps);

    Cmd_AddCommand ("ambiencedelete", Ambience_Delete_f);
    Cmd_AddCommand ("ambiencelist", Ambience_List_f);
    Cmd_AddCommand ("ambiencenext", Ambience_Next_f);
    Cmd_AddCommand ("ambienceprev", Ambience_Prev_f);
    Cmd_AddCommand ("ambiencesave", Ambience_Save_f);
    Cmd_AddCommand ("resetambience", Ambience_Reset_f);

    // add engine ambiences
    for (i = 0; i < NUM_AMBIENCES; i++)
        Ambience_Add (enginelist[i].name, enginelist[i].apply, enginelist[i].select);

    // add custom ambiences
    RDFlags |= RD_STRIPEXT;
    FS_ReadDir (com_ambiencedir, "*.cfg");
    for (i = 0; i < num_files; i++)
        Ambience_Add (filelist[i].name, NULL, 2);
}
#endif

/*
 * Copyright (C) 2009-2010 lxndr.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// gl_levels

#include "quakedef.h"

qboolean      OnChange_r_levels (cvar_t * var, char *string);
qboolean      OnChange_r_levelsfast (cvar_t * var, char *string);
qboolean      OnChange_r_levelsslow (cvar_t * var, char *string);

// management
cvar_t        r_levels = { "r_levels", "1", CVAR_NONE, OnChange_r_levels };
cvar_t        r_levelsloadtextures = { "r_levelsloadtextures", "0" };
cvar_t        r_levelslowmemory = { "r_levelslowmemory", "1" };
cvar_t        r_levelsthreads = { "r_levelsthreads", "1" };

// fast
cvar_t        r_levelshud = { "r_levelshud", "0", CVAR_AFTER_CHANGE, OnChange_r_levelsfast };
cvar_t        r_levelskeys = { "r_levelskeys", "0", CVAR_AFTER_CHANGE, OnChange_r_levelsfast };
cvar_t        r_levelsmodels = { "r_levelsmodels", "0.8", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_levelsfast };
cvar_t        r_levelssky = { "r_levelssky", "1", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_levelsfast };
cvar_t        r_levelsweapon = { "r_levelsweapon", "0.9", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_levelsfast };

// slow
cvar_t        r_levelsblood = { "r_levelsblood", "0", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_levelsslow };
cvar_t        r_levelsgrey = { "r_levelsgrey", "0", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_levelsslow };
cvar_t        r_levelsmax = { "r_levelsmax", "1", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_levelsslow };
cvar_t        r_levelsthresholdhigh = { "r_levelsthresholdhigh", "0.7", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_levelsslow };
cvar_t        r_levelsthresholdlow = { "r_levelsthresholdlow", "0.1", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_levelsslow };
cvar_t        r_levelswater = { "r_levelswater", "1", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_levelsslow };
cvar_t        r_levelsworld = { "r_levelsworld", "1", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_levelsslow };

qboolean      refresh_levels = false;
int           thread_levels = 0;

#define MAX_LEVELS 127
#define BLOOD_THRESHOLD 16
#define blood(v, r) v / ((float) MAX_LEVELS / (v + 1) * r / (v + 1) + 1)
#define increase(t, l) (    (t) + (1 - (t)) * (1 - (l))    )
#define decrease(t, l) (    (t) * (l)                      )

qboolean OnChange_r_levels (cvar_t * var, char *string) {
    int           i;
    gltexture_t  *glt;
    float         newval = Q_atof (string);

    if (newval == r_levels.value)
        return true;

    VID_ThreadLock ();
    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++)
        glt->action = TEX_ACTION_RETRIEVE;
    VID_ThreadUnlock ();

    if (newval)
        R_SetupLevels ();

    refresh_levels = true;
    return false;
}

qboolean OnChange_r_levelsfast (cvar_t * var, char *string) {
    int           i;
    gltexture_t  *glt;

    if (!r_levels.value)
        return true;

    VID_ThreadLock ();
    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++)
        if (!(glt->texmode & TEX_BRUSH))
            glt->action = TEX_ACTION_RETRIEVE;
    VID_ThreadUnlock ();

    refresh_levels = true;
    return false;
}

qboolean OnChange_r_levelsslow (cvar_t * var, char *string) {
    int           i;
    gltexture_t  *glt;

    if (!r_levels.value)
        return true;

    VID_ThreadLock ();
    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++)
        glt->action = TEX_ACTION_RETRIEVE;
    VID_ThreadUnlock ();

    refresh_levels = true;
    return false;
}

static inline unsigned RGB_Filter (unsigned in, byte limit, float thresholdlow, float thresholdhigh, qboolean blood, qboolean grey) {      // was from QMB
    byte         *rgb, value;
    unsigned      output;

    output = in;
    rgb = ((byte *) & output);

    value = rgb[0] * SAT_WEIGHT_RED + rgb[1] * SAT_WEIGHT_GREEN + rgb[2] * SAT_WEIGHT_BLUE;
    if (grey && blood) {
        if (rgb[0] > value * (2 - r_levelsblood.value)) {
            rgb[0] /= 1.5;
            rgb[1] = rgb[2] = blood (value, rgb[0]);
        } else {
            rgb[0] = rgb[1] = rgb[2] = value;
        }
    } else if (blood) {
        if (rgb[0] > value * (2 - r_levelsblood.value)) {
            rgb[0] /= 1.5;
            rgb[1] = blood (rgb[1], rgb[0]);
            rgb[2] = blood (rgb[2], rgb[0]);
        }
    } else if (grey) {
        rgb[0] = rgb[1] = rgb[2] = value;
    }

    value = (rgb[0] + rgb[1] + rgb[2]) / 3;
    if (value < limit) {
        if (value < thresholdlow) {
            rgb[0] = max (rgb[0] + value - thresholdlow, 0);
            rgb[1] = max (rgb[1] + value - thresholdlow, 0);
            rgb[2] = max (rgb[2] + value - thresholdlow, 0);
        }
        if (value > thresholdhigh) {
            rgb[0] = min (rgb[0] + value - thresholdhigh, limit);
            rgb[1] = min (rgb[1] + value - thresholdhigh, limit);
            rgb[2] = min (rgb[2] + value - thresholdhigh, limit);
        }
    }

    return output;
}

static inline void R_LevelsThresholds (int texmode, byte * limit, float *thresholdlow, float *thresholdhigh) {
    if (texmode & (TEX_TEXT | TEX_KEY))
        *limit = MAX_LEVELS * r_levelsmax.value + 1;
    else if (texmode & (TEX_ALIAS | TEX_LAVA | TEX_WEAPON))
        *limit = MAX_LEVELS * 2 - 1;
    else
        *limit = MAX_LEVELS * (1 + r_levelsmax.value) - 1;

    *thresholdlow = r_levelsthresholdlow.value;
    *thresholdhigh = r_levelsthresholdhigh.value;
    if (texmode & TEX_BRUSH) {
        *thresholdlow = decrease (*thresholdlow, r_levelsworld.value);
        *thresholdhigh = increase (*thresholdhigh, r_levelsworld.value);
    } else if (texmode & TEX_ITEM) {
        *thresholdlow = increase (*thresholdlow, r_levelsmodels.value);
        *thresholdhigh = increase (*thresholdhigh, r_levelsmodels.value);
    } else if (texmode & TEX_ALIAS) {
        *thresholdlow = increase (*thresholdlow, r_levelsmodels.value);
        *thresholdhigh = increase (*thresholdhigh, r_levelsmodels.value);
    } else if (texmode & TEX_WEAPON) {
        *thresholdlow = increase (*thresholdlow, r_levelsweapon.value);
        *thresholdhigh = increase (*thresholdhigh, r_levelsweapon.value);
    }
    *thresholdlow *= MAX_LEVELS;
    *thresholdhigh *= MAX_LEVELS;
}

static inline void R_LevelsProcess (gltexture_t * glt) {
    int           i, sum, size;
    byte         *rgb, limit;
    float         thresholdlow, thresholdhigh;
    qboolean      blood, grey;

    VID_ThreadLock ();
    if (glt->transition || glt->action != TEX_ACTION_PROCESS) {
        VID_ThreadUnlock ();
        return;
    }
    glt->transition = TEX_TRANS_PROCESSING;
    VID_ThreadUnlock ();

    VID_ThreadLock2 ();
    if (!glt->backup || !glt->process) { // lxndr: glt->process should be null when restoring original texture
        VID_ThreadUnlock2 ();
        Con_Debugf (4, "couldn't process levels, will retry texture %s: %i\n", glt->identifier, glt->texnum);
        VID_ThreadLock ();
        glt->action = TEX_ACTION_RETRIEVE;
        VID_ThreadUnlock ();
        return;
    } else {
        VID_ThreadUnlock2 ();
    }

    size = glt->scaled_width * glt->scaled_height;
    sum = 0;

    for (i = 0; i < size; i++) {
        rgb = ((byte *) & glt->backup[i]);
        sum += (rgb[0] + rgb[1] + rgb[2]) / 3;
    }
    R_LevelsThresholds (glt->texmode, &limit, &thresholdlow, &thresholdhigh);
    if (sum / size > limit && !(glt->texmode & TEX_TEXT)) {
        Con_DPrintf ("%s limit:%d mean:%d\n", glt->identifier, limit, sum / size);
        for (i = 0; i < size; i++)
            glt->process[i] = glt->backup[i];
    } else {
        if (r_levelsgrey.value || (r_levelshud.value == 2 && (glt->texmode & (TEX_CONSOLE | TEX_MENU | TEX_SBAR | TEX_TEXT))))
            grey = true;
        else
            grey = false;
        if (r_levelsblood.value && !(r_levelssky.value == 2 && (glt->texmode & TEX_SKY)) && !(r_levelswater.value == 2 && (glt->texmode & TEX_WATER)))
            blood = true;
        else
            blood = false;
        for (i = 0; i < size; i++)
            glt->process[i] = RGB_Filter (glt->backup[i], limit, thresholdlow, thresholdhigh, blood, grey);
    }

    VID_ThreadLock ();
    glt->action = TEX_ACTION_UPLOAD;
    glt->transition = TEX_TRANS_NONE;
    VID_ThreadUnlock ();
}

static inline void R_LevelsRetrieve (gltexture_t * glt, qboolean pbo) {
    int           size;

    VID_ThreadLock ();
    if (glt->transition || glt->action != TEX_ACTION_RETRIEVE) {
        VID_ThreadUnlock ();
        return;
    }
    glt->transition = TEX_TRANS_RETRIEVING;

    if ((glt->texmode & (TEX_DECAL | TEX_LUMA | TEX_NOLEVELS | TEX_TRIGGER))) {
        glt->action = TEX_ACTION_NONE;
        glt->transition = TEX_TRANS_NONE;
        VID_ThreadUnlock ();
        return;
    }
    VID_ThreadUnlock ();

    VID_ThreadLock2 ();
    if (!r_levelsloadtextures.value && !glt->loaded) {
        VID_ThreadUnlock2 ();
        VID_ThreadLock ();
        glt->transition = TEX_TRANS_NONE;
        VID_ThreadUnlock ();
        return;
    }
    VID_ThreadUnlock2 ();

    if (r_levels.value && (!(glt->texmode & TEX_KEY) || r_levelskeys.value) && (!(glt->texmode & TEX_SKY) || r_levelssky.value) && (!(glt->texmode & TEX_WATER) || r_levelswater.value)
        && (!(glt->texmode & (TEX_CONSOLE | TEX_MENU | TEX_SBAR | TEX_TEXT)) || r_levelshud.value)) {
        size = glt->scaled_width * glt->scaled_height;

        glBindTexture (GL_TEXTURE_2D, glt->texnum);     // lxndr: make sure texture is currently bound
        VID_ThreadLock2 ();
        if (!glt->backup) {
            Con_Debugf (4, "no backup for %s: %i\n", glt->identifier, glt->texnum);
            if (r_levelslowmemory.value == 2)
                glt->loaded = false;   // lxndr: force uploading
            if (r_levelsloadtextures.value && !glt->loaded) {
                VID_ThreadUnlock2 ();
                Con_Debugf (4, "image not loaded for %s: %i\n", glt->identifier, glt->texnum);
                if (glt->filename) {
                    if (glt->texmode & (TEX_CONSOLE | TEX_MENU | TEX_SBAR | TEX_TEXT))
                        GL_LoadPicImage (glt->filename, glt->identifier, glt->width, glt->height, glt->texmode);
                    else
                        GL_LoadTextureImage (glt->filename, glt->identifier, glt->width, glt->height, glt->texmode);        // lxndr: upload from file
                } else {
                    Con_Debugf (4, "unable to load image for %s: %i\n", glt->identifier, glt->texnum);
                }
            } else {
                VID_ThreadUnlock2 ();
            }
            glt->count++;
            glt->allocated = size * 4;
            VID_ThreadLock2 ();
            glt->backup = Q_malloc (size * 4);
            VID_ThreadUnlock2 ();
            glGetTexImage (GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, glt->backup);   // lxndr: download from graphic card memory
            Con_Debugf (4, "allocated backup memory at %p for %s: %i\n", glt->backup, glt->identifier, glt->texnum);
        } else {
            VID_ThreadUnlock2 ();
            Con_Debugf (4, "use backup memory at %p for %s: %i\n", glt->backup, glt->identifier, glt->texnum);
        }
 
        VID_ThreadLock2 ();
        if (pbo) {
            glt->process = GL_BindPBO (size * 4, false);
        } else {
            glt->process = Q_malloc (size * 4);
            Con_Debugf (4, "allocated processing memory at %p for %s: %i\n", glt->process, glt->identifier, glt->texnum);
        }
        if (glt->process) {
            VID_ThreadUnlock2 ();
            VID_ThreadLock ();
            glt->action = TEX_ACTION_PROCESS; // lxndr: if !glt->process, action is unchanged, retry later
            VID_ThreadUnlock ();
        } else {
            VID_ThreadUnlock2 ();
        }
    } else {
        VID_ThreadLock ();
        glt->action = TEX_ACTION_UPLOAD;
        VID_ThreadUnlock ();
    }

    VID_ThreadLock ();
    glt->transition = TEX_TRANS_NONE;
    VID_ThreadUnlock ();
}

static inline void R_LevelsUpload (gltexture_t * glt, qboolean pbo) {
    VID_ThreadLock ();
    if (glt->transition || glt->action != TEX_ACTION_UPLOAD) {
        VID_ThreadUnlock ();
        return;
    }
    glt->transition = TEX_TRANS_UPLOADING;
    VID_ThreadUnlock ();

    VID_ThreadLock2 ();
    if (glt->process) {
        VID_ThreadUnlock2 ();
        glBindTexture (GL_TEXTURE_2D, glt->texnum);     // lxndr: make sure texture is currently bound
        if (pbo) {
            GL_UploadPBO (glt->process, glt->scaled_width, glt->scaled_height, glt->texmode);
        } else {
            GL_TryUploadFast (glt->process, glt);
            Con_Debugf (4, "freeing processing memory at %x for %s: %i\n", glt->process, glt->identifier, glt->texnum);
            VID_ThreadLock2 ();
            if (glt->process) {
                Q_free (glt->process);
                glt->process = NULL;
            }
            VID_ThreadUnlock2 ();
        }
        if ((glt->texmode & TEX_BRUSH) && r_levelslowmemory.value == 2 && glt->filename) {
            Con_Debugf (4, "freeing backup memory at %x for %s: %i\n", glt->backup, glt->identifier, glt->texnum);
            glt->allocated = 0;
            glt->count--;
            VID_ThreadLock2 ();
            if (glt->backup) {
                Q_free (glt->backup);
                glt->backup = NULL;
            }
            VID_ThreadUnlock2 ();
        }
    } else {
        Con_Debugf (4, "not processed texture %s: %i\n", glt->identifier, glt->texnum);
        if (glt->backup) {
            VID_ThreadUnlock2 ();
            Con_Debugf (4, "freeing backup memory at %x for %s: %i\n", glt->backup, glt->identifier, glt->texnum);
            glBindTexture (GL_TEXTURE_2D, glt->texnum); // lxndr: make sure texture is currently bound
            GL_TryUploadFast ((void *) glt->backup, glt);
            glt->allocated = 0;
            glt->count--;
            VID_ThreadLock2 ();
            if (glt->backup) {
                Q_free (glt->backup);
                glt->backup = NULL;
            }
            VID_ThreadUnlock2 ();
        } else if (r_levelsloadtextures.value && !glt->loaded) {
            VID_ThreadUnlock2 ();
            if (glt->texmode & (TEX_CONSOLE | TEX_MENU | TEX_SBAR | TEX_TEXT))
                GL_LoadPicImage (glt->filename, glt->identifier, glt->width, glt->height, glt->texmode);
            else
                GL_LoadTextureImage (glt->filename, glt->identifier, glt->width, glt->height, glt->texmode);

        } else {
            VID_ThreadUnlock2 ();
        }
    }

    VID_ThreadLock ();
    glt->action = TEX_ACTION_NONE;
    glt->transition = TEX_TRANS_NONE;
    VID_ThreadUnlock ();
}

/*
 * R_LevelsThread
 * retrieves original textures and process them in a different thread
 */
void R_LevelsThread (void) {
    int           i;
    gltexture_t  *glt;
    qboolean      pbo;

    thread_levels++;
    pbo = gl_ext.pixel_bufferobject && gl_pbo.value;

    while (refresh_levels) {
        for (i = 0, glt = gltextures; i < numgltextures && refresh_levels; i++, glt++) {
            VID_ThreadLock ();
            if (glt->used && glt->action && !glt->locked) {
                glt->locked = true;
                VID_ThreadUnlock ();
                Con_Debugf (3, "applying levels to %s from working thread\n", glt->identifier);
                R_LevelsRetrieve (glt, pbo);
                R_LevelsProcess (glt);
                R_LevelsUpload (glt, pbo);
                glt->locked = false;
            } else {
                if (glt->locked)
                    Con_Debugf (2, "texture %s is locked\n", glt->identifier);
                VID_ThreadUnlock ();
            }
        }
        sleep (1);
    }

    thread_levels--;
}

/*
 * R_Levels
 *
 */
void R_Levels (void) {
    int           i;
    gltexture_t  *glt;
    qboolean      needrefresh = false;
    qboolean      pbo;

    if (!refresh_levels)
        return;

    pbo = gl_ext.pixel_bufferobject && gl_pbo.value;

    if (r_levelsthreads.value && !thread_levels)
        VID_ThreadStart (R_LevelsThread, (int) r_levelsthreads.value);

    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++) {
        VID_ThreadLock ();
        if (glt->used && glt->action) {
            if (!r_levelsthreads.value && !glt->locked) {
                glt->locked = true;
                VID_ThreadUnlock ();
                Con_Debugf (3, "applying levels to %s from main thread\n", glt->identifier);
                R_LevelsRetrieve (glt, pbo);
                R_LevelsProcess (glt);
                R_LevelsUpload (glt, pbo);
                glt->locked = false;
            } else {
                if (glt->locked)
                    Con_Debugf (2, "texture %s is locked\n", glt->identifier);
                VID_ThreadUnlock ();
            }
            if (glt->action)
                needrefresh = true;    // lxndr: tracks unresfreshed textures
        } else {
            VID_ThreadUnlock ();
        }
    }

    if (!needrefresh)
        refresh_levels = false;
}

/*
 * R_LevelsBind
 *
 * directly called from GL_Bind
 */
void R_LevelsBind (int texnum) {
    gltexture_t  *glt;
    qboolean      pbo;

    if (!refresh_levels || ambience_switchtime.value)
        return;

    glt = GL_FindTextureByTexnum (texnum);
    VID_ThreadLock ();
    if (glt && (r_levelsloadtextures.value || glt->loaded)) {
        if (!glt->used) {
            glt->action = TEX_ACTION_RETRIEVE;
            glt->used = true;
        }
        if (glt->action == TEX_ACTION_RETRIEVE && !glt->transition && !glt->locked) {
            glt->locked = true;
            VID_ThreadUnlock ();
            Con_Debugf (3, "applying levels to %s from main thread\n", glt->identifier);
            pbo = gl_ext.pixel_bufferobject && gl_pbo.value;
            R_LevelsRetrieve (glt, pbo);
            R_LevelsProcess (glt);
            R_LevelsUpload (glt, pbo);
            glt->locked = false;
        } else {
            if (glt->locked)
                Con_Debugf (2, "texture %s is locked\n", glt->identifier);
            VID_ThreadUnlock ();
        }
    } else {
        VID_ThreadUnlock ();
    }
}

void R_ResetLevels (void) {
    int           i;
    gltexture_t  *glt;
    extern int    thread_textures;

    if (!refresh_levels && !r_levels.value)
        return;

    refresh_levels = false;            // lxndr: tell to stop current refresh if any

    while (thread_levels || thread_textures) {
        Con_DPrintf ("%d threads remaining...\n");
        sleep (1);
    }

    Con_DPrintf ("levels clearing started\n");
    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++) {
        if (glt->used) {
            glBindTexture (GL_TEXTURE_2D, glt->texnum); // lxndr: GL_Bind calls R_LevelsBind
            if (glt->filename && (glt->texmode & TEX_BRUSH)) {
                if (glt->backup) {
                    Con_Debugf (4, "uploading and freeing backup memory at %x for %s: %i %i\n", glt->backup, glt->identifier, glt->texnum, i);
                    GL_TryUploadFast ((void *) glt->backup, glt);       // lxndr: upload from system memory
                    Q_free (glt->backup);
                    glt->allocated = 0;
                    glt->backup = NULL;
                    glt->count--;
                } else if (sv.active) { // lxndr: shutdown
                    Con_Debugf (4, "uploading %s from file: %i %i\n", glt->identifier, glt->texnum, i);
                    glt->loaded = false;        // lxndr: force uploading
                    if (r_levelsloadtextures.value) {
                        if (glt->texmode & (TEX_CONSOLE | TEX_MENU | TEX_SBAR | TEX_TEXT))
                            GL_LoadPicImage (glt->filename, glt->identifier, glt->width, glt->height, glt->texmode);
                        else
                           GL_LoadTextureImage (glt->filename, glt->identifier, glt->width, glt->height, glt->texmode);
                    }
                }
                if (r_levelslowmemory.value == 2) {
                    Con_Debugf (4, "deleting texture %s: %i %i\n", glt->identifier, glt->texnum, i);
                    glDeleteTextures (1, (unsigned int *) &glt->texnum);
                    glt->loaded = false;
                }
            } else if (glt->backup) {
                Con_Debugf (4, "uploading from system memory for %s: %i %i\n", glt->identifier, glt->texnum, i);
                GL_TryUploadFast ((void *) glt->backup, glt);
            }
            if (cls.signon && !scr_disabled_for_loading)
                glt->action = TEX_ACTION_RETRIEVE;  // lxndr: still in the game
            else if (glt->texmode & TEX_BRUSH)
                glt->used = false;
        }
    }
    Con_DPrintf ("levels clearing ended\n");

    if (r_levels.value)
        refresh_levels = true;
}

void R_ClearLevels (void) {
    if (r_levelslowmemory.value)
        R_ResetLevels ();
}

void R_ResetLevels_f (void) {
    R_ResetLevels ();
}

void R_SetupLevels (void) {
    int           i;
    gltexture_t  *glt;
    texture_t    *tx;
    model_t      *mod;

    if (!r_levels.value)
        return;

    mod = cl.worldmodel;
    for (i = 0; i < mod->numtextures; i++) {
        tx = mod->textures[i];
        if (!tx)
            continue;

        glt = GL_FindTextureByTexnum (tx->gl_texturenum);
        if (glt) {
            VID_ThreadLock ();
            glt->action = TEX_ACTION_RETRIEVE;
            glt->used = true;
            VID_ThreadUnlock ();
        }
    }

    for (i = 0, glt = gltextures; i < numgltextures; i++, glt++) {
        if (!(glt->texmode & TEX_BRUSH)) {
            VID_ThreadLock ();
            glt->action = TEX_ACTION_RETRIEVE;
            VID_ThreadUnlock ();
        }
    }

    refresh_levels = true;
}

/*
 * R_InitLevels
 */
void R_InitLevels (void) {
    Cvar_Register (&r_levels);
    Cvar_Register (&r_levelsblood);
    Cvar_Register (&r_levelsgrey);
    Cvar_Register (&r_levelshud);
    Cvar_Register (&r_levelskeys);
    Cvar_Register (&r_levelsloadtextures);
    Cvar_Register (&r_levelslowmemory);
    Cvar_Register (&r_levelsmax);
    Cvar_Register (&r_levelsmodels);
    Cvar_Register (&r_levelssky);
    Cvar_Register (&r_levelsthreads);
    Cvar_Register (&r_levelsthresholdhigh);
    Cvar_Register (&r_levelsthresholdlow);
    Cvar_Register (&r_levelswater);
    Cvar_Register (&r_levelsweapon);
    Cvar_Register (&r_levelsworld);

    Cmd_AddCommand ("resetlevels", R_ResetLevels_f);
}

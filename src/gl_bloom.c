/*
 * Copyright (C) 1997-2001 Id Software, Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
// gl_bloom.c: 2D lighting post process effect

#include "quakedef.h"

/*
 * ==============================================================================
 * 
 * LIGHT BLOOMS
 * 
 * ==============================================================================
 */

static float  Diamond8x[8][8] = {
    {0.0f, 0.0f, 0.0f, 0.1f, 0.1f, 0.0f, 0.0f, 0.0f},
    {0.0f, 0.0f, 0.2f, 0.3f, 0.3f, 0.2f, 0.0f, 0.0f},
    {0.0f, 0.2f, 0.4f, 0.6f, 0.6f, 0.4f, 0.2f, 0.0f},
    {0.1f, 0.3f, 0.6f, 0.9f, 0.9f, 0.6f, 0.3f, 0.1f},
    {0.1f, 0.3f, 0.6f, 0.9f, 0.9f, 0.6f, 0.3f, 0.1f},
    {0.0f, 0.2f, 0.4f, 0.6f, 0.6f, 0.4f, 0.2f, 0.0f},
    {0.0f, 0.0f, 0.2f, 0.3f, 0.3f, 0.2f, 0.0f, 0.0f},
    {0.0f, 0.0f, 0.0f, 0.1f, 0.1f, 0.0f, 0.0f, 0.0f}
};

static float  Diamond6x[6][6] = {
    {0.0f, 0.0f, 0.1f, 0.1f, 0.0f, 0.0f},
    {0.0f, 0.3f, 0.5f, 0.5f, 0.3f, 0.0f},
    {0.1f, 0.5f, 0.9f, 0.9f, 0.5f, 0.1f},
    {0.1f, 0.5f, 0.9f, 0.9f, 0.5f, 0.1f},
    {0.0f, 0.3f, 0.5f, 0.5f, 0.3f, 0.0f},
    {0.0f, 0.0f, 0.1f, 0.1f, 0.0f, 0.0f}
};

static float  Diamond4x[4][4] = {
    {0.3f, 0.4f, 0.4f, 0.3f},
    {0.4f, 0.9f, 0.9f, 0.4f},
    {0.4f, 0.9f, 0.9f, 0.4f},
    {0.3f, 0.4f, 0.4f, 0.3f}
};

static float  BLOOM_ALPHA;
static float  BLOOM_INTENSITY;
static int    BLOOM_SIZE;

cvar_t        gl_bloom = { "gl_bloom", "0" };
cvar_t        gl_bloomalpha = { "gl_bloomalpha", "0.5", CVAR_AMBIENCE };
cvar_t        gl_bloomcolor = { "gl_bloomcolor", "0.5", CVAR_AMBIENCE };
cvar_t        gl_bloomdarken = { "gl_bloomdarken", "2", CVAR_AMBIENCE };
cvar_t        gl_bloomdiamond = { "gl_bloomdiamond", "1", CVAR_AMBIENCE };
cvar_t        gl_bloomdiamondsize = { "gl_bloomdiamondsize", "8", CVAR_INIT };
cvar_t        gl_bloomintensity = { "gl_bloomintensity", "1", CVAR_AMBIENCE };
cvar_t        gl_bloomsamplesize = { "gl_bloomsamplesize", "512", CVAR_INIT };
cvar_t        gl_bloomfastsample = { "gl_bloomfastsample", "1" };

int           bloomscreentexture;
int           bloomeffecttexture;
int           bloombackuptexture;
int           bloomdownsamplingtexture;

static int    r_screendownsamplingtexture_size;
static int    screen_texture_width, screen_texture_height;
static int    r_screenbackuptexture_size;

// Current refdef size:
static int    curView_x;
static int    curView_y;
static int    curView_width;
static int    curView_height;

// Texture coordinates of screen data inside screentexture.
static float  screenText_tcw;
static float  screenText_tch;

static int    sample_width;
static int    sample_height;

static int    current_pass;

// Texture coordinates of adjusted textures.
static float  sampleText_tcw;
static float  sampleText_tch;

//#define NEARVAL       -10
//#define FARVAL        100

static int    passcount;
static float *acolor;
static float *avertex;
static float *atexcoord;

// This macro is in sample size workspace coordinates.
#define R_Bloom_SamplePass2( xpos, ypos )                           \
	glBegin(GL_QUADS);                                             \
	glTexCoord2f(  0,                      sampleText_tch);        \
	glVertex2f(    xpos,                   ypos);                  \
	glTexCoord2f(  0,                      0);                     \
	glVertex2f(    xpos,                   ypos+sample_height);    \
	glTexCoord2f(  sampleText_tcw,         0);                     \
	glVertex2f(    xpos+sample_width,      ypos+sample_height);    \
	glTexCoord2f(  sampleText_tcw,         sampleText_tch);        \
	glVertex2f(    xpos+sample_width,      ypos);                  \
	glEnd();

void InitVertexArrays (int maxpass) {
    passcount = 0;
    acolor = Q_realloc (acolor, maxpass * 12 * sizeof (float));
    avertex = Q_realloc (avertex, maxpass * 8 * sizeof (float));
    atexcoord = Q_realloc (atexcoord, maxpass * 8 * sizeof (float));
}

inline void R_Bloom_SamplePassColor (int xpos, int ypos, float r, float g, float b) {
    GL_FillArray12f (&acolor[passcount * 3], r, g, b, r, g, b, r, g, b, r, g, b);
    GL_FillArray8f (&atexcoord[passcount * 2], 0, sampleText_tch, 0, 0, sampleText_tcw, 0, sampleText_tcw, sampleText_tch);
    GL_FillArray8f (&avertex[passcount * 2], xpos, ypos, xpos, ypos + sample_height, xpos + sample_width, ypos + sample_height, xpos + sample_width,
                    ypos);
    passcount += 4;
}

inline void R_Bloom_SamplePass (int xpos, int ypos) {
    GL_FillArray8f (&atexcoord[passcount * 2], 0, sampleText_tch, 0, 0, sampleText_tcw, 0, sampleText_tcw, sampleText_tch);
    GL_FillArray8f (&avertex[passcount * 2], xpos, ypos, xpos, ypos + sample_height, xpos + sample_width, ypos + sample_height, xpos + sample_width,
                    ypos);
    passcount += 4;
}

#define R_Bloom_Quad( x, y, width, height, textwidth, textheight ) \
	glBegin(GL_QUADS);                                             \
	glTexCoord2f(  0,          textheight);                        \
	glVertex2f(    x,          y);                                 \
	glTexCoord2f(  0,          0);                                 \
	glVertex2f(    x,          y+height);                          \
	glTexCoord2f(  textwidth,  0);                                 \
	glVertex2f(    x+width,    y+height);                          \
	glTexCoord2f(  textwidth,  textheight);                        \
	glVertex2f(    x+width,    y);                                 \
	glEnd();

//=================
// R_Bloom_InitBackUpTexture
// =================
void R_Bloom_InitBackUpTexture (int width, int height) {
    unsigned char *data;

    data = (unsigned char *) Q_calloc (width * height, sizeof (int));

    r_screenbackuptexture_size = width;

    bloombackuptexture = GL_LoadTexture ("bloom:backup", width, height, data, TEX_NOLEVELS | TEX_LINEAR, 4);

    Q_free (data);
}

// =================
// R_Bloom_InitEffectTexture
// =================
void R_Bloom_InitEffectTexture (void) {
    unsigned char *data;
    float         bloomsizecheck;

    if (gl_bloomsamplesize.value < 32)
        Cvar_SetValue (&gl_bloomsamplesize, 32);

    // Make sure bloom size is a power of 2.
    BLOOM_SIZE = gl_bloomsamplesize.value;
    bloomsizecheck = (float) BLOOM_SIZE;

    while (bloomsizecheck > 1.0f)
        bloomsizecheck /= 2.0f;

    if (bloomsizecheck != 1.0f) {
        BLOOM_SIZE = 32;

        while (BLOOM_SIZE < gl_bloomsamplesize.value)
            BLOOM_SIZE *= 2;
    }
    // Make sure bloom size doesn't have stupid values.
    if (BLOOM_SIZE > screen_texture_width || BLOOM_SIZE > screen_texture_height)
        BLOOM_SIZE = min (screen_texture_width, screen_texture_height);

    if (BLOOM_SIZE != gl_bloomsamplesize.value)
        Cvar_SetValue (&gl_bloomsamplesize, BLOOM_SIZE);

    data = (unsigned char *) Q_calloc (BLOOM_SIZE * BLOOM_SIZE, sizeof (int));
    bloomeffecttexture = GL_LoadTexture ("bloom:effect", BLOOM_SIZE, BLOOM_SIZE, data, TEX_NOLEVELS | TEX_LINEAR, 4);

    Q_free (data);
}

// =================
// R_Bloom_InitTextures
// =================
void R_Bloom_InitTextures (void) {
    unsigned char *data;
    int           maxtexsize;
    size_t        size;

    // Find closer power of 2 to screen size.
    for (screen_texture_width = 1; screen_texture_width < glwidth; screen_texture_width *= 2);
    for (screen_texture_height = 1; screen_texture_height < glheight; screen_texture_height *= 2);

    // Disable blooms if we can't handle a texture of that size.
    glGetIntegerv (GL_MAX_TEXTURE_SIZE, &maxtexsize);
    if (screen_texture_width > maxtexsize || screen_texture_height > maxtexsize) {
        screen_texture_width = screen_texture_height = 0;
        Cvar_SetValue (&gl_bloom, 0);
        Con_Warnf ("'R_InitBloomScreenTexture' too high resolution for Light Bloom. Effect disabled\n");
        return;
    }
    // Init the screen texture.
    size = screen_texture_width * screen_texture_height * sizeof (int);
    data = Q_malloc (size);
    memset (data, 255, size);
    bloomscreentexture = GL_LoadTexture ("bloom:screen", screen_texture_width, screen_texture_height, data, TEX_NOLEVELS | TEX_LINEAR, 4);

    if (!bloomscreentexture)
        bloomscreentexture = texture_extension_number++;

    GL_Bind (bloomscreentexture);
//    glTexImage2D (GL_TEXTURE_2D, 0, gl_solid_format, screen_texture_width, screen_texture_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, screen_texture_width, screen_texture_height, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    Q_free (data);

    // Validate bloom size and init the bloom effect texture.
    R_Bloom_InitEffectTexture ();

    // If screensize is more than 2x the bloom effect texture, set up for stepped downsampling.
    bloomdownsamplingtexture = 0;
    r_screendownsamplingtexture_size = 0;
    if (glwidth > (BLOOM_SIZE * 2) && !gl_bloomfastsample.value) {
        r_screendownsamplingtexture_size = (int) (BLOOM_SIZE * 2);
        data = Q_calloc (r_screendownsamplingtexture_size * r_screendownsamplingtexture_size, sizeof (int));
        bloomdownsamplingtexture =
            GL_LoadTexture ("bloom:downsampling", r_screendownsamplingtexture_size, r_screendownsamplingtexture_size, data,
                            TEX_NOLEVELS | TEX_TRILINEAR, 4);
        Q_free (data);
    }
    // Init the screen backup texture.
    if (r_screendownsamplingtexture_size)
        R_Bloom_InitBackUpTexture (r_screendownsamplingtexture_size, r_screendownsamplingtexture_size);
    else
        R_Bloom_InitBackUpTexture (BLOOM_SIZE, BLOOM_SIZE);
}

// =================
// R_InitBloom
// =================
void R_InitBloom (void) {
    BLOOM_SIZE = 0;

    Cvar_Register (&gl_bloom);
    Cvar_Register (&gl_bloomalpha);
    Cvar_Register (&gl_bloomcolor);
    Cvar_Register (&gl_bloomdarken);
    Cvar_Register (&gl_bloomdiamond);
    Cvar_Register (&gl_bloomfastsample);
    Cvar_Register (&gl_bloomintensity);

    if (!gl_bloom.value) {
        return;
    }

    bloomscreentexture = 0;            // This came from a vid_restart, where none of the textures are valid any more.
    R_Bloom_InitTextures ();
}

// =================
// R_BloomClear
// =================
void R_BloomClear (void) {
    current_pass = 0;
}

// =================
// R_Bloom_DrawEffect
// =================
static inline void R_Bloom_DrawEffect (void) {
    GL_Bind (bloomeffecttexture);

    glEnable (GL_BLEND);
    glBlendFunc (GL_ONE, GL_ONE);
    glColor4f (BLOOM_ALPHA, BLOOM_ALPHA, BLOOM_ALPHA, 1.0f);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glBegin (GL_QUADS);
    glTexCoord2f (0, sampleText_tch);
    glVertex2f (curView_x, curView_y);
    glTexCoord2f (0, 0);
    glVertex2f (curView_x, curView_y + curView_height);
    glTexCoord2f (sampleText_tcw, 0);
    glVertex2f (curView_x + curView_width, curView_y + curView_height);
    glTexCoord2f (sampleText_tcw, sampleText_tch);
    glVertex2f (curView_x + curView_width, curView_y);
    glEnd ();

    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glDisable (GL_BLEND);
}

// =================
// R_Bloom_GeneratexCross - alternative bluring method
// =================
void R_Bloom_GeneratexCross (void) {
    int           i;
    static int    BLOOM_BLUR_RADIUS = 8;
    static float  intensity;
    static float  range;
    float         rgb[3];

    // Setup sample size workspace.
    glViewport (0, 0, sample_width, sample_height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho (0, sample_width, sample_height, 0, NEARVAL, FARVAL);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();

    // Copy small scene into bloomeffecttexture.
    GL_Bind (bloomeffecttexture);
    glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);

    // Start modifying the small scene corner.
    glColor4f (1.0f, 1.0f, 1.0f, 1.0f);
    glEnable (GL_BLEND);

    // Darkening passes.
    if (gl_bloomdarken.value) {
        glBlendFunc (GL_DST_COLOR, GL_ZERO);

        InitVertexArrays (gl_bloomdarken.value);
        for (i = 0; i < gl_bloomdarken.value; i++)
            R_Bloom_SamplePass (0, 0);
        GL_QuadVertex2fTexcoord2f (&avertex[0], &atexcoord[0], passcount);

        glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);
    }
    // Bluring passes.
    if (BLOOM_BLUR_RADIUS) {
        //glBlendFunc (GL_ONE, GL_ONE);
        glBlendFunc (GL_ONE, GL_ONE_MINUS_SRC_COLOR);

        range = (float) BLOOM_BLUR_RADIUS;
        R_CalcRGB (gl_bloomcolor.value, 1, (float *) rgb);
        glColor4f (0.5f, 0.5f, 0.5f, 1.0);

        //diagonal-cross draw 4 passes to add initial smooth
        InitVertexArrays (4);
        R_Bloom_SamplePass (1, 1);
        R_Bloom_SamplePass (-1, 1);
        R_Bloom_SamplePass (-1, -1);
        R_Bloom_SamplePass (1, -1);
        GL_QuadVertex2fTexcoord2f (&avertex[0], &atexcoord[0], passcount);

        glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);

        InitVertexArrays (BLOOM_BLUR_RADIUS * 2);
        for (i = -(BLOOM_BLUR_RADIUS + 1); i < BLOOM_BLUR_RADIUS; i++) {
            intensity = BLOOM_INTENSITY / (range * 2 + 1) * (1 - fabs (i * i) / (float) (range * range));
            if (intensity < 0.05f)
                continue;

//            glColor4f (intensity * rgb[0], intensity * rgb[1], intensity * rgb[2], 1.0f);
            R_Bloom_SamplePassColor (i, 0, intensity * rgb[0], intensity * rgb[1], intensity * rgb[2]);
        }
        GL_QuadColor3fVertex2fTexcoord2f (&acolor[0], &avertex[0], &atexcoord[0], passcount);

        glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);

        InitVertexArrays (BLOOM_BLUR_RADIUS * 2);
        //for(i = 0; i < BLOOM_BLUR_RADIUS; i++)
        for (i = -(BLOOM_BLUR_RADIUS + 1); i < BLOOM_BLUR_RADIUS; i++) {
            intensity = BLOOM_INTENSITY / (range * 2 + 1) * (1 - fabs (i * i) / (float) (range * range));
            if (intensity < 0.05f)
                continue;

//            glColor4f (intensity * rgb[0], intensity * rgb[1], intensity * rgb[2], 1.0f);
            R_Bloom_SamplePassColor (0, i, intensity * rgb[0], intensity * rgb[1], intensity * rgb[2]);
        }
        GL_QuadColor3fVertex2fTexcoord2f (&acolor[0], &avertex[0], &atexcoord[0], passcount);

        glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);
    }
    // Restore full screen workspace.
    glViewport (0, 0, glwidth, glheight);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho (0, glwidth, glheight, 0, NEARVAL, FARVAL);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
}

// =================
// R_Bloom_GeneratexDiamonds
//=================
void R_Bloom_GeneratexDiamonds (void) {
    int           i, j;
    static float  intensity;
    float         rgb[3];

    // Setup sample size workspace
    glViewport (0, 0, sample_width, sample_height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho (0, sample_width, sample_height, 0, NEARVAL, FARVAL);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();

    // Copy small scene into bloomeffecttexture.
    GL_Bind (bloomeffecttexture);
    glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);

    // Start modifying the small scene corner.
    glColor4f (1.0f, 1.0f, 1.0f, 1.0f);
    glEnable (GL_BLEND);

    // Darkening passes
    if (gl_bloomdarken.value) {
        glBlendFunc (GL_DST_COLOR, GL_ZERO);

        InitVertexArrays (gl_bloomdarken.value);
        for (i = 0; i < gl_bloomdarken.value; i++)
            R_Bloom_SamplePass (0, 0);
        GL_QuadVertex2fTexcoord2f (&avertex[0], &atexcoord[0], passcount);

        glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);
    }
    // Bluring passes.
    //glBlendFunc(GL_ONE, GL_ONE);
    glBlendFunc (GL_ONE, GL_ONE_MINUS_SRC_COLOR);

    R_CalcRGB (gl_bloomcolor.value, 1, (float *) rgb);
    glColor4f (0.5f, 0.5f, 0.5f, 1.0); // lxndr: added

    if (gl_bloomdiamondsize.value > 7 || gl_bloomdiamondsize.value <= 3) {
        if (gl_bloomdiamondsize.value != 8)
            Cvar_SetValue (&gl_bloomdiamondsize, 8);

        InitVertexArrays (gl_bloomdiamondsize.value * gl_bloomdiamondsize.value);
        for (i = 0; i < gl_bloomdiamondsize.value; i++) {
            for (j = 0; j < gl_bloomdiamondsize.value; j++) {
                intensity = BLOOM_INTENSITY * 0.2 * Diamond8x[i][j];    // lxndr: was 0.3
                if (intensity < 0.01f)
                    continue;

//                glColor4f (intensity * rgb[0], intensity * rgb[1], intensity * rgb[2], 1.0);
                R_Bloom_SamplePassColor (i - 4, j - 4, intensity * rgb[0], intensity * rgb[1], intensity * rgb[2]);
            }
        }
        GL_QuadColor3fVertex2fTexcoord2f (&acolor[0], &avertex[0], &atexcoord[0], passcount);

    } else if (gl_bloomdiamondsize.value > 5) {
        if (gl_bloomdiamondsize.value != 6)
            Cvar_SetValue (&gl_bloomdiamondsize, 6);

        InitVertexArrays (gl_bloomdiamondsize.value * gl_bloomdiamondsize.value);
        for (i = 0; i < gl_bloomdiamondsize.value; i++) {
            for (j = 0; j < gl_bloomdiamondsize.value; j++) {
                intensity = BLOOM_INTENSITY * 0.5 * Diamond6x[i][j];
                if (intensity < 0.01f)
                    continue;

//                glColor4f (intensity * rgb[0], intensity * rgb[1], intensity * rgb[2], 1.0);
                R_Bloom_SamplePassColor (i - 3, j - 3, intensity * rgb[0], intensity * rgb[1], intensity * rgb[2]);
            }
        }
        GL_QuadColor3fVertex2fTexcoord2f (&acolor[0], &avertex[0], &atexcoord[0], passcount);

    } else if (gl_bloomdiamondsize.value > 3) {
        if (gl_bloomdiamondsize.value != 4)
            Cvar_SetValue (&gl_bloomdiamondsize, 4);

        InitVertexArrays (gl_bloomdiamondsize.value * gl_bloomdiamondsize.value);
        for (i = 0; i < gl_bloomdiamondsize.value; i++) {
            for (j = 0; j < gl_bloomdiamondsize.value; j++) {
                intensity = BLOOM_INTENSITY * 0.8 * Diamond4x[i][j];
                if (intensity < 0.01f)
                    continue;

//                glColor4f (intensity * rgb[0], intensity * rgb[1], intensity * rgb[2], 1.0);
                R_Bloom_SamplePassColor (i - 2, j - 2, intensity * rgb[0], intensity * rgb[1], intensity * rgb[2]);
            }
        }
        GL_QuadColor3fVertex2fTexcoord2f (&acolor[0], &avertex[0], &atexcoord[0], passcount);

    }
    glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, sample_width, sample_height);

    // Restore full screen workspace.
    glViewport (0, 0, glwidth, glheight);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho (0, glwidth, glheight, 0, NEARVAL, FARVAL);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
}

// =================
// R_Bloom_DownsampleView
// =================
void R_Bloom_DownsampleView (void) {
    glDisable (GL_BLEND);
    glColor4f (1.0f, 1.0f, 1.0f, 1.0f);

    // Stepped downsample.
    if (r_screendownsamplingtexture_size) {
        int           midsample_width = r_screendownsamplingtexture_size * sampleText_tcw;
        int           midsample_height = r_screendownsamplingtexture_size * sampleText_tch;

        // Copy the screen and draw resized.
        GL_Bind (bloomscreentexture);
        glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, curView_x, glheight - (curView_y + curView_height), curView_width, curView_height);
        R_Bloom_Quad (0, glheight - midsample_height, midsample_width, midsample_height, screenText_tcw, screenText_tch);

        // Now copy into Downsampling (mid-sized) texture.
        GL_Bind (bloomdownsamplingtexture);
        glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, midsample_width, midsample_height);

        // Now draw again in bloom size.
        glColor4f (0.5f, 0.5f, 0.5f, 1.0f);
        R_Bloom_Quad (0, glheight - sample_height, sample_width, sample_height, sampleText_tcw, sampleText_tch);

        // Now blend the big screen texture into the bloom generation space (hoping it adds some blur).
        glEnable (GL_BLEND);
        glBlendFunc (GL_ONE, GL_ONE);
        glColor4f (0.5f, 0.5f, 0.5f, 1.0f);
        GL_Bind (bloomscreentexture);
        R_Bloom_Quad (0, glheight - sample_height, sample_width, sample_height, screenText_tcw, screenText_tch);
        glColor4f (1.0f, 1.0f, 1.0f, 1.0f);
        glDisable (GL_BLEND);
    } else {
        // Downsample simple.
        GL_Bind (bloomscreentexture);
        glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, curView_x, glheight - (curView_y + curView_height), curView_width, curView_height);
        R_Bloom_Quad (0, glheight - sample_height, sample_width, sample_height, screenText_tcw, screenText_tch);
    }
}

// =================
// R_BloomBlend
// =================
void R_RenderBloom (void) {
    extern vrect_t scr_vrect;
    int           l;

#ifdef DEVEL
    if (!((int) gl_bloom.value & (0x00000001 << current_pass++)))
        return;
#else
    if (!gl_bloom.value)
        return;
#endif

    glEnable (GL_TEXTURE_2D);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    if (!BLOOM_SIZE || screen_texture_width < glwidth || screen_texture_height < glheight)
        R_Bloom_InitTextures ();

    if (screen_texture_width < BLOOM_SIZE || screen_texture_height < BLOOM_SIZE)
        return;

    // Setup current sizes
    curView_x = scr_vrect.x * ((float) glwidth / vid.width);
    curView_y = scr_vrect.y * ((float) glheight / vid.height);
    curView_width = scr_vrect.width * ((float) glwidth / vid.width);
    curView_height = scr_vrect.height * ((float) glheight / vid.height);
    screenText_tcw = ((float) curView_width / (float) screen_texture_width);
    screenText_tch = ((float) curView_height / (float) screen_texture_height);
    if (scr_vrect.height > scr_vrect.width) {
        sampleText_tcw = ((float) scr_vrect.width / (float) scr_vrect.height);
        sampleText_tch = 1.0f;
    } else {
        sampleText_tcw = 1.0f;
        sampleText_tch = ((float) scr_vrect.height / (float) scr_vrect.width);
    }
    sample_width = BLOOM_SIZE * sampleText_tcw;
    sample_height = BLOOM_SIZE * sampleText_tch;

    // Set up full screen workspace.
//    GL_Set2D (glx, gly, glwidth, glheight, NEARVAL, FARVAL);

    // Copy the screen space we'll use to work into the backup texture.
    GL_Bind (bloombackuptexture);
    glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, r_screenbackuptexture_size * sampleText_tcw, r_screenbackuptexture_size * sampleText_tch);

    // Create the bloom image.
    R_Bloom_DownsampleView ();
    BLOOM_ALPHA = gl_bloomalpha.value;
    BLOOM_INTENSITY = gl_bloomintensity.value;
    if (gl_dcontrast.value) {
        if (v_lumhist[0] > v_lummean) {
            l = (1 + gl_dcontrastscale.value * (v_lumhist[0] - v_lummean) / v_lumhist[0]);
            BLOOM_ALPHA *= l;
            BLOOM_INTENSITY *= l;
        }
    }
#ifdef DEVEL
    if (gl_bloomdiamond.value >= 0) {
        if (current_pass <= gl_bloomdiamond.value)
            R_Bloom_GeneratexDiamonds ();
        else
            R_Bloom_GeneratexCross ();
    } else {
        if (current_pass <= fabs (gl_bloomdiamond.value))
            R_Bloom_GeneratexCross ();
        else
            R_Bloom_GeneratexDiamonds ();
    }
#else
    if (gl_bloomdiamond.value)
        R_Bloom_GeneratexDiamonds ();
    else
        R_Bloom_GeneratexCross ();
#endif

    // Restore the screen-backup to the screen.
    glDisable (GL_BLEND);
    glColor4f (1, 1, 1, 1);
    GL_Bind (bloombackuptexture);
    R_Bloom_Quad (0, glheight - (r_screenbackuptexture_size * sampleText_tch), r_screenbackuptexture_size * sampleText_tcw,
                  r_screenbackuptexture_size * sampleText_tch, sampleText_tcw, sampleText_tch);

    R_Bloom_DrawEffect ();

    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glDisable (GL_TEXTURE_2D);
}

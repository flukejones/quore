/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// cmd.c -- Quake script command processing module

#include "quakedef.h"

static cmd_function_t *cmd_functions;
static cmd_alias_t *cmd_aliases;
static cmd_syn_t *cmd_synonyms;
qboolean      cmd_wait;

// 

/*
 * Cmd_Wait_f
 *
 * Causes execution of the remainder of the command buffer to be delayed until
 * next frame.  This allows commands like: bind g "impulse 5 ; +attack ; wait ;
 * -attack ; impulse 2"
 */
void Cmd_Wait_f (void) {
    cmd_wait = true;
}

/*
 *
 *
 * COMMAND BUFFER
 *
 *
 */

sizebuf_t     cmd_text;

/*
 * Cbuf_Init
 */
void Cbuf_Init (void) {
    SZ_Alloc (&cmd_text, MAX_TEXTSIZE); // space for commands and script files
}

/*
 * Cbuf_AddText
 *
 * Adds command text at the end of the buffer
 */
void Cbuf_AddText (char *text) {
    int           l;

    l = strlen (text);

    if (cmd_text.cursize + l >= cmd_text.maxsize) {
        Con_Printf ("Cbuf_AddText: overflow\n");
        return;
    }

    SZ_Write (&cmd_text, text, l);
}

/*
 * Cbuf_InsertText
 *
 * Adds command text immediately after the current command
 * Adds a \n to the text
 * FIXME: actually change the command buffer to do less copying
 *
 */
void Cbuf_InsertText (char *text) {
    char         *temp = NULL;
    int           templen;

    // copy off any commands still remaining in the exec buffer
    if ((templen = cmd_text.cursize)) {
        temp = Z_Malloc (templen);
        memcpy (temp, cmd_text.data, templen);
        SZ_Clear (&cmd_text);
    }

    // add the entire text of the file
    Cbuf_AddText (text);

    // add the copied off data
    if (templen) {
        SZ_Write (&cmd_text, temp, templen);
        Z_Free (temp);
    }
}

/*
 * Cbuf_Execute
 */
void Cbuf_Execute (void) {
    int           i, quotes;
    char         *text, line[MAXCMDLINE];

    while (cmd_text.cursize) {
        // find a \n or ; line break
        text = (char *) cmd_text.data;

        quotes = 0;
        for (i = 0; i < cmd_text.cursize; i++) {
            if (text[i] == '"')
                quotes++;
            if (!(quotes & 1) && text[i] == ';')
                break;                 // don't break if inside a quoted string
            if (text[i] == '\n')
                break;
        }

        memcpy (line, text, i);
        line[i] = 0;

        // delete the text from the command buffer and move remaining commands down
        // this is necessary because commands (exec, alias) can insert data at the
        // beginning of the text buffer
        if (i == cmd_text.cursize) {
            cmd_text.cursize = 0;
        } else {
            i++;
            cmd_text.cursize -= i;
            memcpy (text, text + i, cmd_text.cursize);
        }

        // execute the command line
        Cmd_ExecuteString (line, src_command);

        if (cmd_wait) {                // skip out while text still remains in buffer, leaving it for next frame
            cmd_wait = false;
            break;
        }
    }
}

/*
 *
 *
 * SCRIPT COMMANDS
 *
 *
 */

/*
 * Set commands are added early, so they are guaranteed to be
 * set before the client and server initialize for the first time.
 *
 * Other commands are added late, after all initialization is complete.
 *
 */
qboolean Cbuf_AddEarlyCommands (void) {
    int           i;
    qboolean directloading = false;

    for (i = 0; i < Com_Argc () - 1; i++)
        if (!Q_strcasecmp (Com_Argv (i), "+connect") || !Q_strcasecmp (Com_Argv (i), "+load") || !Q_strcasecmp (Com_Argv (i), "+map"))
            directloading = true;

    for (i = 0; i < Com_Argc () - 2; i++) {
        if (Q_strcasecmp (Com_Argv (i), "+set"))
            continue;
        Cbuf_AddText (va ("set %s %s\n", Com_Argv (i + 1), Com_Argv (i + 2)));
        i += 2;
    }

    return directloading;
}

/*
 * Cmd_StuffCmds_f
 *
 * Adds command line parameters as script statements Commands lead with a +,
 * and continue until a - or another + quake +prog jctest.qp +cmd amlev1 quake
 * -nosound +cmd amlev1
 */
void Cmd_StuffCmds_f (void) {
    int           i, j, s;
    char         *text, *build, c;

    // build the combined string to parse from
    s = 0;
    for (i = 1; i < com_argc; i++) {
        if (!com_argv[i])
            continue;                  // NEXTSTEP nulls out -NXHost
        s += strlen (com_argv[i]) + 1;
    }
    if (!s)
        return;

    text = Z_Malloc (s + 1);
    text[0] = 0;
    for (i = 1; i < com_argc; i++) {
        if (!com_argv[i])
            continue;                  // NEXTSTEP nulls out -NXHost
        strcat (text, com_argv[i]);
        if (i != com_argc - 1)
            strcat (text, " ");
    }

    // pull out the commands
    build = Z_Malloc (s + 1);
    build[0] = 0;

    for (i = 0; i < s - 1; i++) {
        if (text[i] == '+') {
            i++;

            for (j = i; (text[j] != '+') && (text[j] != '-') && (text[j] != 0); j++);

            c = text[j];
            text[j] = 0;

            strcat (build, text + i);
            strcat (build, "\n");
            text[j] = c;
            i = j - 1;
        }
    }

    if (build[0])
        Cbuf_InsertText (build);

    Z_Free (text);
    Z_Free (build);
}

/*
 * Cmd_Exec_f
 */
void Cmd_Exec_f (void) {
    char         *f, name[MAX_OSPATH];
    int           mark;

    if (Cmd_Argc () != 2) {
        Con_Printf ("exec <filename> : execute a script file\n");
        return;
    }

    Q_strncpyz (name, Cmd_Argv (1), sizeof (name));
    mark = Hunk_LowMark ();
    if (!(f = (char *) Com_LoadHunkFile (name))) {
        char         *p;

        p = Com_SkipPath (name);
        if (!strchr (p, '.')) {        // no extension, so try the default (.cfg)
            strcat (name, ".cfg");
            f = (char *) Com_LoadHunkFile (name);
        }

        if (!f) {
            Con_VPrintf ("Couldn't exec %s\n", name);
            return;
        }
    }
    if (cl_warncmd.value || developer.value)
        Con_Printf ("execing %s\n", name);

    Cbuf_InsertText (f);
    if (cmd_text.cursize)
        Con_DPrintf ("Command buffer size: %d\n", cmd_text.cursize);
    Hunk_FreeToLowMark (mark);
}

/*
 * Cmd_Echo_f
 *
 * Just prints the rest of the line to the console
 */
void Cmd_Echo_f (void) {
    int           i;

    for (i = 1; i < Cmd_Argc (); i++)
        Con_Printf ("%s ", Cmd_Argv (i));
    Con_Printf ("\n");
}

/*
 * Cmd_FindAlias
 */
cmd_alias_t  *Cmd_FindAlias (char *name) {
    cmd_alias_t  *alias;

    for (alias = cmd_aliases; alias; alias = alias->next)
        if (!Q_strcasecmp (name, alias->name))
            return alias;

    return NULL;
}

/*static qboolean Cmd_AliasCommand (void) {
    char          text[MAXCMDLINE];
    static qboolean recursive = false;
    cmd_alias_t  *alias;

    for (alias = cmd_aliases; alias; alias = alias->next)
        if (!Q_strcasecmp (alias->name, Cmd_Argv (0)))
            break;

    if (!alias)
        return false;

    if (!alias->value)
        return true;                   // just ignore this command

    // build new command string
    Q_strncpyz (text, alias->value, sizeof (text) - 1);
    strcat (text, " ");
    strncat (text, Cmd_Args (), sizeof (text) - strlen (text) - 1);

    assert (!recursive);
    recursive = true;
    Cmd_ExecuteString (text, src_command);
    recursive = false;

    return true;
}*/

/*
 * Cmd_Alias_f
 *
 * Creates a new command that executes a command string (possibly ; seperated)
 *
 */
void Cmd_Alias_f (void) {
    cmd_alias_t  *alias;
    char         *s, cmd[MAXCMDLINE];
    int           i, c;

    if (Cmd_Argc () == 1) {
        Con_Printf ("Usage: alias <alias> \"[<command...>]\"\n");
        return;
    }

    s = Cmd_Argv (1);

    if (Cvar_FindVar (s))
        Con_Warnf ("%s is already defined as a variable\n", s);

    if (Cmd_FindCommand (s) || Cmd_FindCommandSyn (s))
        Con_Warnf ("%s is already defined as a command\n", s);

    if (strlen (s) >= MAX_ALIAS_NAME) {
        Con_Printf ("Alias name is too long\n");
        return;
    }

    alias = Cmd_FindAlias (s);

    if (Cmd_Argc () == 2) {
        if (alias)
            Con_Printf ("%s : %s\n", alias->name, alias->value);
        else
            Con_Printf ("unknown alias name\n");
        return;
    }

    // if the alias already exists, reuse it
    if (alias) {
        Z_Free (alias->value);
    } else {
        alias = Z_Malloc (sizeof (cmd_alias_t));
        alias->next = cmd_aliases;
        cmd_aliases = alias;
    }
    strcpy (alias->name, s);

    // copy the rest of the command line
    cmd[0] = 0;                        // start out with a null string
    c = Cmd_Argc ();
    for (i = 2; i < c; i++) {
        strcat (cmd, Cmd_Argv (i));
        if (i < c - 1) // lxndr: FIXME: line ending with space
            strcat (cmd, " ");
    }

    alias->value = Z_Strdup (cmd);
}

void Cmd_AliasList_f (void) {
    cmd_alias_t  *alias;
    int           counter;

    for (counter = 0, alias = cmd_aliases; alias; alias = alias->next, counter++)
        Con_Printf ("%10s : %s\n", alias->name, alias->value);

    Con_Printf ("------------\n%d aliases\n", counter);
}

void Cmd_Unalias_f (void) {
    cmd_alias_t  *alias, *drop;
    char         *s;

    if (Cmd_Argc () == 1) {
        Con_Printf ("Usage: unalias <alias>\n");
        return;
    }

    s = Cmd_Argv (1);
    if (!Q_strcasecmp (s, cmd_aliases->name)) {
        drop = cmd_aliases;
        cmd_aliases = cmd_aliases->next;
        Z_Free (drop->value);
        Z_Free (drop);
    } else {
        for (alias = cmd_aliases; alias->next; alias = alias->next) {
            if (!Q_strcasecmp (s, alias->next->name)) {
                drop = alias->next;
                alias->next = drop->next;
                Z_Free (drop->value);
                Z_Free (drop);
                break;
            }
        }
    }
}

void Cmd_WriteAliases (FILE *f) {
    cmd_alias_t  *alias;

    for (alias = cmd_aliases; alias; alias = alias->next)
        fprintf (f, "alias \"%s\" \"%s\"\n", alias->name, alias->value);
}

// from FuhQuake legacy commands

void Cmd_AddCommandSyn (char *synonym, char *name) {
    cmd_syn_t  *cmd;

    cmd = (cmd_syn_t *) Z_Malloc (sizeof (cmd_syn_t));
    cmd->next = cmd_synonyms;
    cmd_synonyms = cmd;

    cmd->name = Z_Strdup (name);
    cmd->syn = Z_Strdup (synonym);
}

void Cmd_AddLegacyCommand (char *oldname, char *newname) {
    if (game.legacy)
        Cmd_AddCommandSyn (oldname, newname);
}

cmd_syn_t  *Cmd_FindCommandSyn (char *syn) {
    cmd_syn_t  *cmd;

    for (cmd = cmd_synonyms; cmd; cmd = cmd->next)
        if (!Q_strcasecmp (cmd->syn, syn))
            return cmd;

    return NULL;
}

static qboolean Cmd_CommandSyn (void) {
    char          text[MAXCMDLINE];
    static qboolean recursive = false;
    cmd_syn_t  *cmd;

    for (cmd = cmd_synonyms; cmd; cmd = cmd->next)
        if (!Q_strcasecmp (cmd->syn, Cmd_Argv (0)))
            break;

    if (!cmd)
        return false;

    if (!cmd->name[0])
        return true;                   // just ignore this command

    // build new command string
    Q_strncpyz (text, cmd->name, sizeof (text) - 1);
    strcat (text, " ");
    strncat (text, Cmd_Args (), sizeof (text) - strlen (text) - 1);

    assert (!recursive);
    recursive = true;
    Cmd_ExecuteString (text, src_command);
    recursive = false;

    return true;
}

/*
 *
 *
 * COMMAND EXECUTION
 *
 *
 */

#define	MAX_ARGS	80

static int    cmd_argc;
static char  *cmd_argv[MAX_ARGS];
static char  *cmd_null_string = "";
static char  *cmd_args = NULL;

cmd_source_t  cmd_source;

/*
 * Cmd_Argc
 */
int Cmd_Argc (void) {
    return cmd_argc;
}

/*
 * Cmd_Argv
 */
char         *Cmd_Argv (int arg) {
    if ((unsigned) arg >= cmd_argc)
        return cmd_null_string;
    return cmd_argv[arg];
}

/*
 * Cmd_Args
 */
char         *Cmd_Args (void) {
    if (!cmd_args)
        return "";
    return cmd_args;
}

/*
 * Cmd_TokenizeString
 *
 * Parses the given string into command line tokens.
 */
void Cmd_TokenizeString (char *text) {
    int           idx;
    static char   argv_buf[MAXCMDLINE];

    idx = 0;

    cmd_argc = 0;
    cmd_args = NULL;

    while (1) {
        // skip whitespace up to a \n
        while (*text == ' ' || *text == '\t' || *text == '\r')
            text++;

        if (*text == '\n') {           // a newline seperates commands in the buffer
            text++;
            break;
        }

        if (!*text)
            return;

        if (cmd_argc == 1)
            cmd_args = text;

        if (!(text = Com_Parse (text)))
            return;

        if (cmd_argc < MAX_ARGS) {
            cmd_argv[cmd_argc] = argv_buf + idx;
            strcpy (cmd_argv[cmd_argc], com_token);
            idx += strlen (com_token) + 1;
            cmd_argc++;
        }
    }
}

/*
 * Cmd_AddCommand
 */
void Cmd_AddCommand (char *cmd_name, xcommand_t function) {
    cmd_function_t *cmd;

    if (host_initialized)              // because hunk allocation would get stomped
        Sys_Error ("Cmd_AddCommand after host_initialized");

    // fail if the command is a variable name
    if (Cvar_FindVar (cmd_name)) {
        Con_Printf ("Cmd_AddCommand: %s already defined as a variable\n", cmd_name);
        return;
    }

    // fail if the command already exists
    if (Cmd_FindCommand (cmd_name) || Cmd_FindCommandSyn (cmd_name)) {
        Con_Printf ("Cmd_AddCommand: %s already defined\n", cmd_name);
        return;
    }

    cmd = Hunk_AllocName (sizeof (cmd_function_t), "command");
    cmd->name = cmd_name;
    cmd->function = function;
    cmd->next = cmd_functions;
    cmd_functions = cmd;
}

/*
 * Cmd_FindCommand
 */
cmd_function_t *Cmd_FindCommand (char *cmd_name) {
    cmd_function_t *cmd;

    for (cmd = cmd_functions; cmd; cmd = cmd->next)
        if (!Q_strcasecmp (cmd_name, cmd->name))
            return cmd;

    return NULL;
}

/*
 * Cmd_CompleteCommand
 */
char         *Cmd_CompleteCommand (char *partial) {
    cmd_function_t *cmd;
    cmd_alias_t  *acmd;
    cmd_syn_t    *scmd;
    int           len;

    if (!(len = strlen (partial)))
        return NULL;

    for (cmd = cmd_functions; cmd; cmd = cmd->next)
        if (!Q_strncasecmp (partial, cmd->name, len))
            return cmd->name;

    for (acmd = cmd_aliases; acmd; acmd = acmd->next)
        if (!Q_strncasecmp (partial, acmd->name, len))
            return acmd->name;

    for (scmd = cmd_synonyms; scmd; scmd = scmd->next)
        if (!Q_strncasecmp (partial, scmd->syn, len))
            return scmd->syn;

    return NULL;
}

/*
 * Cmd_CompleteCount
 */
int Cmd_CompleteCount (char *partial) {
    cmd_function_t *cmd;
    cmd_alias_t  *acmd;
    cmd_syn_t    *scmd;
    int           len, c = 0;

    if (!(len = strlen (partial)))
        return 0;

    for (cmd = cmd_functions; cmd; cmd = cmd->next)
        if (!Q_strncasecmp (partial, cmd->name, len))
            c++;

    for (acmd = cmd_aliases; acmd; acmd = acmd->next)
        if (!Q_strncasecmp (partial, acmd->name, len))
            c++;

    for (scmd = cmd_synonyms; scmd; scmd = scmd->next)
        if (!Q_strncasecmp (partial, scmd->syn, len))
            c++;

    return c;
}

// 

static char   filetype[8] = "file";

static char   compl_common[MAX_FILENAME];
static int    compl_len;
static int    compl_clen;

static void FindCommonSubString (char *s) {
    if (!compl_clen) {
        Q_strncpyz (compl_common, s, sizeof (compl_common));
        compl_clen = strlen (compl_common);
    } else {
        while (compl_clen > compl_len && Q_strncasecmp (s, compl_common, compl_clen))
            compl_clen--;
    }
}

static void CompareParams (void) {
    int           i;

    compl_clen = 0;

    for (i = 0; i < num_files; i++)
        FindCommonSubString (filelist[i].name);

    if (compl_clen)
        compl_common[compl_clen] = 0;
}

static void   PrintEntries (void);
extern char   key_lines[32][MAXCMDLINE];
extern int    edit_line;
extern int    key_linepos;

/*
 * Cmd_CompleteParameter
 *
 * parameter completion for various commands
 */
void Cmd_CompleteParameter (char *partial, char *attachment) {
    char         *s, *param, stay[MAX_QPATH], subdir[MAX_QPATH] = "", param2[MAX_QPATH];
    int           alt_len;

    Q_strncpyz (stay, partial, sizeof (stay));

    // we don't need "<command> + space(s)" included
    param = strrchr (stay, ' ') + 1;

    compl_len = strlen (param);
    strcat (param, attachment);

    FS_EraseAllEntries ();

    if (!strcmp (attachment, "*.bsp")) {
        Q_strncpyz (subdir, "maps/", sizeof (subdir));
    } else if (!strcmp (attachment, "*.tga")) {
        if (strstr (stay, "loadsky ") == stay || strstr (stay, "sky ") == stay) {
            Q_strncpyz (subdir, "gfx/env/", sizeof (subdir));
            RDFlags |= RD_SKYBOX;
        } else if (strstr (stay, "loadcharset ") == stay || strstr (stay, "loadconchars ") == stay || strstr (stay, "loadhudchars ") == stay) {
            Q_strncpyz (subdir, "gfx/charsets/", sizeof (subdir));
        } else if (strstr (stay, "loadconback ") == stay) {
            Q_strncpyz (subdir, "gfx/conbacks/", sizeof (subdir));
        } else if (strstr (stay, "loadcrosshair ") == stay) {
            Q_strncpyz (subdir, "gfx/crosshairs/", sizeof (subdir));
        }
    }

    if (strstr (stay, "game ") == stay) {
        RDFlags |= RD_GAMEDIR;
        FS_ReadDir (com_basedir, param);
    } else if (strstr (stay, "load ") == stay || strstr (stay, "printtxt ") == stay) {
        RDFlags |= RD_STRIPEXT;
        FS_ReadDir (com_gamedir, param);
    } else {
        alt_len = strlen (param) - 3;
        FS_SearchAll (subdir, param, 0);

        if (!strcmp (param + alt_len, "tga")) {
            Q_strncpyz (param2, param, alt_len + 1);
            strcat (param2, "png");
            FS_SearchAll (subdir, param2, 0);
            if (!strcmp (param + alt_len, "tga")) {
                Q_strncpyz (param2, param, alt_len + 1);
                strcat (param2, "jpg");
                FS_SearchAll (subdir, param2, 0);
            }
        } else if (!strcmp (param + alt_len, "dem")) {
            Q_strncpyz (param2, param, alt_len + 1);
            strcat (param2, "dz"); // lxndr: TESTME: wrong string end
            FS_SearchAll (subdir, param2, 0);
        }
    }

    if (!filelist)
        return;

    s = strchr (partial, ' ') + 1;
    // just made this to avoid printing the filename twice when there's only one match
    if (num_files == 1) {
        *s = '\0';
        strcat (partial, filelist[0].name);
        key_linepos = strlen (partial) + 1;
        key_lines[edit_line][key_linepos] = 0;
        return;
    }

    CompareParams ();

    Con_Printf ("]%s\n", partial);
    PrintEntries ();

    *s = '\0';
    if (compl_len)
        strcat (partial, compl_common);
    key_linepos = strlen (partial) + 1;
    key_lines[edit_line][key_linepos] = 0;
}

/*
 * Cmd_ExecuteString
 *
 * A complete command line has been parsed, so try to execute it
 * FIXME: lookupnoadd the token to speed search?
 */
void Cmd_ExecuteString (char *text, cmd_source_t src) {
    cmd_function_t *cmd;
    cmd_alias_t  *a;
    int i;
    char tmp[MAXCMDLINE];

    cmd_source = src;
    Cmd_TokenizeString (text);

    // execute the command line
    if (!Cmd_Argc ())
        return;                        // no tokens

    if (developer.value)
        Con_Debugf (5, "Cmd_ExecuteString: %s\n", text);

    // check functions
    for (cmd = cmd_functions; cmd; cmd = cmd->next) {
        if (!Q_strcasecmp (cmd_argv[0], cmd->name)) {
            cmd->function ();
            return;
        }
    }

    // check alias
    for (a = cmd_aliases; a; a = a->next) {
        if (!Q_strcasecmp (cmd_argv[0], a->name)) {
            memset (tmp, sizeof (tmp), 0);
            strcpy (tmp, a->value);
            for (i = 1; i < cmd_argc; i++) {
                strcat (tmp, " ");
                strcat (tmp, cmd_argv[i]);
            }
            strcat (tmp, ";");
            Cbuf_InsertText (tmp);
            return;
        }
    }

    // check cvars
    if (Cvar_Command ())
        return;

    // joe: check command synonyms
    if (Cmd_CommandSyn ())
        return;

    if (cl_warncmd.value || developer.value || key_dest == key_console)
        Con_Printf ("Unknown command \"%s\"\n", cmd_argv[0]);
}

/*
 * Cmd_ForwardToServer
 *
 * Sends the entire command line over to the server
 */
void Cmd_ForwardToServer (void) {
    if (cls.state != ca_connected) {
        Con_Printf ("Not connected to server\n");
        return;
    }

    if (cls.demoplayback)              // not really connected
        return;

    MSG_WriteByte (&cls.message, clc_stringcmd);
    if (Q_strcasecmp (Cmd_Argv (0), "cmd")) {
        SZ_Print (&cls.message, Cmd_Argv (0));
        SZ_Print (&cls.message, " ");
    }
    if (Cmd_Argc () > 1)
        SZ_Print (&cls.message, Cmd_Args ());
    else
        SZ_Print (&cls.message, "\n");
}

/*
 * Cmd_CheckParm
 *
 * Returns the position (1 to argc-1) in the command's argument list where the
 * given parameter apears, or 0 if not present
 */
int Cmd_CheckParm (char *parm) {
    int           i;

    if (!parm)
        Sys_Error ("Cmd_CheckParm: NULL");

    for (i = 1; i < Cmd_Argc (); i++)
        if (!Q_strcasecmp (parm, Cmd_Argv (i)))
            return i;

    return 0;
}

/*
 * Cmd_Find_f
 *
 * List found console commands
 */
void Cmd_Find_f (void) {
    cmd_function_t *cmd;
    int           counter;

    if (cmd_source != src_command)
        return;

    if (Cmd_Argc () != 2) {
        Con_Printf ("Usage: %s <pattern>\n", Cmd_Argv (0));
        return;
    }

    for (counter = 0, cmd = cmd_functions; cmd; cmd = cmd->next) {
        if (strstr (cmd->name, Cmd_Argv (1))) {
            Con_Printf ("%s\n", cmd->name);
            counter++;
        }
    }

    Con_Printf ("------------\nFound %d commands\n", counter);
}

/*
 * Cmd_List_f
 *
 * List all console commands
 */
void Cmd_List_f (void) {
    cmd_function_t *cmd;
    int           counter;

    if (cmd_source != src_command)
        return;

    for (counter = 0, cmd = cmd_functions; cmd; cmd = cmd->next, counter++)
        Con_Printf ("%s\n", cmd->name);

    Con_Printf ("------------\n%d commands\n", counter);
}

/*
 * Cmd_Dir_f
 *
 * List all files in the mod's directory
 */
void Cmd_Dir_f (void) {
    char          myarg[MAX_FILENAME];

    if (cmd_source != src_command)
        return;

    if (!strcmp (Cmd_Argv (1), "")) {
        Q_strncpyz (myarg, "*", sizeof (myarg));
        Q_strncpyz (filetype, "file", sizeof (filetype));
    } else {
        Q_strncpyz (myarg, Cmd_Argv (1), sizeof (myarg));
        // first two are exceptional cases
        if (strstr (myarg, "*"))
            Q_strncpyz (filetype, "file", sizeof (filetype));
        else if (strstr (myarg, "*.dem"))
            Q_strncpyz (filetype, "demo", sizeof (filetype));
        else {
            if (strchr (myarg, '.')) {
                Q_strncpyz (filetype, Com_FileExtension (myarg), sizeof (filetype));
                filetype[strlen (filetype)] = 0x60;     // right-shadowed apostrophe
            } else {
                strcat (myarg, "*");
                Q_strncpyz (filetype, "file", sizeof (filetype));
            }
        }
    }

    RDFlags |= RD_COMPLAIN;
    FS_ReadDir (com_gamedir, myarg);
    if (!filelist)
        return;

    Con_Printf ("\x02" "%ss in current folder are:\n", filetype);
    PrintEntries ();
}

#ifdef _WIN32
static void toLower (char *str) {      // for strings
    char         *s;
    int           i;

    i = 0;
    s = str;

    while (*s) {
        if (*s >= 'A' && *s <= 'Z')
            *(str + i) = *s + 32;
        i++;
        s++;
    }
}
#endif

/*
 * PaddedPrint
 */
#define	COLUMNWIDTH	20
#define	MINCOLUMNWIDTH	18             // the last column may be slightly smaller smaller

extern int    con_x;

static void PaddedPrint (char *s) {
    extern int    con_linewidth;
    int           nextcolx = 0;

    if (con_x)
        nextcolx = (int) ((con_x + COLUMNWIDTH) / COLUMNWIDTH) * COLUMNWIDTH;

    if (nextcolx > con_linewidth - MINCOLUMNWIDTH || (con_x && nextcolx + strlen (s) >= con_linewidth))
        Con_Printf ("\n");

    if (con_x)
        Con_Printf (" ");
    while (con_x % COLUMNWIDTH)
        Con_Printf (" ");
    Con_Printf ("%s", s);
}

static void PrintEntries (void) {
    int           i;

    Con_Printf ("\x02" "inside packs:\n");
    for (i = 0; i < num_files; i++)
        if (filelist[i].pack)
            PaddedPrint (filelist[i].name);
    if (con_x)
        Con_Printf ("\n");

    Con_Printf ("\x02" "outside packs:\n");
    for (i = 0; i < num_files; i++)
        if (!filelist[i].pack)
            PaddedPrint (filelist[i].name);
    if (con_x)
        Con_Printf ("\n");
}

/*
 * Cmd_CompleteAdvanced
 *
 * Advanced completion
 */
void Cmd_CompleteAdvanced (void) {
    int           c, v;
    char         *s, *cmd;
    cmd_function_t *fcmd;
    cmd_alias_t  *acmd;
    cmd_syn_t    *scmd;
    cvar_t       *var;

    s = key_lines[edit_line] + 1;
    if (!(compl_len = strlen (s)))
        return;
    compl_clen = 0;

    c = Cmd_CompleteCount (s);
    v = Cvar_CompleteCount (s);

    if (c + v > 1) {
        Con_Printf ("\n");

        if (c) {
            Con_Printf ("\x02" "commands:\n");
            // check commands
            for (fcmd = cmd_functions; fcmd; fcmd = fcmd->next) {
                if (!Q_strncasecmp (s, fcmd->name, compl_len)) {
                    PaddedPrint (fcmd->name);
                    FindCommonSubString (fcmd->name);
                }
            }

            // lxndr: check for aliases
            for (acmd = cmd_aliases; acmd; acmd = acmd->next) {
                if (!Q_strncasecmp (s, acmd->name, compl_len)) {
                    PaddedPrint (acmd->name);
                    FindCommonSubString (acmd->name);
                }
            }

            // joe: check for command synomys
            for (scmd = cmd_synonyms; scmd; scmd = scmd->next) {
                if (!Q_strncasecmp (s, scmd->syn, compl_len)) {
                    PaddedPrint (scmd->syn);
                    FindCommonSubString (scmd->syn);
                }
            }

            if (con_x)
                Con_Printf ("\n");
        }

        if (v) {
            Con_Printf ("\x02" "variables:\n");
            // check variables
            for (var = cvar_vars; var; var = var->next) {
                if (!Q_strncasecmp (s, var->name, compl_len)) {
                    PaddedPrint (var->name);
                    FindCommonSubString (var->name);
                }
            }

            if (con_x)
                Con_Printf ("\n");
        }
    }

    if (c + v == 1) {
        if (!(cmd = Cmd_CompleteCommand (s)))
            cmd = Cvar_CompleteVariable (s);
    } else if (compl_clen) {
        compl_common[compl_clen] = 0;
        cmd = compl_common;
    } else
        return;

    strcpy (key_lines[edit_line] + 1, cmd);
    key_linepos = strlen (cmd) + 1;
    if (c + v == 1)
        key_lines[edit_line][key_linepos++] = ' ';
    key_lines[edit_line][key_linepos] = 0;
}

/*
 * Cmd_DemoList_f
 *
 * List all demo files
 */
void Cmd_DemoList_f (void) {
    char          myarg[MAX_FILENAME];

    if (cmd_source != src_command)
        return;

    if (!strcmp (Cmd_Argv (1), "")) {
        Q_strncpyz (myarg, "*.dem", sizeof (myarg));
    } else {
        Q_strncpyz (myarg, Cmd_Argv (1), sizeof (myarg));
        if (strchr (myarg, '.')) {
            Con_Printf ("You needn't use dots in demolist parameters\n");
            if (strcmp (Com_FileExtension (myarg), "dem")) {
                Con_Printf ("demolist is for demo files only\n");
                return;
            }
        } else {
            strcat (myarg, "*.dem");
        }
    }

    Q_strncpyz (filetype, "demo", sizeof (filetype));

    FS_EraseAllEntries ();
    RDFlags |= (RD_STRIPEXT | RD_COMPLAIN);
    FS_ReadDir (com_demodir, myarg);
    FS_ReadDir (com_gamedir, myarg);
    if (!filelist)
        return;

//    Con_Printf ("\x02" "%ss in current folder are:\n", filetype);
    PrintEntries ();
}

/*
 * AddTabs
 *
 * Replaces nasty tab character with spaces
 */
static void AddTabs (char *buf) {
    unsigned char *s;
    char          tmp[256];
    int           i;

    for (s = (unsigned char *) buf, i = 0; *s; s++, i++) {
        switch (*s) {
            case 0xb4:
            case 0x27:
                *s = 0x60;
                break;

            case '\t':
                strcpy (tmp, (char *) s + 1);
                while (i++ < 8)
                    *s++ = ' ';
                *s-- = '\0';
                strcat (buf, tmp);
                break;
        }

        if (i >= 7)
            i = -1;
    }
}

/*
 * Cmd_PrintTxt_f
 *
 * Prints a text file into the console
 */
void Cmd_PrintTxt_f (void) {
    char          name[MAX_FILENAME], buf[256] = { 0 };
    FILE         *f;

    if (cmd_source != src_command)
        return;

    if (Cmd_Argc () != 2) {
        Con_Printf ("printtxt <txtfile> : prints a text file\n");
        return;
    }

    Q_strncpyz (name, Cmd_Argv (1), sizeof (name));

    Com_DefaultExtension (name, ".txt");

    Q_strncpyz (buf, va ("%s/%s", com_gamedir, name), sizeof (buf));
    if (!(f = fopen (buf, "rt"))) {
        Con_Warnf ("Couldn't open %s\n", name);
        return;
    }

    Con_Printf ("\n");
    while (fgets (buf, 256, f)) {
        AddTabs (buf);
        Con_Printf ("%s", buf);
        memset (buf, 0, sizeof (buf));
    }

    Con_Printf ("\n\n");
    fclose (f);
}

/*
 * Cmd_Init
 */
void Cmd_Init (void) {
    // register our commands
    Cmd_AddCommand ("stuffcmds", Cmd_StuffCmds_f);
    Cmd_AddCommand ("exec", Cmd_Exec_f);
    Cmd_AddCommand ("echo", Cmd_Echo_f);
    Cmd_AddCommand ("alias", Cmd_Alias_f);
    Cmd_AddCommand ("aliaslist", Cmd_AliasList_f);
    Cmd_AddCommand ("unalias", Cmd_Unalias_f);
    Cmd_AddCommand ("cmd", Cmd_ForwardToServer);
    Cmd_AddCommand ("wait", Cmd_Wait_f);

    Cmd_AddCommand ("cmdfind", Cmd_Find_f);
    Cmd_AddCommand ("cmdlist", Cmd_List_f);
    Cmd_AddCommand ("dir", Cmd_Dir_f);
    Cmd_AddCommand ("demolist", Cmd_DemoList_f);
    Cmd_AddCommand ("printtxt", Cmd_PrintTxt_f);
}

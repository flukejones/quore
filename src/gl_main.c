/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// gl_rmain.c

#include "quakedef.h"

entity_t      r_worldentity;

qboolean      r_cache_thrash;          // compatibility

vec3_t        modelorg, r_entorigin;
entity_t     *currententity;

int           r_visframecount;         // bumped when going to a new PVS
int           r_framecount;            // used for dlight push checking

mplane_t      frustum[4];

int           c_oldbrush_polys, c_brush_polys, c_md1_polys, c_md3_polys;

int           blurtexture;
int           damagetexture;
int           particletexture;         // little dot for particles
int           playertextures;          // up to 16 color translated skins
int           skyboxtextures;
int           underwatertexture, detailtexture;

float         r_avertexnormal_dots[SHADEDOT_QUANT][256] =
#include "anorm_dots.h"
    ;
float        *shadedots = r_avertexnormal_dots[0];

float         r_avertexnormals[NUMVERTEXNORMALS][3] =
#include "anorms.h"
    ;

// view origin
vec3_t        vup;
vec3_t        vpn;
vec3_t        vright;
vec3_t        r_origin;

float         r_world_matrix[16];
float         r_base_world_matrix[16];

float         v_lumhist[LUM_HIST_SIZE];
float         v_lummean;

float         fogcolor[4];

// screen size info
refdef_t      r_refdef;

mleaf_t      *r_viewleaf, *r_oldviewleaf;
mleaf_t      *r_viewleaf2, *r_oldviewleaf2;     // for watervis hack

texture_t    *r_notexture_mip;

int           d_lightstylevalue[256];  // 8.8 fraction of base light value

cvar_t        r_farclip = { "r_farclip", "4096" };
cvar_t        r_novis = { "r_novis", "0" };       // lxndr: fps eater
qboolean      OnChange_r_skybox (cvar_t * var, char *string);
cvar_t        r_skybox = { "r_skybox", "", CVAR_NONE, OnChange_r_skybox };
cvar_t        r_skycolor = { "r_skycolor", "4" };
cvar_t        r_speeds = { "r_speeds", "0", CVAR_VOLATILE };

cvar_t        r_alpha = { "r_alpha", "0" };
cvar_t        r_alphalava = { "r_alphalava", "0.5" };
cvar_t        r_alphaslime = { "r_alphaslime", "0.5" };
cvar_t        r_alphateleport = { "r_alphateleport", "0.5" };
cvar_t        r_alphawater = { "r_alphawater", "0.5" };

cvar_t        r_colorlava = { "r_colorlava", "0.5" };
cvar_t        r_colorslime = { "r_colorslime", "0.5" };
cvar_t        r_colorteleport = { "r_colorteleport", "0.5" };
cvar_t        r_colorwater = { "r_colorwater", "0.5" };

cvar_t        r_drawaliases = { "r_drawaliases", "1", CVAR_VOLATILE };
cvar_t        r_drawbrushes = { "r_drawbrushes", "1", CVAR_VOLATILE };
cvar_t        r_drawflames = { "r_drawflames", "1" };
cvar_t        r_drawsprites = { "r_drawsprites", "1", CVAR_VOLATILE };
cvar_t        r_drawsky = { "r_drawsky", "1" };
cvar_t        r_drawtris = { "r_drawtris", "0", CVAR_VOLATILE };
cvar_t        r_drawweapon = { "r_drawweapon", "1" };
cvar_t        r_drawworld = { "r_drawworld", "2", CVAR_VOLATILE };

cvar_t        r_dynamic = { "r_dynamic", "1" };
cvar_t        r_dynamicintensity = { "r_dynamicintensity", "1", CVAR_AMBIENCE };
cvar_t        r_dynamicradius = { "r_dynamicradius", "0.666", CVAR_AMBIENCE };

cvar_t        r_fullbright = { "r_fullbright", "0" };
cvar_t        r_fullbrightkeys = { "r_fullbrightkeys", "0" };
cvar_t        r_fullbrightskins = { "r_fullbrightskins", "0" };

qboolean      OnChange_r_quality (cvar_t * var, char *string);
cvar_t        r_quality = { "r_quality", "0.6", CVAR_AGGREGATE, OnChange_r_quality };

qboolean      OnChange_r_shadows (cvar_t * var, char *string);
cvar_t        r_shadows = { "r_shadows", "0", CVAR_AFTER_CHANGE, OnChange_r_shadows };
cvar_t        r_shadowsintensity = { "r_shadowsintensity", "0.4", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_shadows };
cvar_t        r_shadowsmax = { "r_shadowsmax", "1", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_shadows };
cvar_t        r_shadowsmin = { "r_shadowsmin", "0", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_shadows };
cvar_t        r_shadowsmodels = { "r_shadowsmodels", "0.4", CVAR_AMBIENCE };
cvar_t        r_shadowssize = { "r_shadowssize", "3", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_shadows };
cvar_t        r_shadowsworld = { "r_shadowsworld", "0.4", CVAR_AFTER_CHANGE | CVAR_AMBIENCE, OnChange_r_shadows };

cvar_t        gl_affinemodels = { "gl_affinemodels", "0" };
cvar_t        gl_caustics = { "gl_caustics", "0" };
cvar_t        gl_clear = { "gl_clear", "1" };     // lxndr: solves rendering issue when alpha && !novis
cvar_t        gl_cull = { "gl_cull", "1" };
cvar_t        gl_detail = { "gl_detail", "0" };   // lxndr: craft paper style
cvar_t        gl_doubleeyes = { "gl_doubleeyes", "0" };
cvar_t        gl_finish = { "gl_finish", "0" };
cvar_t        gl_interdist = { "gl_interdist", "165" }; // lxndr: was 135
cvar_t        gl_lerptextures = { "gl_lerptextures", "1" };
cvar_t        gl_nocolors = { "gl_nocolors", "0" };
cvar_t        gl_playermip = { "gl_playermip", "0" };
cvar_t        gl_polyblend = { "gl_polyblend", "1" };
cvar_t        gl_ringalpha = { "gl_ringalpha", "0.4" };
cvar_t        gl_smoothmodels = { "gl_smoothmodels", "1" };
cvar_t        gl_solidparticles = { "gl_solidparticles", "0" };
qboolean      OnChange_gl_vertexarrays (cvar_t * var, char *string);
cvar_t        gl_vertexarrays = { "gl_vertexarrays", "0", CVAR_NONE, OnChange_gl_vertexarrays };
cvar_t        gl_vertexlights = { "gl_vertexlights", "0" };
cvar_t        gl_ztrick = { "gl_ztrick", "0" };

cvar_t        gl_blur = { "gl_blur", "0" };
cvar_t        gl_bluralpha = { "gl_bluralpha", "0.2", CVAR_AMBIENCE };
cvar_t        gl_blurblood = { "gl_blurblood", "0" };
cvar_t        gl_blurbrightness = { "gl_blurbrightness", "0.5", CVAR_AMBIENCE };
cvar_t        gl_blurscale = { "gl_blurscale", "0.2", CVAR_AMBIENCE };

cvar_t        gl_dcontrast = { "gl_dcontrast", "1" };
cvar_t        gl_dcontrastcolor = { "gl_dcontrastcolor", "0.5", CVAR_AMBIENCE };
cvar_t        gl_dcontrastfastsample = { "gl_dcontrastfastsample", "1" };
cvar_t        gl_dcontrastscale = { "gl_dcontrastscale", "0.2", CVAR_AMBIENCE };
cvar_t        gl_dcontrastspeed = { "gl_dcontrastspeed", "0.5", CVAR_AMBIENCE };

cvar_t        gl_blendintensity = { "gl_blendintensity", "0.25", CVAR_AMBIENCE };
cvar_t        gl_blendradius = { "gl_blendradius", "0.25", CVAR_AMBIENCE };

qboolean      OnChange_gl_fogbrightness (cvar_t * var, char *string);
qboolean      OnChange_gl_fogcolor (cvar_t * var, char *string);
cvar_t        gl_fog = { "gl_fog", "0" };
cvar_t        gl_fogbrightness = { "gl_fogbrightness", "0.2", CVAR_AMBIENCE, OnChange_gl_fogbrightness };
cvar_t        gl_fogcolor = { "gl_fogcolor", "0.5", CVAR_AMBIENCE, OnChange_gl_fogcolor };
cvar_t        gl_fogdensity = { "gl_fogdensity", "0.2", CVAR_AMBIENCE};
cvar_t        gl_fogdistance = { "gl_fogdistance", "0.5", CVAR_AMBIENCE };

cvar_t        gl_loadlits = { "gl_loadlits", "0", CVAR_AFTER_CHANGE, OnChange_reload };
cvar_t        gl_loadq3models = { "gl_loadq3models", "0", CVAR_AFTER_CHANGE, OnChange_reload };

cvar_t        gl_overbright = { "gl_overbright", "1", CVAR_AFTER_CHANGE, OnChange_r_shadows };
cvar_t        gl_overbrightlights = { "gl_overbrightlights", "2" };
cvar_t        gl_overbrightmodels = { "gl_overbrightmodels", "1" };        // lxndr: may reduce dynamic lights
cvar_t        gl_overbrightworld = { "gl_overbrightworld", "1", CVAR_AFTER_CHANGE, OnChange_r_shadows };

cvar_t        gl_part_blobs = { "gl_part_blobs", "1" };
cvar_t        gl_part_blood = { "gl_part_blood", "1" };
cvar_t        gl_part_damagesplash = { "gl_part_damagesplash", "0" };
cvar_t        gl_part_explosions = { "gl_part_explosions", "1" };
cvar_t        gl_part_flames = { "gl_part_flames", "1" };
cvar_t        gl_part_gunshots = { "gl_part_gunshots", "1" };
cvar_t        gl_part_lavasplash = { "gl_part_lavasplash", "1" };
cvar_t        gl_part_lightning = { "gl_part_lightning", "1" };
cvar_t        gl_part_spikes = { "gl_part_spikes", "1" };
cvar_t        gl_part_telesplash = { "gl_part_telesplash", "0" };
cvar_t        gl_part_trails = { "gl_part_trails", "1" };
extern qboolean OnChange_gl_part_muzzleflash (cvar_t * var, char *value);
cvar_t        gl_part_muzzleflash = { "gl_part_muzzleflash", "1", CVAR_NONE, OnChange_gl_part_muzzleflash };

cvar_t        gl_warpcaustics = { "gl_warpcaustics", "0.25" };
cvar_t        gl_warpwater = { "gl_warpwater", "2" };
cvar_t        gl_warpalphaskyspeed = { "gl_warpalphaskyspeed", "16" };
cvar_t        gl_warpsolidskyspeed = { "gl_warpsolidskyspeed", "8" };

void          R_MarkLeaves (void);
void          R_InitBubble (void);

/*
 * OnChange_gl_fogcolor
 */
qboolean OnChange_gl_fogcolor (cvar_t * var, char *string) {
    R_CalcRGB (Q_atof (string), gl_fogbrightness.value, (float *) fogcolor);
    fogcolor[3] = 0;
    return false;
}

/*
 * OnChange_gl_fogbrightness
 */
qboolean OnChange_gl_fogbrightness (cvar_t * var, char *string) {
    R_CalcRGB (gl_fogcolor.value, Q_atof (string), (float *) fogcolor);
    fogcolor[3] = 0;
    return false;
}

/*
 * OnChange_gl_vertexarrays
 */
qboolean OnChange_gl_vertexarrays (cvar_t * var, char *string) {
    int           i;
    qboolean      newval = Q_atof (string);
    static qboolean initialized = false;

    if (newval && !initialized) {
        for (i = 0; i < MAX_VERTEXARRAY; i++) {
            vertexarrayf[i] = Hunk_AllocName (MAX_VERTEXSIZE * sizeof (float), "vertexarray");
            vertexarrayi[i] = Hunk_AllocName (MAX_VERTEXSIZE * sizeof (int), "vertexarray");
        }
        initialized = true;
    }

    return false;
}

/*
 * OnChange_r_quality
 */
qboolean OnChange_r_quality (cvar_t * var, char *string) {
    float         value = Q_atof (string);

    R_SetParticleMode (pm_classic);
//    glDisable (GL_POINT_SMOOTH);
//    glDisable (GL_LINE_SMOOTH);
//    glDisable (GL_POLYGON_SMOOTH);

    Cvar_Set (&gl_texturefilter, "GL_NEAREST");
    Cvar_SetValue (&gl_anisotropy, 0);
    Cvar_SetValue (&gl_bloom, 0);
    Cvar_SetValue (&gl_blur, 0);
    Cvar_SetValue (&gl_clear, 0);
    Cvar_SetValue (&gl_dcontrast, 0);
    Cvar_SetValue (&gl_decalsmax, 512);
    Cvar_SetValue (&gl_decalstime, 128);
    Cvar_SetValue (&gl_fog, 0);
    Cvar_SetValue (&gl_overbright, 0);
    Cvar_SetValue (&gl_polyblend, 0);
    Cvar_SetValue (&gl_smokes, 0);
    Cvar_SetValue (&gl_vertexlights, 0);
    Cvar_SetValue (&r_alpha, 0);
    Cvar_SetValue (&r_dynamic, 0);
    Cvar_SetValue (&r_explosiontype, 0);
    Cvar_SetValue (&r_farclip, 2048);
    Cvar_SetValue (&r_shadows, 0);

    if (value > 0.05) {
        Cvar_Set (&gl_texturefilter, "GL_LINEAR");
        Cvar_SetValue (&gl_anisotropy, 2);
        Cvar_SetValue (&gl_decalsmax, 1024);
        Cvar_SetValue (&gl_decalstime, 256);
        Cvar_SetValue (&gl_polyblend, 1);
    }
    if (value > 0.15) {
        Cvar_Set (&gl_texturefilter, "GL_NEAREST_MIPMAP_NEAREST");
        Cvar_SetValue (&r_explosiontype, 4);
    }
    if (value > 0.25) {
        Cvar_Set (&gl_texturefilter, "GL_NEAREST_MIPMAP_LINEAR");
        Cvar_SetValue (&gl_anisotropy, 4);
        Cvar_SetValue (&gl_decalsmax, 2048);
        Cvar_SetValue (&gl_decalstime, 512);
        Cvar_SetValue (&r_farclip, 4096);
    }
    if (value > 0.35) {
        Cvar_Set (&gl_texturefilter, "GL_LINEAR_MIPMAP_NEAREST");
    }
    if (value > 0.45) {
        Cvar_Set (&gl_texturefilter, "GL_LINEAR_MIPMAP_LINEAR");
        Cvar_SetValue (&gl_anisotropy, 8);
        Cvar_SetValue (&gl_decalsmax, 4096);
        Cvar_SetValue (&gl_decalstime, 1024);
        Cvar_SetValue (&gl_dcontrast, 1);
        Cvar_SetValue (&gl_overbright, 1);
        Cvar_SetValue (&gl_vertexlights, 1);
        Cvar_SetValue (&r_alpha, 1);
        Cvar_SetValue (&r_dynamic, 1);
        Cvar_SetValue (&r_farclip, 8192);
        Cvar_SetValue (&r_shadows, 1);
    }
    if (value > 0.55) {
        Cvar_SetValue (&gl_clear, 1);
        Cvar_SetValue (&r_levels, 1);
    } else {
        Cvar_SetValue (&r_levels, 0);
    }
    if (value > 0.65) {
        Cvar_SetValue (&gl_anisotropy, 16);
        Cvar_SetValue (&gl_decalsmax, 8192);
        Cvar_SetValue (&gl_decalstime, 2048);
        Cvar_SetValue (&gl_fog, 1);
    }
    if (value > 0.75) {
        Cvar_SetValue (&gl_smokes, 1);
        R_SetParticleMode (pm_qmb);
    }
    if (value > 0.85) {
        Cvar_SetValue (&gl_anisotropy, 32);
        Cvar_SetValue (&gl_decalsmax, 16384);
        Cvar_SetValue (&gl_decalstime, 4096);
        Cvar_SetValue (&r_farclip, 16384);
    }
    if (value > 0.95) {
        Cvar_SetValue (&gl_bloom, 1);
        Cvar_SetValue (&gl_blur, 1);
/*        glEnable (GL_POINT_SMOOTH);
        glEnable (GL_LINE_SMOOTH);
        if (!gl_nopolysmooth) {   // lxndr: FIXME
            glEnable (GL_POLYGON_SMOOTH);
            glHint (GL_POLYGON_SMOOTH_HINT, GL_NICEST);
        }
        glHint (GL_POINT_SMOOTH_HINT, GL_NICEST);
        glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);*/
#ifdef DEVEL
        long          r, g;
        glGetIntegerv (GL_SMOOTH_LINE_WIDTH_RANGE, &r);
        glGetIntegerv (GL_SMOOTH_LINE_WIDTH_GRANULARITY, &g);
        Con_Printf ("%li %li\n", r, g);
        glLineWidth (2);
#endif
    }

    return false;
}

/*
 * OnChange_r_shadows
 */
qboolean OnChange_r_shadows (cvar_t * var, char *string) {
    R_ResetLightmaps ();

    return false;
}

/*
 * R_CullBox
 *
 * Returns true if the box is completely outside the frustum
 */
inline qboolean R_CullBox (vec3_t mins, vec3_t maxs) {
    int           i;

    for (i = 0; i < 4; i++)
        if (BOX_ON_PLANE_SIDE (mins, maxs, &frustum[i]) == 2)
            return true;

    return false;
}

/*
 * R_CullSphere
 *
 * Returns true if the sphere is completely outside the frustum
 *
 */
inline qboolean R_CullSphere (vec3_t centre, float radius) {
    int           i;
    mplane_t     *p;

    for (i = 0, p = frustum; i < 4; i++, p++)
        if (PlaneDiff (centre, p) <= -radius)
            return true;

    return false;
}

/*
 * R_RotateForEntity
 */
void R_RotateForEntity (entity_t * ent, qboolean shadow) {
    float         lerpfrac, timepassed;
    vec3_t        interpolated;

    // positional interpolation
    timepassed = cl.time - ent->translate_start_time;
    if (ent->translate_start_time == 0 || timepassed > 1) {
        ent->translate_start_time = cl.time;
        VectorCopy (ent->origin, ent->origin1);
        VectorCopy (ent->origin, ent->origin2);
    }
    if (!VectorCompare (ent->origin, ent->origin2)) {
        ent->translate_start_time = cl.time;
        VectorCopy (ent->origin2, ent->origin1);
        VectorCopy (ent->origin, ent->origin2);
        lerpfrac = 0;
    } else {
        lerpfrac = timepassed / 0.1;
        if (cl.paused || lerpfrac > 1)
            lerpfrac = 1;
    }
    VectorInterpolate (ent->origin1, lerpfrac, ent->origin2, interpolated);
    glTranslatef (interpolated[0], interpolated[1], interpolated[2]);

    // orientation interpolation
    timepassed = cl.time - ent->rotate_start_time;
    if (ent->rotate_start_time == 0 || timepassed > 1) {
        ent->rotate_start_time = cl.time;
        VectorCopy (ent->angles, ent->angles1);
        VectorCopy (ent->angles, ent->angles2);
    }
    if (!VectorCompare (ent->angles, ent->angles2)) {
        ent->rotate_start_time = cl.time;
        VectorCopy (ent->angles2, ent->angles1);
        VectorCopy (ent->angles, ent->angles2);
        lerpfrac = 0;
    } else {
        lerpfrac = timepassed / 0.1;
        if (cl.paused || lerpfrac > 1)
            lerpfrac = 1;
    }
    VectorInterpolate (ent->angles1, lerpfrac, ent->angles2, interpolated);
    glRotatef (interpolated[1], 0, 0, 1);
    if (!shadow) {
        glRotatef (-interpolated[0], 0, 1, 0);
        glRotatef (interpolated[2], 1, 0, 0);
    }
}

/*
 * R_RotateForViewEntity
 */
void R_RotateForViewEntity (entity_t * ent) {
    glTranslatef (ent->origin[0], ent->origin[1], ent->origin[2]);
    glRotatef (ent->angles[1], 0, 0, 1);
    glRotatef (-ent->angles[0], 0, 1, 0);
    glRotatef (ent->angles[2], 1, 0, 0);
}

/*
 *
 *
 * SPRITE MODELS
 *
 *
 */

/*
 * R_GetSpriteFrame
 */
mspriteframe_t *R_GetSpriteFrame (entity_t * currententity) {
    int           i, numframes, frame;
    float        *pintervals, fullinterval, targettime, time;
    msprite_t    *psprite;
    mspritegroup_t *pspritegroup;
    mspriteframe_t *pspriteframe;

    psprite = currententity->model->cache.data;
    frame = currententity->frame;

    if ((frame >= psprite->numframes) || (frame < 0)) {
        Con_DPrintf ("R_DrawSprite: no such frame %d\n", frame);
        frame = 0;
    }

    if (psprite->frames[frame].type == SPR_SINGLE) {
        pspriteframe = psprite->frames[frame].frameptr;
    } else {
        pspritegroup = (mspritegroup_t *) psprite->frames[frame].frameptr;
        pintervals = pspritegroup->intervals;
        numframes = pspritegroup->numframes;
        fullinterval = pintervals[numframes - 1];

        time = cl.time + currententity->syncbase;

        // when loading in Mod_LoadSpriteGroup, we guaranteed all interval values
        // are positive, so we don't have to worry about division by 0
        targettime = time - ((int) (time / fullinterval)) * fullinterval;

        for (i = 0; i < (numframes - 1); i++) {
            if (pintervals[i] > targettime)
                break;
        }

        pspriteframe = pspritegroup->frames[i];
    }

    return pspriteframe;
}

/*
 * R_DrawSpriteModel
 */
void R_DrawSpriteModel (entity_t * ent) {
    vec3_t        point, right, up;
    mspriteframe_t *frame;
    msprite_t    *psprite;

    // don't even bother culling, because it's just a single polygon without a surface cache
    frame = R_GetSpriteFrame (ent);
    psprite = currententity->model->cache.data;

    if (psprite->type == SPR_ORIENTED) {
        // bullet marks on walls
        AngleVectors (currententity->angles, NULL, right, up);
    } else if (psprite->type == SPR_FACING_UPRIGHT) {
        VectorSet (up, 0, 0, 1);
        right[0] = ent->origin[1] - r_origin[1];
        right[1] = -(ent->origin[0] - r_origin[0]);
        right[2] = 0;
        VectorNormalize (right);
    } else if (psprite->type == SPR_VP_PARALLEL_UPRIGHT) {
        VectorSet (up, 0, 0, 1);
        VectorCopy (vright, right);
    } else {                           // normal sprite
        VectorCopy (vup, up);
        VectorCopy (vright, right);
    }

    GL_Bind (frame->gl_texturenum);

    glBegin (GL_QUADS);

    glTexCoord2f (0, 1);
    VectorMA (ent->origin, frame->down, up, point);
    VectorMA (point, frame->left, right, point);
    glVertex3fv (point);

    glTexCoord2f (0, 0);
    VectorMA (ent->origin, frame->up, up, point);
    VectorMA (point, frame->left, right, point);
    glVertex3fv (point);

    glTexCoord2f (1, 0);
    VectorMA (ent->origin, frame->up, up, point);
    VectorMA (point, frame->right, right, point);
    glVertex3fv (point);

    glTexCoord2f (1, 1);
    VectorMA (ent->origin, frame->down, up, point);
    VectorMA (point, frame->right, right, point);
    glVertex3fv (point);

    glEnd ();
}

//

/*
 * R_SetupLighting
 */
void R_SetupLighting (entity_t * ent) {
    int           i, lnum;
    float         add;
    vec3_t        dist, dlight_color;
    model_t      *clmodel = ent->model;

    if (clmodel->modhint == MOD_THUNDERBOLT || clmodel->modhint == MOD_LASER || clmodel->modhint == MOD_LAVABALL || clmodel->modhint == MOD_SPIKE) {
        ambientlight = 200;
        shadelight = 0;
        full_light = ent->noshadow = true;
        return;
    } else if (clmodel->modhint == MOD_FLAME) {
        ambientlight = 200;
        shadelight = 0;
        full_light = ent->noshadow = true;
        ent->istransparent = true;
        ent->transparency = r_drawflames.value;
        return;
    } else if (clmodel->modhint == MOD_Q3GUNSHOT || clmodel->modhint == MOD_Q3TELEPORT) {
        ambientlight = 128;
        shadelight = 0;
        full_light = ent->noshadow = true;
        return;
    } else if (r_fullbrightkeys.value && clmodel->modhint == MOD_KEY) {
        ambientlight = 200;
        shadelight = 0;
        full_light = true;
        ent->noshadow = false;
        return;
    } else if (r_fullbrightskins.value && (clmodel->modhint == MOD_MONSTER || clmodel->modhint == MOD_PLAYER)) {
        ambientlight = shadelight = 128;
        full_light = true;
        ent->noshadow = false;
        return;
    } else {
        ambientlight = shadelight = R_LightPoint (ent->origin);
        full_light = false;
        ent->noshadow = (ent == &cl.viewent);
    }

    for (lnum = 0; lnum < MAX_DLIGHTS; lnum++) {
        if (cl_dlights[lnum].die < cl.time || !cl_dlights[lnum].radius)
            continue;

        VectorSubtract (ent->origin, cl_dlights[lnum].origin, dist);
        add = cl_dlights[lnum].radius - VectorLength (dist);
        if (add > 0) {
            if (r_dynamic.value) { // joe: only allow colorlight affection if dynamic lights are on
                VectorCopy (dlightcolor[cl_dlights[lnum].type], dlight_color);
                for (i = 0; i < 3; i++) {
                    lightcolor[i] += dlight_color[i] * add;
                    if (lightcolor[i] > 256) {
                        lightcolor[i] = 256;
                        switch (i) {
                            case 0:
                                lightcolor[1] /= 1.5;
                                lightcolor[2] /= 1.5;
                                break;

                            case 1:
                                lightcolor[0] /= 1.5;
                                lightcolor[2] /= 1.5;
                                break;

                            case 2:
                                lightcolor[1] /= 1.5;
                                lightcolor[0] /= 1.5;
                                break;
                        }
                    }
                }
            } else {
                ambientlight += add;
                shadelight += add;
            }
        }
    }

    // calculate pitch and yaw for vertex lighting
    if (gl_vertexlights.value) {
        apitch = ent->angles[0];
        ayaw = ent->angles[1];
    }

    // clamp lighting
    ambientlight = bound (8, ambientlight, 128);
    if (ambientlight + shadelight > 192)
        shadelight = 192 - ambientlight;
}

// 

int SetFlameModelState (void) {
    if (!gl_part_flames.value && !strcmp (currententity->model->name, "progs/flame0.mdl")) {
        currententity->model = cl.model_precache[cl_modelindex[mi_flame1]];
    } else if (gl_part_flames.value) {
        vec3_t        liteorg;

        VectorCopy (currententity->origin, liteorg);
        if (currententity->baseline.modelindex == cl_modelindex[mi_flame0]) {
            if (gl_part_flames.value == 2) {
                liteorg[2] += 14;
                QMB_Q3TorchFlame (liteorg, 15);
            } else {
                liteorg[2] += 5.5;
                QMB_TorchFlame (liteorg, 7, 0.8);
            }
        } else if (currententity->baseline.modelindex == cl_modelindex[mi_flame0_md3]) {
            if (gl_part_flames.value == 2) {
                liteorg[2] += 12;
                QMB_Q3TorchFlame (liteorg, 15);
            } else {
                liteorg[2] += 3.5;
                QMB_TorchFlame (liteorg, 7, 0.8);
            }
        } else if (currententity->baseline.modelindex == cl_modelindex[mi_flame1]) {
            if (gl_part_flames.value == 2) {
                liteorg[2] += 14;
                QMB_Q3TorchFlame (liteorg, 15);
            } else {
                liteorg[2] += 5.5;
                QMB_TorchFlame (liteorg, 7, 0.8);
            }
            currententity->model = cl.model_precache[cl_modelindex[mi_flame0]];
        } else if (currententity->baseline.modelindex == cl_modelindex[mi_flame2]) {
            if (gl_part_flames.value == 2) {
                liteorg[2] += 14;
                QMB_Q3TorchFlame (liteorg, 32);
            } else {
                liteorg[2] -= 1;
                QMB_TorchFlame (liteorg, 12, 1);
            }
            return -1;                 // continue;
        }
    }

    return 0;
}

void SortEntitiesByTransparency (void) {
    int           i, j;
    entity_t     *tmp;

    for (i = 0; i < cl_numvisedicts; i++) {
        if (cl_visedicts[i]->istransparent) {
            for (j = cl_numvisedicts - 1; j > i; j--) {
                // if not transparent, exchange with transparent
                if (!(cl_visedicts[j]->istransparent)) {
                    tmp = cl_visedicts[i];
                    cl_visedicts[i] = cl_visedicts[j];
                    cl_visedicts[j] = tmp;
                    break;
                }
            }
            if (j == i)
                return;
        }
    }
}

/*
 * R_DrawEntities
 */
void R_DrawEntities (void) {
    int           i;

    SortEntitiesByTransparency ();

    // draw sprites seperately, because of alpha blending
    for (i = 0; i < cl_numvisedicts; i++) {
        currententity = cl_visedicts[i];

        if (qmb_initialized && SetFlameModelState () == -1)
            continue;

        switch (currententity->model->type) {
            case mod_md1:
                if (r_drawaliases.value)
                    R_DrawQ1Model (currententity);
                break;

            case mod_md3:
                if (r_drawaliases.value)
                    R_DrawQ3Model (currententity);
                break;

            case mod_brush:
                if (r_drawbrushes.value)
                    R_DrawBrushModel (currententity);
                break;

            default:
                break;
        }
    }

    GL_DisableMultitexture ();

    glEnable (GL_ALPHA_TEST);
    glEnable (GL_BLEND);

    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthMask (GL_FALSE);
    for (i = 0; i < cl_numvisedicts; i++) {
        currententity = cl_visedicts[i];

        switch (currententity->model->type) {
            case mod_sprite:
                if (r_drawsprites.value)
                    R_DrawSpriteModel (currententity);
                break;

            default:
                break;
        }
    }
    glDepthMask (GL_TRUE);

    glDisable (GL_ALPHA_TEST);
    glDisable (GL_BLEND);
}

/*
 * R_DrawViewModel
 */
void R_DrawViewModel (void) {
    currententity = &cl.viewent;

    if (!r_drawweapon.value || chase_active.value || (cl.stats[STAT_HEALTH] <= 0) || !currententity->model)
        return;

    currententity->transparency = (cl.items & IT_INVISIBILITY) ? gl_ringalpha.value : bound (0, r_drawweapon.value, 1);
    currententity->istransparent = true;

    // hack the depth range to prevent view model from poking into walls
    glDepthRange (gldepthmin, gldepthmin + 0.3 * (gldepthmax - gldepthmin));

    switch (currententity->model->type) {
        case mod_md1:
            R_DrawQ1Model (currententity);
            break;

        case mod_md3:
            R_DrawQ3Model (currententity);
            break;

        default:
            break;
    }

    glDepthRange (gldepthmin, gldepthmax);
}

inline void GL_ReadPBO (int x, int y, int width, int height, int channel) {
    static int    current = 0, old = 0;
    int           size, value;
    unsigned     *buffer;

    if (current == 0) {
        current = 1;
        old = 0;
    } else {
        current = 0;
        old = 1;
    }
    size = width * height * channel;

    glGetError (); // lxndr: clear previous errors
    qglBindBuffer (GL_PIXEL_PACK_BUFFER_ARB, pboIds[current]);
    qglGetBufferParameteriv (GL_PIXEL_PACK_BUFFER_ARB, GL_BUFFER_MAPPED, &value);
    if (glGetError () != GL_NO_ERROR) {
        Con_Debugf (0, "PBO buffer is already mapped (%d, %d)\n", size, value);
    } else {
        glReadPixels (x, y, width, height, channel == 1 ? GL_LUMINANCE : GL_RGB, GL_UNSIGNED_BYTE, 0);
        if (glGetError () != GL_NO_ERROR)
            Con_Debugf (0, "Couldn't read pixels (%d, %d)\n", size, value);
    }

    qglBindBuffer (GL_PIXEL_PACK_BUFFER_ARB, pboIds[old]);
    qglGetBufferParameteriv (GL_PIXEL_PACK_BUFFER_ARB, GL_BUFFER_MAPPED, &value);
    if (glGetError () != GL_NO_ERROR) {
        Con_Debugf (0, "PBO buffer is already mapped (%d, %d)\n", size, value);
        return;
    }
    qglGetBufferParameteriv (GL_PIXEL_PACK_BUFFER_ARB, GL_BUFFER_SIZE, &value);
    if (glGetError () != GL_NO_ERROR) {
        Con_Debugf (0, "Couldn't retrieve PBO buffer size in read mode (%d, %d)\n", size, value);
        return;
    }

    while (1) {
        if (size > value)
            qglBufferData (GL_PIXEL_PACK_BUFFER_ARB, size, 0, GL_STREAM_READ_ARB);
        else
            qglBufferSubData (GL_PIXEL_PACK_BUFFER_ARB, 0, size, 0);
        if (glGetError () != GL_NO_ERROR) {
            Con_Debugf (0, "Couldn't allocate PBO buffer in read mode (%d, %d), retrying...\n", size, value);
            continue;
        }
        break;
    }

    buffer = qglMapBuffer (GL_PIXEL_PACK_BUFFER_ARB, GL_READ_ONLY);
    if (buffer) {
        memcpy (vid.buffer, buffer, size);
        qglUnmapBuffer (GL_PIXEL_PACK_BUFFER_ARB);
    } else {
        Con_Debugf (0, "Couldn't map PBO buffer in read mode (%d, %d)\n", size, value);
    }

    qglBindBuffer (GL_PIXEL_PACK_BUFFER_ARB, 0);
}

void R_ClearLuminance (float clear) {
    int           i;

    for (i = 0; i < LUM_HIST_SIZE; i++)
        v_lumhist[i] = clear;
}

void R_ClearLuminance_f (void) {
    float         f = atof (Cmd_Argv (1));

    R_ClearLuminance (f);
}

void R_CalcLuminance (void) {
    int           i, size;
    int           w = glwidth, h = glheight;
    double        sum, histdepth;
    static double lastrealtime;

    if (!gl_dcontrast.value)
        return;

    if (key_dest != key_game)
        return;

    if (gl_dcontrastfastsample.value)
        h /= (10 * gl_dcontrastfastsample.value);
    size = w * h;

    if (gl_dcontrastfastsample.value) {
        if (gl_ext.pixel_bufferobject && gl_pbo.value)
            GL_ReadPBO (glx, gly + glheight / 2 - h / 2, w, h, 1);
        else
            glReadPixels (glx, gly + glheight / 2 - h / 2, w, h, GL_LUMINANCE, GL_UNSIGNED_BYTE, vid.buffer);

        for (i = 0; i < size; i++)
            sum += vid.buffer[i];
        sum /= 50.0;                   // lxndr: for having results compatibles with the GL_RGB method below
    } else {
        if (gl_ext.pixel_bufferobject && gl_pbo.value)
            GL_ReadPBO (glx, gly, w, h, 3);
        else
            glReadPixels (glx, gly, w, h, GL_RGB, GL_UNSIGNED_BYTE, vid.buffer);

        for (i = 0; i < size; i += 3)
            sum += vid.buffer[i + 0] * SAT_WEIGHT_RED + vid.buffer[i + 1] * SAT_WEIGHT_GREEN + vid.buffer[i + 2] * SAT_WEIGHT_BLUE;
        sum /= 5.0;
    }
    v_lumhist[0] = sum / size;

    if (realtime - lastrealtime > 0.1) {
        lastrealtime = realtime;
        for (i = LUM_HIST_SIZE - 1; i > 0; i--)
            v_lumhist[i] = v_lumhist[i - 1];
    }

    sum = 0;
    histdepth = LUM_HIST_SIZE * bound (0.01, 1 - gl_dcontrastspeed.value, 1);
    for (i = 0; i < histdepth; i++)
        sum += v_lumhist[i];
    v_lummean = sum / histdepth;
}

void R_CalcRGB (float color, float intensity, float *rgb) {
    float         c, i;

    if (color == 0.5) {
        rgb[0] = rgb[1] = rgb[2] = intensity;
    } else {
        c = (color * 0.8 + 0.1) * M_PI;
        i = intensity * 1.1 - 1;
        rgb[0] = Q_cos (c / 2) + i;
        rgb[1] = Q_sin (c) + i;
        rgb[2] = Q_sin (c / 2) + i;
    }
}

void R_CalcScreenTextureSize (int *width, int *height) {
    if (gl_ext.tex_nonpoweroftwo) {    //we can use any size, supposedly
        *width = glwidth;
        *height = glheight;
    } else {                           //limit the texture size to square and use padding.
        for (*width = 1; *width < glwidth; *width *= 2);
        for (*height = 1; *height < glheight; *height *= 2);
    }
}

/*
 * R_RenderBlur
 *
 */
void R_RenderBlur (void) {
    if (gl_blur.value) {
        int           vwidth = 1, vheight = 1;
        float         vs, vt, cs, ct;
        float         c, r, hscale = 0, sscale = 0, zscale = 0;
        vec3_t        dir;
        extern float  zoom;

        R_CalcScreenTextureSize (&vwidth, &vheight);

        // blend the last frame onto the scene
        // the maths is because our texture is over-sized (must be power of two)
        cs = vs = (float) glwidth / vwidth * 0.5;
        ct = vt = (float) glheight / vheight * 0.5;

        if (gl_blurscale.value) {
            AngleVectors (cl_entities[cl.viewentity].angles, dir, NULL, NULL);
            r = DotProduct (cl.velocity, dir) < 0 ? -1 : 1;
            sscale = VectorLength (cl.velocity) / sv_maxvelocity.value * 10 * r;

            if (sv_healthimpact.value)
                hscale = (healthscale - 1) * 0.5;

            if (in_zoom.state & 1)
                zscale = logf (zoom + 1) * 0.5;

            r = 0.999 - (hscale + sscale + zscale) * gl_blurscale.value / 100.0;
        } else {
            r = 1;
        }
        vs *= r;
        vt *= r;

        if (sv_healthimpact.value)
            r = sqrt (hscale * (gl_bluralpha.value + 1));
        else
            r = gl_bluralpha.value / 2.0;       // lxndr: reduced effect
        r += ((v_fpshist[0] + v_fpshist[1]) / 2.0 - DEFAULT_FPS) / (2 * DEFAULT_FPS);

        if (gl_blurblood.value) {
            c = gl_blurbrightness.value / 2.0;
            glColor4f (0.5 + c, 0, 0, bound (0, r, 1));
        } else {
            c = 0.5 + gl_blurbrightness.value / 2.0;
            glColor4f (c, c, c, bound (0, r, 1));
        }

        GL_Bind (blurtexture);

        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho (glx, glwidth, gly, glheight, NEARVAL, FARVAL);
        glMatrixMode (GL_MODELVIEW);
        glLoadIdentity ();

        glEnable (GL_BLEND);
        glEnable (GL_TEXTURE_2D);

        glBegin (GL_QUADS);
        glTexCoord2f (cs - vs, ct - vt);
        glVertex2f (0, 0);
        glTexCoord2f (cs + vs, ct - vt);
        glVertex2f (glwidth, 0);
        glTexCoord2f (cs + vs, ct + vt);
        glVertex2f (glwidth, glheight);
        glTexCoord2f (cs - vs, ct + vt);
        glVertex2f (0, glheight);
        glEnd ();

        glDisable (GL_BLEND);
        glDisable (GL_TEXTURE_2D);

        glMatrixMode (GL_PROJECTION);
        glLoadIdentity ();
        glOrtho (glx, glwidth, glheight, gly, NEARVAL, FARVAL);
        glMatrixMode (GL_MODELVIEW);
        glLoadIdentity ();

        //copy the image into the texture so that we can play with it next frame too!
        glCopyTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, 0, 0, vwidth, vheight, 0);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
}

inline void R_ClearFogColor (void) {
    float         color[4] = { 0, 0, 0, 0 };

    if (gl_fog.value)
        glFogfv (GL_FOG_COLOR, color);
}

inline void R_ResetFogColor (void) {
    if (gl_fog.value)
        glFogfv (GL_FOG_COLOR, fogcolor);
}

#ifdef DEVEL
/*
 * R_AddWaterFog
 *
 * Fog in liquids, from FuhQuake
 */
void R_AddWaterFog (int contents) {
    float        *colors;
    float         lava[4] = { 1.0f, 0.314f, 0.0f, 0.5f };
    float         slime[4] = { 0.039f, 0.738f, 0.333f, 0.5f };
    float         water[4] = { 0.039f, 0.584f, 0.888f, 0.5f };

    if (!gl_fog.value || contents == CONTENTS_EMPTY || contents == CONTENTS_SOLID) {
        glDisable (GL_FOG);
        return;
    }

    switch (contents) {
        case CONTENTS_LAVA:
            colors = lava;
            break;
        case CONTENTS_SLIME:
            colors = slime;
            break;
        default:
            colors = water;
            break;
    }

    glFogfv (GL_FOG_COLOR, colors);
    if (((int) gl_fog.value) == 2) {
        glFogf (GL_FOG_DENSITY, 0.0002 + (0.0009 - 0.0002) * gl_fogdensity.value);
        glFogi (GL_FOG_MODE, GL_EXP);
    } else {
        glFogi (GL_FOG_MODE, GL_LINEAR);
        glFogf (GL_FOG_START, 150.0f);
        glFogf (GL_FOG_END, 4250.0f - (4250.0f - 1536.0f) * gl_fogdensity.value);
    }
    glEnable (GL_FOG);
}
#endif

/*
 * R_SetupFog
 */
void R_SetupFog (void) {
    float         density, distance;

    if (gl_fog.value) {
        distance = 2048 * (0.5 + gl_fogdistance.value);
        density = 65536 / powf (2, gl_fogdensity.value * 10);
        // Con_Printf ("density=%f distance=%f\n", density, distance);

        glFogfv (GL_FOG_COLOR, fogcolor);
        glFogi (GL_FOG_MODE, GL_LINEAR);
        glFogf (GL_FOG_START, distance - 2048);
        glFogf (GL_FOG_END, distance + density);

        glEnable (GL_FOG);
    } else {
        glDisable (GL_FOG);
    }
}

/*
 * R_DynamicContrast
 */
void R_DynamicContrast (void) {
    float         l;
    float         rgb[3];

    if (!gl_dcontrast.value)
        return;

    l = 0.5 + v_gamma.value;
    l = bound (-gl_dcontrastscale.value, (v_lummean - l) / l, gl_dcontrastscale.value / 5);

#ifdef DEBUG
    Com_Debugf ("%f %f %f", v_lumhist[0], v_lummean, l);
#endif

    if (l > 0)
        return;

    R_CalcRGB (gl_dcontrastcolor.value, 1, (float *) rgb);

    glEnable (GL_BLEND);

    glBlendFunc (GL_DST_COLOR, GL_ONE);
    glColor3f (-l * rgb[0], -l * rgb[1], -l * rgb[2]);

    glBegin (GL_QUADS);
    glVertex2f (r_refdef.vrect.x, r_refdef.vrect.y);
    glVertex2f (r_refdef.vrect.x + r_refdef.vrect.width, r_refdef.vrect.y);
    glVertex2f (r_refdef.vrect.x + r_refdef.vrect.width, r_refdef.vrect.y + r_refdef.vrect.height);
    glVertex2f (r_refdef.vrect.x, r_refdef.vrect.y + r_refdef.vrect.height);
    glEnd ();

    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor3ubv (color_white);

    glDisable (GL_BLEND);
}

/*
 * R_PolyBlend
 */
void R_PolyBlend (void) {
    if (!v_blend[3])
        return;

    glEnable (GL_BLEND);

    glColor4fv (v_blend);

    glBegin (GL_QUADS);
    glVertex2f (r_refdef.vrect.x, r_refdef.vrect.y);
    glVertex2f (r_refdef.vrect.x + r_refdef.vrect.width, r_refdef.vrect.y);
    glVertex2f (r_refdef.vrect.x + r_refdef.vrect.width, r_refdef.vrect.y + r_refdef.vrect.height);
    glVertex2f (r_refdef.vrect.x, r_refdef.vrect.y + r_refdef.vrect.height);
    glEnd ();

    glColor3ubv (color_white);

    glDisable (GL_BLEND);
}

/*
 * R_BrightenScreen
 */
void R_BrightenScreen (void) {
    float         f;
    extern float  vid_gamma;

    if (vid_hwgamma_enabled || v_contrast.value <= 1.0)
        return;

    f = min (v_contrast.value, 4);
    f = pow (f, vid_gamma);

    glEnable (GL_BLEND);

    glBlendFunc (GL_DST_COLOR, GL_ONE);

    glBegin (GL_QUADS);
    while (f > 1) {
        if (f >= 2)
            glColor3ubv (color_white);
        else
            glColor3f (f - 1, f - 1, f - 1);
        glVertex2f (0, 0);
        glVertex2f (vid.width, 0);
        glVertex2f (vid.width, vid.height);
        glVertex2f (0, vid.height);
        f *= 0.5;
    }
    glEnd ();

    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor3ubv (color_white);

    glDisable (GL_BLEND);
}

/*
 * R_Q3Damage
 */
void R_Q3Damage (void) {
    float         scale, halfwidth, halfheight;
    vec3_t        center;
    extern float  damagetime, damagecount;

    if (!qmb_initialized || !gl_part_damagesplash.value || damagetime + 0.5 < cl.time) {
        return;
    } else if (damagetime > cl.time) {
        damagetime = 0;
        return;
    }

    GL_Bind (damagetexture);

    glEnable (GL_BLEND);

    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glPushMatrix ();

    center[0] = vid.width / 2;
    center[1] = vid.height / 2;
    scale = 1 + damagecount / 100;
    halfwidth = vid.width * scale / 2;
    halfheight = vid.height * scale / 2;
    glColor4f (1, 1, 1, 0.8 * (damagetime + 0.5 - cl.time));

    glBegin (GL_QUADS);
    glTexCoord2f (0, 0);
    glVertex2f (center[0] - halfwidth, center[1] - halfheight);
    glTexCoord2f (0, 1);
    glVertex2f (center[0] + halfwidth, center[1] - halfheight);
    glTexCoord2f (1, 1);
    glVertex2f (center[0] + halfwidth, center[1] + halfheight);
    glTexCoord2f (1, 0);
    glVertex2f (center[0] - halfwidth, center[1] + halfheight);
    glEnd ();

    glPopMatrix ();

    glColor3ubv (color_white);

    glDisable (GL_BLEND);
}

static int SignbitsForPlane (mplane_t * out) {
    int           bits, j;

    // for fast box on planeside test
    bits = 0;
    for (j = 0; j < 3; j++) {
        if (out->normal[j] < 0)
            bits |= 1 << j;
    }

    return bits;
}

/*
 * R_SetupFrame
 */
static void R_SetupFrame (void) {
    vec3_t        testorigin;
    mleaf_t      *leaf;

    r_framecount++;

    // build the transformation matrix for the given view angles
    VectorCopy (r_refdef.vieworg, r_origin);
    AngleVectors (r_refdef.viewangles, vpn, vright, vup);

    // current viewleaf
    r_oldviewleaf = r_viewleaf;
    r_oldviewleaf2 = r_viewleaf2;

    r_viewleaf = Mod_PointInLeaf (r_origin, cl.worldmodel);
    r_viewleaf2 = NULL;

    // check above and below so crossing solid water doesn't draw wrong
    if (r_viewleaf->contents <= CONTENTS_WATER && r_viewleaf->contents >= CONTENTS_LAVA) { // look up a bit
        VectorCopy (r_origin, testorigin);
        testorigin[2] += 16;
        leaf = Mod_PointInLeaf (testorigin, cl.worldmodel);
        if (leaf->contents == CONTENTS_EMPTY)
            r_viewleaf2 = leaf;
    } else if (r_viewleaf->contents == CONTENTS_EMPTY) { // look down a bit
        VectorCopy (r_origin, testorigin);
        testorigin[2] -= 16;
        leaf = Mod_PointInLeaf (testorigin, cl.worldmodel);
        if (leaf->contents <= CONTENTS_WATER && leaf->contents >= CONTENTS_LAVA)
            r_viewleaf2 = leaf;
    }
    V_SetContentsColor (r_viewleaf->contents);
#ifdef DEVEL
    R_AddWaterFog (r_viewleaf->contents);
#endif
    R_SetupFog ();

    if (nehahra)
        Neh_SetupFrame ();

    r_cache_thrash = false;
}

static void R_SetFrustum (void) {
    int           i;

    for (i = 0; i < 4; i++) {
        frustum[i].type = PLANE_ANYZ;
        frustum[i].dist = DotProduct (r_origin, frustum[i].normal);
        frustum[i].signbits = SignbitsForPlane (&frustum[i]);
    }
}

static void MYgluPerspective (GLdouble fovy, GLdouble aspect, GLdouble zNear, GLdouble zFar) {
    GLdouble      xmin, xmax, ymin, ymax;
    extern cvar_t v_ratiocorrection;

    ymax = zNear * Q_tan (fovy * M_PI / 360.0);
    if (v_ratiocorrection.value == 1)
        ymax *= aspect * 0.75;
    ymin = -ymax;

    xmin = ymin * aspect;
    xmax = ymax * aspect;

    glFrustum (xmin, xmax, ymin, ymax, zNear, zFar);
}

static void GL_AccFrustum (GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble near, GLdouble far, GLdouble pixdx, GLdouble pixdy, GLdouble eyedx, GLdouble eyedy, GLdouble focus) {
    GLdouble xwsize, ywsize, dx, dy;
    GLint viewport[4];

    glGetIntegerv (GL_VIEWPORT, viewport);

    xwsize = right - left;
    ywsize = top - bottom;
    dx = -(pixdx*xwsize/(GLdouble) viewport[2] + eyedx*near/focus);
    dy = -(pixdy*ywsize/(GLdouble) viewport[3] + eyedy*near/focus);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum (left + dx, right + dx, bottom + dy, top + dy, near, far);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef (-eyedx, -eyedy, 0.0);
}

static void GL_AccPerspective (GLdouble fovy, GLdouble aspect, GLdouble near, GLdouble far, GLdouble pixdx, GLdouble pixdy, GLdouble eyedx, GLdouble eyedy, GLdouble focus) {
    GLdouble fov2,left,right,bottom,top;

    fov2 = ((fovy * M_PI) / 180.0) / 2.0;
    top = near / (Q_cos (fov2) / Q_sin (fov2));
    bottom = -top;
    right = top * aspect;
    left = -right;

    GL_AccFrustum (left, right, bottom, top, near, far, pixdx, pixdy, eyedx, eyedy, focus);
}

/*
 * R_SetupGL
 */
void R_SetupGL (void) {
    float         screenaspect;
    int           x, x2, y2, y, w, h, farclip;

    screenaspect = (float) r_refdef.vrect.width / r_refdef.vrect.height;
    farclip = max ((int) r_farclip.value, 4096);

    x = r_refdef.vrect.x * glwidth / vid.width;
    x2 = (r_refdef.vrect.x + r_refdef.vrect.width) * glwidth / vid.width;
    y = (vid.height - r_refdef.vrect.y) * glheight / vid.height;
    y2 = (vid.height - (r_refdef.vrect.y + r_refdef.vrect.height)) * glheight / vid.height;

    w = x2 - x;
    h = y - y2;

    // set up viewpoint
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glViewport (glx + x, gly + y2, w, h);
    MYgluPerspective (r_refdef.fov_y, screenaspect, 1, farclip);        // lxndr: was 4
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();

//    glClear(GL_ACCUM_BUFFER_BIT);
//    GL_AccPerspective (r_refdef.fov_y, screenaspect, 1, farclip, glx+x, gly+y2, 0, 0, 0.1);
//    GL_AccPerspective (r_refdef.fov_y, screenaspect, 1, farclip, glx+x, gly+y2, 0, 0, 0.1);
//    GL_AccPerspective (r_refdef.fov_y, screenaspect, 1, farclip, glx+x2, gly+y2, 0, 0, 0.1);
//    GL_AccPerspective (r_refdef.fov_y, screenaspect, 1, farclip, glx+x2, gly+y, 0, 0, 0.1);
//    glAccum(GL_ACCUM, 4);

    glRotatef (-90, 1, 0, 0);          // put Z going up
    glRotatef (90, 0, 0, 1);           // put Z going up
    glRotatef (-r_refdef.viewangles[2], 1, 0, 0);
    glRotatef (-r_refdef.viewangles[0], 0, 1, 0);
    glRotatef (-r_refdef.viewangles[1], 0, 0, 1);
    glTranslatef (-r_refdef.vieworg[0], -r_refdef.vieworg[1], -r_refdef.vieworg[2]);

    glGetFloatv (GL_MODELVIEW_MATRIX, r_world_matrix);

    // set drawing parms
    glDisable (GL_ALPHA_TEST);
    glDisable (GL_BLEND);
    if (gl_cull.value)
        glEnable (GL_CULL_FACE);
    else
        glDisable (GL_CULL_FACE);
    glEnable (GL_DEPTH_TEST);
    glEnable (GL_TEXTURE_2D);
}

/*
 * R_Init
 */
void R_Init (void) {
    Cvar_Register (&r_farclip);
    Cvar_Register (&r_novis);
    Cvar_Register (&r_skycolor);
    Cvar_Register (&r_skybox);
    Cvar_Register (&r_speeds);

    Cvar_Register (&r_alpha);
    Cvar_Register (&r_alphalava);
    Cvar_Register (&r_alphaslime);
    Cvar_Register (&r_alphateleport);
    Cvar_Register (&r_alphawater);

    Cvar_Register (&r_colorlava);
    Cvar_Register (&r_colorslime);
    Cvar_Register (&r_colorteleport);
    Cvar_Register (&r_colorwater);

    Cvar_Register (&r_drawaliases);
    Cvar_Register (&r_drawbrushes);
    Cvar_Register (&r_drawflames);
    Cvar_Register (&r_drawsprites);
    Cvar_Register (&r_drawsky);
    Cvar_Register (&r_drawtris);
    Cvar_Register (&r_drawweapon);
    Cvar_Register (&r_drawworld);

    Cvar_Register (&r_dynamic);
    Cvar_Register (&r_dynamicintensity);
    Cvar_Register (&r_dynamicradius);

    Cvar_Register (&r_fullbright);
    Cvar_Register (&r_fullbrightkeys);
    Cvar_Register (&r_fullbrightskins);

    Cvar_Register (&r_quality);

    Cvar_Register (&r_shadows);
    Cvar_Register (&r_shadowsintensity);
    Cvar_Register (&r_shadowsmax);
    Cvar_Register (&r_shadowsmin);
    Cvar_Register (&r_shadowsmodels);
    Cvar_Register (&r_shadowssize);
    Cvar_Register (&r_shadowsworld);

    Cvar_Register (&gl_affinemodels);
    Cvar_Register (&gl_caustics);
    Cvar_Register (&gl_clear);
    Cvar_Register (&gl_cull);
    Cvar_Register (&gl_detail);
    Cvar_Register (&gl_doubleeyes);
    Cvar_Register (&gl_finish);
    Cvar_Register (&gl_interdist);
    Cvar_Register (&gl_lerptextures);
    Cvar_Register (&gl_loadlits);
    Cvar_Register (&gl_loadq3models);
    Cvar_Register (&gl_nocolors);
    Cvar_Register (&gl_playermip);
    Cvar_Register (&gl_polyblend);
    Cvar_Register (&gl_ringalpha);
    Cvar_Register (&gl_smoothmodels);
    Cvar_Register (&gl_solidparticles);
    Cvar_Register (&gl_vertexarrays);
    Cvar_Register (&gl_vertexlights);
    Cvar_Register (&gl_ztrick);

    Cvar_Register (&gl_anisotropy);

    Cvar_Register (&gl_blendintensity);
    Cvar_Register (&gl_blendradius);

    Cvar_Register (&gl_blur);
    Cvar_Register (&gl_bluralpha);
    Cvar_Register (&gl_blurblood);
    Cvar_Register (&gl_blurbrightness);
    Cvar_Register (&gl_blurscale);

    Cvar_Register (&gl_dcontrast);
    Cvar_Register (&gl_dcontrastcolor);
    Cvar_Register (&gl_dcontrastfastsample);
    Cvar_Register (&gl_dcontrastscale);
    Cvar_Register (&gl_dcontrastspeed);

    Cvar_Register (&gl_fog);
    Cvar_Register (&gl_fogbrightness);
    Cvar_Register (&gl_fogcolor);
    Cvar_Register (&gl_fogdensity);
    Cvar_Register (&gl_fogdistance);

    Cvar_Register (&gl_overbright);
    Cvar_Register (&gl_overbrightlights);
    Cvar_Register (&gl_overbrightmodels);
    Cvar_Register (&gl_overbrightworld);

    Cvar_Register (&gl_part_blobs);
    Cvar_Register (&gl_part_blood);
    Cvar_Register (&gl_part_damagesplash);
    Cvar_Register (&gl_part_explosions);
    Cvar_Register (&gl_part_flames);
    Cvar_Register (&gl_part_gunshots);
    Cvar_Register (&gl_part_lavasplash);
    Cvar_Register (&gl_part_lightning);
    Cvar_Register (&gl_part_muzzleflash);
    Cvar_Register (&gl_part_spikes);
    Cvar_Register (&gl_part_telesplash);
    Cvar_Register (&gl_part_trails);

    Cvar_Register (&gl_warpcaustics);
    Cvar_Register (&gl_warpwater);
    Cvar_Register (&gl_warpalphaskyspeed);
    Cvar_Register (&gl_warpsolidskyspeed);

    Cmd_AddCommand ("clearlum", R_ClearLuminance_f);
    Cmd_AddCommand ("timerefresh", R_TimeRefresh_f);
    Cmd_AddCommand ("pointfile", R_ReadPointFile_f);
    Cmd_AddCommand ("toggleparticles", R_ToggleParticles_f);

    Cmd_AddCommandSyn ("sky", "r_skybox"); // lxndr: compat fitzquake only ?
    Cmd_AddCommandSyn ("loadsky", "r_skybox");

    Cmd_AddLegacyCommand ("r_wateralpha", "r_alphawater");

    // this minigl driver seems to slow us down if the particles are drawn WITHOUT Z buffer bits
    if (!strcmp (gl_vendor, "METABYTE/WICKED3D"))
        Cvar_SetDefault (&gl_solidparticles, "1");

    if (!gl_allow_ztrick)
        Cvar_SetDefault (&gl_ztrick, "0");

    Ambience_Init ();

    R_InitTexture ();

    R_InitBloom ();
    R_InitBubble ();
    R_InitParticles ();                // lxndr: must be called before R_InitDecals
    R_InitDecals ();
    R_InitLevels ();
#ifdef DEVEL
    R_InitSandbox ();
#endif
    R_InitSmokes ();
    R_InitVertexLights ();

    R_InitOtherTextures ();

    blurtexture = texture_extension_number++;

    playertextures = texture_extension_number;
    texture_extension_number += 16;

    // fullbright skins
    texture_extension_number += 16;

    skyboxtextures = texture_extension_number;
    texture_extension_number += 6;
}

int           gl_ztrickframe = 0;

/*
 * R_Clear
 */
void R_Clear (void) {
    int           clearbits = 0;

    if (gl_clear.value || (!vid_hwgamma_enabled && v_contrast.value > 1))
        clearbits |= GL_COLOR_BUFFER_BIT;

    if (gl_ztrick.value) {
        if (clearbits)
            glClear (clearbits);

        gl_ztrickframe = !gl_ztrickframe;
        if (gl_ztrickframe) {
            gldepthmin = 0;
            gldepthmax = 0.49999;
            glDepthFunc (GL_LEQUAL);
        } else {
            gldepthmin = 1;
            gldepthmax = 0.5;
            glDepthFunc (GL_GEQUAL);
        }
    } else {
        clearbits |= GL_DEPTH_BUFFER_BIT;
        glClear (clearbits);
        gldepthmin = 0;
        gldepthmax = 1;
        glDepthFunc (GL_LEQUAL);
    }

    glDepthRange (gldepthmin, gldepthmax);

    if (r_shadows.value && gl_have_stencil) {
        glClearStencil (GL_TRUE);
        glClear (GL_STENCIL_BUFFER_BIT);
    }
}

/*
 * R_RenderScene
 *
 * r_refdef must be set before the first call
 */
void R_RenderScene (void) {
    R_SetupFrame ();
    R_SetFrustum ();
    R_MarkLeaves ();                   // done here so we know if we're in water
    R_DrawWorld ();                    // adds static entities to the list
    R_DrawViewModel ();
    R_DrawEntities ();
    R_DrawSkySphere ();
    R_DrawWaterSurfaces ();
}

/*
 * R_RenderView
 *
 * r_refdef must be set before the first call
 */
void R_RenderView (void) {
    double        time1 = 0, time2;

    if (!r_worldentity.model || !cl.worldmodel)
        Sys_Error ("R_RenderView: NULL worldmodel");

    if (r_speeds.value) {
        glFinish ();
        time1 = Sys_DoubleTime ();
        c_oldbrush_polys = c_brush_polys = c_md1_polys = c_md3_polys = 0;
    }

    if (gl_finish.value)
        glFinish ();

    R_AnimateLight ();
    R_Clear ();
    R_Levels ();
    R_Textures ();

    // render normal view

    R_SetupGL ();
    R_RenderScene ();
    S_ExtraUpdate ();                  // don't let sound get messed up if going slow

    R_ClearFogColor ();
    if (gl_cull.value)
        glDisable (GL_CULL_FACE);
    glDisable (GL_ALPHA_TEST);
    glDisable (GL_BLEND);
    glDisable (GL_DEPTH_TEST);
    glDisable (GL_TEXTURE_2D);

    // 3d fx
    R_DrawDecals ();
    R_DrawLocations ();
    R_DrawParticles ();
    R_RenderDlights ();
    R_RenderSmokes ();
    S_ExtraUpdate ();                  // don't let sound get messed up if going slow

    GL_Set2D (glx, gly, glwidth, glheight, NEARVAL, FARVAL);
    if (gl_fog.value)
        glDisable (GL_FOG);

    // 2d fx
    R_RenderBloom ();
    R_RenderBlur ();
#ifdef DEVEL
    R_RenderSandbox ();
#endif
    R_BrightenScreen ();
    R_DynamicContrast ();
    R_PolyBlend ();
    R_Q3Damage ();
    S_ExtraUpdate ();                  // don't let sound get messed up if going slow

    R_CalcLuminance ();

    if (r_speeds.value) {
        time2 = Sys_DoubleTime ();
        if (r_speeds.value > 1)
            Con_Printf ("%3i ms %4i brush %4i md1 %4i md3\n", (int) ((time2 - time1) * 1000), c_brush_polys, c_md1_polys, c_md3_polys);
        else
            Con_Printf ("%3i ms %4i wpoly %4i epoly\n", (int) ((time2 - time1) * 1000), c_oldbrush_polys, c_md1_polys);
    }
}

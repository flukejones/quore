/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// gl_rmain.c

#include "quakedef.h"

/*
 *
 * Q1 MODELS
 *
 */

/*
 * R_DrawQ1Frame
 */
void R_DrawQ1Frame (aliashdr_t * paliashdr, entity_t * ent, int distance) {
    int           i, j, k, pose, numposes, frame;
    float        *order = NULL;
    float         l, lerpfrac;
    vec3_t        lightvec, interpolated_verts;
    trivertx_t   *verts1, *verts2;

    vbo_t         acolor, atexcoord1, atexcoord2, avertex;
    qboolean      vbo = gl_ext.vertex_bufferobject && gl_vbo.value;
    count_t       c;

    frame = ent->frame;
    if ((frame >= paliashdr->numframes) || (frame < 0)) {
        Con_DPrintf ("R_DrawQ1Frame: no such frame %d\n", frame);
        frame = 0;
    }

    pose = paliashdr->frames[frame].firstpose;
    numposes = paliashdr->frames[frame].numposes;

    if (numposes > 1) {
        ent->frame_interval = paliashdr->frames[frame].interval;
        pose += (int) (cl.time / ent->frame_interval) % numposes;
    } else {
        ent->frame_interval = 0.1;
    }

    if (ent->pose2 != pose) {
        ent->frame_start_time = cl.time;
        ent->pose1 = ent->pose2;
        ent->pose2 = pose;
        ent->framelerp = 0;
    } else {
        ent->framelerp = (cl.time - ent->frame_start_time) / ent->frame_interval;
    }

    // weird things start happening if blend passes 1
    if (cl.paused || ent->framelerp > 1)
        ent->framelerp = 1;

    verts1 = (trivertx_t *) ((byte *) paliashdr + paliashdr->posedata);
    verts2 = verts1;

    verts1 += ent->pose1 * paliashdr->poseverts;
    verts2 += ent->pose2 * paliashdr->poseverts;

    if (ISTRANSPARENT (ent))
        glEnable (GL_BLEND);

    float        *texcoords = (float *) ((byte *) paliashdr + paliashdr->ofstexcoords);
    int          *counts = (int *) ((byte *) paliashdr + paliashdr->ofscounts);
    qboolean     *strips = (qboolean *) ((byte *) paliashdr + paliashdr->ofsstrips);

    if (vbo) {
        GL_BindVBO (&acolor, false);
        GL_BindVBO (&atexcoord1, false);
        if (gl_ext.multitex)
            GL_BindVBO (&atexcoord2, false);
        GL_BindVBO (&avertex, false);

        atexcoord1.target = GL_TEXTURE0_ARB;
        atexcoord2.target = GL_TEXTURE1_ARB;

        c.primverts = GL_GetArrayi ();
        c.primoffset = GL_GetArrayi ();
        c.primstrip = strips;
        c.sumverts = c.primitives = 0;
        for (i = 0; counts[i]; i++) {
            c.primoffset[c.primitives] = c.sumverts;
            c.primverts[c.primitives] = counts[i];
            c.sumverts += counts[i];
            c.primitives++;
        }

        // lxndr: should use a fix vbo instead
        memcpy (atexcoord1.buffer, texcoords, 2 * c.sumverts * sizeof (float)); // lxndr: 2 commands per cycle
        if (gl_ext.multitex)
            memcpy (atexcoord2.buffer, texcoords, 2 * c.sumverts * sizeof (float));     // lxndr: 2 commands per cycle
    } else {
        order = texcoords;
    }

    for (i = 0; counts[i]; i++) {
        if (!vbo) {
            if (strips[i])
                glBegin (GL_TRIANGLE_STRIP);
            else
                glBegin (GL_TRIANGLE_FAN);
        }

        for (j = 0; j < counts[i]; j++) {
            lerpfrac = VectorL2Compare (verts1->v, verts2->v, distance) ? ent->framelerp : 1;

            // normals and vertexes come from the frame list
            // blend the light intensity from the two frames together
            if (gl_vertexlights.value && !full_light) {
//                l = R_VertexLight (lightvec, verts1->anorm_pitch, verts1->anorm_yaw, verts2->anorm_pitch, verts2->anorm_yaw, lerpfrac, apitch, ayaw);
                l = R_LerpVertexLight (anorm_pitch[verts1->lightnormalindex], anorm_yaw[verts1->lightnormalindex],
                                       anorm_pitch[verts2->lightnormalindex], anorm_yaw[verts2->lightnormalindex], lerpfrac, apitch, ayaw);
                for (k = 0; k < 3; k++)
                    lightvec[k] = lightcolor[k] / 256 + l;
                if (vbo)
                    GL_FillArray4f (&acolor.p, lightvec[0], lightvec[1], lightvec[2], ent->transparency);
                else
                    glColor4f (lightvec[0], lightvec[1], lightvec[2], ent->transparency);
//Con_Printf ("%f %f %f %f\n", l, lightcolor[0], lightcolor[1], lightcolor[2]);
            } else {
                l = FloatInterpolate (shadedots[verts1->lightnormalindex], lerpfrac, shadedots[verts2->lightnormalindex]);
                l = min ((l * shadelight + ambientlight) / 256, 1);
                if (vbo)
                    GL_FillArray4f (&acolor.p, l, l, l, ent->transparency);
                else
                    glColor4f (l, l, l, ent->transparency);
            }

            VectorInterpolate (verts1->v, lerpfrac, verts2->v, interpolated_verts);
            if (vbo) {
                GL_FillArray3f (&avertex.p, interpolated_verts[0], interpolated_verts[1], interpolated_verts[2]);
            } else {
                // texture coordinates come from the draw list
                if (gl_ext.multitex) {
                    qglMultiTexCoord2f (GL_TEXTURE0_ARB, order[0], order[1]);
                    qglMultiTexCoord2f (GL_TEXTURE1_ARB, order[0], order[1]);
                } else {
                    glTexCoord2f (order[0], order[1]);
                }
                glVertex3fv (interpolated_verts);
                order += 2;
            }

            verts1++;
            verts2++;
        }

        if (!vbo)
            glEnd ();
    }

    if (vbo) {
        if (gl_ext.multitex)
            GL_Tri4Color4f2TexCoord2fVertex3f (&acolor, &atexcoord1, &atexcoord2, &avertex, &c);
        else
            GL_Tri4Color4fTexCoord2fVertex3f (&acolor, &atexcoord1, &avertex, &c);
    }

    if (ISTRANSPARENT (ent))
        glDisable (GL_BLEND);

    if (r_drawtris.value) {
        verts1 = (trivertx_t *) ((byte *) paliashdr + paliashdr->posedata);
        verts2 = verts1;

        verts1 += ent->pose1 * paliashdr->poseverts;
        verts2 += ent->pose2 * paliashdr->poseverts;

        if (r_drawtris.value == 2)
            glDisable (GL_DEPTH_TEST);
        glDisable (GL_TEXTURE_2D);
        glColor3f (0.1, 0.1, 0.1);
        glLineWidth (2.0);
        glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        glPolygonOffset(-1, -1);
        for (i = 0; counts[i]; i++) {
            if (strips[i])
                glBegin (GL_LINE_STRIP);
            else
                glBegin (GL_LINE_LOOP);
            for (j = 0; j < counts[i]; j++) {
                lerpfrac = VectorL2Compare (verts1->v, verts2->v, distance) ? ent->framelerp : 1;
                VectorInterpolate (verts1->v, lerpfrac, verts2->v, interpolated_verts);
                glVertex3fv (interpolated_verts);
                verts1++;
                verts2++;
            }
            glEnd ();
        }
        glColor3f (1, 1, 1);
        glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        if (r_drawtris.value == 2)
            glEnable (GL_DEPTH_TEST);
        glEnable (GL_TEXTURE_2D);
    }
}

/*
 * R_DrawQ1Shadow
 */
void R_DrawQ1Shadow (aliashdr_t * paliashdr, entity_t * ent, int distance, trace_t downtrace) {
    int           i, j;
    float         lheight, lerpfrac, s1, c1;
    vec3_t        point1, point2, interpolated;
    trivertx_t   *verts1, *verts2;

    int          *counts = (int *) ((byte *) paliashdr + paliashdr->ofscounts);
    qboolean     *strips = (qboolean *) ((byte *) paliashdr + paliashdr->ofsstrips);

    lheight = ent->origin[2] - lightspot[2];

    s1 = Q_sin (ent->angles[1] / 180 * M_PI);
    c1 = Q_cos (ent->angles[1] / 180 * M_PI);

    verts1 = (trivertx_t *) ((byte *) paliashdr + paliashdr->posedata);
    verts2 = verts1;

    verts1 += ent->pose1 * paliashdr->poseverts;
    verts2 += ent->pose2 * paliashdr->poseverts;

    for (i = 0; counts[i]; i++) {
        if (strips[i])
            glBegin (GL_TRIANGLE_STRIP);
        else
            glBegin (GL_TRIANGLE_FAN);

        for (j = 0; j < counts[i]; j++) {
            lerpfrac = VectorL2Compare (verts1->v, verts2->v, distance) ? ent->framelerp : 1;

            point1[0] = verts1->v[0] * paliashdr->scale[0] + paliashdr->scale_origin[0];
            point1[1] = verts1->v[1] * paliashdr->scale[1] + paliashdr->scale_origin[1];
            point1[2] = verts1->v[2] * paliashdr->scale[2] + paliashdr->scale_origin[2];

            point1[0] -= shadevector[0] * (point1[2] + lheight);
            point1[1] -= shadevector[1] * (point1[2] + lheight);

            point2[0] = verts2->v[0] * paliashdr->scale[0] + paliashdr->scale_origin[0];
            point2[1] = verts2->v[1] * paliashdr->scale[1] + paliashdr->scale_origin[1];
            point2[2] = verts2->v[2] * paliashdr->scale[2] + paliashdr->scale_origin[2];

            point2[0] -= shadevector[0] * (point2[2] + lheight);
            point2[1] -= shadevector[1] * (point2[2] + lheight);

            VectorInterpolate (point1, lerpfrac, point2, interpolated);

            interpolated[2] = -(ent->origin[2] - downtrace.endpos[2]);
            interpolated[2] +=
                ((interpolated[1] * (s1 * downtrace.plane.normal[0])) - (interpolated[0] * (c1 * downtrace.plane.normal[0])) -
                 (interpolated[0] * (s1 * downtrace.plane.normal[1])) - (interpolated[1] * (c1 * downtrace.plane.normal[1])));
            interpolated[2] += ((1 - downtrace.plane.normal[2]) * 20) + 0.2;

            glVertex3fv (interpolated);

            verts1++;
            verts2++;
        }

        glEnd ();
    }
}

/*
 * R_SetupInterpolation
 */
void R_SetupInterpolation (entity_t * ent, int *distance) {
    *distance = INTERP_MAXDIST;

    if (ent->model->modhint == MOD_FLAME) {
        *distance = 0;
    } else if (ent->model->modhint == MOD_WEAPON) {     // lxndr: !MOD_AXE
        *distance = (int) gl_interdist.value;
    } else if (ent->model->modhint == MOD_AXE) {        // lxndr: MOD_AXE
        *distance = 500;
    } else if (ent->modelindex == cl_modelindex[mi_player]) {
        if (ent->frame == 103)         // nailatt
            *distance = 0;
        else if (ent->frame == 104)
            *distance = 59;
        else if (ent->frame == 107)    // rockatt
            *distance = 0;
        else if (ent->frame == 108)
            *distance = 115;
        else if (ent->frame == 113)    // shotatt
            *distance = 76;
        else if (ent->frame == 115)
            *distance = 79;
    } else if (ent->modelindex == cl_modelindex[mi_soldier]) {
        if (ent->frame == 84)
            *distance = 63;
        else if (ent->frame == 85)
            *distance = 49;
        else if (ent->frame == 86)
            *distance = 106;
    } else if (ent->modelindex == cl_modelindex[mi_enforcer]) {
        if (ent->frame == 36)
            *distance = 115;
        else if (ent->frame == 37)
            *distance = 125;
    }
}

inline static void R_DrawQ1FrameAlpha (aliashdr_t * paliashdr, entity_t * ent, int distance) {
    glEnable (GL_ALPHA_TEST);
    glDepthMask (GL_FALSE);
    R_DrawQ1Frame (paliashdr, ent, distance);
    glDepthMask (GL_TRUE);
    glDisable (GL_ALPHA_TEST);
}

inline static void R_DrawQ1FrameBlend (aliashdr_t * paliashdr, entity_t * ent, int distance) {
    glEnable (GL_BLEND);
    glBlendFunc (GL_ONE, GL_ONE);
    glDepthMask (GL_FALSE);
    R_DrawQ1Frame (paliashdr, ent, distance);
    glDepthMask (GL_TRUE);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable (GL_BLEND);
}

inline static void R_DrawQ1FrameDecal (aliashdr_t * paliashdr, entity_t * ent, int distance) {
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glEnable (GL_BLEND);
    R_DrawQ1Frame (paliashdr, ent, distance);
    glDisable (GL_BLEND);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

inline static void R_DrawQ1FrameFullbright (aliashdr_t * paliashdr, entity_t * ent, int distance) {
//    glEnable (GL_BLEND);
    glDepthMask (GL_FALSE);
    R_DrawQ1Frame (paliashdr, ent, distance);
    glDepthMask (GL_TRUE);
//    glDisable (GL_BLEND);
}

/*
 * R_DrawQ1Model
 */
void R_DrawQ1Model (entity_t * ent) {
    int           i, anim, skinnum, distance, texture, fb_texture;
    vec3_t        mins, maxs;
    aliashdr_t   *paliashdr;
    model_t      *clmodel = ent->model;
    qboolean      isLumaSkin;
    extern vec3_t v_ofs;

    VectorAdd (ent->origin, clmodel->mins, mins);
    VectorAdd (ent->origin, clmodel->maxs, maxs);

    if (ent->angles[0] || ent->angles[1] || ent->angles[2]) {
        if (R_CullSphere (ent->origin, clmodel->radius))
            return;
    } else {
        if (R_CullBox (mins, maxs))
            return;
    }

    VectorCopy (ent->origin, r_entorigin);
    VectorSubtract (r_origin, r_entorigin, modelorg);

    R_SetupLighting (ent);             // get lighting information
    R_SetupInterpolation (ent, &distance);

    shadedots = r_avertexnormal_dots[((int) (ent->angles[1] * (SHADEDOT_QUANT / 360.0))) & (SHADEDOT_QUANT - 1)];

    // locate the proper data
    paliashdr = (aliashdr_t *) Mod_Extradata (clmodel);

    c_md1_polys += paliashdr->numtris;

    // draw all the triangles
    glPushMatrix ();

    if (ent == &cl.viewent)
        R_RotateForViewEntity (ent);
    else
        R_RotateForEntity (ent, false);

    if (clmodel->modhint == MOD_EYES && gl_doubleeyes.value) {
        glTranslatef (paliashdr->scale_origin[0], paliashdr->scale_origin[1], paliashdr->scale_origin[2] - (22 + 8));

        // double size of eyes, since they are really hard to see in gl
        glScalef (paliashdr->scale[0] * 2, paliashdr->scale[1] * 2, paliashdr->scale[2] * 2);
    } else if (ent == &cl.viewent) {
        float         scale = FovScale ();

        glTranslatef (paliashdr->scale_origin[0] + v_ofs[0], paliashdr->scale_origin[1] - v_ofs[1], paliashdr->scale_origin[2] + v_ofs[2]);
        glScalef (paliashdr->scale[0] * scale, paliashdr->scale[1], paliashdr->scale[2]);
    } else {
        glTranslatef (paliashdr->scale_origin[0], paliashdr->scale_origin[1], paliashdr->scale_origin[2]);
        glScalef (paliashdr->scale[0], paliashdr->scale[1], paliashdr->scale[2]);
    }

    anim = (int) (cl.time * 10) & 3;
    skinnum = ent->skinnum;
    if ((skinnum >= paliashdr->numskins) || (skinnum < 0)) {
        Con_DPrintf ("R_DrawQ1Model: no such skin # %d\n", skinnum);
        skinnum = 0;
    }

    texture = paliashdr->gl_texturenum[skinnum][anim];
    fb_texture = paliashdr->fb_texturenum[skinnum][anim];
    isLumaSkin = paliashdr->isLumaSkin[skinnum][anim];

    // we can't dynamically colormap textures, so they are cached
    // seperately for the players. Heads are just uncolored.
    if (ent->colormap != vid.colormap && !gl_nocolors.value) {
        i = ent - cl_entities;

        if (i > 0 && i <= cl.maxclients) {
            texture = playertextures - 1 + i;
            fb_texture = fb_skins[i - 1];
        }
    }

    if (gl_smoothmodels.value)
        glShadeModel (GL_SMOOTH);

    if (gl_affinemodels.value)
        glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);

    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    if (fb_texture && gl_ext.multitex)
        GL_DisableMultitexture ();
    GL_Bind (texture);

    if (gl_overbright.value) {
        if (fb_texture) {
            if (gl_ext.multitex) {
                if (gl_ext.tex_envcombine && gl_overbrightmodels.value) {
                    GL_EnableCombine (gl_overbrightmodels.value * 2);
                    GL_EnableMultitexture ();       // selects TEXTURE1
                    GL_Bind (fb_texture);
                    R_DrawQ1FrameDecal (paliashdr, ent, distance);
                    GL_DisableMultitexture ();
                    GL_DisableCombine ();
                } else if (gl_ext.tex_envadd && isLumaSkin) {
                    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_ADD);
                    GL_EnableMultitexture ();       // selects TEXTURE1
                    GL_Bind (fb_texture);
                    R_DrawQ1Frame (paliashdr, ent, distance);
                    GL_DisableMultitexture ();
                } else {
                    GL_EnableMultitexture ();       // selects TEXTURE1
                    GL_Bind (fb_texture);
                    R_DrawQ1FrameDecal (paliashdr, ent, distance);
                    GL_DisableMultitexture ();
                }
            } else if (gl_ext.tex_envcombine && gl_overbrightmodels.value) {
                GL_EnableCombine (gl_overbrightmodels.value * 2);
                R_DrawQ1Frame (paliashdr, ent, distance); // first pass
                GL_Bind (fb_texture);
                R_DrawQ1FrameBlend (paliashdr, ent, distance); // second pass
                GL_DisableCombine ();
            } else {
                R_DrawQ1Frame (paliashdr, ent, distance);   // first pass
                GL_Bind (fb_texture);
                R_DrawQ1FrameDecal (paliashdr, ent, distance);     // second pass
            }
        } else if (gl_ext.tex_envcombine && gl_overbrightmodels.value) {
            GL_EnableCombine (gl_overbrightmodels.value * 2);
            R_DrawQ1Frame (paliashdr, ent, distance);
            GL_DisableCombine ();
        } else {
            R_DrawQ1Frame (paliashdr, ent, distance);
        }
    } else if (fb_texture) {
        if (gl_ext.multitex) {
            GL_EnableMultitexture ();  // selects TEXTURE1
            GL_Bind (fb_texture);
            R_DrawQ1FrameDecal (paliashdr, ent, distance);
            GL_DisableMultitexture ();
        } else {
            R_DrawQ1Frame (paliashdr, ent, distance); // first pass
            GL_Bind (fb_texture);
            R_DrawQ1FrameDecal (paliashdr, ent, distance); // second pass
        }
    } else {
        R_DrawQ1Frame (paliashdr, ent, distance);
    }
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glShadeModel (GL_FLAT);
    if (gl_affinemodels.value)
        glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glPopMatrix ();

    if (r_shadows.value && !ent->noshadow) {
        int           farclip;
        float         theta;
        vec3_t        downmove;
        trace_t       downtrace;
        static float  shadescale = 0;

        farclip = max ((int) r_farclip.value, 4096);

        if (!shadescale)
            shadescale = 1 / sqrt (2);
        theta = -ent->angles[1] / 180 * M_PI;

        VectorSet (shadevector, Q_cos (theta) * shadescale, Q_sin (theta) * shadescale, shadescale);

        glPushMatrix ();

        R_RotateForEntity (ent, true);

        VectorCopy (ent->origin, downmove);
        downmove[2] -= farclip;
        memset (&downtrace, 0, sizeof (downtrace));
        SV_RecursiveHullCheck (cl.worldmodel->hulls, 0, 0, 1, ent->origin, downmove, &downtrace);

        glDepthMask (GL_FALSE);
        glDisable (GL_TEXTURE_2D);
        glEnable (GL_BLEND);
        glColor4f (0, 0, 0, (ambientlight - (mins[2] - downtrace.endpos[2])) / 150.0);
        if (gl_have_stencil) {
            glEnable (GL_STENCIL_TEST);
            glStencilFunc (GL_EQUAL, 1, 2);
            glStencilOp (GL_KEEP, GL_KEEP, GL_INCR);
        }

        R_DrawQ1Shadow (paliashdr, ent, distance, downtrace);

        glDepthMask (GL_TRUE);
        glEnable (GL_TEXTURE_2D);
        glDisable (GL_BLEND);
        if (gl_have_stencil)
            glDisable (GL_STENCIL_TEST);

        glPopMatrix ();
    }

    glColor3ubv (color_white);
}

/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// gl_rmain.c

#include "quakedef.h"

/*
 *
 * Q3 MODELS
 *
 */

void R_RotateForTagEntity (tagentity_t * tagent, md3tag_t * tag, float *m) {
    int           i;
    float         lerpfrac, timepassed;

    // positional interpolation
    timepassed = cl.time - tagent->tag_translate_start_time;

    if (tagent->tag_translate_start_time == 0 || timepassed > 1) {
        tagent->tag_translate_start_time = cl.time;
        VectorCopy (tag->pos, tagent->tag_pos1);
        VectorCopy (tag->pos, tagent->tag_pos2);
    }

    if (!VectorCompare (tag->pos, tagent->tag_pos2)) {
        tagent->tag_translate_start_time = cl.time;
        VectorCopy (tagent->tag_pos2, tagent->tag_pos1);
        VectorCopy (tag->pos, tagent->tag_pos2);
        lerpfrac = 0;
    } else {
        lerpfrac = timepassed / 0.1;
        if (cl.paused || lerpfrac > 1)
            lerpfrac = 1;
    }

    VectorInterpolate (tagent->tag_pos1, lerpfrac, tagent->tag_pos2, m + 12);
    m[15] = 1;

    for (i = 0; i < 3; i++) {
        // orientation interpolation (Euler angles, yuck!)
        timepassed = cl.time - tagent->tag_rotate_start_time[i];

        if (tagent->tag_rotate_start_time[i] == 0 || timepassed > 1) {
            tagent->tag_rotate_start_time[i] = cl.time;
            VectorCopy (tag->rot[i], tagent->tag_rot1[i]);
            VectorCopy (tag->rot[i], tagent->tag_rot2[i]);
        }

        if (!VectorCompare (tag->rot[i], tagent->tag_rot2[i])) {
            tagent->tag_rotate_start_time[i] = cl.time;
            VectorCopy (tagent->tag_rot2[i], tagent->tag_rot1[i]);
            VectorCopy (tag->rot[i], tagent->tag_rot2[i]);
            lerpfrac = 0;
        } else {
            lerpfrac = timepassed / 0.1;
            if (cl.paused || lerpfrac > 1)
                lerpfrac = 1;
        }

        VectorInterpolate (tagent->tag_rot1[i], lerpfrac, tagent->tag_rot2[i], m + i * 4);
        m[i * 4 + 3] = 0;
    }
}

int           bodyframe = 0, legsframe = 0;
animtype_t    bodyanim, legsanim;

void R_ReplaceQ3Frame (int frame) {
    animdata_t   *currbodyanim, *currlegsanim;
    static animtype_t oldbodyanim, oldlegsanim;
    static float  bodyanimtime, legsanimtime;
    static qboolean deathanim = false;

    if (deathanim) {
        bodyanim = oldbodyanim;
        legsanim = oldlegsanim;
    }

    if (frame < 41 || frame > 102)
        deathanim = false;

    if (frame >= 0 && frame <= 5) {    // axrun
        bodyanim = torso_stand2;
        legsanim = legs_run;
    } else if (frame >= 6 && frame <= 11) {     // rockrun
        bodyanim = torso_stand;
        legsanim = legs_run;
    } else if ((frame >= 12 && frame <= 16) || (frame >= 35 && frame <= 40)) {  // stand, pain
        bodyanim = torso_stand;
        legsanim = legs_idle;
    } else if ((frame >= 17 && frame <= 28) || (frame >= 29 && frame <= 34)) {  // axstand, axpain
        bodyanim = torso_stand2;
        legsanim = legs_idle;
    } else if (frame >= 41 && frame <= 102 && !deathanim) {     // axdeath, deatha, b, c, d, e
        bodyanim = legsanim = rand () % 3;
        deathanim = true;
    } else if (frame >= 103 && frame <= 118) {  // gun attacks
        bodyanim = torso_attack;
    } else if (frame >= 119) {         // axe attacks
        bodyanim = torso_attack2;
    }

    currbodyanim = &anims[bodyanim];
    currlegsanim = &anims[legsanim];

    if (bodyanim == oldbodyanim) {
        if (cl.time >= bodyanimtime + currbodyanim->interval) {
            if (currbodyanim->loop_frames && bodyframe + 1 == currbodyanim->offset + currbodyanim->loop_frames)
                bodyframe = currbodyanim->offset;
            else if (bodyframe + 1 < currbodyanim->offset + currbodyanim->num_frames)
                bodyframe++;
            bodyanimtime = cl.time;
        }
    } else {
        bodyframe = currbodyanim->offset;
        bodyanimtime = cl.time;
    }

    if (legsanim == oldlegsanim) {
        if (cl.time >= legsanimtime + currlegsanim->interval) {
            if (currlegsanim->loop_frames && legsframe + 1 == currlegsanim->offset + currlegsanim->loop_frames)
                legsframe = currlegsanim->offset;
            else if (legsframe + 1 < currlegsanim->offset + currlegsanim->num_frames)
                legsframe++;
            legsanimtime = cl.time;
        }
    } else {
        legsframe = currlegsanim->offset;
        legsanimtime = cl.time;
    }

    oldbodyanim = bodyanim;
    oldlegsanim = legsanim;
}

int           multimodel_level;
qboolean      surface_transparent;

/*
 * R_DrawQ3Frame
 */
void R_DrawQ3Frame (int frame, md3header_t * pmd3hdr, md3surface_t * pmd3surf, entity_t * ent, int distance) {
    int           i, j, numtris, pose, pose1, pose2;
    float         l, lerpfrac;
    vec3_t        lightvec, interpolated_verts;
    unsigned int *tris;
    md3tc_t      *tc;
    md3vert_mem_t *verts, *v1, *v2;
    model_t      *clmodel = ent->model;

    if ((frame >= pmd3hdr->numframes) || (frame < 0)) {
        Con_DPrintf ("R_DrawQ3Frame: no such frame %d\n", frame);
        frame = 0;
    }

    if (ent->pose1 >= pmd3hdr->numframes)
        ent->pose1 = 0;

    pose = frame;

    if (!strcmp (clmodel->name, "progs/player/lower.md3"))
        ent->frame_interval = anims[legsanim].interval;
    else if (!strcmp (clmodel->name, "progs/player/upper.md3"))
        ent->frame_interval = anims[bodyanim].interval;
    else
        ent->frame_interval = 0.1;

    if (ent->pose2 != pose) {
        ent->frame_start_time = cl.time;
        ent->pose1 = ent->pose2;
        ent->pose2 = pose;
        ent->framelerp = 0;
    } else {
        ent->framelerp = (cl.time - ent->frame_start_time) / ent->frame_interval;
    }

    // weird things start happening if blend passes 1
    if (cl.paused || ent->framelerp > 1)
        ent->framelerp = 1;

    verts = (md3vert_mem_t *) ((byte *) pmd3hdr + pmd3surf->ofsverts);
    tc = (md3tc_t *) ((byte *) pmd3surf + pmd3surf->ofstc);
    tris = (unsigned int *) ((byte *) pmd3surf + pmd3surf->ofstris);
    numtris = pmd3surf->numtris * 3;
    pose1 = ent->pose1 * pmd3surf->numverts;
    pose2 = ent->pose2 * pmd3surf->numverts;

    if (surface_transparent) {
        glEnable (GL_BLEND);
        if (clmodel->modhint == MOD_Q3GUNSHOT || clmodel->modhint == MOD_Q3TELEPORT)
            glBlendFunc (GL_SRC_ALPHA, GL_ONE);
        else
            glBlendFunc (GL_ONE, GL_ONE);
        glDepthMask (GL_FALSE);
        glDisable (GL_CULL_FACE);
    } else if (ISTRANSPARENT (ent)) {
        glEnable (GL_BLEND);
    }

    glBegin (GL_TRIANGLES);
    for (i = 0; i < numtris; i++) {
        float         s, t;

        v1 = verts + *tris + pose1;
        v2 = verts + *tris + pose2;

        if (clmodel->modhint == MOD_Q3TELEPORT)
            s = tc[*tris].s, t = tc[*tris].t * 4;
        else
            s = tc[*tris].s, t = tc[*tris].t;

        if (gl_ext.multitex) {
            qglMultiTexCoord2f (GL_TEXTURE0_ARB, s, t);
            qglMultiTexCoord2f (GL_TEXTURE1_ARB, s, t);
        } else {
            glTexCoord2f (s, t);
        }

        lerpfrac = VectorL2Compare (v1->vec, v2->vec, distance) ? ent->framelerp : 1;

        if (gl_vertexlights.value && !full_light) {
            l = R_LerpVertexLight (v1->anorm_pitch, v1->anorm_yaw, v2->anorm_pitch, v2->anorm_yaw, lerpfrac, apitch, ayaw);
            l = min ((l * shadelight + ambientlight) / 256, 1);
            for (j = 0; j < 3; j++)
                lightvec[j] = lightcolor[j] / 256 + l;
            glColor4f (lightvec[0], lightvec[1], lightvec[2], ent->transparency);
        } else {
            l = FloatInterpolate (shadedots[v1->oldnormal >> 8], lerpfrac, shadedots[v2->oldnormal >> 8]);
            l = min ((l * shadelight + ambientlight) / 256, 1);
            glColor4f (l, l, l, ent->transparency);
        }

        VectorInterpolate (v1->vec, lerpfrac, v2->vec, interpolated_verts);
        glVertex3fv (interpolated_verts);

        tris++;                        // lxndr: was *tris++
    }
    glEnd ();

    if (r_drawtris.value) {
        vec3_t        temp;

        tris = (unsigned int *) ((byte *) pmd3surf + pmd3surf->ofstris);
        glDisable (GL_TEXTURE_2D);
        glColor3f (0.1, 0.1, 0.1);
        glLineWidth (2.0);
        glBegin (GL_LINES);
        for (i = 0; i < numtris; i++) {
            glVertex3fv (verts[*tris + pose1].vec);
            VectorMA (verts[*tris + pose1].vec, 2, verts[*tris + pose1].normal, temp);
            glVertex3fv (temp);
            tris++;                    // lxndr: was *tris++
        }
        glEnd ();
        glEnable (GL_TEXTURE_2D);
    }

    if (surface_transparent) {
        glDisable (GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDepthMask (GL_TRUE);
        glEnable (GL_CULL_FACE);
    } else if (ISTRANSPARENT (ent)) {
        glDisable (GL_BLEND);
    }
}

/*
 * R_DrawQ3Shadow
 */
void R_DrawQ3Shadow (entity_t * ent, float lheight, float s1, float c1, trace_t downtrace) {
    int           i, j, numtris, pose1, pose2;
    vec3_t        point1, point2, interpolated;
    md3header_t  *pmd3hdr;
    md3surface_t *pmd3surf;
    unsigned int *tris;
    md3vert_mem_t *verts;
    model_t      *clmodel = ent->model;
#if 0
    float         m[16];
    md3tag_t     *tag;
    tagentity_t  *tagent;
#endif

    pmd3hdr = (md3header_t *) Mod_Extradata (clmodel);

    pmd3surf = (md3surface_t *) ((byte *) pmd3hdr + pmd3hdr->ofssurfs);
    for (i = 0; i < pmd3hdr->numsurfs; i++) {
        verts = (md3vert_mem_t *) ((byte *) pmd3hdr + pmd3surf->ofsverts);
        tris = (unsigned int *) ((byte *) pmd3surf + pmd3surf->ofstris);
        numtris = pmd3surf->numtris * 3;
        pose1 = ent->pose1 * pmd3surf->numverts;
        pose2 = ent->pose2 * pmd3surf->numverts;

        glBegin (GL_TRIANGLES);
        for (j = 0; j < numtris; j++) {
            // normals and vertexes come from the frame list
            VectorCopy (verts[*tris + pose1].vec, point1);

            point1[0] -= shadevector[0] * (point1[2] + lheight);
            point1[1] -= shadevector[1] * (point1[2] + lheight);

            VectorCopy (verts[*tris + pose2].vec, point2);

            point2[0] -= shadevector[0] * (point2[2] + lheight);
            point2[1] -= shadevector[1] * (point2[2] + lheight);

            VectorInterpolate (point1, ent->framelerp, point2, interpolated);

            interpolated[2] = -(ent->origin[2] - downtrace.endpos[2]);
            interpolated[2] +=
                ((interpolated[1] * (s1 * downtrace.plane.normal[0])) - (interpolated[0] * (c1 * downtrace.plane.normal[0])) -
                 (interpolated[0] * (s1 * downtrace.plane.normal[1])) - (interpolated[1] * (c1 * downtrace.plane.normal[1])));
            interpolated[2] += ((1 - downtrace.plane.normal[2]) * 20) + 0.2;

            glVertex3fv (interpolated);

            tris++;                    // lxndr: was *tris++
        }
        glEnd ();

        pmd3surf = (md3surface_t *) ((byte *) pmd3surf + pmd3surf->ofsend);
    }

    if (!pmd3hdr->numtags)             // single model, done
        return;

    // no multimodel shadow support yet
#if 0
    tag = (md3tag_t *) ((byte *) pmd3hdr + pmd3hdr->ofstags);
    tag += ent->pose2 * pmd3hdr->numtags;
    for (i = 0; i < pmd3hdr->numtags; i++, tag++) {
        if (multimodel_level == 0 && !strcmp (tag->name, "tag_torso")) {
            tagent = &q3player_body;
            ent = &q3player_body.ent;
            multimodel_level++;
        } else if (multimodel_level == 1 && !strcmp (tag->name, "tag_head")) {
            tagent = &q3player_head;
            ent = &q3player_head.ent;
            multimodel_level++;
        } else {
            continue;
        }

        glPushMatrix ();
        R_RotateForTagEntity (tagent, tag, m);
        glMultMatrixf (m);
        R_DrawQ3Shadow (ent, lheight, s1, c1, downtrace);
        glPopMatrix ();
    }
#endif
}

#define	ADD_EXTRA_TEXTURE(_texture, _param)	\
	glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, _param);\
	GL_Bind (_texture);	\
	\
	glDepthMask (GL_FALSE);	\
	glEnable (GL_BLEND);	\
	glBlendFunc (GL_ONE, GL_ONE);	\
	\
	R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST);\
	\
	glDisable (GL_BLEND);	\
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	\
	glDepthMask (GL_TRUE);

inline static void R_DrawQ3FrameAlpha (int frame, md3header_t * pmd3hdr, md3surface_t * pmd3surf, entity_t * ent, int distance) {
    glEnable (GL_ALPHA_TEST);
    glDepthMask (GL_FALSE);
    R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, distance);
    glDepthMask (GL_TRUE);
    glDisable (GL_ALPHA_TEST);
}

inline static void R_DrawQ3FrameBlend (int frame, md3header_t * pmd3hdr, md3surface_t * pmd3surf, entity_t * ent, int distance) {
    glEnable (GL_BLEND);
    glBlendFunc (GL_ONE, GL_ONE);
    glDepthMask (GL_FALSE);
    R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, distance);
    glDepthMask (GL_TRUE);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable (GL_BLEND);
}

inline static void R_DrawQ3FrameDecal (int frame, md3header_t * pmd3hdr, md3surface_t * pmd3surf, entity_t * ent, int distance) {
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    glEnable (GL_BLEND);
    R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, distance);
    glDisable (GL_BLEND);
    glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

inline static void R_DrawQ3FrameFullbright (int frame, md3header_t * pmd3hdr, md3surface_t * pmd3surf, entity_t * ent, int distance) {
//    glEnable (GL_BLEND);
    glDepthMask (GL_FALSE);
    R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, distance);
    glDepthMask (GL_TRUE);
//    glDisable (GL_BLEND);
}


/*
 * R_SetupQ3Frame
 */
void R_SetupQ3Frame (entity_t * ent) {
    int           i, j, frame, shadernum, texture, fb_texture;
    float         m[16];
    md3header_t  *pmd3hdr;
    md3surface_t *pmd3surf;
    md3tag_t     *tag;
    model_t      *clmodel = ent->model;
    tagentity_t  *tagent;

    if (!strcmp (clmodel->name, "progs/player/lower.md3"))
        frame = legsframe;
    else if (!strcmp (clmodel->name, "progs/player/upper.md3"))
        frame = bodyframe;
    else
        frame = ent->frame;

    // locate the proper data
    pmd3hdr = (md3header_t *) Mod_Extradata (clmodel);

    // draw all the triangles

    // draw non-transparent surfaces first, then the transparent ones
    for (i = 0; i < 2; i++) {
        pmd3surf = (md3surface_t *) ((byte *) pmd3hdr + pmd3hdr->ofssurfs);
        for (j = 0; j < pmd3hdr->numsurfs; j++) {
            md3shader_mem_t *shader;

            surface_transparent = (strstr (pmd3surf->name, "energy")
                                   || strstr (pmd3surf->name, "f_")
                                   || strstr (pmd3surf->name, "flare")
                                   || strstr (pmd3surf->name, "flash")
                                   || strstr (pmd3surf->name, "Sphere")
                                   || strstr (pmd3surf->name, "telep"));

            if ((!i && surface_transparent) || (i && !surface_transparent)) {
                pmd3surf = (md3surface_t *) ((byte *) pmd3surf + pmd3surf->ofsend);
                continue;
            }

            c_md3_polys += pmd3surf->numtris;

            shadernum = ent->skinnum;
            if ((shadernum >= pmd3surf->numshaders) || (shadernum < 0)) {
                Con_DPrintf ("R_SetupQ3Frame: no such skin # %d\n", shadernum);
                shadernum = 0;
            }

            shader = (md3shader_mem_t *) ((byte *) pmd3hdr + pmd3surf->ofsshaders);

            texture = shader[shadernum].gl_texnum;
            fb_texture = shader[shadernum].fb_texnum;

            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
//          if (fb_texture && gl_ext.multitex)
                GL_DisableMultitexture ();
            GL_Bind (texture);

            if (gl_overbright.value) {
                if (fb_texture) {
                    if (gl_ext.multitex) {
                        if (gl_ext.tex_envcombine && gl_overbrightmodels.value) {
                            GL_EnableCombine (gl_overbrightmodels.value * 2);
                            GL_EnableMultitexture ();       // selects TEXTURE1
                            GL_Bind (fb_texture);
                            R_DrawQ3FrameDecal (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST);
                            GL_DisableMultitexture ();
                            GL_DisableCombine ();
                        } else if (gl_ext.tex_envadd /*&& isLumaSkin*/) {
                            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_ADD);
                            GL_EnableMultitexture ();       // selects TEXTURE1
                            GL_Bind (fb_texture);
                            R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST);
                            GL_DisableMultitexture ();
                        } else {
                            GL_EnableMultitexture ();       // selects TEXTURE1
                            GL_Bind (fb_texture);
                            R_DrawQ3FrameDecal (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST);
                            GL_DisableMultitexture ();
                        }
                    } else if (gl_ext.tex_envcombine && gl_overbrightmodels.value) {
                        GL_EnableCombine (gl_overbrightmodels.value * 2);
                        R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST); // first pass
                        GL_Bind (fb_texture);
                        R_DrawQ3FrameBlend (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST); // second pass
                        GL_DisableCombine ();
                    } else {
                        R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST); // first pass
                        GL_Bind (fb_texture);
                        R_DrawQ3FrameDecal (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST); // second pass
                    }
                } else if (gl_ext.tex_envcombine && gl_overbrightmodels.value) {
                    GL_EnableCombine (gl_overbrightmodels.value * 2);
                    R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST);
                    GL_DisableCombine ();
                } else {
                    R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST);
                }
            } else if (fb_texture) {
                if (gl_ext.multitex) {
                    GL_EnableMultitexture ();  // selects TEXTURE1
                    GL_Bind (fb_texture);
                    R_DrawQ3FrameDecal (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST);
                    GL_DisableMultitexture ();
                } else {
                    R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST); // first pass
                    GL_Bind (fb_texture);
                    R_DrawQ3FrameDecal (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST); // second pass
                }
            } else {
                R_DrawQ3Frame (frame, pmd3hdr, pmd3surf, ent, INTERP_MAXDIST);
            }
            glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

            pmd3surf = (md3surface_t *) ((byte *) pmd3surf + pmd3surf->ofsend);
        }
    }

    if (!pmd3hdr->numtags)             // single model, done
        return;

    tag = (md3tag_t *) ((byte *) pmd3hdr + pmd3hdr->ofstags);
    tag += frame * pmd3hdr->numtags;
    for (i = 0; i < pmd3hdr->numtags; i++, tag++) {
        if (multimodel_level == 0 && !strcmp (tag->name, "tag_torso")) {
            tagent = &q3player_body;
            ent = &q3player_body.ent;
            multimodel_level++;
        } else if (multimodel_level == 1 && !strcmp (tag->name, "tag_head")) {
            tagent = &q3player_head;
            ent = &q3player_head.ent;
            multimodel_level++;
        } else {
            continue;
        }

        glPushMatrix ();
        R_RotateForTagEntity (tagent, tag, m);
        glMultMatrixf (m);
        R_SetupQ3Frame (ent);
        glPopMatrix ();
    }
}

/*
 * R_DrawQ3Model
 */
void R_DrawQ3Model (entity_t * ent) {
    vec3_t        mins, maxs, md3_scale_origin = { 0, 0, 0 };
    model_t      *clmodel = ent->model;
    extern vec3_t v_ofs;

    if (clmodel->modhint == MOD_Q3TELEPORT)
        ent->origin[2] -= 30;

    VectorAdd (ent->origin, clmodel->mins, mins);
    VectorAdd (ent->origin, clmodel->maxs, maxs);

    if (ent->angles[0] || ent->angles[1] || ent->angles[2]) {
        if (R_CullSphere (ent->origin, clmodel->radius))
            return;
    } else {
        if (R_CullBox (mins, maxs))
            return;
    }

    VectorCopy (ent->origin, r_entorigin);
    VectorSubtract (r_origin, r_entorigin, modelorg);

    // get lighting information
    R_SetupLighting (ent);

    shadedots = r_avertexnormal_dots[((int) (ent->angles[1] * (SHADEDOT_QUANT / 360.0))) & (SHADEDOT_QUANT - 1)];

    glPushMatrix ();

    if (ent == &cl.viewent)
        R_RotateForViewEntity (ent);
    else
        R_RotateForEntity (ent, false);

    if (ent == &cl.viewent) {
        float         scale = FovScale ();

        glTranslatef (md3_scale_origin[0] + v_ofs[0], md3_scale_origin[1] - v_ofs[1], md3_scale_origin[2] + v_ofs[2]);
        glScalef (scale, 1, 1);
    } else {
        glTranslatef (md3_scale_origin[0], md3_scale_origin[1], md3_scale_origin[2]);
    }

    if (gl_smoothmodels.value)
        glShadeModel (GL_SMOOTH);

    if (gl_affinemodels.value)
        glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);

    if (!strcmp (clmodel->name, "progs/player/lower.md3")) {
        R_ReplaceQ3Frame (ent->frame);
        ent->noshadow = true;
    }

    multimodel_level = 0;
    R_SetupQ3Frame (ent);

    glShadeModel (GL_FLAT);
    if (gl_affinemodels.value)
        glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glPopMatrix ();

    if (r_shadows.value && !ent->noshadow) {
        int           farclip;
        float         theta, lheight, s1, c1;
        vec3_t        downmove;
        trace_t       downtrace;
        static float  shadescale = 0;

        farclip = max ((int) r_farclip.value, 4096);

        if (!shadescale)
            shadescale = 1 / sqrt (2);
        theta = -ent->angles[1] / 180 * M_PI;

        VectorSet (shadevector, Q_cos (theta) * shadescale, Q_sin (theta) * shadescale, shadescale);

        glPushMatrix ();

        R_RotateForEntity (ent, true);

        VectorCopy (ent->origin, downmove);
        downmove[2] -= farclip;
        memset (&downtrace, 0, sizeof (downtrace));
        SV_RecursiveHullCheck (cl.worldmodel->hulls, 0, 0, 1, ent->origin, downmove, &downtrace);

        lheight = ent->origin[2] - lightspot[2];

        s1 = Q_sin (ent->angles[1] / 180 * M_PI);
        c1 = Q_cos (ent->angles[1] / 180 * M_PI);

        glDepthMask (GL_FALSE);
        glDisable (GL_TEXTURE_2D);
        glEnable (GL_BLEND);
        glColor4f (0, 0, 0, (ambientlight - (mins[2] - downtrace.endpos[2])) / 150.0);
        if (gl_have_stencil) {
            glEnable (GL_STENCIL_TEST);
            glStencilFunc (GL_EQUAL, 1, 2);
            glStencilOp (GL_KEEP, GL_KEEP, GL_INCR);
        }

        multimodel_level = 0;
        R_DrawQ3Shadow (ent, lheight, s1, c1, downtrace);

        glDepthMask (GL_TRUE);
        glEnable (GL_TEXTURE_2D);
        glDisable (GL_BLEND);
        if (gl_have_stencil)
            glDisable (GL_STENCIL_TEST);

        glPopMatrix ();
    }

    glColor3ubv (color_white);
}

/*
 * Copyright (C) 1996-1997 Id Software, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */
// snd_dma.c -- main control for any streaming sound output device

#include "quakedef.h"
#include "winquake.h"
#ifdef _WIN32
#  include "movie.h"
#endif

void          S_Play_f (void);
void          S_PlayVol_f (void);
void          S_Print_f (void);
void          S_Update_ ();
void          S_StopAllSounds (qboolean clear);
void          S_StopAllSounds_f (void);

int           s_rates[] = { 96000, 48000, 44100, 22050, 11025, 8000 };

// 
// Internal sound data & structures
// 

channel_t     channels[MAX_CHANNELS];
int           total_channels;

int           snd_blocked = 0;
static qboolean snd_ambient = true;

// pointer should go away
volatile dma_t *shm = 0;
volatile dma_t sn;

vec3_t        listener_origin;
vec3_t        listener_forward;
vec3_t        listener_right;
vec3_t        listener_up;
float         sound_nominal_clip_dist = 1000.0;

int           soundtime;               // sample PAIRS
int           paintedtime;             // sample PAIRS

sfx_t        *known_sfx;               // hunk allocated [MAX_SFX]
int           num_sfx;

sfx_t        *ambient_sfx[NUM_AMBIENTS];

#define       NUM_FOOTSTEPS	4	
sfx_t        *footstep_sfx[NUM_FOOTSTEPS];
qboolean      footstep_initialized = false;

int           desired_speed = 11025;
int           desired_bits = 16;

qboolean      sound_initialized = false;

cvar_t        bgmvolume = { "bgmvolume", "1" };
cvar_t        s_ambientlevel = { "s_ambientlevel", "0.25", CVAR_AMBIENCE };
cvar_t        s_ambientfade = { "s_ambientfade", "100" };
cvar_t        s_bgmbuffer = { "s_bgmbuffer", "4096" };
cvar_t        s_bits = { "s_bits", "16" };
cvar_t        s_channels = { "s_channels", "2" };
cvar_t        s_footsteps = { "s_footsteps", "1" };
cvar_t        s_footstepsshift = { "s_footstepsshift", "1" };
cvar_t        s_loadas8bit = { "s_loadas8bit", "0" };
cvar_t        s_mixahead = { "s_mixahead", "0.1" };
cvar_t        s_noextraupdate = { "s_noextraupdate", "0" };
cvar_t        s_nosound = { "s_nosound", "0" };
cvar_t        s_precache = { "s_precache", "1" };
cvar_t        s_rate = { "s_rate", "48000", CVAR_INIT };
cvar_t        s_show = { "s_show", "0", CVAR_VOLATILE };
cvar_t        s_volume = { "s_volume", "0.5" };

// 
// User-setable variables
// 

// Fake dma is a synchronous faking of the DMA progress used for isolating performance in the renderer.
// The fakedma_updates is number of times S_Update() is called per second.

qboolean      fakedma = false;
int           fakedma_updates = 15;

void S_Footsteps (vec3_t origin) {
    float         cycle;
    static float  oldcycle;
    static qboolean ascending;
    static qboolean oldonground;
    extern float  speedcycle;
    extern float  speedscale;

    if (!footstep_initialized || !s_footsteps.value)
        return;

    if (game.legacy)
        return;

    cycle = Q_sin (speedcycle - s_footstepsshift.value * M_PI);
    if (cycle > oldcycle) {
        if (!ascending) {
            if (cl.onground && !cl.inwater)
                S_StartSound (cl.viewentity, -1, footstep_sfx[rand () % NUM_FOOTSTEPS], origin, (speedscale / 1000.0 + 0.5) * s_footsteps.value, 1);
            ascending = true;
        }
    } else if (ascending) {
        ascending = false;
    }
    oldcycle = cycle;

    if (cl.onground && !cl.inwater) {
        if (!oldonground) {
            S_StartSound (cl.viewentity, -1, footstep_sfx[rand () % NUM_FOOTSTEPS], origin, s_footsteps.value, 1);
            oldonground = true;
        }
    } else if (oldonground) {
        oldonground = false;
    }
}

void S_AmbientOff (void) {
    snd_ambient = false;
}

void S_AmbientOn (void) {
    snd_ambient = true;
}

void S_Info_f (void) {
    if (!sound_initialized) {
        Con_Printf ("Sound system not started\n");
        return;
    }

    Con_Printf ("%5d channels\n", shm->channels);
    Con_Printf ("%5d samples\n", shm->samples);
    Con_Printf ("%5d samplepos\n", shm->samplepos);
    Con_Printf ("%5d samplebits\n", shm->samplebits);
    Con_Printf ("%5d submission_chunk\n", shm->submission_chunk);
    Con_Printf ("%5d speed\n", shm->speed);
    Con_Printf ("0x%x dma buffer\n", shm->buffer);
    Con_Printf ("%5d total_channels\n", total_channels);
}

/*
 * S_Startup
 */
void S_Startup (void) {
    int           rc;

    if (fakedma) {
        shm = (void *) Hunk_AllocName (sizeof (*shm), "shm");
        shm->splitbuffer = 0;
        shm->samplebits = 16;
        shm->speed = 22050;
        shm->channels = 2;
        shm->samples = 32768;
        shm->samplepos = 0;
        shm->soundalive = true;
        shm->gamealive = true;
        shm->submission_chunk = 1;
        shm->buffer = Hunk_AllocName (1 << 16, "shmbuf");
    } else {
        if (!(rc = SNDDMA_Init ())) {
            sound_initialized = false;
            return;
        }
    }

    sound_initialized = true;
}

/*
 * Sound_Init
 */
void Sound_Init (void) {
    Cvar_Register (&bgmvolume);
    Cvar_Register (&s_ambientlevel);
    Cvar_Register (&s_ambientfade);
    Cvar_Register (&s_bgmbuffer);
    Cvar_Register (&s_bits);
    Cvar_Register (&s_channels);
    Cvar_Register (&s_footsteps);
    Cvar_Register (&s_footstepsshift);
    Cvar_Register (&s_loadas8bit);
    Cvar_Register (&s_mixahead);
    Cvar_Register (&s_noextraupdate);
    Cvar_Register (&s_precache);
    Cvar_Register (&s_show);
    Cvar_Register (&s_volume);

    // compatibility with old configs
    Cmd_AddLegacyCommand ("volume", "s_volume");
    Cmd_AddLegacyCommand ("nosound", "s_nosound");
    Cmd_AddLegacyCommand ("precache", "s_precache");
    if (game.legacy) {
        Cmd_AddLegacyCommand ("loadas8bit", "s_loadas8bit");
        Cmd_AddLegacyCommand ("ambient_level", "s_ambientlevel");
        Cmd_AddLegacyCommand ("ambient_fade", "s_ambientfade");
        Cmd_AddLegacyCommand ("snd_noextraupdate", "s_noextraupdate");
        Cmd_AddLegacyCommand ("snd_show", "s_show");
        Cmd_AddLegacyCommand ("_snd_mixahead", "s_mixahead");
        Cmd_AddLegacyCommand ("play2", "play");
    }

    if (Com_CheckParm ("-nosound"))
        Cvar_SetValue (&s_nosound, 1);
    else if (Com_CheckParm ("-sound"))
        Cvar_SetValue (&s_nosound, 0);

    if (s_nosound.value)
        return;

    if (Com_CheckParm ("-simsound"))
        fakedma = true;

    Cmd_AddCommand ("play", S_Play_f);
    Cmd_AddCommand ("playvol", S_PlayVol_f);
    Cmd_AddCommand ("stopsound", S_StopAllSounds_f);
    Cmd_AddCommand ("soundlist", S_Print_f);
    Cmd_AddCommand ("soundinfo", S_Info_f);
    Cmd_AddCommand ("volumedown", S_VolumeDown_f);
    Cmd_AddCommand ("volumeup", S_VolumeUp_f);
    Cmd_AddCommand ("volumemute", S_VolumeMute_f);

    if (host_parms.memsize < 0x800000) {
        Cvar_SetValue (&s_loadas8bit, 1);
        Con_Printf ("loading all sounds as 8bit\n");
    }

    S_Startup ();

    if (!sound_initialized) {
        Con_Warnf ("Sound initialization failed.\n");
        return;
    }

    SND_InitScaletable ();

    known_sfx = Hunk_AllocName (MAX_SFX * sizeof (sfx_t), "sfx_t");
    num_sfx = 0;

    Con_Printf ("Sound sampling rate: %i Hz\n", shm->speed);

    // provides a tick sound until washed clean

    // if (shm->buffer)
    // shm->buffer[4] = shm->buffer[5] = 0x7f; // force a pop for debugging

    ambient_sfx[AMBIENT_WATER] = S_PrecacheSound ("ambience/water1.wav");
    ambient_sfx[AMBIENT_SKY] = S_PrecacheSound ("ambience/wind2.wav");

    int i;
    for (i = 0; i < NUM_FOOTSTEPS; i++) {
        footstep_sfx[i] = S_PrecacheSound (va ("misc/foot%i.wav", i + 1));
        if (!S_LoadSound (footstep_sfx[i]))
            break;
        if (i == NUM_FOOTSTEPS - 1);
            footstep_initialized = true;
    }

    S_StopAllSounds (true);
}

// 
// Shutdown sound engine
// 

void S_Shutdown (void) {
    if (!sound_initialized)
        return;

    S_StopAllSounds (true);            // lxndr: added

    if (shm)
        shm->gamealive = 0;

    shm = 0;
    sound_initialized = false;

    if (!fakedma)
        SNDDMA_Shutdown ();
}

// 
// Load a sound
// 

/*
 * S_FindName
 */
sfx_t        *S_FindName (char *name) {
    int           i;
    sfx_t        *sfx;

    if (!name)
        Sys_Error ("S_FindName: NULL\n");

    if (strlen (name) >= MAX_QPATH)
        Sys_Error ("Sound name too long: %s", name);

    // see if already loaded
    for (i = 0; i < num_sfx; i++)
        if (!strcmp (known_sfx[i].name, name))
            return &known_sfx[i];

    if (num_sfx == MAX_SFX)
        Sys_Error ("S_FindName: out of sfx_t");

    sfx = &known_sfx[i];
    strcpy (sfx->name, name);

    num_sfx++;

    return sfx;
}

/*
 * S_TouchSound
 */
void S_TouchSound (char *name) {
    sfx_t        *sfx;

    if (!sound_initialized)
        return;

    sfx = S_FindName (name);
    Cache_Check (&sfx->cache);
}

/*
 * S_PrecacheSound
 */
sfx_t        *S_PrecacheSound (char *name) {
    sfx_t        *sfx;

    if (!sound_initialized || s_nosound.value)
        return NULL;

    sfx = S_FindName (name);

    // cache it in
    if (s_precache.value)
        S_LoadSound (sfx);

    return sfx;
}

// 

/*
 * SND_PickChannel
 */
channel_t    *SND_PickChannel (int entnum, int entchannel) {
    int           ch_idx, first_to_die, life_left;

    // Check for replacement sound, or find the best one to replace
    first_to_die = -1;
    life_left = 0x7fffffff;
    for (ch_idx = NUM_AMBIENTS; ch_idx < NUM_AMBIENTS + MAX_DYNAMIC_CHANNELS; ch_idx++) {
        if (entchannel != 0 && channels[ch_idx].entnum == entnum && (channels[ch_idx].entchannel == entchannel || entchannel == -1)) {
            // channel 0 never overrides
            // always override sound from same entity
            first_to_die = ch_idx;
            break;
        }
        // don't let monster sounds override player sounds
        if (channels[ch_idx].entnum == cl.viewentity && entnum != cl.viewentity && channels[ch_idx].sfx)
            continue;

        if (channels[ch_idx].end - paintedtime < life_left) {
            life_left = channels[ch_idx].end - paintedtime;
            first_to_die = ch_idx;
        }
    }

    if (first_to_die == -1)
        return NULL;

    if (channels[first_to_die].sfx)
        channels[first_to_die].sfx = NULL;

    return &channels[first_to_die];
}

/*
 * SND_Spatialize
 */
static inline void SND_Spatialize (channel_t * ch) {
    float         dot, dist, lscale, rscale, scale;
    vec3_t        source_vec;
    sfx_t        *snd;

    // anything coming from the view entity will always be full volume
    if (ch->entnum == cl.viewentity) {
        ch->leftvol = ch->master_vol;
        ch->rightvol = ch->master_vol;
        return;
    }
    // calculate stereo seperation and distance attenuation

    snd = ch->sfx;
    VectorSubtract (ch->origin, listener_origin, source_vec);

    dist = VectorNormalize (source_vec) * ch->dist_mult;

    dot = DotProduct (listener_right, source_vec);

    if (shm->channels == 1) {
        rscale = 1.0;
        lscale = 1.0;
    } else {
        rscale = 1.0 + dot;
        lscale = 1.0 - dot;
    }

    // add in distance effect
    scale = (1.0 - dist) * rscale;
    ch->rightvol = (int) (ch->master_vol * scale);
    if (ch->rightvol < 0)
        ch->rightvol = 0;

    scale = (1.0 - dist) * lscale;
    ch->leftvol = (int) (ch->master_vol * scale);
    if (ch->leftvol < 0)
        ch->leftvol = 0;
}

// 
// Start a sound effect
// 

void S_StartSound (int entnum, int entchannel, sfx_t * sfx, vec3_t origin, float fvol, float attenuation) {
    channel_t    *target_chan, *check;
    sfxcache_t   *sc;
    int           vol, ch_idx, skip;

    if (!sound_initialized || !sfx || s_nosound.value)
        return;

    vol = fvol * 255;

    // pick a channel to play on
    target_chan = SND_PickChannel (entnum, entchannel);
    if (!target_chan)
        return;

    // spatialize
    memset (target_chan, 0, sizeof (*target_chan));
    VectorCopy (origin, target_chan->origin);
    target_chan->dist_mult = attenuation / sound_nominal_clip_dist;
    target_chan->master_vol = vol;
    target_chan->entnum = entnum;
    target_chan->entchannel = entchannel;
    SND_Spatialize (target_chan);

    if (!target_chan->leftvol && !target_chan->rightvol)
        return;                        // not audible at all

    // new channel
    if (!(sc = S_LoadSound (sfx))) {
        target_chan->sfx = NULL;
//        Con_Printf ("couldn't load the sound's data");
        return;                        // couldn't load the sound's data
    }

    target_chan->sfx = sfx;
    target_chan->pos = 0.0;
    target_chan->end = paintedtime + sc->length;

    // if an identical sound has also been started this frame, offset the pos
    // a bit to keep it from just making the first one louder
    check = &channels[NUM_AMBIENTS];
    for (ch_idx = NUM_AMBIENTS; ch_idx < NUM_AMBIENTS + MAX_DYNAMIC_CHANNELS; ch_idx++, check++) {
        if (check == target_chan)
            continue;
        if (check->sfx == sfx && !check->pos) {
            skip = rand () % (int) (0.1 * shm->speed);
            if (skip >= target_chan->end)
                skip = target_chan->end - 1;
            target_chan->pos += skip;
            target_chan->end -= skip;
            break;
        }
    }
}

void S_StopSound (int entnum, int entchannel) {
    int           i;

    for (i = 0; i < MAX_DYNAMIC_CHANNELS; i++) {
        if (channels[i].entnum == entnum && channels[i].entchannel == entchannel) {
            channels[i].end = 0;
            channels[i].sfx = NULL;
            return;
        }
    }
}

void S_StopAllSounds (qboolean clear) {
    int           i;

    if (!sound_initialized)
        return;

    total_channels = MAX_DYNAMIC_CHANNELS + NUM_AMBIENTS;       // no statics

    for (i = 0; i < MAX_CHANNELS; i++)
        if (channels[i].sfx)
            channels[i].sfx = NULL;

    memset (channels, 0, MAX_CHANNELS * sizeof (channel_t));

    if (clear)
        S_ClearBuffer ();
}

void S_StopAllSounds_f (void) {
    S_StopAllSounds (true);
}

void S_ClearBuffer (void) {
    int           clear;

    if (!sound_initialized)
        return;

    clear = (shm->samplebits == 8) ? 0x80 : 0;
    memset (shm->buffer, clear, shm->samples * shm->samplebits / 8);
}

/*
 * S_StaticSound
 */
void S_StaticSound (sfx_t * sfx, vec3_t origin, float vol, float attenuation) {
    channel_t    *ss;
    sfxcache_t   *sc;

    if (!sfx)
        return;

    if (total_channels == MAX_CHANNELS) {
        Con_DPrintf ("total_channels == MAX_CHANNELS\n");
        return;
    }

    ss = &channels[total_channels];
    total_channels++;

    if (!(sc = S_LoadSound (sfx))) {
//        Con_Printf ("Sound %s not loaded\n", sfx->name);
        return;
    }

    if (sc->loopstart == -1) {
        Con_Printf ("Sound %s not looped\n", sfx->name);
        return;
    }

    ss->sfx = sfx;
    VectorCopy (origin, ss->origin);
    ss->master_vol = vol;
    ss->dist_mult = (attenuation / 64) / sound_nominal_clip_dist;
    ss->end = paintedtime + sc->length;

    SND_Spatialize (ss);
}

// 

/*
 * S_UpdateAmbientSounds
 */
void S_UpdateAmbientSounds (void) {
    mleaf_t      *l;
    float         vol;
    int           ambient_channel;
    channel_t    *chan;

    if (!snd_ambient || !cl.worldmodel)
        return;

    l = Mod_PointInLeaf (listener_origin, cl.worldmodel);
    if (!l || !s_ambientlevel.value) {
        for (ambient_channel = 0; ambient_channel < NUM_AMBIENTS; ambient_channel++)
            channels[ambient_channel].sfx = NULL;
        return;
    }

    for (ambient_channel = 0; ambient_channel < NUM_AMBIENTS; ambient_channel++) {
        chan = &channels[ambient_channel];
        chan->sfx = ambient_sfx[ambient_channel];

        vol = s_ambientlevel.value * l->ambient_sound_level[ambient_channel];
        if (vol < 8)
            vol = 0;

        // don't adjust volume too fast
        if (chan->master_vol < vol) {
            chan->master_vol += Q_rint (host_frametime * s_ambientfade.value);
            if (chan->master_vol > vol)
                chan->master_vol = vol;
        } else if (chan->master_vol > vol) {
            chan->master_vol -= Q_rint (host_frametime * s_ambientfade.value);
            if (chan->master_vol < vol)
                chan->master_vol = vol;
        }

        chan->leftvol = chan->rightvol = chan->master_vol;
    }
}

/*
 * S_Update
 *
 * Called once each time through the main loop
 */
void S_Update (vec3_t origin, vec3_t forward, vec3_t right, vec3_t up) {
    int           i, j, total;
    channel_t    *ch, *combine;

    if (!sound_initialized || (snd_blocked > 0) || s_nosound.value)
        return;

    VectorCopy (origin, listener_origin);
    VectorCopy (forward, listener_forward);
    VectorCopy (right, listener_right);
    VectorCopy (up, listener_up);

    // update general area ambient sound sources
    S_UpdateAmbientSounds ();

    combine = NULL;

    // update spatialization for static and dynamic sounds
    ch = channels + NUM_AMBIENTS;
    for (i = NUM_AMBIENTS; i < total_channels; i++, ch++) {
        if (!ch->sfx)
            continue;
        SND_Spatialize (ch);           // respatialize channel
        if (!ch->leftvol && !ch->rightvol)
            continue;

        // try to combine static sounds with a previous channel of the same
        // sound effect so we don't mix five torches every frame

        if (i >= MAX_DYNAMIC_CHANNELS + NUM_AMBIENTS) {
            // see if it can just use the last one
            if (combine && combine->sfx == ch->sfx) {
                combine->leftvol += ch->leftvol;
                combine->rightvol += ch->rightvol;
                ch->leftvol = ch->rightvol = 0;
                continue;
            }
            // search for one
            combine = channels + MAX_DYNAMIC_CHANNELS + NUM_AMBIENTS;
            for (j = MAX_DYNAMIC_CHANNELS + NUM_AMBIENTS; j < i; j++, combine++)
                if (combine->sfx == ch->sfx)
                    break;

            if (j == total_channels) {
                combine = NULL;
            } else {
                if (combine != ch) {
                    combine->leftvol += ch->leftvol;
                    combine->rightvol += ch->rightvol;
                    ch->leftvol = ch->rightvol = 0;
                }
                continue;
            }
        }
    }

    // debugging output
    if (s_show.value) {
        total = 0;
        ch = channels;
        for (i = 0; i < total_channels; i++, ch++)
            if (ch->sfx && (ch->leftvol || ch->rightvol)) {
                // Con_Printf ("%3i %3i %s\n", ch->leftvol, ch->rightvol,
                // ch->sfx->name);
                total++;
            }

        Con_Printf ("----(%i)----\n", total);
    }
    // mix some sound
    S_Update_ ();
}

void GetSoundtime (void) {
    int           samplepos, fullsamples;
    static int    buffers, oldsamplepos;

#ifdef _WIN32
    if (Movie_GetSoundtime ())
        return;
#endif

    fullsamples = shm->samples / shm->channels;

    // it is possible to miscount buffers if it has wrapped twice between
    // calls to S_Update. Oh well.
    samplepos = SNDDMA_GetDMAPos ();

    if (samplepos < oldsamplepos) {
        buffers++;                     // buffer wrapped

        if (paintedtime > 0x40000000) { // time to chop things off to avoid 32
            // bit limits
            buffers = 0;
            paintedtime = fullsamples;
            S_StopAllSounds (true);
        }
    }
    oldsamplepos = samplepos;

    soundtime = buffers * fullsamples + samplepos / shm->channels;
}

void S_ExtraUpdate (void) {
    extern void   IN_Accumulate (void);

#ifdef _WIN32
    if (Movie_IsActive ())
        return;

    IN_Accumulate ();
#endif

    if (s_noextraupdate.value)
        return;                        // don't pollute timings

    S_Update_ ();
}

void S_Update_ (void) {
    unsigned      endtime;
    int           samps;

    if (!sound_initialized || (snd_blocked > 0))
        return;

    // Updates DMA time
    GetSoundtime ();

    // check to make sure that we haven't overshot
    if (paintedtime < soundtime) {
        // Con_Printf ("S_Update_ : overflow\n");
        paintedtime = soundtime;
    }
    // mix ahead of current position
    endtime = soundtime + s_mixahead.value * shm->speed;
    samps = shm->samples >> (shm->channels - 1);
    if (endtime - soundtime > samps)
        endtime = soundtime + samps;

    S_PaintChannels (endtime);

    SNDDMA_Submit ();
}

/*
 *
 *
 * console functions
 *
 *
 */

void S_Play_f (void) {
    int           i;
    static int    hash = 345;
    char          name[256];
    sfx_t        *sfx;

    if (Cmd_Argc () != 2) {
        Con_Printf ("Usage: play <filename>\n");
        return;
    }

    for (i = 1; i < Cmd_Argc (); i++) {
        Q_strcpy (name, Cmd_Argv (i));
        //        Com_DefaultExtension (name, ".wav"); // lxndr:
        sfx = S_PrecacheSound (name);
        S_StartSound (hash++, 0, sfx, listener_origin, 1.0, 0.0);
    }
}

void S_PlayVol_f (void) {
    static int    hash = 543;
    int           i;
    float         vol;
    char          name[256];
    sfx_t        *sfx;

    if (Cmd_Argc () != 2) {
        Con_Printf ("Usage: playvol <filename>\n");
        return;
    }

    for (i = 1; i < Cmd_Argc (); i += 2) {
        Q_strcpy (name, Cmd_Argv (i));
        //        Com_DefaultExtension (name, ".wav"); // lxndr:
        sfx = S_PrecacheSound (name);
        vol = Q_atof (Cmd_Argv (i + 1));
        S_StartSound (hash++, 0, sfx, listener_origin, vol, 0.0);
    }
}

void S_Print_f (void) {
    int           i, size, total;
    sfx_t        *sfx;
    sfxcache_t   *sc;

    total = 0;
    for (sfx = known_sfx, i = 0; i < num_sfx; i++, sfx++) {
        if (!(sc = Cache_Check (&sfx->cache)))
            continue;
        size = sc->length * sc->width * (sc->stereo + 1);
        total += size;
        if (sc->loopstart >= 0)
            Con_Printf ("L");
        else
            Con_Printf (" ");
        Con_Printf ("(%2db) %6i : %s\n", sc->width * 8, size, sfx->name);
    }
    Con_Printf ("Total resident: %i\n", total);
}

qboolean      volume_changed;

void S_VolumeDown_f (void) {
    if (s_nosound.value)
        Cvar_SetValue (&s_nosound, !s_nosound.value);
    S_LocalSound ("misc/menu3.wav");
    s_volume.value = bound (0, s_volume.value - 0.1, 1);
    Cvar_SetValue (&s_volume, s_volume.value);
    volume_changed = true;
}

void S_VolumeUp_f (void) {
    if (s_nosound.value)
        Cvar_SetValue (&s_nosound, !s_nosound.value);
    S_LocalSound ("misc/menu3.wav");
    s_volume.value = bound (0, s_volume.value + 0.1, 1);
    Cvar_SetValue (&s_volume, s_volume.value);
    volume_changed = true;
}

void S_VolumeMute_f (void) {
    if (!s_nosound.value)
        S_LocalSound ("misc/menu3.wav");
    Cvar_SetValue (&s_nosound, !s_nosound.value);
    if (!s_nosound.value)
        S_LocalSound ("misc/menu3.wav");
    else
        S_StopAllSounds (true);
    volume_changed = true;
}

void S_LocalSound (char *sound) {
    sfx_t        *sfx;

    if (!sound_initialized || s_nosound.value)
        return;

    if (!(sfx = S_PrecacheSound (sound))) {
        Con_Printf ("S_LocalSound: can't cache %s\n", sound);
        return;
    }
    S_StartSound (cl.viewentity, -1, sfx, vec3_origin, 1, 1);
}

void S_ClearPrecache (void) {
}

void S_BeginPrecaching (void) {
}

void S_EndPrecaching (void) {
}

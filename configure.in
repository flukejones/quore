@dnl Process this file with autoconf to produce a configure script
###########################################################################
AC_PREREQ(2.50)
AC_INIT(quore, 0.4.1, quore@free.fr, quore, http://quore.free.fr)
AC_PREFIX_DEFAULT(/usr/games)
AM_INIT_AUTOMAKE(${PACKAGE_NAME}, ${PACKAGE_VERSION})
PROG_NAME=Quore
#AC_CONFIG_HEADERS(config.h)

dnl autoheader workaround
###########################################################################
Qdefine () {
  if test "x${2}" != "x"; then
    echo "#define ${1} \"${2}\"" >>include/config.h
  else
    echo "#define ${1} 1" >>include/config.h
    AC_DEFINE(${1})
  fi
}
rm -f include/config.h

dnl Checks for options
###########################################################################
AC_ARG_ENABLE(alsa,
  [  --disable-alsa          Disable ALSA support],
  WITH_ALSA="${enableval}", WITH_ALSA=yes)
AC_ARG_ENABLE(dga,
  [  --enable-dga            Enable XF86 DGA extension],
  WITH_DGA="${enableval}", WITH_DGA=no)
AC_ARG_ENABLE(misc,
  [  --enable-misc           Enable XF86 misc extension],
  WITH_MISC="${enableval}", WITH_MISC=no)
AC_ARG_ENABLE(vmode,
  [  --disable-vmode         Disable XF86 video mode extension],
  WITH_VMODE="${enableval}", WITH_VMODE=yes)
AC_ARG_ENABLE(dbus,
  [  --enable-dbus           Enable DBus support],
  WITH_DBUS="${enableval}", WITH_DBUS=no)
AC_ARG_ENABLE(mp3,
  [  --enable-mp3            Enable experimental MP3 support],
  WITH_MP3="${enableval}", WITH_MP3=no)
#AC_ARG_ENABLE(ogg,
#  [  --enable-ogg            Enable OGG-Vorbis support],
#  WITH_OGG="${enableval}", WITH_OGG=no)

dnl Checks for programs
###########################################################################
AC_PROG_CC
#AC_PROG_INSTALL

dnl Check for header files
###########################################################################
AC_HEADER_STDC
if test "x${WITH_ALSA}" = "xyes"; then
  AC_CHECK_HEADERS(alsa/asoundlib.h,, \
    AC_MSG_WARN(missing alsa headers))
fi
if test "x${WITH_MP3}" = "xyes"; then
  AC_CHECK_HEADERS(mpg123.h,, \
    AC_MSG_WARN(missing mpg123 headers))
fi
if test "x${WITH_OGG}" = "xyes"; then
  AC_CHECK_HEADERS(ogg/ogg.h vorbis/vorbisfile.h,, \
    AC_MSG_WARN(missing ogg or vorbis headers))
fi
AC_CHECK_HEADERS(GL/gl.h GL/glx.h,, AC_MSG_WARN(missing opengl headers))
AC_CHECK_HEADERS(jpeglib.h,, AC_MSG_WARN(missing jpeg headers))
AC_CHECK_HEADERS(png.h,, AC_MSG_WARN(missing png headers))
AC_CHECK_HEADERS(vga.h,, AC_MSG_WARN(missing vga headers))
AC_CHECK_HEADERS(X11/Xlib.h,, AC_MSG_WARN(missing Xlib headers))
AC_CHECK_HEADERS(X11/keysym.h X11/cursorfont.h) 
if test "x${WITH_DGA}" = "xyes"; then
  AC_CHECK_HEADERS(X11/extensions/xf86dga.h,, \
    AC_MSG_ERROR(missing xf86dga headers), \
    [#include <X11/Xlib.h>])
fi
if test "x${WITH_MISC}" = "xyes"; then
  AC_CHECK_HEADERS(X11/extensions/xf86misc.h,, \
    AC_MSG_WARN(missing xf86misc headers), \
    [#include <X11/Xlib.h>])
fi
if test "x${WITH_VMODE}" = "xyes"; then
  AC_CHECK_HEADERS(X11/extensions/xf86vmode.h,, \
    AC_MSG_WARN(missing xf86vmode headers), \
    [#include <X11/Xlib.h>])
fi

dnl Checks for libraries
###########################################################################
AC_CHECK_LIB(m, pow)
if test "x${WITH_ALSA}" = "xyes"; then
  AC_CHECK_LIB(asound, snd_pcm_close, \
    LIBS="${LIBS} -lasound" Qdefine HAVE_ALSA, \
    AC_MSG_WARN(missing alsa library))
fi
if test "x${WITH_MP3}" = "xyes"; then
  AC_CHECK_LIB(mpg123, mpg123_init, \
    LIBS="${LIBS} -lmpg123" Qdefine HAVE_MPG123, \
    AC_MSG_WARN(missing mpg123 library))
fi
if test "x${WITH_OGG}" = "xyes"; then
  AC_CHECK_LIB(ogg, ogg_sync_init,, AC_MSG_WARN(missing ogg library))
  if test "x${HAVE_LIBOGG}" = "x1"; then
    AC_CHECK_LIB(vorbisfile, ov_open, \
      LIBS="${LIBS} -logg -lvorbisfile" Qdefine HAVE_OGG, \
      AC_MSG_WARN(missing vorbisfile library))
    fi
fi
AC_CHECK_LIB(GL, glEnable, GLXLIBS="${GLXLIBS} -lGL -lpthread", \
  AC_MSG_WARN(missing opengl library))
AC_CHECK_LIB(jpeg, main,, AC_MSG_ERROR(missing jpeg library))
AC_CHECK_LIB(png, main,, AC_MSG_ERROR(missing png library))
AC_CHECK_LIB(vga, main, VGALIBS="${VGALIBS} -lvga", \
  AC_MSG_WARN(missing vga library))
AC_CHECK_LIB(X11, XOpenDisplay, X11LIBS="${X11LIBS} -lX11" Qdefine HAVE_X11, \
  AC_MSG_WARN(missing x11 library))
AC_CHECK_LIB(Xext, XextAddDisplay, X11LIBS="${X11LIBS} -lXext" Qdefine HAVE_XEXT)
if test "x${WITH_DGA}" = "xyes"; then
  AC_CHECK_LIB(Xxf86dga, XF86DGADirectVideo, \
    X11LIBS="${X11LIBS} -lXxf86dga" Qdefine HAVE_DGA, \
    AC_MSG_ERROR(missing xf86dga library))
fi
if test "x${WITH_MISC}" = "xyes"; then
  AC_CHECK_LIB(Xxf86misc, XF86MiscGetMouseSettings, \
    X11LIBS="${X11LIBS} -lXxf86misc" Qdefine HAVE_MISC, \
    AC_MSG_ERROR(missing xf86misc library))
fi
if test "x${WITH_VMODE}" = "xyes"; then
  AC_CHECK_LIB(Xxf86vm, XF86VidModeQueryExtension, \
    X11LIBS="${X11LIBS} -lXxf86vm" Qdefine HAVE_VMODE, \
    AC_MSG_WARN(missing xf86vmode library))
fi

dnl DBus support
###########################################################################
if test "x${WITH_DBUS}" = "xyes"; then
  PKG_CHECK_MODULES(DBus, dbus-1, \
    X11LIBS="${X11LIBS} ${DBus_LIBS}" CFLAGS="${CFLAGS} ${DBus_CFLAGS}" \
      Qdefine HAVE_DBUS, \
    AC_MSG_ERROR(missing dbus library))
  #AC_CHECK_HEADERS(dbus/dbus.h, AC_MSG_WARN(missing dbus headers))
  # Can't use AC_CHECK_HEADERS() because dbus insists on #defining
  # DBUS_API_SUBJECT_TO_CHANGE, and that breaks the check.
  #if test ! -e "/usr/include/dbus-1.0/dbus/dbus.h"; then
  #  AC_MSG_ERROR(missing dbus headers)
  #fi
fi

dnl Creating files
###########################################################################
Qdefine ENGINE_VERSION ${PACKAGE_VERSION}
Qdefine SYSTEM_VERSION "$(cat /proc/version)"
AC_SUBST(GLXLIBS)
AC_SUBST(VGALIBS)
AC_SUBST(X11LIBS)
AC_SUBST(PROG_NAME)
AC_OUTPUT(Makefile)

typedef struct direntry_s {
    int           type;                // 0 = file, 1 = dir, 2 = ".."
    char         *name;
    int           size;
    qboolean      pack;
} direntry_t;

extern direntry_t *filelist;
extern int    num_files;
extern int    RDFlags;

#define RD_MENU_DEMOS           1      // for demos menu printing
#define RD_ROOT                 2      // to avoid printing ".." in the main Quake folder
#define RD_COMPLAIN             4      // to avoid printing "No such file"
#define RD_STRIPEXT             8      // for stripping file's extension
#define RD_NOERASE              16     // to avoid deleting the filelist
#define RD_SKYBOX               32     // for skyboxes
#define RD_GAMEDIR              64     // for the "gamedir" command

void          FS_EraseAllEntries (void);
void          FS_ReadDir (char *path, char *arg);
void          FS_SearchAll (char *subdir, char *param, int flags);
void          FS_SearchPack (char *subdir, char *param, int flags);
void          FS_SearchDir (char *subdir, char *param, int flags);
void          FS_Shutdown (void);


#define MAX_DLIGHTS     64             // joe: doubled

typedef enum {
    lt_default, lt_muzzleflash, lt_explosion, lt_rocket,
    lt_red, lt_blue, lt_redblue, lt_green, NUM_DLIGHTTYPES,
    lt_explosion2, lt_explosion3
} dlighttype_t;

extern float  explosioncolor[3];           // joe: for color mapped explosions
extern float  dlightcolor[NUM_DLIGHTTYPES][3];

typedef struct {
    int           key;                 // so entities can reuse same entry
    vec3_t        origin;
    float         radius;
    float         die;                 // stop lighting after this time
    float         decay;               // drop this each second
    float         minlight;            // don't add when contributing less
#ifdef GLQUAKE
    int           type;                // color
#endif
} dlight_t;

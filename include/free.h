typedef struct freepic_s {
    const char *name;
    int width;
    int height;
    int flag;
} freepic_t;

freepic_t freepics[] = {
  { "disc", 24, 24, TEX_SBAR },
  { "backtile", 64, 64, TEX_SBAR },
  { "inv_shotgun", 24, 16, TEX_SBAR },
  { "inv_sshotgun", 24, 16, TEX_SBAR },
  { "inv_nailgun", 24, 16, TEX_SBAR },
  { "inv_snailgun", 24, 16, TEX_SBAR },
  { "inv_rlaunch", 24, 16, TEX_SBAR},
  { "inv_srlaunch", 24, 16, TEX_SBAR },
  { "inv_lightng", 48, 16, TEX_SBAR },
  { "inv2_shotgun", 24, 16, TEX_SBAR },
  { "inv2_sshotgun", 24, 16, TEX_SBAR },
  { "inv2_nailgun", 24, 16, TEX_SBAR },
  { "inv2_snailgun", 24, 16, TEX_SBAR },
  { "inv2_rlaunch", 24, 16, TEX_SBAR },
  { "inv2_srlaunch", 24, 16, TEX_SBAR },
  { "inv2_lightng", 48, 16, TEX_SBAR },
  { "inva1_shotgun", 24, 16, TEX_SBAR },
  { "inva1_sshotgun", 24, 16, TEX_SBAR },
  { "inva1_nailgun", 24, 16, TEX_SBAR },
  { "inva1_snailgun", 24, 16, TEX_SBAR },
  { "inva1_rlaunch", 24, 16, TEX_SBAR },
  { "inva1_srlaunch", 24, 16, TEX_SBAR },
  { "inva1_lightng", 48, 16, TEX_SBAR },
  { "inva2_shotgun", 24, 16, TEX_SBAR },
  { "inva2_sshotgun", 24, 16, TEX_SBAR },
  { "inva2_nailgun", 24, 16, TEX_SBAR },
  { "inva2_snailgun", 24, 16, TEX_SBAR },
  { "inva2_rlaunch", 24, 16, TEX_SBAR },
  { "inva2_srlaunch", 24, 16, TEX_SBAR },
  { "inva2_lightng", 48, 16, TEX_SBAR },
  { "inva3_shotgun", 24, 16, TEX_SBAR },
  { "inva3_sshotgun", 24, 16, TEX_SBAR },
  { "inva3_nailgun", 24, 16, TEX_SBAR },
  { "inva3_snailgun", 24, 16, TEX_SBAR },
  { "inva3_rlaunch", 24, 16, TEX_SBAR },
  { "inva3_srlaunch", 24, 16, TEX_SBAR },
  { "inva3_lightng", 48, 16, TEX_SBAR },
  { "inva4_shotgun", 24, 16, TEX_SBAR },
  { "inva4_sshotgun", 24, 16, TEX_SBAR },
  { "inva4_nailgun", 24, 16, TEX_SBAR },
  { "inva4_snailgun", 24, 16, TEX_SBAR },
  { "inva4_rlaunch", 24, 16, TEX_SBAR },
  { "inva4_srlaunch", 24, 16, TEX_SBAR },
  { "inva4_lightng", 48, 16, TEX_SBAR },
  { "inva5_shotgun", 24, 16, TEX_SBAR },
  { "inva5_sshotgun", 24, 16, TEX_SBAR },
  { "inva5_nailgun", 24, 16, TEX_SBAR },
  { "inva5_snailgun", 24, 16, TEX_SBAR },
  { "inva5_rlaunch", 24, 16, TEX_SBAR },
  { "inva5_srlaunch", 24, 16, TEX_SBAR },
  { "inva5_lightng", 48, 16, TEX_SBAR },
  { "sb_shells", 24, 24, TEX_TEXT },
  { "sb_nails", 24, 24, TEX_TEXT },
  { "sb_rocket", 24, 24, TEX_TEXT },
  { "sb_cells", 24, 24, TEX_TEXT },
  { "sb_armor1", 24, 24, TEX_TEXT },
  { "sb_armor2", 24, 24, TEX_TEXT },
  { "sb_armor3", 24, 24, TEX_TEXT },
  { "sb_key1", 16, 16, TEX_TEXT },
  { "sb_key2", 16, 16, TEX_TEXT },
  { "sb_invis", 16, 16, TEX_TEXT },
  { "sb_invuln", 16, 16, TEX_TEXT },
  { "sb_suit", 16, 16, TEX_TEXT},
  { "sb_quad", 16, 16, TEX_TEXT},
  { "sb_sigil1", 8, 16, TEX_TEXT},
  { "sb_sigil2", 8, 16, TEX_TEXT},
  { "sb_sigil3", 8, 16, TEX_TEXT},
  { "sb_sigil4", 8, 16, TEX_TEXT},
  { "face1", 24, 24, TEX_TEXT},
  { "face_p1", 24, 24, TEX_TEXT},
  { "face2", 24, 24, TEX_TEXT},
  { "face_p2", 24, 24, TEX_TEXT},
  { "face3", 24, 24, TEX_TEXT},
  { "face_p3", 24, 24, TEX_TEXT},
  { "face4", 24, 24, TEX_TEXT},
  { "face_p4", 24, 24, TEX_TEXT},
  { "face5", 24, 24, TEX_TEXT},
  { "face_p5", 24, 24, TEX_TEXT},
  { "face_invis", 24, 24, TEX_TEXT},
  { "face_invul2", 24, 24, TEX_TEXT},
  { "face_inv2", 24, 24, TEX_TEXT},
  { "face_quad", 24, 24, TEX_TEXT},
  { "sbar", 320, 24, TEX_SBAR },
  { "ibar", 320, 24, TEX_SBAR },
  { "scorebar", 320, 24, TEX_SBAR },
  { "num_0", 24, 24, TEX_TEXT },
  { "anum_0", 24, 24, TEX_TEXT},
  { "num_1", 24, 24, TEX_TEXT},
  { "anum_1", 24, 24, TEX_TEXT},
  { "num_2", 24, 24, TEX_TEXT},
  { "anum_2", 24, 24, TEX_TEXT},
  { "num_3", 24, 24, TEX_TEXT},
  { "anum_3", 24, 24, TEX_TEXT},
  { "num_4", 24, 24, TEX_TEXT},
  { "anum_4", 24, 24, TEX_TEXT},
  { "num_5", 24, 24, TEX_TEXT},
  { "anum_5", 24, 24, TEX_TEXT},
  { "num_6", 24, 24, TEX_TEXT},
  { "anum_6", 24, 24, TEX_TEXT},
  { "num_7", 24, 24, TEX_TEXT},
  { "anum_7", 24, 24, TEX_TEXT},
  { "num_8", 24, 24, TEX_TEXT},
  { "anum_8", 24, 24, TEX_TEXT},
  { "num_9", 24, 24, TEX_TEXT},
  { "anum_9", 24, 24, TEX_TEXT},
  { "num_minus", 24, 24, TEX_TEXT},
  { "anum_minus", 24, 24, TEX_TEXT},
  { "num_colon", 16, 24, TEX_TEXT},
  { "num_slash", 16, 24, TEX_TEXT},
  { "net", 32, 32, TEX_SBAR },
  { "ram", 32, 32, TEX_SBAR },
  { "turtle", 32, 32, TEX_SBAR },
  { "gfx/box_tl.lmp", 8, 8, TEX_MENU },
  { "gfx/box_ml.lmp", 8, 8, TEX_MENU },
  { "gfx/box_bl.lmp", 8, 8, TEX_MENU },
  { "gfx/box_tm.lmp", 16, 8, TEX_MENU },
  { "gfx/box_mm.lmp", 16, 8, TEX_MENU },
  { "gfx/box_bm.lmp", 16, 8, TEX_MENU },
  { "gfx/box_tr.lmp", 8, 8, TEX_MENU },
  { "gfx/box_mr.lmp", 8, 8, TEX_MENU },
  { "gfx/box_br.lmp", 8, 8, TEX_MENU },
  { "gfx/p_save.lmp", 88, 24, TEX_MENU },
  { "gfx/p_load.lmp", 104, 24, TEX_MENU },
  { "gfx/qplaque.lmp", 32, 144, TEX_MENU },
  { "gfx/p_option.lmp", 144, 24, TEX_MENU },
  { "gfx/ttl_cstm.lmp", 184, 24, TEX_MENU },
  { "gfx/ttl_main.lmp", 96, 24, TEX_MENU },
  { "gfx/mainmenu.lmp", 240, 112, TEX_MENU },
  { "gfx/menudot4.lmp", 16, 24, TEX_MENU },
  { "gfx/menudot5.lmp", 16, 24, TEX_MENU },
  { "gfx/menudot6.lmp", 16, 24, TEX_MENU },
  { "gfx/menudot1.lmp", 16, 24, TEX_MENU },
  { "gfx/menudot2.lmp", 16, 24, TEX_MENU },
  { "gfx/menudot3.lmp", 16, 24, TEX_MENU },
  { "gfx/ttl_sgl.lmp", 128, 24, TEX_MENU },
  { "gfx/sp_menu.lmp", 232, 64, TEX_MENU },
  { "gfx/p_multi.lmp", 216, 24, TEX_MENU },
  { "gfx/mp_menu.lmp", 232, 64, TEX_MENU },
  { "gfx/bigbox.lmp", 72, 72, TEX_MENU },
  { "gfx/menuplyr.lmp", 48, 56, TEX_MENU },
  { "gfx/help0.lmp", 320, 200, TEX_MENU },
  { "gfx/box_mm2.lmp", 16, 8, TEX_MENU },
  { "gfx/pause.lmp", 128, 24, TEX_MENU },
  { "gfx/loading.lmp", 144, 24, TEX_MENU },
  { "gfx/dim_modm.lmp", 176, 19, TEX_MENU },
  { "gfx/dim_drct.lmp", 176, 19, TEX_MENU },
  { "gfx/dim_ipx.lmp", 176, 19, TEX_MENU },
  { "gfx/netmen4.lmp", 176, 19, TEX_MENU },
  { "gfx/help1.lmp", 320, 200, TEX_MENU },
  { "gfx/help2.lmp", 320, 200, TEX_MENU },
  { "gfx/help3.lmp", 320, 200, TEX_MENU },
  { "gfx/help4.lmp", 320, 200, TEX_MENU },
  { "gfx/help5.lmp", 320, 200, TEX_MENU },
  { "gfx/ranking.lmp", 168, 24, TEX_MENU },
  { "gfx/complete.lmp", 192, 24, TEX_MENU },
  { "gfx/inter.lmp", 160, 144, TEX_MENU },
  { "gfx/finale.lmp", 288, 24, TEX_MENU },
//  { "gfx/vidmodes.lmp, 0, 0, TEX_MENU },
  { "gfx/console.lmp", 320, 200, TEX_CONSOLE },
  { NULL, 0, 0, 0 }
};

#define ARG_HELP "\
  Useful arguments: \n\
-base <basedir>            : set the base directory containing id1\n\
-[no]cd [<device>]         : enable/disable audio cd\n\
+connect <hostname>        : connect to the given host\n\
-debug [<level>]           : increase verbosity and print messages to stdout\n\
-dedicated [<maxclients>]  : launch a dedicated server\n\
-default                   : reset everything\n\
-[no]extra [<extradir>]... : set/unset extra content directories\n\
-[no]free [<freedir>]...   : set/unset the default content directories\n\
-[no]game [<gamedir>]...   : set/unset the default game directories\n\
-legacy [<legacydir>]      : enable legacy mode\n\
-log                       : log console messages to ~/.quore/console.log\n\
+map <mapname>             : load the given map\n\
-[no]multi [<multidir>]... : enable/disable multiplayer mode\n\
-[no]quore [<quoredir>]... : enable/disable quore mode\n\
-[no]sound [<device>]      : enable/disable sound\n\
-window [<width> <height>] : use windowed mode with the given size\n\
\n\
Have fun ! \n\
"

#define BIND_HELP "\
Escape       : main menu \n\
Enter        : player information \n\
Tab          : map information\n\
F1           : game menu \n\
F2           : save game \n\
F3           : load game \n\
F4           : options \n\
F5           : video options \n\
F6           : quick save \n\
F9           : quick load \n\
F10          : quit \n\
print screen : screenshot \n\
"

#define CMD_HELP "\
cmdlist \n\
cmdfind \n\
cvarlist \n\
cvarfind \n\
"
